import { environment } from '../../environments/environment';

export class wordpressCustomPagePostType {

    public pageModules: object;
    public pageEssentials: object;
    public baseUrl?: string = `${environment.baseurl}`;
    // public baseUrl?: string = this.mainUrl();

    //  When class is instantiated - pass in the DATA from API into constructor and then the WorspressCustomPagePostType has been created
    constructor(data: Object){
        //  Assign Class Props To Incoming Data
        this.pageEssentials = data[0];
        this.pageModules = data[1];
    }

    //  mainUrl () {

    //     if(location.hostname == 'localhost'){
      
    //       return 'http://hostfront-api.local';
      
    //      }
      
    //      console.log(location.hostname);
      
    //    }



}