export class User {

 
    constructor(

        public name?: string,
        public email?: string,
        public phone?: string,
        public website?: string,
        public businessName?: string,
        public topic?: string,
        public message?: string,
        public checked?: boolean

    ){}




}