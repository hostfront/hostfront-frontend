import { Component, OnInit } from '@angular/core';
import { PricingTabService } from '../pricing-tab.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  constructor(public _pricingTabService: PricingTabService) { }

  yearlyTab() {
    this._pricingTabService.yearlyClick();
  }

  monthlyTab() {
    this._pricingTabService.monthlyClick();
  }

  ngOnInit() {
  }

}
