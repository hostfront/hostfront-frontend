import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PricingTabService {

  constructor() { }

  yearlyClick() {
    const indicator = document.getElementById('indicator');
    const secondTab = document.getElementById('second-tab');
    const firstTab = document.getElementById('first-tab');
    const yearlyTabColor = document.getElementById('yearly-tab');
    const monthlyTabColor = document.getElementById('monthly-tab');

    secondTab.style.display = 'block';
    secondTab.style.opacity = '1';
    secondTab.style.transition = 'opacity 1s ease-in';

    firstTab.style.display = 'none';
    firstTab.style.opacity = '0';

    yearlyTabColor.style.color = '#ffffff';
    monthlyTabColor.style.color = '#2c3e50';

    indicator.style.left = '120px';
    indicator.style.transition = 'all 0.35s ease-Out';
  }

  monthlyClick() {
    const indicator = document.getElementById('indicator');
    const secondTab = document.getElementById('second-tab');
    const firstTab = document.getElementById('first-tab');
    const yearlyTabColor = document.getElementById('yearly-tab');
    const monthlyTabColor = document.getElementById('monthly-tab');

    firstTab.style.display = 'block';
    firstTab.style.opacity = '1';
    firstTab.style.transition = 'opacity 1s ease-in';

    indicator.style.left = '5px';
    indicator.style.transition = 'all 0.35s ease-Out';
    secondTab.style.display = 'none';
    secondTab.style.opacity = '0';

    yearlyTabColor.style.color = '#2c3e50';
    monthlyTabColor.style.color = '#ffffff';
  }
}
