import { TestBed } from '@angular/core/testing';

import { PricingTabService } from './pricing-tab.service';

describe('PricingTabService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PricingTabService = TestBed.get(PricingTabService);
    expect(service).toBeTruthy();
  });
});
