import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule, RoutingComponents } from './app-routing.module';
import { AppComponent } from './app.component';

import { ProductsComponent } from './products/products.component';
import { WordpressMaintenanceComponent } from './wordpress-maintenance/wordpress-maintenance.component';
import { ContactComponent } from './contact/contact.component';
import { ScrollComponent } from './shared/scroll/scroll.component';
import { ButtonComponent } from './shared/button/button.component';
import { HeaderComponent } from './shared/header/header.component';
import { PriceCardComponent } from './shared/price-card/price-card.component';
import { CtaComponent } from './shared/cta/cta.component';
import { HeroTextComponent } from './shared/hero-text/hero-text.component';
import { FormComponent } from './shared/form/form.component';

@NgModule({
  declarations: [
    AppComponent,
    RoutingComponents,
    ProductsComponent,
    WordpressMaintenanceComponent,
    ContactComponent,
    ScrollComponent,
    ButtonComponent,
    HeaderComponent,
    PriceCardComponent,
    CtaComponent,
    HeroTextComponent,
    FormComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
