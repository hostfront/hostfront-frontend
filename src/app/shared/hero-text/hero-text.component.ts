import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-hero-text',
  templateUrl: './hero-text.component.html',
  styleUrls: ['./hero-text.component.scss']
})
export class HeroTextComponent implements OnInit {

  @Input() data: any;
  @Input() heroClass: any;

  constructor() { }

  ngOnInit(): void {
  }

}
