import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-cta',
  templateUrl: './cta.component.html',
  styleUrls: ['./cta.component.scss']
})
export class CtaComponent implements OnInit {

  @Input() data: any;
  @Input() baseUrl: string;

  @Input() buttonConfig: any;
  @Input() buttonText: any;

  primaryBtn = {
    primary: true,
  };

  secondaryBtn = {
    secondary: true
  };

  constructor() { }

  ngOnInit(): void {
  }

}
