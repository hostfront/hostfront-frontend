import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-price-card',
  templateUrl: './price-card.component.html',
  styleUrls: ['./price-card.component.scss']
})
export class PriceCardComponent implements OnInit {

  /**
   * @input prices - The Prices Information
   */
  @Input() prices: any;

  /**
   * @input baseUrl - Recieve The Base Url
   */
  @Input() baseUrl: string;


  primaryBtn = {
    primary: true,
  };

  secondaryBtn = {
    secondary: true
  };

  showMoreContent() {
    let cardExtra = <HTMLElement[]><any>document.querySelectorAll(".card__body__extra")
    var i
    for (i = 0; i < cardExtra.length; i++) {
      if (cardExtra[i].style.display === "none") {
        cardExtra[i].style.display = "block"
      } else {
        cardExtra[i].style.display = "none"
      }
    }
  }

  constructor() { }

  ngOnInit(): void {

  }

}
