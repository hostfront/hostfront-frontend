import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WordpressMaintenanceComponent } from './wordpress-maintenance.component';

describe('WordpressMaintenanceComponent', () => {
  let component: WordpressMaintenanceComponent;
  let fixture: ComponentFixture<WordpressMaintenanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WordpressMaintenanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WordpressMaintenanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
