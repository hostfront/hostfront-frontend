import { Component, OnInit } from '@angular/core';
import { PricingTabService } from '../pricing-tab.service';

@Component({
  selector: 'app-wordpress-maintenance',
  templateUrl: './wordpress-maintenance.component.html',
  styleUrls: ['./wordpress-maintenance.component.scss']
})
export class WordpressMaintenanceComponent implements OnInit {

  constructor(public _pricingTabService: PricingTabService) { }

  yearlyTab() {
    this._pricingTabService.yearlyClick();
  }

  monthlyTab() {
    this._pricingTabService.monthlyClick();
  }

  ngOnInit() {
  }

}
