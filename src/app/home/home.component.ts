import { Component, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';

import { wordpressCustomPagePostType } from '../models/Page';
import { HostfrontApiService } from '../services/hostfront-api.service'
// Meta Service:  Creates a dynamic <link rel="canonical" href=''>
import { MetaService } from '../services/meta-service.service'

import { environment } from '../../environments/environment';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {

  constructor(private _http: HostfrontApiService, private _titleService: Title, private _metaService: Meta, private _customMetaService: MetaService) { }

  //  Model Classes
  pageModel: wordpressCustomPagePostType;

  //  Component Properties
  pageModelEssentialData: any;
  pageModelDynamicGlobalModules: any;
  baseUrl: string;

  home = "home"

  title = 'Angular 9 Universal Example';

  public pagePostData = {
    meta_title: '',
    meta_description: '',
    meta_facebook_title: '',
    meta_facebook_desc: '',
    meta_facebook_image: '',
    meta_twitter_title: '',
    meta_twitter_desc: '',
    meta_twitter_image: '',
    meta_keywords: '',
    homeData: function (data) {
      //  Page SEO Data
      this.meta_title = data[0].post_seo.meta_seo_title;
      this.meta_description = data[0].post_seo.meta_seo_desc;
      this.meta_facebook_title = data[0].post_seo.meta_metaopengraph_title;
      this.meta_facebook_desc = data[0].post_seo.meta_metaopengraph_desc;
      this.meta_facebook_image = data[0].post_seo.meta_metaopengraph_image;
      this.meta_twitter_title = data[0].post_seo._metaseo_metatwitter_title;
      this.meta_twitter_desc = data[0].post_seo._metaseo_metatwitter_desc;
      this.meta_twitter_image = data[0].post_seo._metaseo_metatwitter_image;
    }
  };

  primaryBtn = {
    primary: true,
  };

  secondaryBtn = {
    secondary: true
  };

  ngOnInit() {

    this._http.getHome().subscribe(data => {

      this.pageModel = new wordpressCustomPagePostType(data);
      this.baseUrl = this.pageModel.baseUrl
      this.pageModelEssentialData = this.pageModel.pageEssentials;
      this.pageModelDynamicGlobalModules = this.pageModel.pageModules;

      // Object Method & Store Data
      this.pagePostData.homeData(data);

      //  Set Page Title 
      this._titleService.setTitle(this.pageModelEssentialData.post_title);

      //  Tell google to use the hostfront site as the real url
      this._customMetaService.createCanonicalURL(`https://hostfront.co.uk/${this.pageModelEssentialData.post_title}`);

      //  Set Page Meta Tags
     
      this._metaService.addTags([
        //  Static Meta Tags
        { name: 'robots', content: 'index, follow' },
        { name: 'og:logcale', content: 'en_GB' },
        { name: 'og:type', content: 'article' },
        { name: 'og:site_name', content: 'Hostfront' },
        //  Dynamic Meta Tags
        { name: 'description', content: this.pagePostData.meta_description },
        { name: 'og:title', content: this.pagePostData.meta_title },
        { name: 'og:description', content: this.pagePostData.meta_facebook_desc },
        { name: 'twitter:card', content: this.pagePostData.meta_twitter_image },
        { name: 'twitter:description', content: this.pagePostData.meta_twitter_desc },
        { name: 'twitter:title', content: this.pagePostData.meta_twitter_title }
      ])


    })


  }

  displayName = false;

}
