import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormArray } from '@angular/forms';
import { Title, Meta } from '@angular/platform-browser';

import { User } from '../models/User';
import { wordpressCustomPagePostType } from '../models/Page';

import { ContactFormService } from '../services/contact-form.service';
import { HostfrontApiService } from '../services/hostfront-api.service'
// Meta Service:  Creates a dynamic <link rel="canonical" href=''>
import { MetaService } from '../services/meta-service.service'

import { environment } from '../../environments/environment';


@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  contactForm: FormGroup;

  urlRegex = '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?';

  //  Model Classes
  userModel: User;
  pageModel: wordpressCustomPagePostType;

  //  Component Properties

  pageModelEssentialData: any;
  pageModelDynamicGlobalModules: any;
  baseUrl: string;

  errorMsg: string = '';

  submitted: boolean = false;

  primaryBtn = {
    primary: true,
  };

  secondaryBtn = {
    secondary: true
  };

  contact = "contact"

  getInputField(input) {
    return this.contactForm.get(input);
  }


  //We want to bind these to a select option in the HTML
  topics: Array<string> = [
    'Wordpress Hosting',
    'Wordpress Maintenance',
    'Wordpress Website Audit',
    'Wordpress Website Migration',
    'Wordpress Website Optimization',
    'Other'
  ]



  constructor(private fb: FormBuilder, private _contactFormService: ContactFormService, private _http: HostfrontApiService, private _titleService: Title, private _metaService: Meta, private _customMetaService: MetaService) { }

  ngOnInit() {

    //  Output Contact Form On Contact Page
    this.contactForm = this.fb.group({

      name: ['', [Validators.required, Validators.minLength(3)]],

      email: ['', [
        Validators.required,
        Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")
      ]],

      website: ['', [Validators.pattern(this.urlRegex)]],

      phone: ['', [Validators.required, Validators.minLength(10), Validators.pattern(new RegExp("[0-9 ]{10}"))]],

      businessName: ['',],

      topic: ['', [Validators.required]],

      message: ['', [Validators.required, Validators.minLength(10)]],

      checked: ['', [Validators.required]],

    });

    //  Call Contact Page From API
    this._http.getContactPage().subscribe(data => {

      this.pageModel = new wordpressCustomPagePostType(data);
      this.baseUrl = this.pageModel.baseUrl
      this.pageModelEssentialData = this.pageModel.pageEssentials;
      this.pageModelDynamicGlobalModules = this.pageModel.pageModules;

      this._titleService.setTitle(this.pageModel.pageEssentials['post_title']);

      //  Tell google to use the hostfront site as the real url
      this._customMetaService.createCanonicalURL(`https://hostfront.co.uk/${this.pageModelEssentialData.post_title}`);

      this._metaService.addTags([
        //  Static Meta Tags
        { name: 'robots', content: 'index, follow' },
        { name: 'og:logcale', content: 'en_GB' },
        { name: 'og:type', content: 'article' },
        { name: 'og:site_name', content: 'Hostfront' },
        // //  Dynamic Meta Tags
        { name: 'description', content: this.pageModelEssentialData.meta_description },
        { name: 'og:title', content: this.pageModelEssentialData.meta_title },
        { name: 'og:description', content: this.pageModelEssentialData.meta_facebook_desc },
        { name: 'twitter:card', content: this.pageModelEssentialData.meta_twitter_image },
        { name: 'twitter:description', content: this.pageModelEssentialData.meta_twitter_desc },
        { name: 'twitter:title', content: this.pageModelEssentialData.meta_twitter_title }

      ])

    })

  }

  //  Contact Form Component Functions
  validateTopic(value) {

    this._contactFormService.validateTopic(value);

  }

  //  When client clicks 'SUBMIT' on form
  onSubmit() {

    this.userModel = new User(
      this.contactForm.value.name,
      this.contactForm.value.email,
      this.contactForm.value.phone,
      this.contactForm.value.website,
      this.contactForm.value.businessName,
      this.contactForm.value.topic,
      this.contactForm.value.message,
      this.contactForm.value.checked
    );

    this._contactFormService.postContactForm(this.userModel)
      .subscribe(
        data => console.log('Success', data),
        error => this.errorMsg = error.statusText
      )

    this.submitted = true;


  }

}
