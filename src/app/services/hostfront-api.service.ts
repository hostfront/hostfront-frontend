//
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})

export class HostfrontApiService {

  constructor(private http: HttpClient) { }

  getHome() {
    return this.http.get(`${environment.baseurl}/wp-json/wp/v2/custom_page/183`)
  }

  getContactPage() {
    return this.http.get(`${environment.baseurl}/wp-json/wp/v2/custom_page/190`)
  }

}
