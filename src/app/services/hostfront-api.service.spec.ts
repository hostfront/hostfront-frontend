//
import { TestBed } from '@angular/core/testing';

import { HostfrontApiService } from './hostfront-api.service';

describe('HostfrontApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HostfrontApiService = TestBed.get(HostfrontApiService);
    expect(service).toBeTruthy();
  });
});
