import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { User } from '../models/User';

import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';


@Injectable({
  providedIn: 'root'
})


export class ContactFormService {

  //  Contact Form Properties
  topicHasError = true;

  _url = 'http://localhost:8080/contact'

  constructor(private _http: HttpClient) { }

  postContactForm(userContactForm: User)
  {
    return this._http.post<any>(this._url, userContactForm).pipe(catchError(this.errorHandler))
  }

  errorHandler(error: HttpErrorResponse)
  {
    return throwError(error)
  }
  

  validateTopic(value) {

    if (value === 'default') {

      this.topicHasError = true

    } else{
      this.topicHasError = false
    }

    return this.topicHasError;

  }


}

