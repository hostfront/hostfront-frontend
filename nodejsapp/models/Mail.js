//  Mail Class
//  Bring In Email Templates
const EmailTemplates = require('./templates/EmailTemplates');

//  @TODO - Sanitize Data -> Sanitize HTML

//  Import Sendgrid Email API & API Key
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY); 


let Mail = function (data) {

    this.data = data

}

Mail.prototype.processContactForm = function () {

    //  @data = Contact Form From Client
    const {name, email, website, phone, businessName, topic, message} = this.data;

    const msg = {
        to: 'chidodesigns@gmail.com',
        content: [
         {
           type: "text/html",
           value: EmailTemplates.createContactFormEmailTemplate(name, email, website, phone, businessName, topic, message),
         }
       ],
        from: {
           email: email,
           name: name,
         },
        subject: topic,
      }
   
      sgMail.send(msg)
   


}

module.exports = Mail;