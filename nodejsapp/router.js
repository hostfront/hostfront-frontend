//  Set Express Router For NodeJS App Routes
const express = require('express');
const router = express.Router();
//  File Path
const path = require('path');
//  Dotenv Config
const dotenv = require('dotenv');
dotenv.config()

//////  Controllers //////

//  Mail Controller
const mailController = require('./controllers/mailController');

//  Load Angular App
router.get('/*', function(req,res) { 
    res.sendFile(path.join(__dirname+'/dist/project/index.html'));
});


//////  Routes //////

////// Mail Related Routes //////

//  Contact Form Posted Routed - From Contact Page *frontend*
router.post('/contact', mailController.contactForm)


module.exports = router