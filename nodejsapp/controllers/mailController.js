//  Import Mail Model
const Mail = require('../models/Mail');

exports.contactForm = (req, res) => {

    //  Initialise Mail Object
    const mail = new Mail(req.body)

    mail.processContactForm();

    console.log('Data: ', req.body);

    res.status(200).send({
        "message": "Data recieved",
        "PORT": process.env.PORT
      })

}