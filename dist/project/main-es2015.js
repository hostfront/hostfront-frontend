(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/about/about.component.ts":
/*!******************************************!*\
  !*** ./src/app/about/about.component.ts ***!
  \******************************************/
/*! exports provided: AboutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutComponent", function() { return AboutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _shared_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../shared/navbar/navbar.component */ "./src/app/shared/navbar/navbar.component.ts");
/* harmony import */ var _shared_scroll_scroll_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../shared/scroll/scroll.component */ "./src/app/shared/scroll/scroll.component.ts");
/* harmony import */ var _shared_footer_footer_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../shared/footer/footer.component */ "./src/app/shared/footer/footer.component.ts");





class AboutComponent {
    constructor() { }
    ngOnInit() {
    }
}
AboutComponent.ɵfac = function AboutComponent_Factory(t) { return new (t || AboutComponent)(); };
AboutComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AboutComponent, selectors: [["app-about"]], decls: 116, vars: 0, consts: [[1, "header"], ["id", "curve", "data-name", "Layer 1", "xmlns", "http://www.w3.org/2000/svg", "viewBox", "0 0 1416.99 174.01"], ["d", "M0,280.8S283.66,59,608.94,163.56s437.93,150.57,808,10.34V309.54H0V280.8Z", "transform", "translate(0 -135.53)", 1, "cls-1"], [1, "header__container"], [1, "section"], [1, "container"], ["data-aos", "fade-left", 1, "flex"], [1, "cta", "cta--left"], ["src", "assets/images/laptop.svg", 1, "cta__img"], ["data-aos", "fade-right", 1, "flex"], ["src", "assets/images/co2.svg", 1, "cta__img"], [1, "cta", "cta--right"], [1, "section", "color--bg"], [1, "flex"], ["data-aos", "fade-left", 1, "cta", "cta--left"], ["data-aos", "fade-right", 1, "cta", "cta--right"], ["id", "carbon-offset", 1, "section", "color"], [1, "flex", "heading"], ["data-aos", "fade-up", 1, "flex"], ["src", "assets/images/cloud-computing.png"], [1, "column"], [1, "column__icon"], [1, "column__body"], [1, "btn", "btn--std", "btn--std01"], [1, "section", "section--color--dark"], [1, "child"], [1, "section", "section--feature"], [1, "cf"], [1, "input-wrapper"], [1, "half", "left", "cf"], ["type", "text", "id", "input-name", "placeholder", "Name"], ["type", "email", "id", "input-email", "placeholder", "Email address"], ["type", "text", "id", "input-subject", "placeholder", "Subject"], [1, "half", "right", "cf"], ["name", "message", "type", "text", "id", "input-message", "placeholder", "Message"], [1, "btn-wrapper"]], template: function AboutComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "header", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "app-navbar");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceSVG"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "svg", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "path", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceHTML"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "Lorem ipsum dolor sit amet.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "section", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "About Us");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](18, "img", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "img", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, "Carbon Offsetting");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "section", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32, "Lorem ipsum dolor sit amet.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, "Lorem ipsum dolor sit amet.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](41, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "section", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](46, "Our trusted climate partners");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](49, "img", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](52, "Our trusted climate partners");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](54, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "section", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](59, "Why Choose Us?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](64, "img", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](67, "Free Website SSL");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](69, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](70, "span", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](71, "Click Here");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](74, "img", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](75, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](76, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](77, "Free Website SSL");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](79, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](80, "span", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](81, "Click Here");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](82, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](83, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](84, "img", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](87, "Free Website SSL");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](88, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](89, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](90, "span", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](91, "Click Here");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](92, "section", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](93, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](94, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](95, "div", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](96, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](97, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](98, "section", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](99, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](100, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](101, "form", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](102, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](103, "Want to contact us?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](104, "div", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](105, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](106, "input", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](107, "input", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](108, "input", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](109, "div", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](110, "textarea", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](111, "div", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](112, "span", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](113, "Click Here");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](114, "app-scroll");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](115, "app-footer");
    } }, directives: [_shared_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_1__["NavbarComponent"], _shared_scroll_scroll_component__WEBPACK_IMPORTED_MODULE_2__["ScrollComponent"], _shared_footer_footer_component__WEBPACK_IMPORTED_MODULE_3__["FooterComponent"]], styles: [".header[_ngcontent-%COMP%] {\n  position: relative;\n  height: 750px;\n  background-image: linear-gradient(to right, #2c3e50, rgba(41, 128, 185, 0.5)), url(\"/assets/images/about-bg.jpg\");\n  background-attachment: fixed;\n  background-position: center top;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n.header[_ngcontent-%COMP%]   svg[_ngcontent-%COMP%] {\n  position: absolute;\n  bottom: 0;\n  width: 100%;\n}\n.header[_ngcontent-%COMP%]   svg[_ngcontent-%COMP%]   path[_ngcontent-%COMP%] {\n  fill: #ffffff;\n}\n.header__container[_ngcontent-%COMP%] {\n  width: 100%;\n  height: 100%;\n  display: flex;\n  margin: auto;\n  color: white;\n}\n.header__container[_ngcontent-%COMP%]   div[_ngcontent-%COMP%] {\n  text-align: center;\n}\n.header__container[_ngcontent-%COMP%]   div[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n  margin: 20 0 20 0;\n}\nsection.color[_ngcontent-%COMP%] {\n  background-color: #f8fafe;\n}\nsection.color--gradient[_ngcontent-%COMP%] {\n  background: linear-gradient(to right, #2980b9, #2c3e50);\n}\nsection.color--bg[_ngcontent-%COMP%] {\n  background-image: linear-gradient(to right, #2980b9, rgba(44, 62, 80, 0.5)), url(\"/assets/images/forest.jpg\");\n  background-attachment: fixed;\n  background-position: 50% 50%;\n  background-repeat: no-repeat;\n  background-size: cover;\n  position: relative;\n}\nsection.color--bg[_ngcontent-%COMP%]::before {\n  content: \"\";\n  position: absolute;\n  top: 0;\n  left: 0;\n  transform: scale(-1, -1);\n  width: 100%;\n  height: 50px;\n  fill: #ffffff;\n  background-image: url(\"/assets/images/wavy.svg\");\n  background-size: cover;\n  background-repeat: no-repeat;\n}\nsection.color--bg[_ngcontent-%COMP%]   .cta[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  color: white;\n}\nsection.color--bg[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%] {\n  justify-content: left;\n}\nsection.color--bg[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%]   .h2[_ngcontent-%COMP%] {\n  color: #ffffff;\n}\nsection.color--bg[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%]   .cta[_ngcontent-%COMP%] {\n  color: #ffffff;\n}\nsection.color--bg[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]:nth-child(2)   .flex[_ngcontent-%COMP%] {\n  justify-content: flex-end;\n}\nsection#carbon-offset[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%] {\n  align-items: center;\n  line-height: 2;\n  text-align: center;\n}\nsection#carbon-offset[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  max-width: 250px;\n  margin: 3em;\n}\nsection--feature[_ngcontent-%COMP%] {\n  padding: 0;\n}\nsection--feature[_ngcontent-%COMP%]   .feature-group[_ngcontent-%COMP%] {\n  padding: 3em;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%] {\n  padding: 30px 0px;\n  max-width: 1140px;\n  margin: auto;\n  overflow: hidden;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: center;\n  flex-direction: column;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .heading[_ngcontent-%COMP%] {\n  text-align: center;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  text-transform: uppercase;\n  font-size: 2em;\n  color: #283e51;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .cta[_ngcontent-%COMP%] {\n  text-align: center;\n  padding: 2em 0em;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .cta[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n  line-height: 1.8;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .cta__img[_ngcontent-%COMP%] {\n  width: 50%;\n  margin: 0 auto;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%] {\n  padding: 20px;\n  line-height: 1.5;\n  color: #2980b9;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .column__icon[_ngcontent-%COMP%] {\n  height: 244px;\n  width: 244px;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .column__icon[_ngcontent-%COMP%], section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .card__icon[_ngcontent-%COMP%] {\n  position: relative;\n  margin: auto;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .column__icon[_ngcontent-%COMP%]   img[_ngcontent-%COMP%], section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .card__icon[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  max-height: 100%;\n  max-width: 100%;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .card__icon[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  max-height: 50%;\n  max-width: 50%;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column__body[_ngcontent-%COMP%] {\n  text-align: center;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column__body[_ngcontent-%COMP%]   h3[_ngcontent-%COMP%] {\n  font-size: 1.5em;\n  color: #283e51;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%] {\n  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);\n  border-radius: 10px;\n  padding: 16px;\n  text-align: center;\n  background-color: #ffffff;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .card__body[_ngcontent-%COMP%] {\n  text-align: center;\n  padding: 2em;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .card__title[_ngcontent-%COMP%] {\n  font-size: 1.5em;\n  color: #283e51;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .card__text[_ngcontent-%COMP%] {\n  line-height: 1.5;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .card__price[_ngcontent-%COMP%] {\n  font-family: \"Arial Black\";\n  font-size: 3em;\n  background: linear-gradient(#2c3e50, #2980b9);\n  -webkit-background-clip: text;\n  -webkit-text-fill-color: transparent;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .active[_ngcontent-%COMP%] {\n  background-image: linear-gradient(#2c3e50, #2980b9);\n  padding: 50px 16px;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .active[_ngcontent-%COMP%]   .card__title[_ngcontent-%COMP%], section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .active[_ngcontent-%COMP%]   .card__body[_ngcontent-%COMP%], section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .active[_ngcontent-%COMP%]   .card__price[_ngcontent-%COMP%] {\n  color: #ffffff;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .active[_ngcontent-%COMP%]   .card__price[_ngcontent-%COMP%] {\n  -webkit-text-fill-color: initial;\n}\nsection--feature[_ngcontent-%COMP%] {\n  padding: 0px;\n}\n.section--feature[_ngcontent-%COMP%] {\n  padding: 0px;\n}\n.section--color--dark[_ngcontent-%COMP%] {\n  background-image: linear-gradient(to right, #2c3e50, rgba(41, 128, 185, 0.5)), url(\"/assets/images/about-bg.jpg\");\n  background-attachment: fixed;\n  background-position: center top;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n.section--color--dark[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%] {\n  text-align: center;\n}\n.section--color--dark[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n  color: #ffffff;\n}\n.section[_ngcontent-%COMP%]   form[_ngcontent-%COMP%] {\n  text-align: center;\n  background-color: #f8fafe;\n  padding: 2em;\n  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);\n  border-radius: 10px;\n  width: 60%;\n}\n.section[_ngcontent-%COMP%]   form[_ngcontent-%COMP%]   .input-wrapper[_ngcontent-%COMP%] {\n  display: flex;\n}\n.section[_ngcontent-%COMP%]   form[_ngcontent-%COMP%]   input[_ngcontent-%COMP%], .section[_ngcontent-%COMP%]   form[_ngcontent-%COMP%]   textarea[_ngcontent-%COMP%] {\n  border: 0;\n  outline: 0;\n  padding: 1em;\n  border-radius: 8px;\n  display: block;\n  width: 100%;\n  margin-top: 1em;\n}\n.section[_ngcontent-%COMP%]   form[_ngcontent-%COMP%]   .btn-wrapper[_ngcontent-%COMP%] {\n  width: 100%;\n  display: flex;\n}\n.section[_ngcontent-%COMP%]   form[_ngcontent-%COMP%]   .btn-wrapper[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  margin: auto;\n}\n.section[_ngcontent-%COMP%]   .half[_ngcontent-%COMP%] {\n  float: left;\n  width: 48%;\n  margin-bottom: 1em;\n}\n.section[_ngcontent-%COMP%]   .right[_ngcontent-%COMP%] {\n  width: 50%;\n  display: flex;\n}\n.section[_ngcontent-%COMP%]   .left[_ngcontent-%COMP%] {\n  margin-right: 2%;\n}\n@media (max-width: 480px) {\n  .section[_ngcontent-%COMP%]   .half[_ngcontent-%COMP%] {\n    width: 100%;\n    float: none;\n    margin-bottom: 0;\n  }\n\n  .section[_ngcontent-%COMP%]   form[_ngcontent-%COMP%]   .input-wrapper[_ngcontent-%COMP%] {\n    display: block;\n  }\n\n  .section[_ngcontent-%COMP%]   form[_ngcontent-%COMP%] {\n    width: auto;\n  }\n}\n\n.cf[_ngcontent-%COMP%]:before, .cf[_ngcontent-%COMP%]:after {\n  content: \" \";\n  \n  display: table;\n  \n}\n.cf[_ngcontent-%COMP%]:after {\n  clear: both;\n}\n@media (min-width: 768px) and (max-width: 812px) {\n  .header__container[_ngcontent-%COMP%] {\n    max-width: 576px;\n  }\n\n  section.color--bg[_ngcontent-%COMP%]::before {\n    height: 75px;\n  }\n  section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%] {\n    flex-direction: row;\n    flex-wrap: wrap;\n  }\n  section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%] {\n    flex-basis: 48%;\n  }\n}\n@media (min-width: 736px) {\n  .header__container[_ngcontent-%COMP%] {\n    max-width: 85%;\n  }\n  .header__container[_ngcontent-%COMP%]   div[_ngcontent-%COMP%] {\n    width: 75%;\n    margin-top: 50px;\n  }\n  .header__container[_ngcontent-%COMP%]   .cta--right[_ngcontent-%COMP%] {\n    text-align: right;\n  }\n\n  .header__container[_ngcontent-%COMP%]   div[_ngcontent-%COMP%]:first-child {\n    text-align: left;\n  }\n  .header__container[_ngcontent-%COMP%]   div[_ngcontent-%COMP%]:first-child   h1[_ngcontent-%COMP%] {\n    font-size: 3.5rem;\n  }\n  .header__container[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n    margin-bottom: 3em;\n  }\n  .header__container[_ngcontent-%COMP%]   .btn-wrapper[_ngcontent-%COMP%] {\n    text-align: left;\n    width: 60%;\n    display: flex;\n    justify-content: space-between;\n  }\n  .header__container[_ngcontent-%COMP%]   .btn-wrapper[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%] {\n    text-transform: uppercase;\n    font-weight: bold;\n    padding: 20px 30px;\n  }\n\n  .section[_ngcontent-%COMP%] {\n    padding: 100px 0;\n  }\n  .section.color--bg[_ngcontent-%COMP%]::before {\n    height: 150px;\n  }\n  .section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%] {\n    flex-direction: row;\n  }\n  .section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .child[_ngcontent-%COMP%] {\n    display: flex;\n  }\n  .section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .child[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n    font-size: 2.5em;\n  }\n  .section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .child[_ngcontent-%COMP%]:first-child {\n    align-items: flex-start;\n  }\n  .section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .child[_ngcontent-%COMP%]   .child[_ngcontent-%COMP%]:nth-child(2) {\n    align-items: flex-end;\n    width: 50%;\n  }\n  .section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .cta[_ngcontent-%COMP%] {\n    width: 60%;\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n    padding: 2em;\n  }\n  .section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .cta--right[_ngcontent-%COMP%] {\n    text-align: right;\n  }\n  .section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .cta--left[_ngcontent-%COMP%] {\n    text-align: left;\n  }\n  .section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .cta__img[_ngcontent-%COMP%] {\n    width: 40%;\n  }\n  .section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .feature-group[_ngcontent-%COMP%] {\n    flex-direction: row;\n    text-align: left;\n  }\n\n  .section--feature[_ngcontent-%COMP%] {\n    position: relative;\n    top: -250px;\n  }\n}\n@media (min-width: 1300px) {\n  .header__container[_ngcontent-%COMP%] {\n    max-width: 1200px;\n  }\n\n  section.color--bg[_ngcontent-%COMP%]::before {\n    height: 150px;\n  }\n  section#carbon-offset[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%]   div[_ngcontent-%COMP%] {\n    text-align: left;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2NoaWRvL3Byb2plY3RzL2hvc3Rmcm9udC1wcm9qZWN0L2hvc3Rmcm9udC1mcm9udGVuZC9zcmMvYXBwL2Fib3V0L2Fib3V0LmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9hYm91dC9hYm91dC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLGlIQUFBO0VBRUEsNEJBQUE7RUFDQSwrQkFBQTtFQUNBLDRCQUFBO0VBQ0Esc0JBQUE7QUNBRjtBREtFO0VBQ0Usa0JBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtBQ0hKO0FES0k7RUFDRSxhQUFBO0FDSE47QURPRTtFQUNFLFdBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0FDTEo7QURPSTtFQUNFLGtCQUFBO0FDTE47QURPTTtFQUNFLGlCQUFBO0FDTFI7QURZRTtFQUNFLHlCQUFBO0FDVEo7QURXSTtFQUNFLHVEQUFBO0FDVE47QURZSTtFQUNFLDZHQUFBO0VBRUEsNEJBQUE7RUFDQSw0QkFBQTtFQUNBLDRCQUFBO0VBQ0Esc0JBQUE7RUFDQSxrQkFBQTtBQ1hOO0FEWU07RUFDRSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxNQUFBO0VBQ0EsT0FBQTtFQUNBLHdCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0EsZ0RBQUE7RUFDQSxzQkFBQTtFQUNBLDRCQUFBO0FDVlI7QURZTTtFQUNFLFlBQUE7QUNWUjtBRFlNO0VBQ0UscUJBQUE7QUNWUjtBRFdRO0VBQ0UsY0FBQTtBQ1RWO0FEV1E7RUFDRSxjQUFBO0FDVFY7QURZTTtFQUNFLHlCQUFBO0FDVlI7QURnQkk7RUFDRSxtQkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtBQ2ROO0FEZ0JNO0VBQ0ksZ0JBQUE7RUFDQSxXQUFBO0FDZFY7QURtQkU7RUFDRSxVQUFBO0FDakJKO0FEa0JJO0VBQ0UsWUFBQTtBQ2hCTjtBRG1CRTtFQUNFLGlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QUNqQko7QURtQkk7RUFDRSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxzQkFBQTtBQ2pCTjtBRG9CSTtFQUNFLGtCQUFBO0FDbEJOO0FEcUJJO0VBQ0UseUJBQUE7RUFDQSxjQUFBO0VBQ0EsY0FBQTtBQ25CTjtBRHNCSTtFQUNFLGtCQUFBO0VBQ0EsZ0JBQUE7QUNwQk47QURzQk07RUFDRSxnQkFBQTtBQ3BCUjtBRHVCTTtFQUNFLFVBQUE7RUFDQSxjQUFBO0FDckJSO0FEeUJJO0VBQ0UsYUFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtBQ3ZCTjtBRHlCTTtFQUNFLGFBQUE7RUFDQSxZQUFBO0FDdkJSO0FEMEJNOztFQUVFLGtCQUFBO0VBQ0EsWUFBQTtBQ3hCUjtBRDBCUTs7RUFDRSxnQkFBQTtFQUNBLGVBQUE7QUN2QlY7QUQ0QlE7RUFDRSxlQUFBO0VBQ0EsY0FBQTtBQzFCVjtBRDhCTTtFQUNFLGtCQUFBO0FDNUJSO0FEOEJRO0VBQ0UsZ0JBQUE7RUFDQSxjQUFBO0FDNUJWO0FEZ0NNO0VBQ0UsMENBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLHlCQUFBO0FDOUJSO0FEZ0NRO0VBQ0Usa0JBQUE7RUFDQSxZQUFBO0FDOUJWO0FEaUNRO0VBQ0UsZ0JBQUE7RUFDQSxjQUFBO0FDL0JWO0FEa0NRO0VBQ0UsZ0JBQUE7QUNoQ1Y7QURtQ1E7RUFDRSwwQkFBQTtFQUNBLGNBQUE7RUFDQSw2Q0FBQTtFQUNBLDZCQUFBO0VBQ0Esb0NBQUE7QUNqQ1Y7QURxQ007RUFDRSxtREFBQTtFQUNBLGtCQUFBO0FDbkNSO0FEb0NROzs7RUFHRSxjQUFBO0FDbENWO0FEb0NRO0VBQ0UsZ0NBQUE7QUNsQ1Y7QUR3Q0U7RUFDRSxZQUFBO0FDdENKO0FEMkNFO0VBQ0UsWUFBQTtBQ3hDSjtBRDBDRTtFQUNFLGlIQUFBO0VBRUEsNEJBQUE7RUFDQSwrQkFBQTtFQUNBLDRCQUFBO0VBQ0Esc0JBQUE7QUN6Q0o7QUQwQ0k7RUFDRSxrQkFBQTtBQ3hDTjtBRHlDTTtFQUNFLGNBQUE7QUN2Q1I7QUQyQ0U7RUFDRSxrQkFBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLDBDQUFBO0VBQ0EsbUJBQUE7RUFDQSxVQUFBO0FDekNKO0FEMENJO0VBQ0UsYUFBQTtBQ3hDTjtBRDBDSTtFQUNFLFNBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0FDeENOO0FEMkNJO0VBQ0UsV0FBQTtFQUNBLGFBQUE7QUN6Q047QUQwQ007RUFDRSxZQUFBO0FDeENSO0FEOENFO0VBQ0UsV0FBQTtFQUNBLFVBQUE7RUFDQSxrQkFBQTtBQzVDSjtBRCtDRTtFQUNFLFVBQUE7RUFDQSxhQUFBO0FDN0NKO0FEZ0RFO0VBQ0ssZ0JBQUE7QUM5Q1A7QURrREE7RUFDRTtJQUNHLFdBQUE7SUFDQSxXQUFBO0lBQ0EsZ0JBQUE7RUMvQ0g7O0VEaURBO0lBQ0UsY0FBQTtFQzlDRjs7RURnREE7SUFDRSxXQUFBO0VDN0NGO0FBQ0Y7QURpREEsYUFBQTtBQUNBOztFQUVJLFlBQUE7RUFBYyxNQUFBO0VBQ2QsY0FBQTtFQUFnQixNQUFBO0FDN0NwQjtBRGdEQTtFQUNJLFdBQUE7QUM3Q0o7QURnREE7RUFDRTtJQUNFLGdCQUFBO0VDN0NGOztFRG1ETTtJQUNFLFlBQUE7RUNoRFI7RURzREk7SUFDRSxtQkFBQTtJQUNBLGVBQUE7RUNwRE47RURxRE07SUFDRSxlQUFBO0VDbkRSO0FBQ0Y7QUR5REE7RUFFRTtJQUNFLGNBQUE7RUN4REY7RUQwREU7SUFDRSxVQUFBO0lBQ0EsZ0JBQUE7RUN4REo7RUQ0REk7SUFDRSxpQkFBQTtFQzFETjs7RURpRUk7SUFDRSxnQkFBQTtFQzlETjtFRGdFTTtJQUNFLGlCQUFBO0VDOURSO0VEa0VJO0lBQ0Usa0JBQUE7RUNoRU47RURtRUk7SUFDRSxnQkFBQTtJQUNBLFVBQUE7SUFDQSxhQUFBO0lBQ0EsOEJBQUE7RUNqRU47RURtRU07SUFDRSx5QkFBQTtJQUNBLGlCQUFBO0lBQ0Esa0JBQUE7RUNqRVI7O0VEdUVBO0lBQ0UsZ0JBQUE7RUNwRUY7RUR3RU07SUFDRSxhQUFBO0VDdEVSO0VENEVJO0lBQ0UsbUJBQUE7RUMxRU47RUQ2RUk7SUFDRSxhQUFBO0VDM0VOO0VENEVNO0lBQ0UsZ0JBQUE7RUMxRVI7RUQ2RU07SUFDRSx1QkFBQTtFQzNFUjtFRDhFTTtJQUNFLHFCQUFBO0lBQ0EsVUFBQTtFQzVFUjtFRGdGSTtJQUNFLFVBQUE7SUFDQSxhQUFBO0lBQ0Esc0JBQUE7SUFDQSx1QkFBQTtJQUNBLFlBQUE7RUM5RU47RURnRk07SUFDRSxpQkFBQTtFQzlFUjtFRGdGTTtJQUNFLGdCQUFBO0VDOUVSO0VEaUZNO0lBQ0UsVUFBQTtFQy9FUjtFRGtGSTtJQUNFLG1CQUFBO0lBQ0EsZ0JBQUE7RUNoRk47O0VEb0ZBO0lBQ0Usa0JBQUE7SUFDQSxXQUFBO0VDakZGO0FBQ0Y7QURvRkE7RUFDRTtJQUNFLGlCQUFBO0VDbEZGOztFRHVGTTtJQUNFLGFBQUE7RUNwRlI7RUR3RkU7SUFDRSxnQkFBQTtFQ3RGSjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvYWJvdXQvYWJvdXQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaGVhZGVyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBoZWlnaHQ6IDc1MHB4O1xuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsIHJnYmEoNDQsIDYyLCA4MCwgMSksIHJnYmEoNDEsIDEyOCwgMTg1LCAwLjUpKSxcbiAgICB1cmwoXCIvYXNzZXRzL2ltYWdlcy9hYm91dC1iZy5qcGdcIik7XG4gIGJhY2tncm91bmQtYXR0YWNobWVudDogZml4ZWQ7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlciB0b3A7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG5cbiAgLy8gIzI5ODBiOSAjMmMzZTUwXG4gIC8vZGV2ZWxvcCBjb21tbWVudFxuXG4gICYgc3ZnIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYm90dG9tOiAwO1xuICAgIHdpZHRoOiAxMDAlO1xuXG4gICAgJiBwYXRoIHtcbiAgICAgIGZpbGw6ICNmZmZmZmY7XG4gICAgfVxuICB9XG5cbiAgJl9fY29udGFpbmVyIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBtYXJnaW46IGF1dG87XG4gICAgY29sb3I6IHdoaXRlO1xuXG4gICAgJiBkaXYge1xuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuXG4gICAgICAmIHAge1xuICAgICAgICBtYXJnaW46IDIwIDAgMjAgMDtcbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cblxuc2VjdGlvbiB7XG4gICYuY29sb3Ige1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmOGZhZmU7XG5cbiAgICAmLS1ncmFkaWVudCB7XG4gICAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICMyOTgwYjksICMyYzNlNTApO1xuICAgIH1cblxuICAgICYtLWJnIHtcbiAgICAgIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgcmdiYSg0MSwgMTI4LCAxODUsIDEpLCByZ2JhKDQ0LCA2MiwgODAsIDAuNSkpLFxuICAgICAgICB1cmwoXCIvYXNzZXRzL2ltYWdlcy9mb3Jlc3QuanBnXCIpO1xuICAgICAgYmFja2dyb3VuZC1hdHRhY2htZW50OiBmaXhlZDtcbiAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IDUwJSA1MCU7XG4gICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICY6OmJlZm9yZXtcbiAgICAgICAgY29udGVudDogJyc7XG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgdG9wOiAwO1xuICAgICAgICBsZWZ0OiAwO1xuICAgICAgICB0cmFuc2Zvcm06IHNjYWxlKC0xLCAtMSk7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBoZWlnaHQ6IDUwcHg7XG4gICAgICAgIGZpbGw6ICNmZmZmZmY7XG4gICAgICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi9hc3NldHMvaW1hZ2VzL3dhdnkuc3ZnXCIpO1xuICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgICAgfVxuICAgICAgJiAuY3RhIGgyIHtcbiAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgICAgfVxuICAgICAgJiAuY29udGFpbmVyIC5mbGV4IHtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBsZWZ0O1xuICAgICAgICAmIC5oMiB7XG4gICAgICAgICAgY29sb3I6ICNmZmZmZmY7XG4gICAgICAgIH1cbiAgICAgICAgJiAuY3RhIHtcbiAgICAgICAgICBjb2xvcjogI2ZmZmZmZjtcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgJiAuY29udGFpbmVyOm50aC1jaGlsZCgyKSAuZmxleCB7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgJiNjYXJib24tb2Zmc2V0IHtcbiAgICAmIC5mbGV4IHtcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICBsaW5lLWhlaWdodDogMjtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcblxuICAgICAgJiBpbWcge1xuICAgICAgICAgIG1heC13aWR0aDogMjUwcHg7XG4gICAgICAgICAgbWFyZ2luOiAzZW07XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgJi0tZmVhdHVyZSB7XG4gICAgcGFkZGluZzogMDtcbiAgICAmIC5mZWF0dXJlLWdyb3VwIHtcbiAgICAgIHBhZGRpbmc6IDNlbTtcbiAgICB9XG4gIH1cbiAgJiAuY29udGFpbmVyIHtcbiAgICBwYWRkaW5nOiAzMHB4IDBweDtcbiAgICBtYXgtd2lkdGg6IDExNDBweDtcbiAgICBtYXJnaW46IGF1dG87XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcblxuICAgICYgLmZsZXgge1xuICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICB9XG5cbiAgICAmIC5oZWFkaW5nIHtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB9XG5cbiAgICAmIGgyIHtcbiAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgICBmb250LXNpemU6IDJlbTtcbiAgICAgIGNvbG9yOiAjMjgzZTUxO1xuICAgIH1cblxuICAgICYgLmN0YSB7XG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICBwYWRkaW5nOiAyZW0gMGVtO1xuXG4gICAgICAmIHAge1xuICAgICAgICBsaW5lLWhlaWdodDogMS44O1xuICAgICAgfVxuXG4gICAgICAmX19pbWcge1xuICAgICAgICB3aWR0aDogNTAlO1xuICAgICAgICBtYXJnaW46IDAgYXV0bztcbiAgICAgIH1cbiAgICB9XG5cbiAgICAmIC5jb2x1bW4ge1xuICAgICAgcGFkZGluZzogMjBweDtcbiAgICAgIGxpbmUtaGVpZ2h0OiAxLjU7XG4gICAgICBjb2xvcjogIzI5ODBiOTtcblxuICAgICAgLmNvbHVtbl9faWNvbiB7XG4gICAgICAgIGhlaWdodDogMjQ0cHg7XG4gICAgICAgIHdpZHRoOiAyNDRweDtcbiAgICAgIH1cblxuICAgICAgLmNvbHVtbl9faWNvbixcbiAgICAgIC5jYXJkX19pY29uIHtcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICBtYXJnaW46IGF1dG87XG5cbiAgICAgICAgJiBpbWcge1xuICAgICAgICAgIG1heC1oZWlnaHQ6IDEwMCU7XG4gICAgICAgICAgbWF4LXdpZHRoOiAxMDAlO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIC5jYXJkX19pY29uIHtcbiAgICAgICAgJiBpbWcge1xuICAgICAgICAgIG1heC1oZWlnaHQ6IDUwJTtcbiAgICAgICAgICBtYXgtd2lkdGg6IDUwJTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICAmX19ib2R5IHtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuXG4gICAgICAgICYgaDMge1xuICAgICAgICAgIGZvbnQtc2l6ZTogMS41ZW07XG4gICAgICAgICAgY29sb3I6ICMyODNlNTE7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgJiAuY2FyZCB7XG4gICAgICAgIGJveC1zaGFkb3c6IDAgNHB4IDhweCAwIHJnYmEoMCwgMCwgMCwgMC4yKTtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICAgICAgcGFkZGluZzogMTZweDtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xuXG4gICAgICAgICZfX2JvZHkge1xuICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICBwYWRkaW5nOiAyZW07XG4gICAgICAgIH1cblxuICAgICAgICAmX190aXRsZSB7XG4gICAgICAgICAgZm9udC1zaXplOiAxLjVlbTtcbiAgICAgICAgICBjb2xvcjogIzI4M2U1MTtcbiAgICAgICAgfVxuXG4gICAgICAgICZfX3RleHQge1xuICAgICAgICAgIGxpbmUtaGVpZ2h0OiAxLjU7XG4gICAgICAgIH1cblxuICAgICAgICAmX19wcmljZSB7XG4gICAgICAgICAgZm9udC1mYW1pbHk6IFwiQXJpYWwgQmxhY2tcIjtcbiAgICAgICAgICBmb250LXNpemU6IDNlbTtcbiAgICAgICAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoIzJjM2U1MCwgIzI5ODBiOSk7XG4gICAgICAgICAgLXdlYmtpdC1iYWNrZ3JvdW5kLWNsaXA6IHRleHQ7XG4gICAgICAgICAgLXdlYmtpdC10ZXh0LWZpbGwtY29sb3I6IHRyYW5zcGFyZW50O1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgICYgLmFjdGl2ZSB7XG4gICAgICAgIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCgjMmMzZTUwLCAjMjk4MGI5KTtcbiAgICAgICAgcGFkZGluZzogNTBweCAxNnB4O1xuICAgICAgICAmIC5jYXJkX190aXRsZSxcbiAgICAgICAgLmNhcmRfX2JvZHksXG4gICAgICAgIC5jYXJkX19wcmljZSB7XG4gICAgICAgICAgY29sb3I6ICNmZmZmZmY7XG4gICAgICAgIH1cbiAgICAgICAgJiAuY2FyZF9fcHJpY2Uge1xuICAgICAgICAgIC13ZWJraXQtdGV4dC1maWxsLWNvbG9yOiBpbml0aWFsO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgJi0tZmVhdHVyZSB7XG4gICAgcGFkZGluZzogMHB4O1xuICB9XG59XG5cbi5zZWN0aW9uIHtcbiAgJi0tZmVhdHVyZSB7XG4gICAgcGFkZGluZzogMHB4O1xuICB9IFxuICAmLS1jb2xvci0tZGFyayB7XG4gICAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCByZ2JhKDQ0LCA2MiwgODAsIDEpLCByZ2JhKDQxLCAxMjgsIDE4NSwgMC41KSksXG4gICAgdXJsKFwiL2Fzc2V0cy9pbWFnZXMvYWJvdXQtYmcuanBnXCIpO1xuICAgIGJhY2tncm91bmQtYXR0YWNobWVudDogZml4ZWQ7XG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyIHRvcDtcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgJiAuY29udGFpbmVyIC5mbGV4IHtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICYgaDEge1xuICAgICAgICBjb2xvcjogI2ZmZmZmZjtcbiAgICAgIH07XG4gICAgfVxuICB9XG4gICYgZm9ybSB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmOGZhZmU7XG4gICAgcGFkZGluZzogMmVtO1xuICAgIGJveC1zaGFkb3c6IDAgNHB4IDhweCAwIHJnYmEoMCwgMCwgMCwgMC4yKTtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgIHdpZHRoOiA2MCU7XG4gICAgJiAuaW5wdXQtd3JhcHBlciB7XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgIH1cbiAgICAmIGlucHV0LCB0ZXh0YXJlYSB7XG4gICAgICBib3JkZXI6IDA7XG4gICAgICBvdXRsaW5lOiAwO1xuICAgICAgcGFkZGluZzogMWVtO1xuICAgICAgYm9yZGVyLXJhZGl1czogOHB4O1xuICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICB3aWR0aDogMTAwJTtcbiAgICAgIG1hcmdpbi10b3A6IDFlbTtcbiAgICAgIFxuICAgIH1cbiAgICAmIC5idG4td3JhcHBlciB7XG4gICAgICB3aWR0aDogMTAwJTtcbiAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAmIHNwYW4ge1xuICAgICAgICBtYXJnaW46IGF1dG87XG4gICAgICB9XG4gICAgfVxuICAgICYgdGV4dGFyZWEge1xuICAgIH1cbiAgfVxuICAuaGFsZiB7XG4gICAgZmxvYXQ6IGxlZnQ7XG4gICAgd2lkdGg6IDQ4JTtcbiAgICBtYXJnaW4tYm90dG9tOiAxZW07XG4gIH1cbiAgXG4gIC5yaWdodCB7IFxuICAgIHdpZHRoOiA1MCU7IFxuICAgIGRpc3BsYXk6IGZsZXg7XG4gIH1cbiAgXG4gIC5sZWZ0IHtcbiAgICAgICBtYXJnaW4tcmlnaHQ6IDIlOyBcbiAgfVxufVxuXG5AbWVkaWEgKG1heC13aWR0aDogNDgwcHgpIHtcbiAgLnNlY3Rpb24gLmhhbGYge1xuICAgICB3aWR0aDogMTAwJTsgXG4gICAgIGZsb2F0OiBub25lO1xuICAgICBtYXJnaW4tYm90dG9tOiAwOyBcbiAgfVxuICAuc2VjdGlvbiBmb3JtIC5pbnB1dC13cmFwcGVyIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgfVxuICAuc2VjdGlvbiBmb3JtIHtcbiAgICB3aWR0aDogYXV0bztcbiAgfVxufVxuXG5cbi8qIENsZWFyZml4ICovXG4uY2Y6YmVmb3JlLFxuLmNmOmFmdGVyIHtcbiAgICBjb250ZW50OiBcIiBcIjsgLyogMSAqL1xuICAgIGRpc3BsYXk6IHRhYmxlOyAvKiAyICovXG59XG5cbi5jZjphZnRlciB7XG4gICAgY2xlYXI6IGJvdGg7XG59XG5cbkBtZWRpYSAgKG1pbi13aWR0aDogNzY4cHgpIGFuZCAobWF4LXdpZHRoOiA4MTJweCkge1xuICAuaGVhZGVyX19jb250YWluZXIge1xuICAgIG1heC13aWR0aDogNTc2cHg7XG4gIH1cblxuICBzZWN0aW9uIHtcbiAgICAmLmNvbG9yIHtcbiAgICAgICYtLWJnIHtcbiAgICAgICAgJjo6YmVmb3JlIHtcbiAgICAgICAgICBoZWlnaHQ6IDc1cHg7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG5cbiAgICAmIC5jb250YWluZXIge1xuICAgICAgJiAuZmxleCB7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIGZsZXgtd3JhcDogd3JhcDtcbiAgICAgICAgJiAuY29sdW1uIHtcbiAgICAgICAgICBmbGV4LWJhc2lzOiA0OCU7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cblxuQG1lZGlhIChtaW4td2lkdGg6IDczNnB4KSB7XG5cbiAgLmhlYWRlcl9fY29udGFpbmVyIHtcbiAgICBtYXgtd2lkdGg6IDg1JTtcblxuICAgICYgZGl2IHtcbiAgICAgIHdpZHRoOiA3NSU7XG4gICAgICBtYXJnaW4tdG9wOiA1MHB4O1xuICAgIH1cblxuICAgICYgLmN0YSB7XG4gICAgICAmLS1yaWdodCB7XG4gICAgICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIC5oZWFkZXIge1xuICAgICZfX2NvbnRhaW5lciB7XG4gICAgICAmIGRpdjpmaXJzdC1jaGlsZCB7XG4gICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG5cbiAgICAgICAgJiBoMSB7XG4gICAgICAgICAgZm9udC1zaXplOiAzLjVyZW07XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgJiBwIHtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogM2VtO1xuICAgICAgfVxuXG4gICAgICAmIC5idG4td3JhcHBlciB7XG4gICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgICAgIHdpZHRoOiA2MCU7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcblxuICAgICAgICAmIC5idG4ge1xuICAgICAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgICAgICAgcGFkZGluZzogMjBweCAzMHB4O1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgLnNlY3Rpb24ge1xuICAgIHBhZGRpbmc6IDEwMHB4IDA7XG5cbiAgICAmLmNvbG9yIHtcbiAgICAgICYtLWJnIHtcbiAgICAgICAgJjo6YmVmb3JlIHtcbiAgICAgICAgICBoZWlnaHQ6IDE1MHB4O1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgJiAuY29udGFpbmVyIHtcbiAgICAgIC5mbGV4IHtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgIH1cblxuICAgICAgLmNoaWxkIHtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgJiBoMSB7XG4gICAgICAgICAgZm9udC1zaXplOiAyLjVlbTtcbiAgICAgICAgfVxuXG4gICAgICAgICY6Zmlyc3QtY2hpbGQge1xuICAgICAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xuICAgICAgICB9XG5cbiAgICAgICAgLmNoaWxkOm50aC1jaGlsZCgyKSB7XG4gICAgICAgICAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xuICAgICAgICAgIHdpZHRoOiA1MCU7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgJiAuY3RhIHtcbiAgICAgICAgd2lkdGg6IDYwJTtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgIHBhZGRpbmc6IDJlbTtcblxuICAgICAgICAmLS1yaWdodCB7XG4gICAgICAgICAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gICAgICAgIH1cbiAgICAgICAgJi0tbGVmdCB7XG4gICAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICAgICAgfVxuXG4gICAgICAgICZfX2ltZyB7XG4gICAgICAgICAgd2lkdGg6IDQwJTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgJiAuZmVhdHVyZS1ncm91cCB7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgICB9XG4gICAgfVxuICB9XG4gIC5zZWN0aW9uLS1mZWF0dXJlIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgdG9wOiAtMjUwcHg7XG4gIH1cbn1cblxuQG1lZGlhIChtaW4td2lkdGg6IDEzMDBweCkge1xuICAuaGVhZGVyX19jb250YWluZXIge1xuICAgIG1heC13aWR0aDogMTIwMHB4O1xuICB9XG4gIHNlY3Rpb24ge1xuICAgICYuY29sb3Ige1xuICAgICAgJi0tYmcge1xuICAgICAgICAmOjpiZWZvcmV7XG4gICAgICAgICAgaGVpZ2h0OiAxNTBweDtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgICAmI2NhcmJvbi1vZmZzZXQgLmNvbnRhaW5lciAuZmxleCBkaXYge1xuICAgICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICB9XG4gIH1cbn1cbiIsIi5oZWFkZXIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGhlaWdodDogNzUwcHg7XG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgIzJjM2U1MCwgcmdiYSg0MSwgMTI4LCAxODUsIDAuNSkpLCB1cmwoXCIvYXNzZXRzL2ltYWdlcy9hYm91dC1iZy5qcGdcIik7XG4gIGJhY2tncm91bmQtYXR0YWNobWVudDogZml4ZWQ7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlciB0b3A7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG59XG4uaGVhZGVyIHN2ZyB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYm90dG9tOiAwO1xuICB3aWR0aDogMTAwJTtcbn1cbi5oZWFkZXIgc3ZnIHBhdGgge1xuICBmaWxsOiAjZmZmZmZmO1xufVxuLmhlYWRlcl9fY29udGFpbmVyIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgZGlzcGxheTogZmxleDtcbiAgbWFyZ2luOiBhdXRvO1xuICBjb2xvcjogd2hpdGU7XG59XG4uaGVhZGVyX19jb250YWluZXIgZGl2IHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLmhlYWRlcl9fY29udGFpbmVyIGRpdiBwIHtcbiAgbWFyZ2luOiAyMCAwIDIwIDA7XG59XG5cbnNlY3Rpb24uY29sb3Ige1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjhmYWZlO1xufVxuc2VjdGlvbi5jb2xvci0tZ3JhZGllbnQge1xuICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICMyOTgwYjksICMyYzNlNTApO1xufVxuc2VjdGlvbi5jb2xvci0tYmcge1xuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICMyOTgwYjksIHJnYmEoNDQsIDYyLCA4MCwgMC41KSksIHVybChcIi9hc3NldHMvaW1hZ2VzL2ZvcmVzdC5qcGdcIik7XG4gIGJhY2tncm91bmQtYXR0YWNobWVudDogZml4ZWQ7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IDUwJSA1MCU7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbnNlY3Rpb24uY29sb3ItLWJnOjpiZWZvcmUge1xuICBjb250ZW50OiBcIlwiO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMDtcbiAgbGVmdDogMDtcbiAgdHJhbnNmb3JtOiBzY2FsZSgtMSwgLTEpO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiA1MHB4O1xuICBmaWxsOiAjZmZmZmZmO1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIvYXNzZXRzL2ltYWdlcy93YXZ5LnN2Z1wiKTtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbn1cbnNlY3Rpb24uY29sb3ItLWJnIC5jdGEgaDIge1xuICBjb2xvcjogd2hpdGU7XG59XG5zZWN0aW9uLmNvbG9yLS1iZyAuY29udGFpbmVyIC5mbGV4IHtcbiAganVzdGlmeS1jb250ZW50OiBsZWZ0O1xufVxuc2VjdGlvbi5jb2xvci0tYmcgLmNvbnRhaW5lciAuZmxleCAuaDIge1xuICBjb2xvcjogI2ZmZmZmZjtcbn1cbnNlY3Rpb24uY29sb3ItLWJnIC5jb250YWluZXIgLmZsZXggLmN0YSB7XG4gIGNvbG9yOiAjZmZmZmZmO1xufVxuc2VjdGlvbi5jb2xvci0tYmcgLmNvbnRhaW5lcjpudGgtY2hpbGQoMikgLmZsZXgge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xufVxuc2VjdGlvbiNjYXJib24tb2Zmc2V0IC5mbGV4IHtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgbGluZS1oZWlnaHQ6IDI7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbnNlY3Rpb24jY2FyYm9uLW9mZnNldCAuZmxleCBpbWcge1xuICBtYXgtd2lkdGg6IDI1MHB4O1xuICBtYXJnaW46IDNlbTtcbn1cbnNlY3Rpb24tLWZlYXR1cmUge1xuICBwYWRkaW5nOiAwO1xufVxuc2VjdGlvbi0tZmVhdHVyZSAuZmVhdHVyZS1ncm91cCB7XG4gIHBhZGRpbmc6IDNlbTtcbn1cbnNlY3Rpb24gLmNvbnRhaW5lciB7XG4gIHBhZGRpbmc6IDMwcHggMHB4O1xuICBtYXgtd2lkdGg6IDExNDBweDtcbiAgbWFyZ2luOiBhdXRvO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuc2VjdGlvbiAuY29udGFpbmVyIC5mbGV4IHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG59XG5zZWN0aW9uIC5jb250YWluZXIgLmhlYWRpbmcge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5zZWN0aW9uIC5jb250YWluZXIgaDIge1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBmb250LXNpemU6IDJlbTtcbiAgY29sb3I6ICMyODNlNTE7XG59XG5zZWN0aW9uIC5jb250YWluZXIgLmN0YSB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgcGFkZGluZzogMmVtIDBlbTtcbn1cbnNlY3Rpb24gLmNvbnRhaW5lciAuY3RhIHAge1xuICBsaW5lLWhlaWdodDogMS44O1xufVxuc2VjdGlvbiAuY29udGFpbmVyIC5jdGFfX2ltZyB7XG4gIHdpZHRoOiA1MCU7XG4gIG1hcmdpbjogMCBhdXRvO1xufVxuc2VjdGlvbiAuY29udGFpbmVyIC5jb2x1bW4ge1xuICBwYWRkaW5nOiAyMHB4O1xuICBsaW5lLWhlaWdodDogMS41O1xuICBjb2xvcjogIzI5ODBiOTtcbn1cbnNlY3Rpb24gLmNvbnRhaW5lciAuY29sdW1uIC5jb2x1bW5fX2ljb24ge1xuICBoZWlnaHQ6IDI0NHB4O1xuICB3aWR0aDogMjQ0cHg7XG59XG5zZWN0aW9uIC5jb250YWluZXIgLmNvbHVtbiAuY29sdW1uX19pY29uLFxuc2VjdGlvbiAuY29udGFpbmVyIC5jb2x1bW4gLmNhcmRfX2ljb24ge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG1hcmdpbjogYXV0bztcbn1cbnNlY3Rpb24gLmNvbnRhaW5lciAuY29sdW1uIC5jb2x1bW5fX2ljb24gaW1nLFxuc2VjdGlvbiAuY29udGFpbmVyIC5jb2x1bW4gLmNhcmRfX2ljb24gaW1nIHtcbiAgbWF4LWhlaWdodDogMTAwJTtcbiAgbWF4LXdpZHRoOiAxMDAlO1xufVxuc2VjdGlvbiAuY29udGFpbmVyIC5jb2x1bW4gLmNhcmRfX2ljb24gaW1nIHtcbiAgbWF4LWhlaWdodDogNTAlO1xuICBtYXgtd2lkdGg6IDUwJTtcbn1cbnNlY3Rpb24gLmNvbnRhaW5lciAuY29sdW1uX19ib2R5IHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuc2VjdGlvbiAuY29udGFpbmVyIC5jb2x1bW5fX2JvZHkgaDMge1xuICBmb250LXNpemU6IDEuNWVtO1xuICBjb2xvcjogIzI4M2U1MTtcbn1cbnNlY3Rpb24gLmNvbnRhaW5lciAuY29sdW1uIC5jYXJkIHtcbiAgYm94LXNoYWRvdzogMCA0cHggOHB4IDAgcmdiYSgwLCAwLCAwLCAwLjIpO1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBwYWRkaW5nOiAxNnB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XG59XG5zZWN0aW9uIC5jb250YWluZXIgLmNvbHVtbiAuY2FyZF9fYm9keSB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgcGFkZGluZzogMmVtO1xufVxuc2VjdGlvbiAuY29udGFpbmVyIC5jb2x1bW4gLmNhcmRfX3RpdGxlIHtcbiAgZm9udC1zaXplOiAxLjVlbTtcbiAgY29sb3I6ICMyODNlNTE7XG59XG5zZWN0aW9uIC5jb250YWluZXIgLmNvbHVtbiAuY2FyZF9fdGV4dCB7XG4gIGxpbmUtaGVpZ2h0OiAxLjU7XG59XG5zZWN0aW9uIC5jb250YWluZXIgLmNvbHVtbiAuY2FyZF9fcHJpY2Uge1xuICBmb250LWZhbWlseTogXCJBcmlhbCBCbGFja1wiO1xuICBmb250LXNpemU6IDNlbTtcbiAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KCMyYzNlNTAsICMyOTgwYjkpO1xuICAtd2Via2l0LWJhY2tncm91bmQtY2xpcDogdGV4dDtcbiAgLXdlYmtpdC10ZXh0LWZpbGwtY29sb3I6IHRyYW5zcGFyZW50O1xufVxuc2VjdGlvbiAuY29udGFpbmVyIC5jb2x1bW4gLmFjdGl2ZSB7XG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCgjMmMzZTUwLCAjMjk4MGI5KTtcbiAgcGFkZGluZzogNTBweCAxNnB4O1xufVxuc2VjdGlvbiAuY29udGFpbmVyIC5jb2x1bW4gLmFjdGl2ZSAuY2FyZF9fdGl0bGUsXG5zZWN0aW9uIC5jb250YWluZXIgLmNvbHVtbiAuYWN0aXZlIC5jYXJkX19ib2R5LFxuc2VjdGlvbiAuY29udGFpbmVyIC5jb2x1bW4gLmFjdGl2ZSAuY2FyZF9fcHJpY2Uge1xuICBjb2xvcjogI2ZmZmZmZjtcbn1cbnNlY3Rpb24gLmNvbnRhaW5lciAuY29sdW1uIC5hY3RpdmUgLmNhcmRfX3ByaWNlIHtcbiAgLXdlYmtpdC10ZXh0LWZpbGwtY29sb3I6IGluaXRpYWw7XG59XG5zZWN0aW9uLS1mZWF0dXJlIHtcbiAgcGFkZGluZzogMHB4O1xufVxuXG4uc2VjdGlvbi0tZmVhdHVyZSB7XG4gIHBhZGRpbmc6IDBweDtcbn1cbi5zZWN0aW9uLS1jb2xvci0tZGFyayB7XG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgIzJjM2U1MCwgcmdiYSg0MSwgMTI4LCAxODUsIDAuNSkpLCB1cmwoXCIvYXNzZXRzL2ltYWdlcy9hYm91dC1iZy5qcGdcIik7XG4gIGJhY2tncm91bmQtYXR0YWNobWVudDogZml4ZWQ7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlciB0b3A7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG59XG4uc2VjdGlvbi0tY29sb3ItLWRhcmsgLmNvbnRhaW5lciAuZmxleCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5zZWN0aW9uLS1jb2xvci0tZGFyayAuY29udGFpbmVyIC5mbGV4IGgxIHtcbiAgY29sb3I6ICNmZmZmZmY7XG59XG4uc2VjdGlvbiBmb3JtIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjhmYWZlO1xuICBwYWRkaW5nOiAyZW07XG4gIGJveC1zaGFkb3c6IDAgNHB4IDhweCAwIHJnYmEoMCwgMCwgMCwgMC4yKTtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgd2lkdGg6IDYwJTtcbn1cbi5zZWN0aW9uIGZvcm0gLmlucHV0LXdyYXBwZXIge1xuICBkaXNwbGF5OiBmbGV4O1xufVxuLnNlY3Rpb24gZm9ybSBpbnB1dCwgLnNlY3Rpb24gZm9ybSB0ZXh0YXJlYSB7XG4gIGJvcmRlcjogMDtcbiAgb3V0bGluZTogMDtcbiAgcGFkZGluZzogMWVtO1xuICBib3JkZXItcmFkaXVzOiA4cHg7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB3aWR0aDogMTAwJTtcbiAgbWFyZ2luLXRvcDogMWVtO1xufVxuLnNlY3Rpb24gZm9ybSAuYnRuLXdyYXBwZXIge1xuICB3aWR0aDogMTAwJTtcbiAgZGlzcGxheTogZmxleDtcbn1cbi5zZWN0aW9uIGZvcm0gLmJ0bi13cmFwcGVyIHNwYW4ge1xuICBtYXJnaW46IGF1dG87XG59XG4uc2VjdGlvbiAuaGFsZiB7XG4gIGZsb2F0OiBsZWZ0O1xuICB3aWR0aDogNDglO1xuICBtYXJnaW4tYm90dG9tOiAxZW07XG59XG4uc2VjdGlvbiAucmlnaHQge1xuICB3aWR0aDogNTAlO1xuICBkaXNwbGF5OiBmbGV4O1xufVxuLnNlY3Rpb24gLmxlZnQge1xuICBtYXJnaW4tcmlnaHQ6IDIlO1xufVxuXG5AbWVkaWEgKG1heC13aWR0aDogNDgwcHgpIHtcbiAgLnNlY3Rpb24gLmhhbGYge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGZsb2F0OiBub25lO1xuICAgIG1hcmdpbi1ib3R0b206IDA7XG4gIH1cblxuICAuc2VjdGlvbiBmb3JtIC5pbnB1dC13cmFwcGVyIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgfVxuXG4gIC5zZWN0aW9uIGZvcm0ge1xuICAgIHdpZHRoOiBhdXRvO1xuICB9XG59XG4vKiBDbGVhcmZpeCAqL1xuLmNmOmJlZm9yZSxcbi5jZjphZnRlciB7XG4gIGNvbnRlbnQ6IFwiIFwiO1xuICAvKiAxICovXG4gIGRpc3BsYXk6IHRhYmxlO1xuICAvKiAyICovXG59XG5cbi5jZjphZnRlciB7XG4gIGNsZWFyOiBib3RoO1xufVxuXG5AbWVkaWEgKG1pbi13aWR0aDogNzY4cHgpIGFuZCAobWF4LXdpZHRoOiA4MTJweCkge1xuICAuaGVhZGVyX19jb250YWluZXIge1xuICAgIG1heC13aWR0aDogNTc2cHg7XG4gIH1cblxuICBzZWN0aW9uLmNvbG9yLS1iZzo6YmVmb3JlIHtcbiAgICBoZWlnaHQ6IDc1cHg7XG4gIH1cbiAgc2VjdGlvbiAuY29udGFpbmVyIC5mbGV4IHtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGZsZXgtd3JhcDogd3JhcDtcbiAgfVxuICBzZWN0aW9uIC5jb250YWluZXIgLmZsZXggLmNvbHVtbiB7XG4gICAgZmxleC1iYXNpczogNDglO1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogNzM2cHgpIHtcbiAgLmhlYWRlcl9fY29udGFpbmVyIHtcbiAgICBtYXgtd2lkdGg6IDg1JTtcbiAgfVxuICAuaGVhZGVyX19jb250YWluZXIgZGl2IHtcbiAgICB3aWR0aDogNzUlO1xuICAgIG1hcmdpbi10b3A6IDUwcHg7XG4gIH1cbiAgLmhlYWRlcl9fY29udGFpbmVyIC5jdGEtLXJpZ2h0IHtcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcbiAgfVxuXG4gIC5oZWFkZXJfX2NvbnRhaW5lciBkaXY6Zmlyc3QtY2hpbGQge1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gIH1cbiAgLmhlYWRlcl9fY29udGFpbmVyIGRpdjpmaXJzdC1jaGlsZCBoMSB7XG4gICAgZm9udC1zaXplOiAzLjVyZW07XG4gIH1cbiAgLmhlYWRlcl9fY29udGFpbmVyIHAge1xuICAgIG1hcmdpbi1ib3R0b206IDNlbTtcbiAgfVxuICAuaGVhZGVyX19jb250YWluZXIgLmJ0bi13cmFwcGVyIHtcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgIHdpZHRoOiA2MCU7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIH1cbiAgLmhlYWRlcl9fY29udGFpbmVyIC5idG4td3JhcHBlciAuYnRuIHtcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIHBhZGRpbmc6IDIwcHggMzBweDtcbiAgfVxuXG4gIC5zZWN0aW9uIHtcbiAgICBwYWRkaW5nOiAxMDBweCAwO1xuICB9XG4gIC5zZWN0aW9uLmNvbG9yLS1iZzo6YmVmb3JlIHtcbiAgICBoZWlnaHQ6IDE1MHB4O1xuICB9XG4gIC5zZWN0aW9uIC5jb250YWluZXIgLmZsZXgge1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIH1cbiAgLnNlY3Rpb24gLmNvbnRhaW5lciAuY2hpbGQge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gIH1cbiAgLnNlY3Rpb24gLmNvbnRhaW5lciAuY2hpbGQgaDEge1xuICAgIGZvbnQtc2l6ZTogMi41ZW07XG4gIH1cbiAgLnNlY3Rpb24gLmNvbnRhaW5lciAuY2hpbGQ6Zmlyc3QtY2hpbGQge1xuICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xuICB9XG4gIC5zZWN0aW9uIC5jb250YWluZXIgLmNoaWxkIC5jaGlsZDpudGgtY2hpbGQoMikge1xuICAgIGFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcbiAgICB3aWR0aDogNTAlO1xuICB9XG4gIC5zZWN0aW9uIC5jb250YWluZXIgLmN0YSB7XG4gICAgd2lkdGg6IDYwJTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgcGFkZGluZzogMmVtO1xuICB9XG4gIC5zZWN0aW9uIC5jb250YWluZXIgLmN0YS0tcmlnaHQge1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICB9XG4gIC5zZWN0aW9uIC5jb250YWluZXIgLmN0YS0tbGVmdCB7XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgfVxuICAuc2VjdGlvbiAuY29udGFpbmVyIC5jdGFfX2ltZyB7XG4gICAgd2lkdGg6IDQwJTtcbiAgfVxuICAuc2VjdGlvbiAuY29udGFpbmVyIC5mZWF0dXJlLWdyb3VwIHtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gIH1cblxuICAuc2VjdGlvbi0tZmVhdHVyZSB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHRvcDogLTI1MHB4O1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogMTMwMHB4KSB7XG4gIC5oZWFkZXJfX2NvbnRhaW5lciB7XG4gICAgbWF4LXdpZHRoOiAxMjAwcHg7XG4gIH1cblxuICBzZWN0aW9uLmNvbG9yLS1iZzo6YmVmb3JlIHtcbiAgICBoZWlnaHQ6IDE1MHB4O1xuICB9XG4gIHNlY3Rpb24jY2FyYm9uLW9mZnNldCAuY29udGFpbmVyIC5mbGV4IGRpdiB7XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgfVxufSJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AboutComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-about',
                templateUrl: './about.component.html',
                styleUrls: ['./about.component.scss']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule, RoutingComponents */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoutingComponents", function() { return RoutingComponents; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _shared_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./shared/navbar/navbar.component */ "./src/app/shared/navbar/navbar.component.ts");
/* harmony import */ var _shared_footer_footer_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./shared/footer/footer.component */ "./src/app/shared/footer/footer.component.ts");
/* harmony import */ var _about_about_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./about/about.component */ "./src/app/about/about.component.ts");
/* harmony import */ var _products_products_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./products/products.component */ "./src/app/products/products.component.ts");
/* harmony import */ var _wordpress_maintenance_wordpress_maintenance_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./wordpress-maintenance/wordpress-maintenance.component */ "./src/app/wordpress-maintenance/wordpress-maintenance.component.ts");
/* harmony import */ var _contact_contact_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./contact/contact.component */ "./src/app/contact/contact.component.ts");











const routes = [
    { path: '', component: _home_home_component__WEBPACK_IMPORTED_MODULE_2__["HomeComponent"] },
    { path: 'about', component: _about_about_component__WEBPACK_IMPORTED_MODULE_5__["AboutComponent"] },
    { path: 'wordpress-hosting', component: _products_products_component__WEBPACK_IMPORTED_MODULE_6__["ProductsComponent"] },
    { path: 'wordpress-maintenance', component: _wordpress_maintenance_wordpress_maintenance_component__WEBPACK_IMPORTED_MODULE_7__["WordpressMaintenanceComponent"] },
    { path: 'contact', component: _contact_contact_component__WEBPACK_IMPORTED_MODULE_8__["ContactComponent"] }
];
class AppRoutingModule {
}
AppRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: AppRoutingModule });
AppRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function AppRoutingModule_Factory(t) { return new (t || AppRoutingModule)(); }, imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
        _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AppRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppRoutingModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
                exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
            }]
    }], null, null); })();
const RoutingComponents = [_home_home_component__WEBPACK_IMPORTED_MODULE_2__["HomeComponent"], _shared_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_3__["NavbarComponent"], _about_about_component__WEBPACK_IMPORTED_MODULE_5__["AboutComponent"], _wordpress_maintenance_wordpress_maintenance_component__WEBPACK_IMPORTED_MODULE_7__["WordpressMaintenanceComponent"], _shared_footer_footer_component__WEBPACK_IMPORTED_MODULE_4__["FooterComponent"], _contact_contact_component__WEBPACK_IMPORTED_MODULE_8__["ContactComponent"]];


/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");



class AppComponent {
    constructor() {
        this.title = 'project';
    }
}
AppComponent.ɵfac = function AppComponent_Factory(t) { return new (t || AppComponent)(); };
AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AppComponent, selectors: [["app-root"]], decls: 1, vars: 0, template: function AppComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "router-outlet");
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterOutlet"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-root',
                templateUrl: './app.component.html',
                styleUrls: ['./app.component.scss']
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _products_products_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./products/products.component */ "./src/app/products/products.component.ts");
/* harmony import */ var _wordpress_maintenance_wordpress_maintenance_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./wordpress-maintenance/wordpress-maintenance.component */ "./src/app/wordpress-maintenance/wordpress-maintenance.component.ts");
/* harmony import */ var _pricing_tab_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./pricing-tab.service */ "./src/app/pricing-tab.service.ts");
/* harmony import */ var _contact_contact_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./contact/contact.component */ "./src/app/contact/contact.component.ts");
/* harmony import */ var _shared_scroll_scroll_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./shared/scroll/scroll.component */ "./src/app/shared/scroll/scroll.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _shared_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./shared/navbar/navbar.component */ "./src/app/shared/navbar/navbar.component.ts");
/* harmony import */ var _about_about_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./about/about.component */ "./src/app/about/about.component.ts");
/* harmony import */ var _shared_footer_footer_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./shared/footer/footer.component */ "./src/app/shared/footer/footer.component.ts");

















class AppModule {
}
AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineNgModule"]({ type: AppModule, bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]] });
AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjector"]({ factory: function AppModule_Factory(t) { return new (t || AppModule)(); }, providers: [_pricing_tab_service__WEBPACK_IMPORTED_MODULE_6__["PricingTabService"]], imports: [[
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpClientModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsetNgModuleScope"](AppModule, { declarations: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"], _home_home_component__WEBPACK_IMPORTED_MODULE_10__["HomeComponent"], _shared_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_11__["NavbarComponent"], _about_about_component__WEBPACK_IMPORTED_MODULE_12__["AboutComponent"], _wordpress_maintenance_wordpress_maintenance_component__WEBPACK_IMPORTED_MODULE_5__["WordpressMaintenanceComponent"], _shared_footer_footer_component__WEBPACK_IMPORTED_MODULE_13__["FooterComponent"], _contact_contact_component__WEBPACK_IMPORTED_MODULE_7__["ContactComponent"], _products_products_component__WEBPACK_IMPORTED_MODULE_4__["ProductsComponent"],
        _wordpress_maintenance_wordpress_maintenance_component__WEBPACK_IMPORTED_MODULE_5__["WordpressMaintenanceComponent"],
        _contact_contact_component__WEBPACK_IMPORTED_MODULE_7__["ContactComponent"],
        _shared_scroll_scroll_component__WEBPACK_IMPORTED_MODULE_8__["ScrollComponent"]], imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
        _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpClientModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](AppModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"],
        args: [{
                declarations: [
                    _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                    _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["RoutingComponents"],
                    _products_products_component__WEBPACK_IMPORTED_MODULE_4__["ProductsComponent"],
                    _wordpress_maintenance_wordpress_maintenance_component__WEBPACK_IMPORTED_MODULE_5__["WordpressMaintenanceComponent"],
                    _contact_contact_component__WEBPACK_IMPORTED_MODULE_7__["ContactComponent"],
                    _shared_scroll_scroll_component__WEBPACK_IMPORTED_MODULE_8__["ScrollComponent"],
                ],
                imports: [
                    _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                    _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"],
                    _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpClientModule"]
                ],
                providers: [_pricing_tab_service__WEBPACK_IMPORTED_MODULE_6__["PricingTabService"]],
                bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/contact/contact.component.ts":
/*!**********************************************!*\
  !*** ./src/app/contact/contact.component.ts ***!
  \**********************************************/
/*! exports provided: ContactComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactComponent", function() { return ContactComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _shared_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../shared/navbar/navbar.component */ "./src/app/shared/navbar/navbar.component.ts");
/* harmony import */ var _shared_scroll_scroll_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../shared/scroll/scroll.component */ "./src/app/shared/scroll/scroll.component.ts");
/* harmony import */ var _shared_footer_footer_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../shared/footer/footer.component */ "./src/app/shared/footer/footer.component.ts");





class ContactComponent {
    constructor() { }
    ngOnInit() {
    }
}
ContactComponent.ɵfac = function ContactComponent_Factory(t) { return new (t || ContactComponent)(); };
ContactComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ContactComponent, selectors: [["app-contact"]], decls: 78, vars: 0, consts: [[1, "header"], ["id", "curve", "data-name", "Layer 1", "xmlns", "http://www.w3.org/2000/svg", "viewBox", "0 0 1416.99 174.01"], ["d", "M0,280.8S283.66,59,608.94,163.56s437.93,150.57,808,10.34V309.54H0V280.8Z", "transform", "translate(0 -135.53)", 1, "cls-1"], [1, "header__container"], [1, "section", "wordpress-hosting"], [1, "container"], [1, "flex"], ["data-aos", "fade-up", 1, "card"], [1, "form-container"], [1, "form-wrapper"], [1, "input-wrapper"], [1, "form-group"], ["for", "input-topic"], ["type", "text", "id", "input-topic", "placeholder", "Topic"], ["for", "input-company"], ["type", "text", "id", "input-company", "placeholder", "Company"], ["for", "input-name"], ["type", "text", "id", "input-name", "placeholder", "Name"], ["for", "input-email"], ["type", "email", "id", "input-email", "placeholder", "Email address"], ["for", "input-subject"], ["type", "text", "id", "input-subject", "placeholder", "Subject"], ["for", "input-message"], ["name", "message", "type", "text", "id", "input-message", "placeholder", "Message"], [1, "btn-wrapper"], [1, "btn", "btn--std", "btn--std01"], [1, "faq"], [1, "faq-content"], [1, "faq-question"], ["id", "q1", "type", "checkbox", 1, "panel"], [1, "plus"], ["for", "q1", 1, "panel-title"], [1, "panel-content"], ["id", "q2", "type", "checkbox", 1, "panel"], ["for", "q2", 1, "panel-title"], ["id", "q3", "type", "checkbox", 1, "panel"], ["for", "q3", 1, "panel-title"], ["href", "https://en.wikipedia.org/wiki/The_Unanswered_Question", "target", "_blank"]], template: function ContactComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "header", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "app-navbar");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceSVG"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "svg", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "path", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceHTML"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "WANT TO ASK US A QUESTION?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "Have questions? Please review our Frequently Asked Questions or contact us.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "section", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "form", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "Send HostFront a Message");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "label", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "Topic");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](22, "input", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "label", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, "Company");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "input", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "label", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "Name");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](30, "input", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "label", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, "Email address");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](34, "input", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "label", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37, "Subject");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](38, "input", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "label", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](41, "Message");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](42, "textarea", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "div", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "span", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45, "Send");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](48, "FAQ'S");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "div", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](51, "input", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "div", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](53, "+");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "label", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](55, "What is the meaning of life?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "div", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](57, "42");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "div", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](59, "input", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "div", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](61, "+");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "label", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](63, "How much wood would a woodchuck chuck?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "div", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](65, "A woodchuck would chuck all the wood he could chuck, if a woodchuck could chuck wood!");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "div", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](67, "input", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "div", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](69, "+");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](70, "label", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](71, "What happens if Pinocchio says, \"my nose will grow now\"?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "div", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](73, "Certain questions are better left \u00A0 ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "a", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](75, "unanswered");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](76, "app-scroll");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](77, "app-footer");
    } }, directives: [_shared_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_1__["NavbarComponent"], _shared_scroll_scroll_component__WEBPACK_IMPORTED_MODULE_2__["ScrollComponent"], _shared_footer_footer_component__WEBPACK_IMPORTED_MODULE_3__["FooterComponent"]], styles: [".header[_ngcontent-%COMP%] {\n  position: relative;\n  height: 750px;\n  background-image: linear-gradient(to right, #2c3e50, rgba(41, 128, 185, 0.5)), url(\"/assets/images/contact-bg.jpg\");\n  background-attachment: fixed;\n  background-position: 50% 50%;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n.header[_ngcontent-%COMP%]   svg[_ngcontent-%COMP%] {\n  position: absolute;\n  bottom: 0;\n  width: 100%;\n}\n.header[_ngcontent-%COMP%]   svg[_ngcontent-%COMP%]   path[_ngcontent-%COMP%] {\n  fill: #ffffff;\n}\n.header__container[_ngcontent-%COMP%] {\n  width: 100%;\n  height: 100%;\n  display: flex;\n  margin: auto;\n  color: white;\n}\n.header__container[_ngcontent-%COMP%]   div[_ngcontent-%COMP%] {\n  text-align: center;\n}\n.header__container[_ngcontent-%COMP%]   div[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n  margin: 20 0 20 0;\n}\n@media (min-width: 768px) and (max-width: 1366px) {\n  .header__container[_ngcontent-%COMP%] {\n    max-width: 960px;\n  }\n  .header__container[_ngcontent-%COMP%]   div[_ngcontent-%COMP%] {\n    width: 50%;\n  }\n}\n@media (min-width: 768px) {\n  .header__container[_ngcontent-%COMP%] {\n    max-width: 85%;\n  }\n  .header__container[_ngcontent-%COMP%]   div[_ngcontent-%COMP%]:first-child {\n    text-align: left;\n    width: 80%;\n    margin-top: 50px;\n  }\n}\n@media (min-width: 1366px) {\n  .header__container[_ngcontent-%COMP%] {\n    max-width: 1200px;\n  }\n  .header__container[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n    font-size: 3.5rem;\n  }\n}\n.section[_ngcontent-%COMP%] {\n  padding: 100px 0;\n}\n.section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%] {\n  padding: 30px 0px;\n  max-width: 1140px;\n  margin: auto;\n}\n.section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: center;\n  flex-direction: column;\n  align-items: center;\n}\n.section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .heading[_ngcontent-%COMP%] {\n  text-align: center;\n}\nform[_ngcontent-%COMP%] {\n  width: 100%;\n  display: flex;\n}\nform[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  text-transform: uppercase;\n  font-size: 2em;\n  color: #283e51;\n}\nform[_ngcontent-%COMP%]   .input-wrapper[_ngcontent-%COMP%] {\n  display: flex;\n  flex-direction: column;\n}\nform[_ngcontent-%COMP%]   .input-wrapper[_ngcontent-%COMP%]   .form-group[_ngcontent-%COMP%] {\n  position: relative;\n  margin-bottom: 16px;\n  display: flex;\n  flex-direction: column;\n}\nform[_ngcontent-%COMP%]   .input-wrapper[_ngcontent-%COMP%]   .form-group[_ngcontent-%COMP%]   label[_ngcontent-%COMP%] {\n  font-weight: 300;\n  margin-bottom: 8px;\n}\nform[_ngcontent-%COMP%]   .input-wrapper[_ngcontent-%COMP%]   .form-group[_ngcontent-%COMP%]   input[_ngcontent-%COMP%], form[_ngcontent-%COMP%]   .input-wrapper[_ngcontent-%COMP%]   .form-group[_ngcontent-%COMP%]   textarea[_ngcontent-%COMP%] {\n  height: 60px;\n  border-radius: 3px;\n  font-size: 17px;\n  padding: 2em;\n  border: 1px solid #d3d6e2;\n  background-color: #fff;\n  box-shadow: none;\n}\nform[_ngcontent-%COMP%]   .input-wrapper[_ngcontent-%COMP%]   .form-group[_ngcontent-%COMP%]   textarea[_ngcontent-%COMP%] {\n  min-height: 286px;\n}\nform[_ngcontent-%COMP%]   .btn-wrapper[_ngcontent-%COMP%] {\n  margin: 2em 0px;\n  display: flex;\n}\nform[_ngcontent-%COMP%]   .btn-wrapper[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%] {\n  min-width: 245px;\n  font-size: 1.5em;\n  text-align: center;\n  margin: auto;\n}\nform[_ngcontent-%COMP%]   .faq[_ngcontent-%COMP%] {\n  border-left: 1px solid #e6e7eb;\n  padding: 0 2em;\n}\nform[_ngcontent-%COMP%]   .faq[_ngcontent-%COMP%]   .faq-content[_ngcontent-%COMP%] {\n  margin: 0 auto;\n}\nform[_ngcontent-%COMP%]   .faq[_ngcontent-%COMP%]   .faq-question[_ngcontent-%COMP%] {\n  padding: 20px 0;\n  border-bottom: 1px dotted #ccc;\n}\nform[_ngcontent-%COMP%]   .faq[_ngcontent-%COMP%]   .panel-title[_ngcontent-%COMP%] {\n  font-size: 1.25em;\n  width: 100%;\n  position: relative;\n  margin: 0;\n  padding: 10px 10px 0 48px;\n  display: block;\n  cursor: pointer;\n}\nform[_ngcontent-%COMP%]   .faq[_ngcontent-%COMP%]   .panel-content[_ngcontent-%COMP%] {\n  padding: 0px 14px;\n  margin: 0 40px;\n  height: 0;\n  overflow: hidden;\n  z-index: 5;\n  position: relative;\n  opacity: 0;\n  transition: 0.4s ease;\n}\nform[_ngcontent-%COMP%]   .faq[_ngcontent-%COMP%]   .panel[_ngcontent-%COMP%]:checked    ~ .panel-content[_ngcontent-%COMP%] {\n  height: auto;\n  opacity: 1;\n  padding: 14px;\n}\nform[_ngcontent-%COMP%]   .faq[_ngcontent-%COMP%]   .plus[_ngcontent-%COMP%] {\n  position: absolute;\n  margin-left: 20px;\n  margin-top: 4px;\n  z-index: 5;\n  font-size: 42px;\n  line-height: 100%;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  -o-user-select: none;\n  user-select: none;\n  transition: 0.2s ease;\n}\nform[_ngcontent-%COMP%]   .faq[_ngcontent-%COMP%]   .panel[_ngcontent-%COMP%]:checked    ~ .plus[_ngcontent-%COMP%] {\n  transform: rotate(45deg);\n}\nform[_ngcontent-%COMP%]   .faq[_ngcontent-%COMP%]   .panel[_ngcontent-%COMP%] {\n  display: none;\n}\n.form-container[_ngcontent-%COMP%] {\n  display: flex;\n  flex-direction: column;\n}\n.form-wrapper[_ngcontent-%COMP%] {\n  text-align: left;\n}\n@media (min-width: 768px) {\n  .form-container[_ngcontent-%COMP%] {\n    flex-direction: row;\n  }\n\n  .form-wrapper[_ngcontent-%COMP%] {\n    width: 60%;\n    padding: 0 2em;\n  }\n\n  .faq[_ngcontent-%COMP%] {\n    width: 40%;\n  }\n\n  form[_ngcontent-%COMP%]   .btn-wrapper[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%] {\n    margin: 0;\n  }\n}\n.card[_ngcontent-%COMP%] {\n  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);\n  border-radius: 10px;\n  padding: 3em;\n  background-color: #ffffff;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2NoaWRvL3Byb2plY3RzL2hvc3Rmcm9udC1wcm9qZWN0L2hvc3Rmcm9udC1mcm9udGVuZC9zcmMvYXBwL2NvbnRhY3QvY29udGFjdC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvY29udGFjdC9jb250YWN0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usa0JBQUE7RUFDQSxhQUFBO0VBQ0EsbUhBQUE7RUFFQSw0QkFBQTtFQUNBLDRCQUFBO0VBQ0EsNEJBQUE7RUFDQSxzQkFBQTtBQ0FGO0FERUU7RUFDRSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxXQUFBO0FDQUo7QURFSTtFQUNFLGFBQUE7QUNBTjtBRElFO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7QUNGSjtBRElJO0VBQ0Usa0JBQUE7QUNGTjtBRElNO0VBQ0UsaUJBQUE7QUNGUjtBRFFBO0VBQ0U7SUFDRSxnQkFBQTtFQ0xGO0VETUU7SUFDRSxVQUFBO0VDSko7QUFDRjtBRFFBO0VBQ0U7SUFDRSxjQUFBO0VDTkY7RURPRTtJQUNFLGdCQUFBO0lBQ0EsVUFBQTtJQUNBLGdCQUFBO0VDTEo7QUFDRjtBRFNBO0VBQ0U7SUFDRSxpQkFBQTtFQ1BGO0VEUUU7SUFDRSxpQkFBQTtFQ05KO0FBQ0Y7QURVQTtFQUNFLGdCQUFBO0FDUkY7QURTRTtFQUNFLGlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0FDUEo7QURRSTtFQUNFLGFBQUE7RUFDQSx1QkFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7QUNOTjtBRFFJO0VBQ0Usa0JBQUE7QUNOTjtBRFdBO0VBQ0UsV0FBQTtFQUNBLGFBQUE7QUNSRjtBRFNFO0VBQ0UseUJBQUE7RUFDQSxjQUFBO0VBQ0EsY0FBQTtBQ1BKO0FEU0U7RUFDRSxhQUFBO0VBQ0Esc0JBQUE7QUNQSjtBRFFJO0VBQ0Usa0JBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtBQ05OO0FET007RUFDRSxnQkFBQTtFQUNBLGtCQUFBO0FDTFI7QURPTTtFQUNFLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0EseUJBQUE7RUFDQSxzQkFBQTtFQUNBLGdCQUFBO0FDTFI7QURPTTtFQUNFLGlCQUFBO0FDTFI7QURTRTtFQUNFLGVBQUE7RUFDQSxhQUFBO0FDUEo7QURRSTtFQUNFLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7QUNOTjtBRFVFO0VBRUUsOEJBQUE7RUFDQSxjQUFBO0FDVEo7QURXSTtFQUNFLGNBQUE7QUNUTjtBRFlJO0VBQ0UsZUFBQTtFQUNBLDhCQUFBO0FDVk47QURhSTtFQUNFLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLHlCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7QUNYTjtBRGNJO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0VBQ0EsU0FBQTtFQUNBLGdCQUFBO0VBQ0EsVUFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUlBLHFCQUFBO0FDWko7QURlSTtFQUNFLFlBQUE7RUFDQSxVQUFBO0VBQ0EsYUFBQTtBQ2JOO0FEZ0JJO0VBQ0Usa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxVQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EseUJBQUE7RUFDQSxzQkFBQTtFQUNBLHFCQUFBO0VBQ0Esb0JBQUE7RUFDQSxpQkFBQTtFQUlBLHFCQUFBO0FDZE47QURpQkk7RUFJRSx3QkFBQTtBQ2ZOO0FEa0JJO0VBQ0UsYUFBQTtBQ2hCTjtBRHFCQTtFQUNFLGFBQUE7RUFDQSxzQkFBQTtBQ2xCRjtBRHFCQTtFQUVFLGdCQUFBO0FDbkJGO0FEdUJBO0VBQ0U7SUFDRSxtQkFBQTtFQ3BCRjs7RURzQkE7SUFDRSxVQUFBO0lBQ0EsY0FBQTtFQ25CRjs7RURxQkE7SUFDRSxVQUFBO0VDbEJGOztFRG9CQTtJQUNFLFNBQUE7RUNqQkY7QUFDRjtBRG9CQTtFQUNFLDBDQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EseUJBQUE7QUNsQkYiLCJmaWxlIjoic3JjL2FwcC9jb250YWN0L2NvbnRhY3QuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaGVhZGVyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBoZWlnaHQ6IDc1MHB4O1xuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsIHJnYmEoNDQsIDYyLCA4MCwgMSksIHJnYmEoNDEsIDEyOCwgMTg1LCAwLjUpKSxcbiAgICB1cmwoXCIvYXNzZXRzL2ltYWdlcy9jb250YWN0LWJnLmpwZ1wiKTtcbiAgYmFja2dyb3VuZC1hdHRhY2htZW50OiBmaXhlZDtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogNTAlIDUwJTtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcblxuICAmIHN2ZyB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJvdHRvbTogMDtcbiAgICB3aWR0aDogMTAwJTtcblxuICAgICYgcGF0aCB7XG4gICAgICBmaWxsOiAjZmZmZmZmO1xuICAgIH1cbiAgfVxuXG4gICZfX2NvbnRhaW5lciB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgbWFyZ2luOiBhdXRvO1xuICAgIGNvbG9yOiB3aGl0ZTtcblxuICAgICYgZGl2IHtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcblxuICAgICAgJiBwIHtcbiAgICAgICAgbWFyZ2luOiAyMCAwIDIwIDA7XG4gICAgICB9XG4gICAgfVxuICB9XG59XG5cbkBtZWRpYSAobWluLXdpZHRoOiA3NjhweCkgYW5kICAobWF4LXdpZHRoOiAxMzY2cHgpIHtcbiAgLmhlYWRlcl9fY29udGFpbmVyIHtcbiAgICBtYXgtd2lkdGg6IDk2MHB4O1xuICAgICYgZGl2IHtcbiAgICAgIHdpZHRoOiA1MCU7XG4gICAgfVxuICB9XG59XG5cbkBtZWRpYSAobWluLXdpZHRoOiA3NjhweCkge1xuICAuaGVhZGVyX19jb250YWluZXIge1xuICAgIG1heC13aWR0aDogODUlO1xuICAgICYgZGl2OmZpcnN0LWNoaWxkIHtcbiAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgICB3aWR0aDogODAlO1xuICAgICAgbWFyZ2luLXRvcDogNTBweDtcbiAgICB9XG4gIH1cbn1cblxuQG1lZGlhIChtaW4td2lkdGg6IDEzNjZweCkge1xuICAuaGVhZGVyX19jb250YWluZXIge1xuICAgIG1heC13aWR0aDogMTIwMHB4O1xuICAgICYgaDEge1xuICAgICAgZm9udC1zaXplOiAzLjVyZW07XG4gICAgfVxuICB9XG59XG5cbi5zZWN0aW9uIHtcbiAgcGFkZGluZzogMTAwcHggMDtcbiAgJiAuY29udGFpbmVyIHtcbiAgICBwYWRkaW5nOiAzMHB4IDBweDtcbiAgICBtYXgtd2lkdGg6IDExNDBweDtcbiAgICBtYXJnaW46IGF1dG87XG4gICAgJiAuZmxleCB7XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICB9XG4gICAgJiAuaGVhZGluZyB7XG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgfVxuICB9XG59XG5cbmZvcm0ge1xuICB3aWR0aDogMTAwJTtcbiAgZGlzcGxheTogZmxleDtcbiAgJiBoMntcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgIGZvbnQtc2l6ZTogMmVtO1xuICAgIGNvbG9yOiAjMjgzZTUxO1xuICB9XG4gICYgLmlucHV0LXdyYXBwZXIge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAmIC5mb3JtLWdyb3VwIHtcbiAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgIG1hcmdpbi1ib3R0b206IDE2cHg7XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICYgbGFiZWwge1xuICAgICAgICBmb250LXdlaWdodDogMzAwO1xuICAgICAgICBtYXJnaW4tYm90dG9tOiA4cHg7XG4gICAgICB9XG4gICAgICAmIGlucHV0LCB0ZXh0YXJlYSB7XG4gICAgICAgIGhlaWdodDogNjBweDtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogM3B4O1xuICAgICAgICBmb250LXNpemU6IDE3cHg7XG4gICAgICAgIHBhZGRpbmc6IDJlbTtcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2QzZDZlMjtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbiAgICAgICAgYm94LXNoYWRvdzogbm9uZTtcbiAgICAgIH1cbiAgICAgICYgdGV4dGFyZWEge1xuICAgICAgICBtaW4taGVpZ2h0OiAyODZweDtcbiAgICAgIH1cbiAgICB9XG4gIH1cbiAgJiAuYnRuLXdyYXBwZXIge1xuICAgIG1hcmdpbjogMmVtIDBweDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgICYgLmJ0biB7XG4gICAgICBtaW4td2lkdGg6IDI0NXB4O1xuICAgICAgZm9udC1zaXplOiAxLjVlbTtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgIG1hcmdpbjogYXV0bztcbiAgICB9XG4gIH1cblxuICAmIC5mYXEge1xuICAgIC8vIHdpZHRoOiA0MCU7XG4gICAgYm9yZGVyLWxlZnQ6IDFweCBzb2xpZCAjZTZlN2ViO1xuICAgIHBhZGRpbmc6IDAgMmVtO1xuXG4gICAgLmZhcS1jb250ZW50IHtcbiAgICAgIG1hcmdpbjogMCBhdXRvO1xuICAgIH1cbiAgICBcbiAgICAuZmFxLXF1ZXN0aW9uIHtcbiAgICAgIHBhZGRpbmc6IDIwcHggMDtcbiAgICAgIGJvcmRlci1ib3R0b206IDFweCBkb3R0ZWQgI2NjYztcbiAgICB9XG4gICAgXG4gICAgLnBhbmVsLXRpdGxlIHtcbiAgICAgIGZvbnQtc2l6ZTogMS4yNWVtO1xuICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICBtYXJnaW46IDA7XG4gICAgICBwYWRkaW5nOiAxMHB4IDEwcHggMCA0OHB4O1xuICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgfVxuICAgIFxuICAgIC5wYW5lbC1jb250ZW50IHtcbiAgICBwYWRkaW5nOiAwcHggMTRweDtcbiAgICBtYXJnaW46IDAgNDBweDtcbiAgICBoZWlnaHQ6IDA7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICB6LWluZGV4OiA1O1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBvcGFjaXR5OiAwO1xuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogLjRzIGVhc2U7XG4gICAgLW1vei10cmFuc2l0aW9uOiAuNHMgZWFzZTtcbiAgICAtby10cmFuc2l0aW9uOiAuNHMgZWFzZTtcbiAgICB0cmFuc2l0aW9uOiAuNHMgZWFzZTtcbiAgICB9XG4gICAgXG4gICAgLnBhbmVsOmNoZWNrZWQgfiAucGFuZWwtY29udGVudHtcbiAgICAgIGhlaWdodDogYXV0bztcbiAgICAgIG9wYWNpdHk6IDE7XG4gICAgICBwYWRkaW5nOiAxNHB4O1xuICAgIH1cbiAgICBcbiAgICAucGx1cyB7XG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICBtYXJnaW4tbGVmdDogMjBweDtcbiAgICAgIG1hcmdpbi10b3A6IDRweDtcbiAgICAgIHotaW5kZXg6IDU7XG4gICAgICBmb250LXNpemU6IDQycHg7XG4gICAgICBsaW5lLWhlaWdodDogMTAwJTtcbiAgICAgIC13ZWJraXQtdXNlci1zZWxlY3Q6IG5vbmU7ICAgIFxuICAgICAgLW1vei11c2VyLXNlbGVjdDogbm9uZTtcbiAgICAgIC1tcy11c2VyLXNlbGVjdDogbm9uZTtcbiAgICAgIC1vLXVzZXItc2VsZWN0OiBub25lO1xuICAgICAgdXNlci1zZWxlY3Q6IG5vbmU7XG4gICAgICAtd2Via2l0LXRyYW5zaXRpb246IC4ycyBlYXNlO1xuICAgICAgLW1vei10cmFuc2l0aW9uOiAuMnMgZWFzZTtcbiAgICAgIC1vLXRyYW5zaXRpb246IC4ycyBlYXNlO1xuICAgICAgdHJhbnNpdGlvbjogLjJzIGVhc2U7XG4gICAgfVxuICAgIFxuICAgIC5wYW5lbDpjaGVja2VkIH4gLnBsdXMge1xuICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSg0NWRlZyk7XG4gICAgICAtbW96LXRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKTtcbiAgICAgIC1vLXRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKTtcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKTtcbiAgICB9XG4gICAgXG4gICAgLnBhbmVsIHtcbiAgICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgfVxuICB9XG59XG5cbi5mb3JtLWNvbnRhaW5lciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG59XG5cbi5mb3JtLXdyYXBwZXIge1xuICAvLyB3aWR0aDogNjAlO1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAvLyBwYWRkaW5nOiAwIDJlbTtcbn1cblxuQG1lZGlhIChtaW4td2lkdGg6IDc2OHB4KSB7XG4gIC5mb3JtLWNvbnRhaW5lciB7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgfVxuICAuZm9ybS13cmFwcGVyIHtcbiAgICB3aWR0aDogNjAlO1xuICAgIHBhZGRpbmc6IDAgMmVtO1xuICB9XG4gIC5mYXEge1xuICAgIHdpZHRoOiA0MCU7XG4gIH1cbiAgZm9ybSAuYnRuLXdyYXBwZXIgLmJ0biB7XG4gICAgbWFyZ2luOiAwO1xuICB9XG59XG5cbi5jYXJkIHtcbiAgYm94LXNoYWRvdzogMCA0cHggOHB4IDAgcmdiYSgwLCAwLCAwLCAwLjIpO1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBwYWRkaW5nOiAzZW07XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XG59IiwiLmhlYWRlciB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgaGVpZ2h0OiA3NTBweDtcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjMmMzZTUwLCByZ2JhKDQxLCAxMjgsIDE4NSwgMC41KSksIHVybChcIi9hc3NldHMvaW1hZ2VzL2NvbnRhY3QtYmcuanBnXCIpO1xuICBiYWNrZ3JvdW5kLWF0dGFjaG1lbnQ6IGZpeGVkO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiA1MCUgNTAlO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xufVxuLmhlYWRlciBzdmcge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJvdHRvbTogMDtcbiAgd2lkdGg6IDEwMCU7XG59XG4uaGVhZGVyIHN2ZyBwYXRoIHtcbiAgZmlsbDogI2ZmZmZmZjtcbn1cbi5oZWFkZXJfX2NvbnRhaW5lciB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIG1hcmdpbjogYXV0bztcbiAgY29sb3I6IHdoaXRlO1xufVxuLmhlYWRlcl9fY29udGFpbmVyIGRpdiB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5oZWFkZXJfX2NvbnRhaW5lciBkaXYgcCB7XG4gIG1hcmdpbjogMjAgMCAyMCAwO1xufVxuXG5AbWVkaWEgKG1pbi13aWR0aDogNzY4cHgpIGFuZCAobWF4LXdpZHRoOiAxMzY2cHgpIHtcbiAgLmhlYWRlcl9fY29udGFpbmVyIHtcbiAgICBtYXgtd2lkdGg6IDk2MHB4O1xuICB9XG4gIC5oZWFkZXJfX2NvbnRhaW5lciBkaXYge1xuICAgIHdpZHRoOiA1MCU7XG4gIH1cbn1cbkBtZWRpYSAobWluLXdpZHRoOiA3NjhweCkge1xuICAuaGVhZGVyX19jb250YWluZXIge1xuICAgIG1heC13aWR0aDogODUlO1xuICB9XG4gIC5oZWFkZXJfX2NvbnRhaW5lciBkaXY6Zmlyc3QtY2hpbGQge1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgd2lkdGg6IDgwJTtcbiAgICBtYXJnaW4tdG9wOiA1MHB4O1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogMTM2NnB4KSB7XG4gIC5oZWFkZXJfX2NvbnRhaW5lciB7XG4gICAgbWF4LXdpZHRoOiAxMjAwcHg7XG4gIH1cbiAgLmhlYWRlcl9fY29udGFpbmVyIGgxIHtcbiAgICBmb250LXNpemU6IDMuNXJlbTtcbiAgfVxufVxuLnNlY3Rpb24ge1xuICBwYWRkaW5nOiAxMDBweCAwO1xufVxuLnNlY3Rpb24gLmNvbnRhaW5lciB7XG4gIHBhZGRpbmc6IDMwcHggMHB4O1xuICBtYXgtd2lkdGg6IDExNDBweDtcbiAgbWFyZ2luOiBhdXRvO1xufVxuLnNlY3Rpb24gLmNvbnRhaW5lciAuZmxleCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLnNlY3Rpb24gLmNvbnRhaW5lciAuaGVhZGluZyB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuZm9ybSB7XG4gIHdpZHRoOiAxMDAlO1xuICBkaXNwbGF5OiBmbGV4O1xufVxuZm9ybSBoMiB7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGZvbnQtc2l6ZTogMmVtO1xuICBjb2xvcjogIzI4M2U1MTtcbn1cbmZvcm0gLmlucHV0LXdyYXBwZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xufVxuZm9ybSAuaW5wdXQtd3JhcHBlciAuZm9ybS1ncm91cCB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbWFyZ2luLWJvdHRvbTogMTZweDtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbn1cbmZvcm0gLmlucHV0LXdyYXBwZXIgLmZvcm0tZ3JvdXAgbGFiZWwge1xuICBmb250LXdlaWdodDogMzAwO1xuICBtYXJnaW4tYm90dG9tOiA4cHg7XG59XG5mb3JtIC5pbnB1dC13cmFwcGVyIC5mb3JtLWdyb3VwIGlucHV0LCBmb3JtIC5pbnB1dC13cmFwcGVyIC5mb3JtLWdyb3VwIHRleHRhcmVhIHtcbiAgaGVpZ2h0OiA2MHB4O1xuICBib3JkZXItcmFkaXVzOiAzcHg7XG4gIGZvbnQtc2l6ZTogMTdweDtcbiAgcGFkZGluZzogMmVtO1xuICBib3JkZXI6IDFweCBzb2xpZCAjZDNkNmUyO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xuICBib3gtc2hhZG93OiBub25lO1xufVxuZm9ybSAuaW5wdXQtd3JhcHBlciAuZm9ybS1ncm91cCB0ZXh0YXJlYSB7XG4gIG1pbi1oZWlnaHQ6IDI4NnB4O1xufVxuZm9ybSAuYnRuLXdyYXBwZXIge1xuICBtYXJnaW46IDJlbSAwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG59XG5mb3JtIC5idG4td3JhcHBlciAuYnRuIHtcbiAgbWluLXdpZHRoOiAyNDVweDtcbiAgZm9udC1zaXplOiAxLjVlbTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW46IGF1dG87XG59XG5mb3JtIC5mYXEge1xuICBib3JkZXItbGVmdDogMXB4IHNvbGlkICNlNmU3ZWI7XG4gIHBhZGRpbmc6IDAgMmVtO1xufVxuZm9ybSAuZmFxIC5mYXEtY29udGVudCB7XG4gIG1hcmdpbjogMCBhdXRvO1xufVxuZm9ybSAuZmFxIC5mYXEtcXVlc3Rpb24ge1xuICBwYWRkaW5nOiAyMHB4IDA7XG4gIGJvcmRlci1ib3R0b206IDFweCBkb3R0ZWQgI2NjYztcbn1cbmZvcm0gLmZhcSAucGFuZWwtdGl0bGUge1xuICBmb250LXNpemU6IDEuMjVlbTtcbiAgd2lkdGg6IDEwMCU7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbWFyZ2luOiAwO1xuICBwYWRkaW5nOiAxMHB4IDEwcHggMCA0OHB4O1xuICBkaXNwbGF5OiBibG9jaztcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuZm9ybSAuZmFxIC5wYW5lbC1jb250ZW50IHtcbiAgcGFkZGluZzogMHB4IDE0cHg7XG4gIG1hcmdpbjogMCA0MHB4O1xuICBoZWlnaHQ6IDA7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIHotaW5kZXg6IDU7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgb3BhY2l0eTogMDtcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiAwLjRzIGVhc2U7XG4gIC1tb3otdHJhbnNpdGlvbjogMC40cyBlYXNlO1xuICAtby10cmFuc2l0aW9uOiAwLjRzIGVhc2U7XG4gIHRyYW5zaXRpb246IDAuNHMgZWFzZTtcbn1cbmZvcm0gLmZhcSAucGFuZWw6Y2hlY2tlZCB+IC5wYW5lbC1jb250ZW50IHtcbiAgaGVpZ2h0OiBhdXRvO1xuICBvcGFjaXR5OiAxO1xuICBwYWRkaW5nOiAxNHB4O1xufVxuZm9ybSAuZmFxIC5wbHVzIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBtYXJnaW4tbGVmdDogMjBweDtcbiAgbWFyZ2luLXRvcDogNHB4O1xuICB6LWluZGV4OiA1O1xuICBmb250LXNpemU6IDQycHg7XG4gIGxpbmUtaGVpZ2h0OiAxMDAlO1xuICAtd2Via2l0LXVzZXItc2VsZWN0OiBub25lO1xuICAtbW96LXVzZXItc2VsZWN0OiBub25lO1xuICAtbXMtdXNlci1zZWxlY3Q6IG5vbmU7XG4gIC1vLXVzZXItc2VsZWN0OiBub25lO1xuICB1c2VyLXNlbGVjdDogbm9uZTtcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiAwLjJzIGVhc2U7XG4gIC1tb3otdHJhbnNpdGlvbjogMC4ycyBlYXNlO1xuICAtby10cmFuc2l0aW9uOiAwLjJzIGVhc2U7XG4gIHRyYW5zaXRpb246IDAuMnMgZWFzZTtcbn1cbmZvcm0gLmZhcSAucGFuZWw6Y2hlY2tlZCB+IC5wbHVzIHtcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSg0NWRlZyk7XG4gIC1tb3otdHJhbnNmb3JtOiByb3RhdGUoNDVkZWcpO1xuICAtby10cmFuc2Zvcm06IHJvdGF0ZSg0NWRlZyk7XG4gIHRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKTtcbn1cbmZvcm0gLmZhcSAucGFuZWwge1xuICBkaXNwbGF5OiBub25lO1xufVxuXG4uZm9ybS1jb250YWluZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xufVxuXG4uZm9ybS13cmFwcGVyIHtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbn1cblxuQG1lZGlhIChtaW4td2lkdGg6IDc2OHB4KSB7XG4gIC5mb3JtLWNvbnRhaW5lciB7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgfVxuXG4gIC5mb3JtLXdyYXBwZXIge1xuICAgIHdpZHRoOiA2MCU7XG4gICAgcGFkZGluZzogMCAyZW07XG4gIH1cblxuICAuZmFxIHtcbiAgICB3aWR0aDogNDAlO1xuICB9XG5cbiAgZm9ybSAuYnRuLXdyYXBwZXIgLmJ0biB7XG4gICAgbWFyZ2luOiAwO1xuICB9XG59XG4uY2FyZCB7XG4gIGJveC1zaGFkb3c6IDAgNHB4IDhweCAwIHJnYmEoMCwgMCwgMCwgMC4yKTtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgcGFkZGluZzogM2VtO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xufSJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ContactComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-contact',
                templateUrl: './contact.component.html',
                styleUrls: ['./contact.component.scss']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _pricing_tab_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../pricing-tab.service */ "./src/app/pricing-tab.service.ts");
/* harmony import */ var _hostfront_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../hostfront-api.service */ "./src/app/hostfront-api.service.ts");
/* harmony import */ var _shared_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../shared/navbar/navbar.component */ "./src/app/shared/navbar/navbar.component.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _shared_scroll_scroll_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../shared/scroll/scroll.component */ "./src/app/shared/scroll/scroll.component.ts");
/* harmony import */ var _shared_footer_footer_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../shared/footer/footer.component */ "./src/app/shared/footer/footer.component.ts");









function HomeComponent_div_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceSVG"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceHTML"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "span", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "span", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r0.pagePostData.hero_module_header);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r0.pagePostData.hero_module_excerpt);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r0.pagePostData.hero_module_left_cta);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r0.pagePostData.hero_module_right_cta);
} }
function HomeComponent_div_11_div_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h3");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const icon_card_r7 = ctx.$implicit;
    const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate2"]("src", "", ctx_r6.pagePostData.baseUrl, "", icon_card_r7.icon.url, "", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](icon_card_r7.card_header);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](icon_card_r7.card_text);
} }
function HomeComponent_div_11_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, HomeComponent_div_11_div_1_Template, 8, 4, "div", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r1.pagePostData.icon_card);
} }
function HomeComponent_div_18_div_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "img", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h3", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "p", 31);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "p", 32);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "span", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "span", 34);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "a", 35);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const price_card_r9 = ctx.$implicit;
    const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate2"]("src", "", ctx_r8.pagePostData.baseUrl, "", price_card_r9.icon.url, "", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](price_card_r9.card_header);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](price_card_r9.card_text);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](price_card_r9.price);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](price_card_r9.cta);
} }
function HomeComponent_div_18_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, HomeComponent_div_18_div_1_Template, 15, 6, "div", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r2.pagePostData.price_card);
} }
function HomeComponent_section_19_div_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 37);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "h2");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "span", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "img", 39);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const card_r11 = ctx.$implicit;
    const ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](card_r11.card_header);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](card_r11.card_text);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](card_r11.cta);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate2"]("src", "", ctx_r10.pagePostData.baseUrl, "", card_r11.icon.url, "", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
} }
function HomeComponent_section_19_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "section", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "h2");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "More Solutions");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, HomeComponent_section_19_div_5_Template, 11, 5, "div", 36);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r3.pagePostData.full_width_card);
} }
function HomeComponent_div_23_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 40);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r4.pagePostData.footer_header_text);
} }
function HomeComponent_div_25_div_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 42);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "img", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h3");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "span", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const card_r13 = ctx.$implicit;
    const ctx_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate2"]("src", "", ctx_r12.pagePostData.baseUrl, "", card_r13.icon.url, "", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](card_r13.card_header);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](card_r13.card_text);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](card_r13.cta);
} }
function HomeComponent_div_25_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, HomeComponent_div_25_div_1_Template, 11, 5, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r5.pagePostData.icon_card_with_cta);
} }
class HomeComponent {
    constructor(_pricingTabService, _http) {
        this._pricingTabService = _pricingTabService;
        this._http = _http;
        this.pagePostData = {
            baseUrl: `${_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].baseurl}`,
            ID: '',
            post_title: '',
            slug: '',
            post_featured_image_url: '',
            post_featured_image_alt_tag: '',
            hero_module_header: '',
            hero_module_excerpt: '',
            hero_module_left_cta: '',
            hero_module_right_cta: '',
            icon_card: '',
            price_card: '',
            full_width_card: '',
            footer_header_text: '',
            footer_image: '',
            icon_card_with_cta: '',
            homeData: function (data) {
                this.ID = data[0].ID;
                this.post_title = data[0].post_title;
                this.slug = data[0].post_name;
                this.post_featured_image_url = data[0].post_featured_image_url;
                this.post_featured_image_alt_tag = data[0].post_featured_image_alt_tag;
                this.hero_module_header = data[1].api_res_hero_module.hero_header;
                this.hero_module_excerpt = data[1].api_res_hero_module.hero_excerpt;
                this.hero_module_left_cta = data[1].api_res_hero_module.hero_double_button_cta[0].hero_cta_left_side_button;
                this.hero_module_right_cta = data[1].api_res_hero_module.hero_double_button_cta[0].hero_cta_right_side_button;
                this.icon_card = data[1].api_res_icon_card;
                this.price_card = data[1].api_res_price_card;
                this.full_width_card = data[1].api_res_full_width_card;
                this.footer_header_text = data[1].api_res_footer_hero_module.footer_hero_text;
                this.footer_image = data[1].api_res_footer_hero_module.footer_image;
                this.icon_card_with_cta = data[1].api_res_icon_card_with_cta;
            }
        };
    }
    yearlyTab() {
        this._pricingTabService.yearlyClick();
    }
    monthlyTab() {
        this._pricingTabService.monthlyClick();
    }
    ngOnInit() {
        this._http.getHome().subscribe(data => {
            // Object Method & Store Data
            this.pagePostData.homeData(data);
        });
    }
}
HomeComponent.ɵfac = function HomeComponent_Factory(t) { return new (t || HomeComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_pricing_tab_service__WEBPACK_IMPORTED_MODULE_2__["PricingTabService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_hostfront_api_service__WEBPACK_IMPORTED_MODULE_3__["HostfrontApiService"])); };
HomeComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: HomeComponent, selectors: [["app-home"]], decls: 28, vars: 6, consts: [[1, "header"], ["id", "curve", "data-name", "Layer 1", "xmlns", "http://www.w3.org/2000/svg", "viewBox", "0 0 1416.99 174.01"], ["d", "M0,280.8S283.66,59,608.94,163.56s437.93,150.57,808,10.34V309.54H0V280.8Z", "transform", "translate(0 -135.53)", 1, "cls-1"], ["class", "header__container", 4, "ngIf"], [1, "section"], [1, "container"], [1, "flex", "heading"], ["class", "flex", "data-aos", "fade-up", 4, "ngIf"], [1, "section", "color"], ["id", "first-tab", 1, "container"], ["class", "flex", 4, "ngIf"], ["class", "section", 4, "ngIf"], [1, "section", "color--bg"], [1, "flex"], ["class", "child", 4, "ngIf"], [1, "section", "section--feature"], ["class", "container", 4, "ngIf"], [1, "header__container"], [1, "btn-wrapper"], [1, "btn", "btn--std", "btn--std01"], [1, "btn", "btn--std", "btn--std02"], ["data-aos", "fade-up", 1, "flex"], ["class", "column", 4, "ngFor", "ngForOf"], [1, "column"], [1, "column__icon"], [3, "src"], [1, "column__body"], [1, "card"], [1, "card__icon"], [1, "card__body"], [1, "card__title"], [1, "card__text"], [1, "card__price"], [1, "btn", "btn--ghost", "btn--ghost--01"], [1, "btn__slide", "btn__slide--01"], ["href", "#"], ["class", "container", 4, "ngFor", "ngForOf"], ["data-aos", "fade-left", 1, "flex"], [1, "cta", "cta--left"], [1, "cta__img", 3, "src"], [1, "child"], ["class", "flex", 4, "ngFor", "ngForOf"], ["data-aos", "fade-right", 1, "feature-group"], [1, "feature-group__img"], [1, "feature-group__body"]], template: function HomeComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "header", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "app-navbar");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceSVG"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "svg", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "path", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, HomeComponent_div_4_Template, 11, 4, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceHTML"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "section", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "Why use our services?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](11, HomeComponent_div_11_Template, 2, 1, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "section", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "Our Plans");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](18, HomeComponent_div_18_Template, 2, 1, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](19, HomeComponent_section_19_Template, 6, 1, "section", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "section", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](23, HomeComponent_div_23_Template, 3, 1, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "section", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](25, HomeComponent_div_25_Template, 2, 1, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "app-scroll");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](27, "app-footer");
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.pagePostData);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.pagePostData.icon_card);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.pagePostData.price_card);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.pagePostData.full_width_card);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.pagePostData.footer_header_text);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.pagePostData.icon_card_with_cta);
    } }, directives: [_shared_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_4__["NavbarComponent"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgIf"], _shared_scroll_scroll_component__WEBPACK_IMPORTED_MODULE_6__["ScrollComponent"], _shared_footer_footer_component__WEBPACK_IMPORTED_MODULE_7__["FooterComponent"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgForOf"]], styles: [".header[_ngcontent-%COMP%] {\n  position: relative;\n  height: 750px;\n  background-image: linear-gradient(to right, #2c3e50, rgba(41, 128, 185, 0.5)), url(\"/assets/images/bg.jpg\");\n  background-attachment: fixed;\n  background-position: center top;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n.header[_ngcontent-%COMP%]   svg[_ngcontent-%COMP%] {\n  position: absolute;\n  bottom: 0;\n  width: 100%;\n}\n.header[_ngcontent-%COMP%]   svg[_ngcontent-%COMP%]   path[_ngcontent-%COMP%] {\n  fill: #ffffff;\n}\n.header__container[_ngcontent-%COMP%] {\n  width: 100%;\n  height: 100%;\n  display: flex;\n  margin: auto;\n  color: white;\n}\n.header__container[_ngcontent-%COMP%]   div[_ngcontent-%COMP%] {\n  text-align: center;\n}\n.header__container[_ngcontent-%COMP%]   div[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n  margin: 20 0 20 0;\n}\nsection.color[_ngcontent-%COMP%] {\n  background-color: #f8fafe;\n}\nsection.color--gradient[_ngcontent-%COMP%] {\n  background: linear-gradient(to right, #2980b9, #2c3e50);\n}\nsection.color--bg[_ngcontent-%COMP%] {\n  background-image: linear-gradient(to right, #2980b9, rgba(44, 62, 80, 0.5)), url(\"/assets/images/bg_02.jpg\");\n  background-attachment: fixed;\n  background-position: center top;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\nsection--feature[_ngcontent-%COMP%] {\n  padding: 0;\n}\nsection--feature[_ngcontent-%COMP%]   .feature-group[_ngcontent-%COMP%] {\n  padding: 3em;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%] {\n  padding: 30px 0px;\n  max-width: 1140px;\n  margin: auto;\n  overflow: hidden;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: center;\n  flex-direction: column;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .heading[_ngcontent-%COMP%] {\n  text-align: center;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  text-transform: uppercase;\n  font-size: 2em;\n  color: #283e51;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .tab[_ngcontent-%COMP%] {\n  display: flex;\n  flex-wrap: wrap;\n  position: relative;\n  box-shadow: 0px 20px 45px 0px rgba(0, 0, 0, 0.08);\n  border-radius: 1.61765rem;\n  max-width: 260px;\n  margin: 0 auto;\n  background-color: #ffffff;\n  justify-content: center;\n  padding: 5px 0;\n  z-index: 1;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .tab[_ngcontent-%COMP%]   li[_ngcontent-%COMP%] {\n  display: inline-block;\n  margin: 0;\n  padding: 0;\n  list-style-type: none;\n  -webkit-tap-highlight-color: transparent;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .tab[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  border-radius: 1.35294rem;\n  display: block;\n  color: #ffffff;\n  min-width: 125px;\n  padding: 8px 31px;\n  text-align: center;\n  text-decoration: none;\n  cursor: pointer;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .tab[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   #yearly-tab[_ngcontent-%COMP%] {\n  color: #2c3e50;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .tab[_ngcontent-%COMP%]   li#indicator[_ngcontent-%COMP%] {\n  width: 125px;\n  background-color: #1868dd;\n  position: absolute;\n  height: calc(100% - 10px);\n  transform: translateY(-50%);\n  top: 50%;\n  border-radius: 1.35294rem;\n  z-index: -1;\n  left: 5px;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .cta[_ngcontent-%COMP%] {\n  text-align: center;\n  padding: 2em 0em;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .cta[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n  margin-bottom: 3em;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .cta__img[_ngcontent-%COMP%] {\n  width: 50%;\n  margin: 0 auto;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%] {\n  padding: 20px;\n  flex: 1;\n  line-height: 1.5;\n  color: #2980b9;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .column__icon[_ngcontent-%COMP%] {\n  height: 244px;\n  width: 244px;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .column__icon[_ngcontent-%COMP%], section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .card__icon[_ngcontent-%COMP%] {\n  position: relative;\n  margin: auto;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .column__icon[_ngcontent-%COMP%]   img[_ngcontent-%COMP%], section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .card__icon[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  max-height: 100%;\n  max-width: 100%;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .card__icon[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  max-height: 50%;\n  max-width: 50%;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column__body[_ngcontent-%COMP%] {\n  text-align: center;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column__body[_ngcontent-%COMP%]   h3[_ngcontent-%COMP%] {\n  font-size: 1.5em;\n  color: #283e51;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%] {\n  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);\n  border-radius: 10px;\n  padding: 16px;\n  text-align: center;\n  background-color: #ffffff;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .card__body[_ngcontent-%COMP%] {\n  text-align: center;\n  padding: 2em;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .card__title[_ngcontent-%COMP%] {\n  font-size: 1.5em;\n  color: #283e51;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .card__text[_ngcontent-%COMP%] {\n  line-height: 1.5;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .card__price[_ngcontent-%COMP%] {\n  font-family: \"Arial Black\";\n  font-size: 3em;\n  background: linear-gradient(#2c3e50, #2980b9);\n  -webkit-background-clip: text;\n  -webkit-text-fill-color: transparent;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .active[_ngcontent-%COMP%] {\n  background-image: linear-gradient(#2c3e50, #2980b9);\n  padding: 50px 16px;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .active[_ngcontent-%COMP%]   .card__title[_ngcontent-%COMP%], section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .active[_ngcontent-%COMP%]   .card__body[_ngcontent-%COMP%], section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .active[_ngcontent-%COMP%]   .card__price[_ngcontent-%COMP%] {\n  color: #ffffff;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .active[_ngcontent-%COMP%]   .card__price[_ngcontent-%COMP%] {\n  -webkit-text-fill-color: initial;\n}\nsection[_ngcontent-%COMP%]   #second-tab[_ngcontent-%COMP%] {\n  display: none;\n  opacity: 0;\n}\nsection[_ngcontent-%COMP%]   .child[_ngcontent-%COMP%] {\n  text-align: center;\n}\nsection[_ngcontent-%COMP%]   .child[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n  text-transform: uppercase;\n  color: white;\n}\nsection--feature[_ngcontent-%COMP%] {\n  padding: 0px;\n}\n.section--feature[_ngcontent-%COMP%] {\n  padding: 0px;\n}\n.section[_ngcontent-%COMP%]   .feature-group[_ngcontent-%COMP%] {\n  margin: 1.25em;\n  padding: 3em;\n  background-color: #f8fafe;\n  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);\n  border-radius: 10px;\n  color: #2980b9;\n  display: flex;\n  flex: 1;\n  flex-direction: column;\n  text-align: center;\n  align-items: center;\n}\n.section[_ngcontent-%COMP%]   .feature-group__img[_ngcontent-%COMP%] {\n  display: flex;\n  min-width: 138px;\n  margin-right: 20px;\n}\n.section[_ngcontent-%COMP%]   .feature-group__body[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%] {\n  display: inline-block;\n}\n.section[_ngcontent-%COMP%]   .feature-group[_ngcontent-%COMP%]   h3[_ngcontent-%COMP%] {\n  font-size: 1.25em;\n  margin: 0;\n}\n@media (min-width: 768px) and (max-width: 1024px) {\n  section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .card__body[_ngcontent-%COMP%] {\n    padding: 2em 0;\n  }\n}\n@media (min-width: 768px) {\n  .header__container[_ngcontent-%COMP%] {\n    max-width: 85%;\n  }\n  .header__container[_ngcontent-%COMP%]   div[_ngcontent-%COMP%] {\n    width: 75%;\n  }\n  .header__container[_ngcontent-%COMP%]   .cta--right[_ngcontent-%COMP%] {\n    text-align: right;\n  }\n\n  .header__container[_ngcontent-%COMP%]   div[_ngcontent-%COMP%]:first-child {\n    text-align: left;\n  }\n  .header__container[_ngcontent-%COMP%]   div[_ngcontent-%COMP%]:first-child   h1[_ngcontent-%COMP%] {\n    font-size: 3.5rem;\n  }\n  .header__container[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n    margin-bottom: 3em;\n  }\n  .header__container[_ngcontent-%COMP%]   .btn-wrapper[_ngcontent-%COMP%] {\n    text-align: left;\n    width: 70%;\n    display: flex;\n    justify-content: space-between;\n  }\n  .header__container[_ngcontent-%COMP%]   .btn-wrapper[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%] {\n    text-transform: uppercase;\n    font-weight: bold;\n    padding: 20px 30px;\n    min-width: 200px;\n    text-align: center;\n    margin-right: 20px;\n  }\n  .header[_ngcontent-%COMP%]   div[_ngcontent-%COMP%]:nth-child(2) {\n    width: 50%;\n  }\n  .header[_ngcontent-%COMP%]   div[_ngcontent-%COMP%]:nth-child(2)   img[_ngcontent-%COMP%] {\n    width: 75%;\n  }\n\n  section[_ngcontent-%COMP%] {\n    padding: 100px 0;\n  }\n  section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%] {\n    flex-direction: row;\n  }\n  section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .child[_ngcontent-%COMP%] {\n    display: flex;\n  }\n  section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .child[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n    font-size: 2.5em;\n  }\n  section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .child[_ngcontent-%COMP%]:first-child {\n    align-items: flex-start;\n  }\n  section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .child[_ngcontent-%COMP%]   .child[_ngcontent-%COMP%]:nth-child(2) {\n    align-items: flex-end;\n    width: 50%;\n  }\n  section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .cta[_ngcontent-%COMP%] {\n    width: 50%;\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n    padding: 2em;\n  }\n  section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .cta--right[_ngcontent-%COMP%] {\n    text-align: right;\n  }\n  section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .cta--left[_ngcontent-%COMP%] {\n    text-align: left;\n  }\n  section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .cta__img[_ngcontent-%COMP%] {\n    width: 40%;\n  }\n  section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .feature-group[_ngcontent-%COMP%] {\n    flex-direction: row;\n    text-align: left;\n  }\n\n  .section--feature[_ngcontent-%COMP%] {\n    position: relative;\n    top: -150px;\n  }\n}\n@media (min-width: 1300px) {\n  .header__container[_ngcontent-%COMP%] {\n    max-width: 1200px;\n  }\n}\n\n@media only screen and (min-width: 1300px) {\n  .header__container[_ngcontent-%COMP%] {\n    max-width: 85%;\n  }\n  .header__container[_ngcontent-%COMP%]   div[_ngcontent-%COMP%] {\n    width: 50%;\n  }\n  .header__container[_ngcontent-%COMP%]   div[_ngcontent-%COMP%]   .btn-wrapper[_ngcontent-%COMP%] {\n    width: 50%;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2NoaWRvL3Byb2plY3RzL2hvc3Rmcm9udC1wcm9qZWN0L2hvc3Rmcm9udC1mcm9udGVuZC9zcmMvYXBwL2hvbWUvaG9tZS5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvaG9tZS9ob21lLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usa0JBQUE7RUFDQSxhQUFBO0VBQ0EsMkdBQUE7RUFFQSw0QkFBQTtFQUNBLCtCQUFBO0VBQ0EsNEJBQUE7RUFDQSxzQkFBQTtBQ0FGO0FESUU7RUFDRSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxXQUFBO0FDRko7QURJSTtFQUNFLGFBQUE7QUNGTjtBRE1FO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7QUNKSjtBRE1JO0VBQ0Usa0JBQUE7QUNKTjtBRE1NO0VBQ0UsaUJBQUE7QUNKUjtBRFdFO0VBQ0UseUJBQUE7QUNSSjtBRFVJO0VBQ0UsdURBQUE7QUNSTjtBRFdJO0VBQ0UsNEdBQUE7RUFFQSw0QkFBQTtFQUNBLCtCQUFBO0VBQ0EsNEJBQUE7RUFDQSxzQkFBQTtBQ1ZOO0FEY0U7RUFDRSxVQUFBO0FDWko7QURhSTtFQUNFLFlBQUE7QUNYTjtBRGNFO0VBQ0UsaUJBQUE7RUFDQSxpQkFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtBQ1pKO0FEY0k7RUFDRSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxzQkFBQTtBQ1pOO0FEZUk7RUFDRSxrQkFBQTtBQ2JOO0FEZ0JJO0VBQ0UseUJBQUE7RUFDQSxjQUFBO0VBQ0EsY0FBQTtBQ2ROO0FEaUJJO0VBQ0UsYUFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGlEQUFBO0VBQ0EseUJBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSx5QkFBQTtFQUNBLHVCQUFBO0VBQ0EsY0FBQTtFQUNBLFVBQUE7QUNmTjtBRGlCTTtFQUNFLHFCQUFBO0VBQ0EsU0FBQTtFQUNBLFVBQUE7RUFDQSxxQkFBQTtFQUNBLHdDQUFBO0FDZlI7QURpQlE7RUFDRSx5QkFBQTtFQUNBLGNBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EscUJBQUE7RUFDQSxlQUFBO0FDZlY7QURrQlE7RUFDRSxjQUFBO0FDaEJWO0FEbUJRO0VBQ0UsWUFBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSx5QkFBQTtFQUNBLDJCQUFBO0VBQ0EsUUFBQTtFQUNBLHlCQUFBO0VBQ0EsV0FBQTtFQUNBLFNBQUE7QUNqQlY7QURzQkk7RUFDRSxrQkFBQTtFQUNBLGdCQUFBO0FDcEJOO0FEc0JNO0VBQ0Usa0JBQUE7QUNwQlI7QUR1Qk07RUFDRSxVQUFBO0VBQ0EsY0FBQTtBQ3JCUjtBRHlCSTtFQUNFLGFBQUE7RUFDQSxPQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0FDdkJOO0FEeUJNO0VBQ0UsYUFBQTtFQUNBLFlBQUE7QUN2QlI7QUQwQk07O0VBRUUsa0JBQUE7RUFDQSxZQUFBO0FDeEJSO0FEMEJROztFQUNFLGdCQUFBO0VBQ0EsZUFBQTtBQ3ZCVjtBRDRCUTtFQUNFLGVBQUE7RUFDQSxjQUFBO0FDMUJWO0FEOEJNO0VBQ0Usa0JBQUE7QUM1QlI7QUQ4QlE7RUFDRSxnQkFBQTtFQUNBLGNBQUE7QUM1QlY7QURnQ007RUFDRSwwQ0FBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7QUM5QlI7QURnQ1E7RUFDRSxrQkFBQTtFQUNBLFlBQUE7QUM5QlY7QURpQ1E7RUFDRSxnQkFBQTtFQUNBLGNBQUE7QUMvQlY7QURrQ1E7RUFDRSxnQkFBQTtBQ2hDVjtBRG1DUTtFQUNFLDBCQUFBO0VBQ0EsY0FBQTtFQUNBLDZDQUFBO0VBQ0EsNkJBQUE7RUFDQSxvQ0FBQTtBQ2pDVjtBRHFDTTtFQUNFLG1EQUFBO0VBQ0Esa0JBQUE7QUNuQ1I7QURvQ1E7OztFQUdFLGNBQUE7QUNsQ1Y7QURvQ1E7RUFDRSxnQ0FBQTtBQ2xDVjtBRHdDRTtFQUNFLGFBQUE7RUFDQSxVQUFBO0FDdENKO0FEeUNFO0VBQ0Usa0JBQUE7QUN2Q0o7QUR3Q0k7RUFDRSx5QkFBQTtFQUNBLFlBQUE7QUN0Q047QUQwQ0U7RUFDRSxZQUFBO0FDeENKO0FENkNFO0VBQ0UsWUFBQTtBQzFDSjtBRDRDRTtFQUNFLGNBQUE7RUFDQSxZQUFBO0VBQ0EseUJBQUE7RUFDQSwwQ0FBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLGFBQUE7RUFDQSxPQUFBO0VBQ0Esc0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0FDMUNKO0FEMkNJO0VBQ0UsYUFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUN6Q047QUQ0Q007RUFDRSxxQkFBQTtBQzFDUjtBRDZDSTtFQUNFLGlCQUFBO0VBQ0EsU0FBQTtBQzNDTjtBRGdEQTtFQUNFO0lBQ0UsY0FBQTtFQzdDRjtBQUNGO0FEZ0RBO0VBQ0U7SUFDRSxjQUFBO0VDOUNGO0VEZ0RFO0lBQ0UsVUFBQTtFQzlDSjtFRGtESTtJQUNFLGlCQUFBO0VDaEROOztFRHVESTtJQUNFLGdCQUFBO0VDcEROO0VEc0RNO0lBQ0UsaUJBQUE7RUNwRFI7RUR3REk7SUFDRSxrQkFBQTtFQ3RETjtFRHlESTtJQUNFLGdCQUFBO0lBQ0EsVUFBQTtJQUNBLGFBQUE7SUFDQSw4QkFBQTtFQ3ZETjtFRHlETTtJQUNFLHlCQUFBO0lBQ0EsaUJBQUE7SUFDQSxrQkFBQTtJQUNBLGdCQUFBO0lBQ0Esa0JBQUE7SUFDQSxrQkFBQTtFQ3ZEUjtFRDJERTtJQUNFLFVBQUE7RUN6REo7RUQwREk7SUFDRSxVQUFBO0VDeEROOztFRDZEQTtJQUNFLGdCQUFBO0VDMURGO0VENkRJO0lBQ0UsbUJBQUE7RUMzRE47RUQ4REk7SUFDRSxhQUFBO0VDNUROO0VENkRNO0lBQ0UsZ0JBQUE7RUMzRFI7RUQ4RE07SUFDRSx1QkFBQTtFQzVEUjtFRCtETTtJQUNFLHFCQUFBO0lBQ0EsVUFBQTtFQzdEUjtFRGlFSTtJQUNFLFVBQUE7SUFDQSxhQUFBO0lBQ0Esc0JBQUE7SUFDQSx1QkFBQTtJQUNBLFlBQUE7RUMvRE47RURpRU07SUFDRSxpQkFBQTtFQy9EUjtFRGlFTTtJQUNFLGdCQUFBO0VDL0RSO0VEa0VNO0lBQ0UsVUFBQTtFQ2hFUjtFRG1FSTtJQUNFLG1CQUFBO0lBQ0EsZ0JBQUE7RUNqRU47O0VEc0VBO0lBQ0Usa0JBQUE7SUFDQSxXQUFBO0VDbkVGO0FBQ0Y7QURzRUE7RUFDRTtJQUNFLGlCQUFBO0VDcEVGO0FBQ0Y7QUR3RUEsb0VBQUE7QUFDQTtFQUNFO0lBQ0UsY0FBQTtFQ3RFRjtFRHVFRTtJQUNFLFVBQUE7RUNyRUo7RURzRUk7SUFDRSxVQUFBO0VDcEVOO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9ob21lL2hvbWUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaGVhZGVyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBoZWlnaHQ6IDc1MHB4O1xuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsIHJnYmEoNDQsIDYyLCA4MCwgMSksIHJnYmEoNDEsIDEyOCwgMTg1LCAwLjUpKSxcbiAgICB1cmwoXCIvYXNzZXRzL2ltYWdlcy9iZy5qcGdcIik7XG4gIGJhY2tncm91bmQtYXR0YWNobWVudDogZml4ZWQ7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlciB0b3A7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG5cbiAgLy8gIzI5ODBiOSAjMmMzZTUwXG5cbiAgJiBzdmcge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBib3R0b206IDA7XG4gICAgd2lkdGg6IDEwMCU7XG5cbiAgICAmIHBhdGgge1xuICAgICAgZmlsbDogI2ZmZmZmZjtcbiAgICB9XG4gIH1cblxuICAmX19jb250YWluZXIge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIG1hcmdpbjogYXV0bztcbiAgICBjb2xvcjogd2hpdGU7XG5cbiAgICAmIGRpdiB7XG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG5cbiAgICAgICYgcCB7XG4gICAgICAgIG1hcmdpbjogMjAgMCAyMCAwO1xuICAgICAgfVxuICAgIH1cbiAgfVxufVxuXG5zZWN0aW9uIHtcbiAgJi5jb2xvciB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2Y4ZmFmZTtcblxuICAgICYtLWdyYWRpZW50IHtcbiAgICAgIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgIzI5ODBiOSwgIzJjM2U1MCk7XG4gICAgfVxuXG4gICAgJi0tYmcge1xuICAgICAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCByZ2JhKDQxLCAxMjgsIDE4NSwgMSksIHJnYmEoNDQsIDYyLCA4MCwgMC41KSksXG4gICAgICAgIHVybChcIi9hc3NldHMvaW1hZ2VzL2JnXzAyLmpwZ1wiKTtcbiAgICAgIGJhY2tncm91bmQtYXR0YWNobWVudDogZml4ZWQ7XG4gICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXIgdG9wO1xuICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgfVxuICB9XG5cbiAgJi0tZmVhdHVyZSB7XG4gICAgcGFkZGluZzogMDtcbiAgICAmIC5mZWF0dXJlLWdyb3VwIHtcbiAgICAgIHBhZGRpbmc6IDNlbTtcbiAgICB9XG4gIH1cbiAgJiAuY29udGFpbmVyIHtcbiAgICBwYWRkaW5nOiAzMHB4IDBweDtcbiAgICBtYXgtd2lkdGg6IDExNDBweDtcbiAgICBtYXJnaW46IGF1dG87XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcblxuICAgICYgLmZsZXgge1xuICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICB9XG5cbiAgICAmIC5oZWFkaW5nIHtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB9XG5cbiAgICAmIGgyIHtcbiAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgICBmb250LXNpemU6IDJlbTtcbiAgICAgIGNvbG9yOiAjMjgzZTUxO1xuICAgIH1cblxuICAgICYgLnRhYiB7XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgZmxleC13cmFwOiB3cmFwO1xuICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgYm94LXNoYWRvdzogMHB4IDIwcHggNDVweCAwcHggcmdiYSgwLCAwLCAwLCAwLjA4KTtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDEuNjE3NjVyZW07XG4gICAgICBtYXgtd2lkdGg6IDI2MHB4O1xuICAgICAgbWFyZ2luOiAwIGF1dG87XG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICBwYWRkaW5nOiA1cHggMDtcbiAgICAgIHotaW5kZXg6IDE7XG5cbiAgICAgICYgbGkge1xuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgICAgIG1hcmdpbjogMDtcbiAgICAgICAgcGFkZGluZzogMDtcbiAgICAgICAgbGlzdC1zdHlsZS10eXBlOiBub25lO1xuICAgICAgICAtd2Via2l0LXRhcC1oaWdobGlnaHQtY29sb3I6IHRyYW5zcGFyZW50O1xuXG4gICAgICAgICYgYSB7XG4gICAgICAgICAgYm9yZGVyLXJhZGl1czogMS4zNTI5NHJlbTtcbiAgICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgICBjb2xvcjogI2ZmZmZmZjtcbiAgICAgICAgICBtaW4td2lkdGg6IDEyNXB4O1xuICAgICAgICAgIHBhZGRpbmc6IDhweCAzMXB4O1xuICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gICAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xuICAgICAgICB9XG5cbiAgICAgICAgJiAjeWVhcmx5LXRhYiB7XG4gICAgICAgICAgY29sb3I6ICMyYzNlNTA7XG4gICAgICAgIH1cblxuICAgICAgICAmI2luZGljYXRvciB7XG4gICAgICAgICAgd2lkdGg6IDEyNXB4O1xuICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICMxODY4ZGQ7XG4gICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICAgIGhlaWdodDogY2FsYygxMDAlIC0gMTBweCk7XG4gICAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xuICAgICAgICAgIHRvcDogNTAlO1xuICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDEuMzUyOTRyZW07XG4gICAgICAgICAgei1pbmRleDogLTE7XG4gICAgICAgICAgbGVmdDogNXB4O1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgJiAuY3RhIHtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgIHBhZGRpbmc6IDJlbSAwZW07XG5cbiAgICAgICYgcCB7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDNlbTtcbiAgICAgIH1cblxuICAgICAgJl9faW1nIHtcbiAgICAgICAgd2lkdGg6IDUwJTtcbiAgICAgICAgbWFyZ2luOiAwIGF1dG87XG4gICAgICB9XG4gICAgfVxuXG4gICAgJiAuY29sdW1uIHtcbiAgICAgIHBhZGRpbmc6IDIwcHg7XG4gICAgICBmbGV4OiAxO1xuICAgICAgbGluZS1oZWlnaHQ6IDEuNTtcbiAgICAgIGNvbG9yOiAjMjk4MGI5O1xuXG4gICAgICAuY29sdW1uX19pY29uIHtcbiAgICAgICAgaGVpZ2h0OiAyNDRweDtcbiAgICAgICAgd2lkdGg6IDI0NHB4O1xuICAgICAgfVxuXG4gICAgICAuY29sdW1uX19pY29uLFxuICAgICAgLmNhcmRfX2ljb24ge1xuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgIG1hcmdpbjogYXV0bztcblxuICAgICAgICAmIGltZyB7XG4gICAgICAgICAgbWF4LWhlaWdodDogMTAwJTtcbiAgICAgICAgICBtYXgtd2lkdGg6IDEwMCU7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgLmNhcmRfX2ljb24ge1xuICAgICAgICAmIGltZyB7XG4gICAgICAgICAgbWF4LWhlaWdodDogNTAlO1xuICAgICAgICAgIG1heC13aWR0aDogNTAlO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgICZfX2JvZHkge1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG5cbiAgICAgICAgJiBoMyB7XG4gICAgICAgICAgZm9udC1zaXplOiAxLjVlbTtcbiAgICAgICAgICBjb2xvcjogIzI4M2U1MTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICAmIC5jYXJkIHtcbiAgICAgICAgYm94LXNoYWRvdzogMCA0cHggOHB4IDAgcmdiYSgwLCAwLCAwLCAwLjIpO1xuICAgICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgICAgICBwYWRkaW5nOiAxNnB4O1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XG5cbiAgICAgICAgJl9fYm9keSB7XG4gICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICAgIHBhZGRpbmc6IDJlbTtcbiAgICAgICAgfVxuXG4gICAgICAgICZfX3RpdGxlIHtcbiAgICAgICAgICBmb250LXNpemU6IDEuNWVtO1xuICAgICAgICAgIGNvbG9yOiAjMjgzZTUxO1xuICAgICAgICB9XG5cbiAgICAgICAgJl9fdGV4dCB7XG4gICAgICAgICAgbGluZS1oZWlnaHQ6IDEuNTtcbiAgICAgICAgfVxuXG4gICAgICAgICZfX3ByaWNlIHtcbiAgICAgICAgICBmb250LWZhbWlseTogXCJBcmlhbCBCbGFja1wiO1xuICAgICAgICAgIGZvbnQtc2l6ZTogM2VtO1xuICAgICAgICAgIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgjMmMzZTUwLCAjMjk4MGI5KTtcbiAgICAgICAgICAtd2Via2l0LWJhY2tncm91bmQtY2xpcDogdGV4dDtcbiAgICAgICAgICAtd2Via2l0LXRleHQtZmlsbC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgJiAuYWN0aXZlIHtcbiAgICAgICAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KCMyYzNlNTAsICMyOTgwYjkpO1xuICAgICAgICBwYWRkaW5nOiA1MHB4IDE2cHg7XG4gICAgICAgICYgLmNhcmRfX3RpdGxlLFxuICAgICAgICAuY2FyZF9fYm9keSxcbiAgICAgICAgLmNhcmRfX3ByaWNlIHtcbiAgICAgICAgICBjb2xvcjogI2ZmZmZmZjtcbiAgICAgICAgfVxuICAgICAgICAmIC5jYXJkX19wcmljZSB7XG4gICAgICAgICAgLXdlYmtpdC10ZXh0LWZpbGwtY29sb3I6IGluaXRpYWw7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICAmICNzZWNvbmQtdGFiIHtcbiAgICBkaXNwbGF5OiBub25lO1xuICAgIG9wYWNpdHk6IDA7XG4gIH1cblxuICAuY2hpbGQge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAmIGgxIHtcbiAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgICBjb2xvcjogd2hpdGU7XG4gICAgfVxuICB9XG5cbiAgJi0tZmVhdHVyZSB7XG4gICAgcGFkZGluZzogMHB4O1xuICB9XG59XG5cbi5zZWN0aW9uIHtcbiAgJi0tZmVhdHVyZSB7XG4gICAgcGFkZGluZzogMHB4O1xuICB9XG4gICYgLmZlYXR1cmUtZ3JvdXAge1xuICAgIG1hcmdpbjogMS4yNWVtO1xuICAgIHBhZGRpbmc6IDNlbTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjhmYWZlO1xuICAgIGJveC1zaGFkb3c6IDAgNHB4IDhweCAwIHJnYmEoMCwgMCwgMCwgMC4yKTtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgIGNvbG9yOiAjMjk4MGI5O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleDogMTtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICZfX2ltZyB7XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgbWluLXdpZHRoOiAxMzhweDtcbiAgICAgIG1hcmdpbi1yaWdodDogMjBweDtcbiAgICB9XG4gICAgJl9fYm9keSB7XG4gICAgICAmIC5idG4ge1xuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgICB9XG4gICAgfVxuICAgICYgaDMge1xuICAgICAgZm9udC1zaXplOiAxLjI1ZW07XG4gICAgICBtYXJnaW46IDA7XG4gICAgfVxuICB9XG59XG5cbkBtZWRpYSAobWluLXdpZHRoOiA3NjhweCkgYW5kIChtYXgtd2lkdGg6IDEwMjRweCkge1xuICBzZWN0aW9uIC5jb250YWluZXIgLmNvbHVtbiAuY2FyZF9fYm9keSB7XG4gICAgcGFkZGluZzogMmVtIDA7XG4gIH1cbn1cblxuQG1lZGlhIChtaW4td2lkdGg6IDc2OHB4KSB7XG4gIC5oZWFkZXJfX2NvbnRhaW5lciB7XG4gICAgbWF4LXdpZHRoOiA4NSU7XG5cbiAgICAmIGRpdiB7XG4gICAgICB3aWR0aDogNzUlO1xuICAgIH1cblxuICAgICYgLmN0YSB7XG4gICAgICAmLS1yaWdodCB7XG4gICAgICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIC5oZWFkZXIge1xuICAgICZfX2NvbnRhaW5lciB7XG4gICAgICAmIGRpdjpmaXJzdC1jaGlsZCB7XG4gICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG5cbiAgICAgICAgJiBoMSB7XG4gICAgICAgICAgZm9udC1zaXplOiAzLjVyZW07XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgJiBwIHtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogM2VtO1xuICAgICAgfVxuXG4gICAgICAmIC5idG4td3JhcHBlciB7XG4gICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgICAgIHdpZHRoOiA3MCU7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcblxuICAgICAgICAmIC5idG4ge1xuICAgICAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgICAgICAgcGFkZGluZzogMjBweCAzMHB4O1xuICAgICAgICAgIG1pbi13aWR0aDogMjAwcHg7XG4gICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICAgIG1hcmdpbi1yaWdodDogMjBweDtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgICAmIGRpdjpudGgtY2hpbGQoMikge1xuICAgICAgd2lkdGg6IDUwJTtcbiAgICAgICYgaW1nIHtcbiAgICAgICAgd2lkdGg6IDc1JTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBzZWN0aW9uIHtcbiAgICBwYWRkaW5nOiAxMDBweCAwO1xuXG4gICAgJiAuY29udGFpbmVyIHtcbiAgICAgIC5mbGV4IHtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgIH1cblxuICAgICAgLmNoaWxkIHtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgJiBoMSB7XG4gICAgICAgICAgZm9udC1zaXplOiAyLjVlbTtcbiAgICAgICAgfVxuXG4gICAgICAgICY6Zmlyc3QtY2hpbGQge1xuICAgICAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xuICAgICAgICB9XG5cbiAgICAgICAgLmNoaWxkOm50aC1jaGlsZCgyKSB7XG4gICAgICAgICAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xuICAgICAgICAgIHdpZHRoOiA1MCU7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgJiAuY3RhIHtcbiAgICAgICAgd2lkdGg6IDUwJTtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgIHBhZGRpbmc6IDJlbTtcblxuICAgICAgICAmLS1yaWdodCB7XG4gICAgICAgICAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gICAgICAgIH1cbiAgICAgICAgJi0tbGVmdCB7XG4gICAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICAgICAgfVxuXG4gICAgICAgICZfX2ltZyB7XG4gICAgICAgICAgd2lkdGg6IDQwJTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgJiAuZmVhdHVyZS1ncm91cCB7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgXG4gICAgICB9XG4gICAgfVxuICB9XG4gIC5zZWN0aW9uLS1mZWF0dXJlIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgdG9wOiAtMTUwcHg7XG4gIH1cbn1cblxuQG1lZGlhIChtaW4td2lkdGg6IDEzMDBweCkge1xuICAuaGVhZGVyX19jb250YWluZXIge1xuICAgIG1heC13aWR0aDogMTIwMHB4O1xuICB9XG59XG5cblxuLyogRXh0cmEgbGFyZ2UgZGV2aWNlcyAobGFyZ2UgbGFwdG9wcyBhbmQgZGVza3RvcHMsIDEyMDBweCBhbmQgdXApICovXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDEzMDBweCkge1xuICAuaGVhZGVyX19jb250YWluZXIge1xuICAgIG1heC13aWR0aDogODUlO1xuICAgICYgZGl2IHtcbiAgICAgIHdpZHRoOiA1MCU7XG4gICAgICAmIC5idG4td3JhcHBlciB7XG4gICAgICAgIHdpZHRoOiA1MCU7XG4gICAgICB9XG4gICAgfVxuICB9XG59IiwiLmhlYWRlciB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgaGVpZ2h0OiA3NTBweDtcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjMmMzZTUwLCByZ2JhKDQxLCAxMjgsIDE4NSwgMC41KSksIHVybChcIi9hc3NldHMvaW1hZ2VzL2JnLmpwZ1wiKTtcbiAgYmFja2dyb3VuZC1hdHRhY2htZW50OiBmaXhlZDtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyIHRvcDtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3Zlcjtcbn1cbi5oZWFkZXIgc3ZnIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBib3R0b206IDA7XG4gIHdpZHRoOiAxMDAlO1xufVxuLmhlYWRlciBzdmcgcGF0aCB7XG4gIGZpbGw6ICNmZmZmZmY7XG59XG4uaGVhZGVyX19jb250YWluZXIge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xuICBkaXNwbGF5OiBmbGV4O1xuICBtYXJnaW46IGF1dG87XG4gIGNvbG9yOiB3aGl0ZTtcbn1cbi5oZWFkZXJfX2NvbnRhaW5lciBkaXYge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uaGVhZGVyX19jb250YWluZXIgZGl2IHAge1xuICBtYXJnaW46IDIwIDAgMjAgMDtcbn1cblxuc2VjdGlvbi5jb2xvciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmOGZhZmU7XG59XG5zZWN0aW9uLmNvbG9yLS1ncmFkaWVudCB7XG4gIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgIzI5ODBiOSwgIzJjM2U1MCk7XG59XG5zZWN0aW9uLmNvbG9yLS1iZyB7XG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgIzI5ODBiOSwgcmdiYSg0NCwgNjIsIDgwLCAwLjUpKSwgdXJsKFwiL2Fzc2V0cy9pbWFnZXMvYmdfMDIuanBnXCIpO1xuICBiYWNrZ3JvdW5kLWF0dGFjaG1lbnQ6IGZpeGVkO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXIgdG9wO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xufVxuc2VjdGlvbi0tZmVhdHVyZSB7XG4gIHBhZGRpbmc6IDA7XG59XG5zZWN0aW9uLS1mZWF0dXJlIC5mZWF0dXJlLWdyb3VwIHtcbiAgcGFkZGluZzogM2VtO1xufVxuc2VjdGlvbiAuY29udGFpbmVyIHtcbiAgcGFkZGluZzogMzBweCAwcHg7XG4gIG1heC13aWR0aDogMTE0MHB4O1xuICBtYXJnaW46IGF1dG87XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG5zZWN0aW9uIC5jb250YWluZXIgLmZsZXgge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbn1cbnNlY3Rpb24gLmNvbnRhaW5lciAuaGVhZGluZyB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbnNlY3Rpb24gLmNvbnRhaW5lciBoMiB7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGZvbnQtc2l6ZTogMmVtO1xuICBjb2xvcjogIzI4M2U1MTtcbn1cbnNlY3Rpb24gLmNvbnRhaW5lciAudGFiIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC13cmFwOiB3cmFwO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGJveC1zaGFkb3c6IDBweCAyMHB4IDQ1cHggMHB4IHJnYmEoMCwgMCwgMCwgMC4wOCk7XG4gIGJvcmRlci1yYWRpdXM6IDEuNjE3NjVyZW07XG4gIG1heC13aWR0aDogMjYwcHg7XG4gIG1hcmdpbjogMCBhdXRvO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgcGFkZGluZzogNXB4IDA7XG4gIHotaW5kZXg6IDE7XG59XG5zZWN0aW9uIC5jb250YWluZXIgLnRhYiBsaSB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgbWFyZ2luOiAwO1xuICBwYWRkaW5nOiAwO1xuICBsaXN0LXN0eWxlLXR5cGU6IG5vbmU7XG4gIC13ZWJraXQtdGFwLWhpZ2hsaWdodC1jb2xvcjogdHJhbnNwYXJlbnQ7XG59XG5zZWN0aW9uIC5jb250YWluZXIgLnRhYiBsaSBhIHtcbiAgYm9yZGVyLXJhZGl1czogMS4zNTI5NHJlbTtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICBtaW4td2lkdGg6IDEyNXB4O1xuICBwYWRkaW5nOiA4cHggMzFweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cbnNlY3Rpb24gLmNvbnRhaW5lciAudGFiIGxpICN5ZWFybHktdGFiIHtcbiAgY29sb3I6ICMyYzNlNTA7XG59XG5zZWN0aW9uIC5jb250YWluZXIgLnRhYiBsaSNpbmRpY2F0b3Ige1xuICB3aWR0aDogMTI1cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICMxODY4ZGQ7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgaGVpZ2h0OiBjYWxjKDEwMCUgLSAxMHB4KTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xuICB0b3A6IDUwJTtcbiAgYm9yZGVyLXJhZGl1czogMS4zNTI5NHJlbTtcbiAgei1pbmRleDogLTE7XG4gIGxlZnQ6IDVweDtcbn1cbnNlY3Rpb24gLmNvbnRhaW5lciAuY3RhIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwYWRkaW5nOiAyZW0gMGVtO1xufVxuc2VjdGlvbiAuY29udGFpbmVyIC5jdGEgcCB7XG4gIG1hcmdpbi1ib3R0b206IDNlbTtcbn1cbnNlY3Rpb24gLmNvbnRhaW5lciAuY3RhX19pbWcge1xuICB3aWR0aDogNTAlO1xuICBtYXJnaW46IDAgYXV0bztcbn1cbnNlY3Rpb24gLmNvbnRhaW5lciAuY29sdW1uIHtcbiAgcGFkZGluZzogMjBweDtcbiAgZmxleDogMTtcbiAgbGluZS1oZWlnaHQ6IDEuNTtcbiAgY29sb3I6ICMyOTgwYjk7XG59XG5zZWN0aW9uIC5jb250YWluZXIgLmNvbHVtbiAuY29sdW1uX19pY29uIHtcbiAgaGVpZ2h0OiAyNDRweDtcbiAgd2lkdGg6IDI0NHB4O1xufVxuc2VjdGlvbiAuY29udGFpbmVyIC5jb2x1bW4gLmNvbHVtbl9faWNvbixcbnNlY3Rpb24gLmNvbnRhaW5lciAuY29sdW1uIC5jYXJkX19pY29uIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtYXJnaW46IGF1dG87XG59XG5zZWN0aW9uIC5jb250YWluZXIgLmNvbHVtbiAuY29sdW1uX19pY29uIGltZyxcbnNlY3Rpb24gLmNvbnRhaW5lciAuY29sdW1uIC5jYXJkX19pY29uIGltZyB7XG4gIG1heC1oZWlnaHQ6IDEwMCU7XG4gIG1heC13aWR0aDogMTAwJTtcbn1cbnNlY3Rpb24gLmNvbnRhaW5lciAuY29sdW1uIC5jYXJkX19pY29uIGltZyB7XG4gIG1heC1oZWlnaHQ6IDUwJTtcbiAgbWF4LXdpZHRoOiA1MCU7XG59XG5zZWN0aW9uIC5jb250YWluZXIgLmNvbHVtbl9fYm9keSB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbnNlY3Rpb24gLmNvbnRhaW5lciAuY29sdW1uX19ib2R5IGgzIHtcbiAgZm9udC1zaXplOiAxLjVlbTtcbiAgY29sb3I6ICMyODNlNTE7XG59XG5zZWN0aW9uIC5jb250YWluZXIgLmNvbHVtbiAuY2FyZCB7XG4gIGJveC1zaGFkb3c6IDAgNHB4IDhweCAwIHJnYmEoMCwgMCwgMCwgMC4yKTtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgcGFkZGluZzogMTZweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xufVxuc2VjdGlvbiAuY29udGFpbmVyIC5jb2x1bW4gLmNhcmRfX2JvZHkge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHBhZGRpbmc6IDJlbTtcbn1cbnNlY3Rpb24gLmNvbnRhaW5lciAuY29sdW1uIC5jYXJkX190aXRsZSB7XG4gIGZvbnQtc2l6ZTogMS41ZW07XG4gIGNvbG9yOiAjMjgzZTUxO1xufVxuc2VjdGlvbiAuY29udGFpbmVyIC5jb2x1bW4gLmNhcmRfX3RleHQge1xuICBsaW5lLWhlaWdodDogMS41O1xufVxuc2VjdGlvbiAuY29udGFpbmVyIC5jb2x1bW4gLmNhcmRfX3ByaWNlIHtcbiAgZm9udC1mYW1pbHk6IFwiQXJpYWwgQmxhY2tcIjtcbiAgZm9udC1zaXplOiAzZW07XG4gIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgjMmMzZTUwLCAjMjk4MGI5KTtcbiAgLXdlYmtpdC1iYWNrZ3JvdW5kLWNsaXA6IHRleHQ7XG4gIC13ZWJraXQtdGV4dC1maWxsLWNvbG9yOiB0cmFuc3BhcmVudDtcbn1cbnNlY3Rpb24gLmNvbnRhaW5lciAuY29sdW1uIC5hY3RpdmUge1xuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoIzJjM2U1MCwgIzI5ODBiOSk7XG4gIHBhZGRpbmc6IDUwcHggMTZweDtcbn1cbnNlY3Rpb24gLmNvbnRhaW5lciAuY29sdW1uIC5hY3RpdmUgLmNhcmRfX3RpdGxlLFxuc2VjdGlvbiAuY29udGFpbmVyIC5jb2x1bW4gLmFjdGl2ZSAuY2FyZF9fYm9keSxcbnNlY3Rpb24gLmNvbnRhaW5lciAuY29sdW1uIC5hY3RpdmUgLmNhcmRfX3ByaWNlIHtcbiAgY29sb3I6ICNmZmZmZmY7XG59XG5zZWN0aW9uIC5jb250YWluZXIgLmNvbHVtbiAuYWN0aXZlIC5jYXJkX19wcmljZSB7XG4gIC13ZWJraXQtdGV4dC1maWxsLWNvbG9yOiBpbml0aWFsO1xufVxuc2VjdGlvbiAjc2Vjb25kLXRhYiB7XG4gIGRpc3BsYXk6IG5vbmU7XG4gIG9wYWNpdHk6IDA7XG59XG5zZWN0aW9uIC5jaGlsZCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbnNlY3Rpb24gLmNoaWxkIGgxIHtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgY29sb3I6IHdoaXRlO1xufVxuc2VjdGlvbi0tZmVhdHVyZSB7XG4gIHBhZGRpbmc6IDBweDtcbn1cblxuLnNlY3Rpb24tLWZlYXR1cmUge1xuICBwYWRkaW5nOiAwcHg7XG59XG4uc2VjdGlvbiAuZmVhdHVyZS1ncm91cCB7XG4gIG1hcmdpbjogMS4yNWVtO1xuICBwYWRkaW5nOiAzZW07XG4gIGJhY2tncm91bmQtY29sb3I6ICNmOGZhZmU7XG4gIGJveC1zaGFkb3c6IDAgNHB4IDhweCAwIHJnYmEoMCwgMCwgMCwgMC4yKTtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgY29sb3I6ICMyOTgwYjk7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXg6IDE7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5zZWN0aW9uIC5mZWF0dXJlLWdyb3VwX19pbWcge1xuICBkaXNwbGF5OiBmbGV4O1xuICBtaW4td2lkdGg6IDEzOHB4O1xuICBtYXJnaW4tcmlnaHQ6IDIwcHg7XG59XG4uc2VjdGlvbiAuZmVhdHVyZS1ncm91cF9fYm9keSAuYnRuIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xufVxuLnNlY3Rpb24gLmZlYXR1cmUtZ3JvdXAgaDMge1xuICBmb250LXNpemU6IDEuMjVlbTtcbiAgbWFyZ2luOiAwO1xufVxuXG5AbWVkaWEgKG1pbi13aWR0aDogNzY4cHgpIGFuZCAobWF4LXdpZHRoOiAxMDI0cHgpIHtcbiAgc2VjdGlvbiAuY29udGFpbmVyIC5jb2x1bW4gLmNhcmRfX2JvZHkge1xuICAgIHBhZGRpbmc6IDJlbSAwO1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogNzY4cHgpIHtcbiAgLmhlYWRlcl9fY29udGFpbmVyIHtcbiAgICBtYXgtd2lkdGg6IDg1JTtcbiAgfVxuICAuaGVhZGVyX19jb250YWluZXIgZGl2IHtcbiAgICB3aWR0aDogNzUlO1xuICB9XG4gIC5oZWFkZXJfX2NvbnRhaW5lciAuY3RhLS1yaWdodCB7XG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gIH1cblxuICAuaGVhZGVyX19jb250YWluZXIgZGl2OmZpcnN0LWNoaWxkIHtcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICB9XG4gIC5oZWFkZXJfX2NvbnRhaW5lciBkaXY6Zmlyc3QtY2hpbGQgaDEge1xuICAgIGZvbnQtc2l6ZTogMy41cmVtO1xuICB9XG4gIC5oZWFkZXJfX2NvbnRhaW5lciBwIHtcbiAgICBtYXJnaW4tYm90dG9tOiAzZW07XG4gIH1cbiAgLmhlYWRlcl9fY29udGFpbmVyIC5idG4td3JhcHBlciB7XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICB3aWR0aDogNzAlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICB9XG4gIC5oZWFkZXJfX2NvbnRhaW5lciAuYnRuLXdyYXBwZXIgLmJ0biB7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBwYWRkaW5nOiAyMHB4IDMwcHg7XG4gICAgbWluLXdpZHRoOiAyMDBweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgbWFyZ2luLXJpZ2h0OiAyMHB4O1xuICB9XG4gIC5oZWFkZXIgZGl2Om50aC1jaGlsZCgyKSB7XG4gICAgd2lkdGg6IDUwJTtcbiAgfVxuICAuaGVhZGVyIGRpdjpudGgtY2hpbGQoMikgaW1nIHtcbiAgICB3aWR0aDogNzUlO1xuICB9XG5cbiAgc2VjdGlvbiB7XG4gICAgcGFkZGluZzogMTAwcHggMDtcbiAgfVxuICBzZWN0aW9uIC5jb250YWluZXIgLmZsZXgge1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIH1cbiAgc2VjdGlvbiAuY29udGFpbmVyIC5jaGlsZCB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgfVxuICBzZWN0aW9uIC5jb250YWluZXIgLmNoaWxkIGgxIHtcbiAgICBmb250LXNpemU6IDIuNWVtO1xuICB9XG4gIHNlY3Rpb24gLmNvbnRhaW5lciAuY2hpbGQ6Zmlyc3QtY2hpbGQge1xuICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xuICB9XG4gIHNlY3Rpb24gLmNvbnRhaW5lciAuY2hpbGQgLmNoaWxkOm50aC1jaGlsZCgyKSB7XG4gICAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xuICAgIHdpZHRoOiA1MCU7XG4gIH1cbiAgc2VjdGlvbiAuY29udGFpbmVyIC5jdGEge1xuICAgIHdpZHRoOiA1MCU7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIHBhZGRpbmc6IDJlbTtcbiAgfVxuICBzZWN0aW9uIC5jb250YWluZXIgLmN0YS0tcmlnaHQge1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICB9XG4gIHNlY3Rpb24gLmNvbnRhaW5lciAuY3RhLS1sZWZ0IHtcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICB9XG4gIHNlY3Rpb24gLmNvbnRhaW5lciAuY3RhX19pbWcge1xuICAgIHdpZHRoOiA0MCU7XG4gIH1cbiAgc2VjdGlvbiAuY29udGFpbmVyIC5mZWF0dXJlLWdyb3VwIHtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gIH1cblxuICAuc2VjdGlvbi0tZmVhdHVyZSB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHRvcDogLTE1MHB4O1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogMTMwMHB4KSB7XG4gIC5oZWFkZXJfX2NvbnRhaW5lciB7XG4gICAgbWF4LXdpZHRoOiAxMjAwcHg7XG4gIH1cbn1cbi8qIEV4dHJhIGxhcmdlIGRldmljZXMgKGxhcmdlIGxhcHRvcHMgYW5kIGRlc2t0b3BzLCAxMjAwcHggYW5kIHVwKSAqL1xuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiAxMzAwcHgpIHtcbiAgLmhlYWRlcl9fY29udGFpbmVyIHtcbiAgICBtYXgtd2lkdGg6IDg1JTtcbiAgfVxuICAuaGVhZGVyX19jb250YWluZXIgZGl2IHtcbiAgICB3aWR0aDogNTAlO1xuICB9XG4gIC5oZWFkZXJfX2NvbnRhaW5lciBkaXYgLmJ0bi13cmFwcGVyIHtcbiAgICB3aWR0aDogNTAlO1xuICB9XG59Il19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](HomeComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-home',
                templateUrl: './home.component.html',
                styleUrls: ['./home.component.scss']
            }]
    }], function () { return [{ type: _pricing_tab_service__WEBPACK_IMPORTED_MODULE_2__["PricingTabService"] }, { type: _hostfront_api_service__WEBPACK_IMPORTED_MODULE_3__["HostfrontApiService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/hostfront-api.service.ts":
/*!******************************************!*\
  !*** ./src/app/hostfront-api.service.ts ***!
  \******************************************/
/*! exports provided: HostfrontApiService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HostfrontApiService", function() { return HostfrontApiService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
//




class HostfrontApiService {
    constructor(http) {
        this.http = http;
    }
    getHome() {
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].baseurl}/wp-json/wp/v2/custom_page/183`);
    }
}
HostfrontApiService.ɵfac = function HostfrontApiService_Factory(t) { return new (t || HostfrontApiService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"])); };
HostfrontApiService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: HostfrontApiService, factory: HostfrontApiService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](HostfrontApiService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "./src/app/pricing-tab.service.ts":
/*!****************************************!*\
  !*** ./src/app/pricing-tab.service.ts ***!
  \****************************************/
/*! exports provided: PricingTabService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PricingTabService", function() { return PricingTabService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


class PricingTabService {
    constructor() { }
    yearlyClick() {
        const indicator = document.getElementById('indicator');
        const secondTab = document.getElementById('second-tab');
        const firstTab = document.getElementById('first-tab');
        const yearlyTabColor = document.getElementById('yearly-tab');
        const monthlyTabColor = document.getElementById('monthly-tab');
        secondTab.style.display = 'block';
        secondTab.style.opacity = '1';
        secondTab.style.transition = 'opacity 1s ease-in';
        firstTab.style.display = 'none';
        firstTab.style.opacity = '0';
        yearlyTabColor.style.color = '#ffffff';
        monthlyTabColor.style.color = '#2c3e50';
        indicator.style.left = '120px';
        indicator.style.transition = 'all 0.35s ease-Out';
    }
    monthlyClick() {
        const indicator = document.getElementById('indicator');
        const secondTab = document.getElementById('second-tab');
        const firstTab = document.getElementById('first-tab');
        const yearlyTabColor = document.getElementById('yearly-tab');
        const monthlyTabColor = document.getElementById('monthly-tab');
        firstTab.style.display = 'block';
        firstTab.style.opacity = '1';
        firstTab.style.transition = 'opacity 1s ease-in';
        indicator.style.left = '5px';
        indicator.style.transition = 'all 0.35s ease-Out';
        secondTab.style.display = 'none';
        secondTab.style.opacity = '0';
        yearlyTabColor.style.color = '#2c3e50';
        monthlyTabColor.style.color = '#ffffff';
    }
}
PricingTabService.ɵfac = function PricingTabService_Factory(t) { return new (t || PricingTabService)(); };
PricingTabService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: PricingTabService, factory: PricingTabService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](PricingTabService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/products/products.component.ts":
/*!************************************************!*\
  !*** ./src/app/products/products.component.ts ***!
  \************************************************/
/*! exports provided: ProductsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductsComponent", function() { return ProductsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _pricing_tab_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../pricing-tab.service */ "./src/app/pricing-tab.service.ts");
/* harmony import */ var _shared_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../shared/navbar/navbar.component */ "./src/app/shared/navbar/navbar.component.ts");
/* harmony import */ var _shared_scroll_scroll_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../shared/scroll/scroll.component */ "./src/app/shared/scroll/scroll.component.ts");
/* harmony import */ var _shared_footer_footer_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../shared/footer/footer.component */ "./src/app/shared/footer/footer.component.ts");






class ProductsComponent {
    constructor(_pricingTabService) {
        this._pricingTabService = _pricingTabService;
    }
    yearlyTab() {
        this._pricingTabService.yearlyClick();
    }
    monthlyTab() {
        this._pricingTabService.monthlyClick();
    }
    ngOnInit() {
    }
}
ProductsComponent.ɵfac = function ProductsComponent_Factory(t) { return new (t || ProductsComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_pricing_tab_service__WEBPACK_IMPORTED_MODULE_1__["PricingTabService"])); };
ProductsComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ProductsComponent, selectors: [["app-products"]], decls: 193, vars: 0, consts: [[1, "header"], ["id", "curve", "data-name", "Layer 1", "xmlns", "http://www.w3.org/2000/svg", "viewBox", "0 0 1416.99 174.01"], ["d", "M0,280.8S283.66,59,608.94,163.56s437.93,150.57,808,10.34V309.54H0V280.8Z", "transform", "translate(0 -135.53)", 1, "cls-1"], [1, "header__container"], [1, "section", "wordpress-hosting"], [1, "container"], [1, "flex", "heading"], [1, "flex"], [1, "section", "color"], ["id", "webhosting-help", 1, "flex"], [1, "column"], [1, "card"], [1, "card__body"], [1, "card__title"], [1, "card__text"], ["data-aos", "fade-left", 1, "card"], ["data-aos", "fade-right", 1, "card"], ["id", "first-tab", 1, "container"], [1, "card__icon"], ["src", "assets/images/cloud-computing.svg"], [1, "card__price"], [1, "btn", "btn--std", "btn--std01"], [1, "card", "active"], [1, "btn", "btn--std", "btn--std02"], ["id", "second-tab", 1, "container"], [1, "btn", "btn--ghost", "btn--ghost--01"], [1, "btn__slide", "btn__slide--01"], ["href", "#"], [1, "btn", "btn--ghost", "btn--ghost--02"], [1, "btn__slide", "btn__slide--02"], [1, "section", "color--bg"], [1, "child"]], template: function ProductsComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "header", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "app-navbar");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceSVG"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "svg", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "path", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceHTML"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, " WORDPRESS WEBSITE HOSTING");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "section", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "What is wordpress hosting?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu..");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "section", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, "How can it help my business?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "h3", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, "Website Speed");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "p", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "h3", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37, "Managed WordPress Hosting");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "p", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "h3", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](44, "Hack Protection Guarantee");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "p", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](46, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "h3", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](51, "Eco-Friendly Website Hosting");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "p", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](53, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "h3", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](58, "Cloud Computing");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "ul", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](61, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](63, "Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](65, "In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a,");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](69, "h3", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](70, "OPTIMISED WEBSITE HOSTING FEATURES ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](71, "ul", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](73, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](75, "Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](76, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](77, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](79, "h3", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](80, "Advanced Site Speed");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "p", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](82, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](83, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](84, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "h3", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](87, "CDN \u2013 Content Delivery Network");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](88, "p", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](89, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](90, "section", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](91, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](92, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](93, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](94, "Our Plans");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](95, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](96, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](97, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](98, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](99, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](100, "img", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](101, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](102, "h3", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](103, "Cloud Computing");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](104, "p", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](105, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](106, "p", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](107, "\u00A31.99");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](108, "span", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](109, "Click Here");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](110, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](111, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](112, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](113, "img", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](114, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](115, "h3", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](116, "Cloud Computing");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](117, "p", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](118, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](119, "p", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](120, "\u00A359.99");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](121, "span", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](122, "Click Here");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](123, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](124, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](125, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](126, "img", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](127, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](128, "h3", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](129, "Cloud Computing");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](130, "p", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](131, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](132, "p", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](133, "\u00A32.99");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](134, "span", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](135, "Click Here");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](136, "div", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](137, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](138, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](139, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](140, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](141, "img", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](142, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](143, "h3", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](144, "Cloud Computing");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](145, "p", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](146, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](147, "p", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](148, "\u00A30.99");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](149, "span", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](150, "span", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](151, "a", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](152, "Search");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](153, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](154, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](155, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](156, "img", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](157, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](158, "h3", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](159, "Cloud Computing");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](160, "p", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](161, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](162, "p", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](163, "\u00A336.99");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](164, "span", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](165, "span", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](166, "a", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](167, "Search");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](168, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](169, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](170, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](171, "img", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](172, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](173, "h3", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](174, "Cloud Computing");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](175, "p", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](176, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](177, "p", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](178, "\u00A36.59");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](179, "span", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](180, "span", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](181, "a", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](182, "Search");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](183, "section", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](184, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](185, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](186, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](187, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](188, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](189, "span", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](190, "Click Here");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](191, "app-scroll");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](192, "app-footer");
    } }, directives: [_shared_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_2__["NavbarComponent"], _shared_scroll_scroll_component__WEBPACK_IMPORTED_MODULE_3__["ScrollComponent"], _shared_footer_footer_component__WEBPACK_IMPORTED_MODULE_4__["FooterComponent"]], styles: [".header[_ngcontent-%COMP%] {\n  position: relative;\n  height: 750px;\n  background-image: linear-gradient(to right, #2c3e50, rgba(41, 128, 185, 0.5)), url(\"/assets/images/services-bg.jpg\");\n  background-attachment: fixed;\n  background-position: 50% 50%;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n.header[_ngcontent-%COMP%]   svg[_ngcontent-%COMP%] {\n  position: absolute;\n  bottom: 0;\n  width: 100%;\n}\n.header[_ngcontent-%COMP%]   svg[_ngcontent-%COMP%]   path[_ngcontent-%COMP%] {\n  fill: #ffffff;\n}\n.header__container[_ngcontent-%COMP%] {\n  width: 100%;\n  height: 100%;\n  display: flex;\n  margin: auto;\n  color: white;\n}\n.header__container[_ngcontent-%COMP%]   div[_ngcontent-%COMP%] {\n  text-align: center;\n}\n.header__container[_ngcontent-%COMP%]   div[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n  margin: 20 0 20 0;\n}\nsection[_ngcontent-%COMP%] {\n  padding: 100px 0;\n}\nsection.wordpress-hosting[_ngcontent-%COMP%] {\n  line-height: 2;\n  text-align: center;\n}\nsection.color[_ngcontent-%COMP%] {\n  background-color: #f8fafe;\n}\nsection.color--bg[_ngcontent-%COMP%] {\n  background-image: linear-gradient(to right, #2980b9, rgba(44, 62, 80, 0.5)), url(\"/assets/images/bg_02.jpg\");\n  background-attachment: fixed;\n  background-position: center top;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%] {\n  padding: 30px 0px;\n  max-width: 1140px;\n  margin: auto;\n  overflow: hidden;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: center;\n  flex-direction: column;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .heading[_ngcontent-%COMP%] {\n  text-align: center;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  text-transform: uppercase;\n  font-size: 2em;\n  color: #283e51;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .tab[_ngcontent-%COMP%] {\n  display: flex;\n  flex-wrap: wrap;\n  position: relative;\n  box-shadow: 0px 20px 45px 0px rgba(0, 0, 0, 0.08);\n  border-radius: 1.61765rem;\n  max-width: 370px;\n  margin: 0 auto;\n  background-color: #ffffff;\n  justify-content: center;\n  padding: 5px 0;\n  z-index: 1;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .tab[_ngcontent-%COMP%]   li[_ngcontent-%COMP%] {\n  display: inline-block;\n  margin: 0;\n  padding: 0;\n  list-style-type: none;\n  -webkit-tap-highlight-color: transparent;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .tab[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  border-radius: 1.35294rem;\n  display: block;\n  color: #ffffff;\n  min-width: 125px;\n  padding: 8px 31px;\n  text-align: center;\n  text-decoration: none;\n  cursor: pointer;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .tab[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   #yearly-tab[_ngcontent-%COMP%] {\n  color: #2c3e50;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .tab[_ngcontent-%COMP%]   li#indicator[_ngcontent-%COMP%] {\n  width: 150px;\n  background-color: #1868dd;\n  position: absolute;\n  height: calc(100% - 10px);\n  transform: translateY(-50%);\n  top: 50%;\n  border-radius: 1.35294rem;\n  z-index: -1;\n  left: 5px;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%] {\n  padding: 20px;\n  flex: 1;\n  line-height: 1.5;\n  color: #2980b9;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .column__icon[_ngcontent-%COMP%] {\n  height: 244px;\n  width: 244px;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .column__icon[_ngcontent-%COMP%], section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .card__icon[_ngcontent-%COMP%] {\n  position: relative;\n  margin: auto;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .column__icon[_ngcontent-%COMP%]   img[_ngcontent-%COMP%], section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .card__icon[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  max-height: 100%;\n  max-width: 100%;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .card__icon[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  max-height: 50%;\n  max-width: 50%;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column__body[_ngcontent-%COMP%] {\n  text-align: center;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column__body[_ngcontent-%COMP%]   h3[_ngcontent-%COMP%] {\n  font-size: 1.5em;\n  color: #283e51;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%] {\n  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);\n  border-radius: 10px;\n  padding: 16px;\n  background-color: #ffffff;\n  text-align: center;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .card__body[_ngcontent-%COMP%] {\n  padding: 2em;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .card__title[_ngcontent-%COMP%] {\n  font-size: 1.5em;\n  color: #283e51;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .card__text[_ngcontent-%COMP%] {\n  line-height: 1.5;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .card__text[_ngcontent-%COMP%]   li[_ngcontent-%COMP%] {\n  margin-bottom: 20px;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .card__price[_ngcontent-%COMP%] {\n  font-family: \"Arial Black\";\n  font-size: 3em;\n  background: linear-gradient(#2c3e50, #2980b9);\n  -webkit-background-clip: text;\n  -webkit-text-fill-color: transparent;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .active[_ngcontent-%COMP%] {\n  background-image: linear-gradient(#2c3e50, #2980b9);\n  padding: 50px 16px;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .active[_ngcontent-%COMP%]   .card__title[_ngcontent-%COMP%], section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .active[_ngcontent-%COMP%]   .card__body[_ngcontent-%COMP%], section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .active[_ngcontent-%COMP%]   .card__price[_ngcontent-%COMP%] {\n  color: #ffffff;\n}\nsection[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .active[_ngcontent-%COMP%]   .card__price[_ngcontent-%COMP%] {\n  -webkit-text-fill-color: initial;\n}\nsection[_ngcontent-%COMP%]   #second-tab[_ngcontent-%COMP%] {\n  display: none;\n  opacity: 0;\n}\nsection[_ngcontent-%COMP%]   .child[_ngcontent-%COMP%] {\n  text-align: center;\n}\nsection[_ngcontent-%COMP%]   .child[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n  text-transform: uppercase;\n  color: white;\n  font-size: 2em;\n}\n@media (min-width: 768px) and (max-width: 1366px) {\n  .header__container[_ngcontent-%COMP%] {\n    max-width: 960px;\n  }\n  .header__container[_ngcontent-%COMP%]   div[_ngcontent-%COMP%] {\n    width: 50%;\n  }\n\n  section[_ngcontent-%COMP%] {\n    padding: 100px 0;\n  }\n  section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%] {\n    width: 85%;\n  }\n  section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%] {\n    flex-direction: row;\n  }\n  section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%]   .card__body[_ngcontent-%COMP%] {\n    padding: 2em 0em;\n  }\n  section[_ngcontent-%COMP%]   #first-tab[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%], section[_ngcontent-%COMP%]   #second-tab[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%] {\n    flex-direction: row;\n  }\n\n  #webhosting-help[_ngcontent-%COMP%] {\n    flex-wrap: wrap;\n  }\n  #webhosting-help[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%] {\n    flex-basis: 50%;\n    display: flex;\n  }\n  #webhosting-help[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%] {\n    display: flex;\n  }\n}\n@media (min-width: 768px) {\n  .header__container[_ngcontent-%COMP%] {\n    max-width: 85%;\n  }\n  .header__container[_ngcontent-%COMP%]   div[_ngcontent-%COMP%]:first-child {\n    text-align: left;\n    width: 80%;\n    margin-top: 50px;\n  }\n  .header__container[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n    font-size: 2em;\n  }\n}\n@media (min-width: 1024px) and (max-width: 1366px) {\n  section[_ngcontent-%COMP%]   #first-tab[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%], section[_ngcontent-%COMP%]   #second-tab[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%] {\n    flex-direction: row;\n  }\n}\n@media (min-width: 1366px) {\n  .header__container[_ngcontent-%COMP%] {\n    max-width: 1200px;\n  }\n  .header__container[_ngcontent-%COMP%]   div[_ngcontent-%COMP%]:first-child {\n    text-align: left;\n    width: 80%;\n    margin-top: 50px;\n  }\n  .header__container[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n    font-size: 3.5rem;\n  }\n\n  section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%] {\n    flex-direction: row;\n  }\n  section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex#webhosting-help[_ngcontent-%COMP%] {\n    flex-wrap: wrap;\n  }\n  section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex#webhosting-help[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%] {\n    flex-basis: 50%;\n    display: flex;\n  }\n  section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex#webhosting-help[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%] {\n    display: flex;\n  }\n}\n@media (min-width: 813px) {\n  .header__container[_ngcontent-%COMP%]   div[_ngcontent-%COMP%] {\n    width: 75%;\n  }\n\n  .header__container[_ngcontent-%COMP%]   div[_ngcontent-%COMP%]:first-child {\n    text-align: left;\n  }\n  .header__container[_ngcontent-%COMP%]   div[_ngcontent-%COMP%]:first-child   h1[_ngcontent-%COMP%] {\n    font-size: 3.5rem;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2NoaWRvL3Byb2plY3RzL2hvc3Rmcm9udC1wcm9qZWN0L2hvc3Rmcm9udC1mcm9udGVuZC9zcmMvYXBwL3Byb2R1Y3RzL3Byb2R1Y3RzLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9wcm9kdWN0cy9wcm9kdWN0cy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLG9IQUFBO0VBRUEsNEJBQUE7RUFDQSw0QkFBQTtFQUNBLDRCQUFBO0VBQ0Esc0JBQUE7QUNBRjtBRElFO0VBQ0Usa0JBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtBQ0ZKO0FESUk7RUFDRSxhQUFBO0FDRk47QURNRTtFQUNFLFdBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0FDSko7QURNSTtFQUNFLGtCQUFBO0FDSk47QURNTTtFQUNFLGlCQUFBO0FDSlI7QURVQTtFQUNFLGdCQUFBO0FDUEY7QURRRTtFQUNFLGNBQUE7RUFDQSxrQkFBQTtBQ05KO0FEUUU7RUFDRSx5QkFBQTtBQ05KO0FEUUk7RUFDRSw0R0FBQTtFQUVBLDRCQUFBO0VBQ0EsK0JBQUE7RUFDQSw0QkFBQTtFQUNBLHNCQUFBO0FDUE47QURXRTtFQUNFLGlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QUNUSjtBRFdJO0VBQ0UsYUFBQTtFQUNBLHVCQUFBO0VBQ0Esc0JBQUE7QUNUTjtBRFlJO0VBQ0Usa0JBQUE7QUNWTjtBRGFJO0VBQ0UseUJBQUE7RUFDQSxjQUFBO0VBQ0EsY0FBQTtBQ1hOO0FEY0k7RUFDRSxhQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsaURBQUE7RUFDQSx5QkFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLHlCQUFBO0VBQ0EsdUJBQUE7RUFDQSxjQUFBO0VBQ0EsVUFBQTtBQ1pOO0FEY007RUFDRSxxQkFBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0VBQ0EscUJBQUE7RUFDQSx3Q0FBQTtBQ1pSO0FEY1E7RUFDRSx5QkFBQTtFQUNBLGNBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EscUJBQUE7RUFDQSxlQUFBO0FDWlY7QURlUTtFQUNFLGNBQUE7QUNiVjtBRGdCUTtFQUNFLFlBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSwyQkFBQTtFQUNBLFFBQUE7RUFDQSx5QkFBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0FDZFY7QURtQkk7RUFDRSxhQUFBO0VBQ0EsT0FBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtBQ2pCTjtBRG1CTTtFQUNFLGFBQUE7RUFDQSxZQUFBO0FDakJSO0FEb0JNOztFQUVFLGtCQUFBO0VBQ0EsWUFBQTtBQ2xCUjtBRG9CUTs7RUFDRSxnQkFBQTtFQUNBLGVBQUE7QUNqQlY7QURzQlE7RUFDRSxlQUFBO0VBQ0EsY0FBQTtBQ3BCVjtBRHdCTTtFQUNFLGtCQUFBO0FDdEJSO0FEd0JRO0VBQ0UsZ0JBQUE7RUFDQSxjQUFBO0FDdEJWO0FEMEJNO0VBQ0UsMENBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0FDeEJSO0FEMEJRO0VBQ0UsWUFBQTtBQ3hCVjtBRDJCUTtFQUNFLGdCQUFBO0VBQ0EsY0FBQTtBQ3pCVjtBRDRCUTtFQUNFLGdCQUFBO0FDMUJWO0FEMkJVO0VBQ0UsbUJBQUE7QUN6Qlo7QUQ2QlE7RUFDRSwwQkFBQTtFQUNBLGNBQUE7RUFDQSw2Q0FBQTtFQUNBLDZCQUFBO0VBQ0Esb0NBQUE7QUMzQlY7QUQrQk07RUFDRSxtREFBQTtFQUNBLGtCQUFBO0FDN0JSO0FEOEJROzs7RUFHRSxjQUFBO0FDNUJWO0FEOEJRO0VBQ0UsZ0NBQUE7QUM1QlY7QURrQ0U7RUFDRSxhQUFBO0VBQ0EsVUFBQTtBQ2hDSjtBRG1DRTtFQUNFLGtCQUFBO0FDakNKO0FEa0NJO0VBQ0UseUJBQUE7RUFDQSxZQUFBO0VBQ0EsY0FBQTtBQ2hDTjtBRHFDQTtFQUNFO0lBQ0UsZ0JBQUE7RUNsQ0Y7RURtQ0U7SUFDRSxVQUFBO0VDakNKOztFRG9DQTtJQUNFLGdCQUFBO0VDakNGO0VEa0NFO0lBQ0UsVUFBQTtFQ2hDSjtFRGlDSTtJQUNFLG1CQUFBO0VDL0JOO0VEZ0NNO0lBQ0UsZ0JBQUE7RUM5QlI7RURtQ0k7SUFDRSxtQkFBQTtFQ2pDTjs7RURxQ0E7SUFDRSxlQUFBO0VDbENGO0VEbUNFO0lBQ0UsZUFBQTtJQUNBLGFBQUE7RUNqQ0o7RURrQ0k7SUFDRSxhQUFBO0VDaENOO0FBQ0Y7QURxQ0E7RUFDRTtJQUNFLGNBQUE7RUNuQ0Y7RURvQ0U7SUFDRSxnQkFBQTtJQUNBLFVBQUE7SUFDQSxnQkFBQTtFQ2xDSjtFRG9DRTtJQUNFLGNBQUE7RUNsQ0o7QUFDRjtBRHNDQTtFQUdNO0lBQ0UsbUJBQUE7RUN0Q047QUFDRjtBRDRDQTtFQUNFO0lBQ0UsaUJBQUE7RUMxQ0Y7RUQyQ0U7SUFDRSxnQkFBQTtJQUNBLFVBQUE7SUFDQSxnQkFBQTtFQ3pDSjtFRDJDRTtJQUNFLGlCQUFBO0VDekNKOztFRDhDSTtJQUNFLG1CQUFBO0VDM0NOO0VENENNO0lBQ0UsZUFBQTtFQzFDUjtFRDJDUTtJQUNFLGVBQUE7SUFDQSxhQUFBO0VDekNWO0VEMENVO0lBQ0UsYUFBQTtFQ3hDWjtBQUNGO0FEZ0RBO0VBRUk7SUFDRSxVQUFBO0VDL0NKOztFRHFESTtJQUNFLGdCQUFBO0VDbEROO0VEb0RNO0lBQ0UsaUJBQUE7RUNsRFI7QUFDRiIsImZpbGUiOiJzcmMvYXBwL3Byb2R1Y3RzL3Byb2R1Y3RzLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmhlYWRlciB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgaGVpZ2h0OiA3NTBweDtcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCByZ2JhKDQ0LCA2MiwgODAsIDEpLCByZ2JhKDQxLCAxMjgsIDE4NSwgMC41KSksXG4gICAgdXJsKFwiL2Fzc2V0cy9pbWFnZXMvc2VydmljZXMtYmcuanBnXCIpO1xuICBiYWNrZ3JvdW5kLWF0dGFjaG1lbnQ6IGZpeGVkO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiA1MCUgNTAlO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuXG4gIC8vICMyOTgwYjkgIzJjM2U1MFxuXG4gICYgc3ZnIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYm90dG9tOiAwO1xuICAgIHdpZHRoOiAxMDAlO1xuXG4gICAgJiBwYXRoIHtcbiAgICAgIGZpbGw6ICNmZmZmZmY7XG4gICAgfVxuICB9XG5cbiAgJl9fY29udGFpbmVyIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBtYXJnaW46IGF1dG87XG4gICAgY29sb3I6IHdoaXRlO1xuXG4gICAgJiBkaXYge1xuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuXG4gICAgICAmIHAge1xuICAgICAgICBtYXJnaW46IDIwIDAgMjAgMDtcbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cblxuc2VjdGlvbiB7XG4gIHBhZGRpbmc6IDEwMHB4IDA7XG4gICYud29yZHByZXNzLWhvc3Rpbmcge1xuICAgIGxpbmUtaGVpZ2h0OiAyO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgfVxuICAmLmNvbG9yIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjhmYWZlO1xuXG4gICAgJi0tYmcge1xuICAgICAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCByZ2JhKDQxLCAxMjgsIDE4NSwgMSksIHJnYmEoNDQsIDYyLCA4MCwgMC41KSksXG4gICAgICAgIHVybChcIi9hc3NldHMvaW1hZ2VzL2JnXzAyLmpwZ1wiKTtcbiAgICAgIGJhY2tncm91bmQtYXR0YWNobWVudDogZml4ZWQ7XG4gICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXIgdG9wO1xuICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgfVxuICB9XG4gXG4gICYgLmNvbnRhaW5lciB7XG4gICAgcGFkZGluZzogMzBweCAwcHg7XG4gICAgbWF4LXdpZHRoOiAxMTQwcHg7XG4gICAgbWFyZ2luOiBhdXRvO1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG5cbiAgICAmIC5mbGV4IHtcbiAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgfVxuXG4gICAgJiAuaGVhZGluZyB7XG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgfVxuXG4gICAgJiBoMiB7XG4gICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgICAgZm9udC1zaXplOiAyZW07XG4gICAgICBjb2xvcjogIzI4M2U1MTtcbiAgICB9XG5cbiAgICAmIC50YWIge1xuICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgIGZsZXgtd3JhcDogd3JhcDtcbiAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgIGJveC1zaGFkb3c6IDBweCAyMHB4IDQ1cHggMHB4IHJnYmEoMCwgMCwgMCwgMC4wOCk7XG4gICAgICBib3JkZXItcmFkaXVzOiAxLjYxNzY1cmVtO1xuICAgICAgbWF4LXdpZHRoOiAzNzBweDtcbiAgICAgIG1hcmdpbjogMCBhdXRvO1xuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgcGFkZGluZzogNXB4IDA7XG4gICAgICB6LWluZGV4OiAxO1xuXG4gICAgICAmIGxpIHtcbiAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgICAgICBtYXJnaW46IDA7XG4gICAgICAgIHBhZGRpbmc6IDA7XG4gICAgICAgIGxpc3Qtc3R5bGUtdHlwZTogbm9uZTtcbiAgICAgICAgLXdlYmtpdC10YXAtaGlnaGxpZ2h0LWNvbG9yOiB0cmFuc3BhcmVudDtcblxuICAgICAgICAmIGEge1xuICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDEuMzUyOTRyZW07XG4gICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgY29sb3I6ICNmZmZmZmY7XG4gICAgICAgICAgbWluLXdpZHRoOiAxMjVweDtcbiAgICAgICAgICBwYWRkaW5nOiA4cHggMzFweDtcbiAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICAgICAgfVxuXG4gICAgICAgICYgI3llYXJseS10YWIge1xuICAgICAgICAgIGNvbG9yOiAjMmMzZTUwO1xuICAgICAgICB9XG5cbiAgICAgICAgJiNpbmRpY2F0b3Ige1xuICAgICAgICAgIHdpZHRoOiAxNTBweDtcbiAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMTg2OGRkO1xuICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICBoZWlnaHQ6IGNhbGMoMTAwJSAtIDEwcHgpO1xuICAgICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNTAlKTtcbiAgICAgICAgICB0b3A6IDUwJTtcbiAgICAgICAgICBib3JkZXItcmFkaXVzOiAxLjM1Mjk0cmVtO1xuICAgICAgICAgIHotaW5kZXg6IC0xO1xuICAgICAgICAgIGxlZnQ6IDVweDtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuICAgICYgLmNvbHVtbiB7XG4gICAgICBwYWRkaW5nOiAyMHB4O1xuICAgICAgZmxleDogMTtcbiAgICAgIGxpbmUtaGVpZ2h0OiAxLjU7XG4gICAgICBjb2xvcjogIzI5ODBiOTtcblxuICAgICAgLmNvbHVtbl9faWNvbiB7XG4gICAgICAgIGhlaWdodDogMjQ0cHg7XG4gICAgICAgIHdpZHRoOiAyNDRweDtcbiAgICAgIH1cblxuICAgICAgLmNvbHVtbl9faWNvbixcbiAgICAgIC5jYXJkX19pY29uIHtcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICBtYXJnaW46IGF1dG87XG5cbiAgICAgICAgJiBpbWcge1xuICAgICAgICAgIG1heC1oZWlnaHQ6IDEwMCU7XG4gICAgICAgICAgbWF4LXdpZHRoOiAxMDAlO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIC5jYXJkX19pY29uIHtcbiAgICAgICAgJiBpbWcge1xuICAgICAgICAgIG1heC1oZWlnaHQ6IDUwJTtcbiAgICAgICAgICBtYXgtd2lkdGg6IDUwJTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICAmX19ib2R5IHtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuXG4gICAgICAgICYgaDMge1xuICAgICAgICAgIGZvbnQtc2l6ZTogMS41ZW07XG4gICAgICAgICAgY29sb3I6ICMyODNlNTE7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgJiAuY2FyZCB7XG4gICAgICAgIGJveC1zaGFkb3c6IDAgNHB4IDhweCAwIHJnYmEoMCwgMCwgMCwgMC4yKTtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICAgICAgcGFkZGluZzogMTZweDtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuXG4gICAgICAgICZfX2JvZHkge1xuICAgICAgICAgIHBhZGRpbmc6IDJlbTtcbiAgICAgICAgfVxuXG4gICAgICAgICZfX3RpdGxlIHtcbiAgICAgICAgICBmb250LXNpemU6IDEuNWVtO1xuICAgICAgICAgIGNvbG9yOiAjMjgzZTUxO1xuICAgICAgICB9XG5cbiAgICAgICAgJl9fdGV4dCB7XG4gICAgICAgICAgbGluZS1oZWlnaHQ6IDEuNTtcbiAgICAgICAgICAmIGxpIHtcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XG4gICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgJl9fcHJpY2Uge1xuICAgICAgICAgIGZvbnQtZmFtaWx5OiBcIkFyaWFsIEJsYWNrXCI7XG4gICAgICAgICAgZm9udC1zaXplOiAzZW07XG4gICAgICAgICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KCMyYzNlNTAsICMyOTgwYjkpO1xuICAgICAgICAgIC13ZWJraXQtYmFja2dyb3VuZC1jbGlwOiB0ZXh0O1xuICAgICAgICAgIC13ZWJraXQtdGV4dC1maWxsLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICAmIC5hY3RpdmUge1xuICAgICAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoIzJjM2U1MCwgIzI5ODBiOSk7XG4gICAgICAgIHBhZGRpbmc6IDUwcHggMTZweDtcbiAgICAgICAgJiAuY2FyZF9fdGl0bGUsXG4gICAgICAgIC5jYXJkX19ib2R5LFxuICAgICAgICAuY2FyZF9fcHJpY2Uge1xuICAgICAgICAgIGNvbG9yOiAjZmZmZmZmO1xuICAgICAgICB9XG4gICAgICAgICYgLmNhcmRfX3ByaWNlIHtcbiAgICAgICAgICAtd2Via2l0LXRleHQtZmlsbC1jb2xvcjogaW5pdGlhbDtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gICYgI3NlY29uZC10YWIge1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgb3BhY2l0eTogMDtcbiAgfVxuXG4gIC5jaGlsZCB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICYgaDEge1xuICAgICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAgIGZvbnQtc2l6ZTogMmVtO1xuICAgIH1cbiAgfVxufVxuXG5AbWVkaWEgKG1pbi13aWR0aDogNzY4cHgpIGFuZCAgKG1heC13aWR0aDogMTM2NnB4KSB7XG4gIC5oZWFkZXJfX2NvbnRhaW5lciB7XG4gICAgbWF4LXdpZHRoOiA5NjBweDtcbiAgICAmIGRpdiB7XG4gICAgICB3aWR0aDogNTAlO1xuICAgIH1cbiAgfVxuICBzZWN0aW9uIHtcbiAgICBwYWRkaW5nOiAxMDBweCAwO1xuICAgICYgLmNvbnRhaW5lciB7XG4gICAgICB3aWR0aDogODUlO1xuICAgICAgLmZsZXgge1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgICAmIC5jYXJkIC5jYXJkX19ib2R5IHtcbiAgICAgICAgICBwYWRkaW5nOiAyZW0gMGVtO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICAgICYgICNmaXJzdC10YWIsICNzZWNvbmQtdGFie1xuICAgICAgLmZsZXgge1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgfVxuICAgIH1cbiAgfVxuICAjd2ViaG9zdGluZy1oZWxwIHtcbiAgICBmbGV4LXdyYXA6IHdyYXA7XG4gICAgJiAuY29sdW1uIHtcbiAgICAgIGZsZXgtYmFzaXM6IDUwJTtcbiAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAmIC5jYXJkIHtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cblxuQG1lZGlhIChtaW4td2lkdGg6IDc2OHB4KSB7XG4gIC5oZWFkZXJfX2NvbnRhaW5lciB7XG4gICAgbWF4LXdpZHRoOiA4NSU7XG4gICAgJiBkaXY6Zmlyc3QtY2hpbGQge1xuICAgICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICAgIHdpZHRoOiA4MCU7XG4gICAgICBtYXJnaW4tdG9wOiA1MHB4O1xuICAgIH1cbiAgICAmIGgxIHtcbiAgICAgIGZvbnQtc2l6ZTogMmVtO1xuICAgIH1cbiAgfVxufVxuXG5AbWVkaWEgKG1pbi13aWR0aDogMTAyNHB4KSBhbmQgIChtYXgtd2lkdGg6IDEzNjZweCkge1xuICBzZWN0aW9uIHtcbiAgICAmICAjZmlyc3QtdGFiLCAjc2Vjb25kLXRhYntcbiAgICAgIC5mbGV4IHtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cblxuXG5AbWVkaWEgKG1pbi13aWR0aDogMTM2NnB4KSB7XG4gIC5oZWFkZXJfX2NvbnRhaW5lciB7XG4gICAgbWF4LXdpZHRoOiAxMjAwcHg7XG4gICAgJiBkaXY6Zmlyc3QtY2hpbGQge1xuICAgICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICAgIHdpZHRoOiA4MCU7XG4gICAgICBtYXJnaW4tdG9wOiA1MHB4O1xuICAgIH1cbiAgICAmIGgxIHtcbiAgICAgIGZvbnQtc2l6ZTogMy41cmVtO1xuICAgIH1cbiAgfVxuICBzZWN0aW9uIHtcbiAgICAuY29udGFpbmVyIHtcbiAgICAgIC5mbGV4IHtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAgJiN3ZWJob3N0aW5nLWhlbHAge1xuICAgICAgICAgIGZsZXgtd3JhcDogd3JhcDtcbiAgICAgICAgICAmIC5jb2x1bW4ge1xuICAgICAgICAgICAgZmxleC1iYXNpczogNTAlO1xuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgICYgLmNhcmQge1xuICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxufVxuXG5AbWVkaWEgKG1pbi13aWR0aDogODEzcHgpIHtcbiAgLmhlYWRlcl9fY29udGFpbmVyIHtcbiAgICAmIGRpdiB7XG4gICAgICB3aWR0aDogNzUlO1xuICAgIH1cbiAgfVxuXG4gIC5oZWFkZXIge1xuICAgICZfX2NvbnRhaW5lciB7XG4gICAgICAmIGRpdjpmaXJzdC1jaGlsZCB7XG4gICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG5cbiAgICAgICAgJiBoMSB7XG4gICAgICAgICAgZm9udC1zaXplOiAzLjVyZW07XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cbn0iLCIuaGVhZGVyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBoZWlnaHQ6IDc1MHB4O1xuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICMyYzNlNTAsIHJnYmEoNDEsIDEyOCwgMTg1LCAwLjUpKSwgdXJsKFwiL2Fzc2V0cy9pbWFnZXMvc2VydmljZXMtYmcuanBnXCIpO1xuICBiYWNrZ3JvdW5kLWF0dGFjaG1lbnQ6IGZpeGVkO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiA1MCUgNTAlO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xufVxuLmhlYWRlciBzdmcge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJvdHRvbTogMDtcbiAgd2lkdGg6IDEwMCU7XG59XG4uaGVhZGVyIHN2ZyBwYXRoIHtcbiAgZmlsbDogI2ZmZmZmZjtcbn1cbi5oZWFkZXJfX2NvbnRhaW5lciB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIG1hcmdpbjogYXV0bztcbiAgY29sb3I6IHdoaXRlO1xufVxuLmhlYWRlcl9fY29udGFpbmVyIGRpdiB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5oZWFkZXJfX2NvbnRhaW5lciBkaXYgcCB7XG4gIG1hcmdpbjogMjAgMCAyMCAwO1xufVxuXG5zZWN0aW9uIHtcbiAgcGFkZGluZzogMTAwcHggMDtcbn1cbnNlY3Rpb24ud29yZHByZXNzLWhvc3Rpbmcge1xuICBsaW5lLWhlaWdodDogMjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuc2VjdGlvbi5jb2xvciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmOGZhZmU7XG59XG5zZWN0aW9uLmNvbG9yLS1iZyB7XG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgIzI5ODBiOSwgcmdiYSg0NCwgNjIsIDgwLCAwLjUpKSwgdXJsKFwiL2Fzc2V0cy9pbWFnZXMvYmdfMDIuanBnXCIpO1xuICBiYWNrZ3JvdW5kLWF0dGFjaG1lbnQ6IGZpeGVkO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXIgdG9wO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xufVxuc2VjdGlvbiAuY29udGFpbmVyIHtcbiAgcGFkZGluZzogMzBweCAwcHg7XG4gIG1heC13aWR0aDogMTE0MHB4O1xuICBtYXJnaW46IGF1dG87XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG5zZWN0aW9uIC5jb250YWluZXIgLmZsZXgge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbn1cbnNlY3Rpb24gLmNvbnRhaW5lciAuaGVhZGluZyB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbnNlY3Rpb24gLmNvbnRhaW5lciBoMiB7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGZvbnQtc2l6ZTogMmVtO1xuICBjb2xvcjogIzI4M2U1MTtcbn1cbnNlY3Rpb24gLmNvbnRhaW5lciAudGFiIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC13cmFwOiB3cmFwO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGJveC1zaGFkb3c6IDBweCAyMHB4IDQ1cHggMHB4IHJnYmEoMCwgMCwgMCwgMC4wOCk7XG4gIGJvcmRlci1yYWRpdXM6IDEuNjE3NjVyZW07XG4gIG1heC13aWR0aDogMzcwcHg7XG4gIG1hcmdpbjogMCBhdXRvO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgcGFkZGluZzogNXB4IDA7XG4gIHotaW5kZXg6IDE7XG59XG5zZWN0aW9uIC5jb250YWluZXIgLnRhYiBsaSB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgbWFyZ2luOiAwO1xuICBwYWRkaW5nOiAwO1xuICBsaXN0LXN0eWxlLXR5cGU6IG5vbmU7XG4gIC13ZWJraXQtdGFwLWhpZ2hsaWdodC1jb2xvcjogdHJhbnNwYXJlbnQ7XG59XG5zZWN0aW9uIC5jb250YWluZXIgLnRhYiBsaSBhIHtcbiAgYm9yZGVyLXJhZGl1czogMS4zNTI5NHJlbTtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICBtaW4td2lkdGg6IDEyNXB4O1xuICBwYWRkaW5nOiA4cHggMzFweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cbnNlY3Rpb24gLmNvbnRhaW5lciAudGFiIGxpICN5ZWFybHktdGFiIHtcbiAgY29sb3I6ICMyYzNlNTA7XG59XG5zZWN0aW9uIC5jb250YWluZXIgLnRhYiBsaSNpbmRpY2F0b3Ige1xuICB3aWR0aDogMTUwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICMxODY4ZGQ7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgaGVpZ2h0OiBjYWxjKDEwMCUgLSAxMHB4KTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xuICB0b3A6IDUwJTtcbiAgYm9yZGVyLXJhZGl1czogMS4zNTI5NHJlbTtcbiAgei1pbmRleDogLTE7XG4gIGxlZnQ6IDVweDtcbn1cbnNlY3Rpb24gLmNvbnRhaW5lciAuY29sdW1uIHtcbiAgcGFkZGluZzogMjBweDtcbiAgZmxleDogMTtcbiAgbGluZS1oZWlnaHQ6IDEuNTtcbiAgY29sb3I6ICMyOTgwYjk7XG59XG5zZWN0aW9uIC5jb250YWluZXIgLmNvbHVtbiAuY29sdW1uX19pY29uIHtcbiAgaGVpZ2h0OiAyNDRweDtcbiAgd2lkdGg6IDI0NHB4O1xufVxuc2VjdGlvbiAuY29udGFpbmVyIC5jb2x1bW4gLmNvbHVtbl9faWNvbixcbnNlY3Rpb24gLmNvbnRhaW5lciAuY29sdW1uIC5jYXJkX19pY29uIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtYXJnaW46IGF1dG87XG59XG5zZWN0aW9uIC5jb250YWluZXIgLmNvbHVtbiAuY29sdW1uX19pY29uIGltZyxcbnNlY3Rpb24gLmNvbnRhaW5lciAuY29sdW1uIC5jYXJkX19pY29uIGltZyB7XG4gIG1heC1oZWlnaHQ6IDEwMCU7XG4gIG1heC13aWR0aDogMTAwJTtcbn1cbnNlY3Rpb24gLmNvbnRhaW5lciAuY29sdW1uIC5jYXJkX19pY29uIGltZyB7XG4gIG1heC1oZWlnaHQ6IDUwJTtcbiAgbWF4LXdpZHRoOiA1MCU7XG59XG5zZWN0aW9uIC5jb250YWluZXIgLmNvbHVtbl9fYm9keSB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbnNlY3Rpb24gLmNvbnRhaW5lciAuY29sdW1uX19ib2R5IGgzIHtcbiAgZm9udC1zaXplOiAxLjVlbTtcbiAgY29sb3I6ICMyODNlNTE7XG59XG5zZWN0aW9uIC5jb250YWluZXIgLmNvbHVtbiAuY2FyZCB7XG4gIGJveC1zaGFkb3c6IDAgNHB4IDhweCAwIHJnYmEoMCwgMCwgMCwgMC4yKTtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgcGFkZGluZzogMTZweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuc2VjdGlvbiAuY29udGFpbmVyIC5jb2x1bW4gLmNhcmRfX2JvZHkge1xuICBwYWRkaW5nOiAyZW07XG59XG5zZWN0aW9uIC5jb250YWluZXIgLmNvbHVtbiAuY2FyZF9fdGl0bGUge1xuICBmb250LXNpemU6IDEuNWVtO1xuICBjb2xvcjogIzI4M2U1MTtcbn1cbnNlY3Rpb24gLmNvbnRhaW5lciAuY29sdW1uIC5jYXJkX190ZXh0IHtcbiAgbGluZS1oZWlnaHQ6IDEuNTtcbn1cbnNlY3Rpb24gLmNvbnRhaW5lciAuY29sdW1uIC5jYXJkX190ZXh0IGxpIHtcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbn1cbnNlY3Rpb24gLmNvbnRhaW5lciAuY29sdW1uIC5jYXJkX19wcmljZSB7XG4gIGZvbnQtZmFtaWx5OiBcIkFyaWFsIEJsYWNrXCI7XG4gIGZvbnQtc2l6ZTogM2VtO1xuICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoIzJjM2U1MCwgIzI5ODBiOSk7XG4gIC13ZWJraXQtYmFja2dyb3VuZC1jbGlwOiB0ZXh0O1xuICAtd2Via2l0LXRleHQtZmlsbC1jb2xvcjogdHJhbnNwYXJlbnQ7XG59XG5zZWN0aW9uIC5jb250YWluZXIgLmNvbHVtbiAuYWN0aXZlIHtcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KCMyYzNlNTAsICMyOTgwYjkpO1xuICBwYWRkaW5nOiA1MHB4IDE2cHg7XG59XG5zZWN0aW9uIC5jb250YWluZXIgLmNvbHVtbiAuYWN0aXZlIC5jYXJkX190aXRsZSxcbnNlY3Rpb24gLmNvbnRhaW5lciAuY29sdW1uIC5hY3RpdmUgLmNhcmRfX2JvZHksXG5zZWN0aW9uIC5jb250YWluZXIgLmNvbHVtbiAuYWN0aXZlIC5jYXJkX19wcmljZSB7XG4gIGNvbG9yOiAjZmZmZmZmO1xufVxuc2VjdGlvbiAuY29udGFpbmVyIC5jb2x1bW4gLmFjdGl2ZSAuY2FyZF9fcHJpY2Uge1xuICAtd2Via2l0LXRleHQtZmlsbC1jb2xvcjogaW5pdGlhbDtcbn1cbnNlY3Rpb24gI3NlY29uZC10YWIge1xuICBkaXNwbGF5OiBub25lO1xuICBvcGFjaXR5OiAwO1xufVxuc2VjdGlvbiAuY2hpbGQge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5zZWN0aW9uIC5jaGlsZCBoMSB7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAyZW07XG59XG5cbkBtZWRpYSAobWluLXdpZHRoOiA3NjhweCkgYW5kIChtYXgtd2lkdGg6IDEzNjZweCkge1xuICAuaGVhZGVyX19jb250YWluZXIge1xuICAgIG1heC13aWR0aDogOTYwcHg7XG4gIH1cbiAgLmhlYWRlcl9fY29udGFpbmVyIGRpdiB7XG4gICAgd2lkdGg6IDUwJTtcbiAgfVxuXG4gIHNlY3Rpb24ge1xuICAgIHBhZGRpbmc6IDEwMHB4IDA7XG4gIH1cbiAgc2VjdGlvbiAuY29udGFpbmVyIHtcbiAgICB3aWR0aDogODUlO1xuICB9XG4gIHNlY3Rpb24gLmNvbnRhaW5lciAuZmxleCB7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgfVxuICBzZWN0aW9uIC5jb250YWluZXIgLmZsZXggLmNhcmQgLmNhcmRfX2JvZHkge1xuICAgIHBhZGRpbmc6IDJlbSAwZW07XG4gIH1cbiAgc2VjdGlvbiAjZmlyc3QtdGFiIC5mbGV4LCBzZWN0aW9uICNzZWNvbmQtdGFiIC5mbGV4IHtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICB9XG5cbiAgI3dlYmhvc3RpbmctaGVscCB7XG4gICAgZmxleC13cmFwOiB3cmFwO1xuICB9XG4gICN3ZWJob3N0aW5nLWhlbHAgLmNvbHVtbiB7XG4gICAgZmxleC1iYXNpczogNTAlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gIH1cbiAgI3dlYmhvc3RpbmctaGVscCAuY29sdW1uIC5jYXJkIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogNzY4cHgpIHtcbiAgLmhlYWRlcl9fY29udGFpbmVyIHtcbiAgICBtYXgtd2lkdGg6IDg1JTtcbiAgfVxuICAuaGVhZGVyX19jb250YWluZXIgZGl2OmZpcnN0LWNoaWxkIHtcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgIHdpZHRoOiA4MCU7XG4gICAgbWFyZ2luLXRvcDogNTBweDtcbiAgfVxuICAuaGVhZGVyX19jb250YWluZXIgaDEge1xuICAgIGZvbnQtc2l6ZTogMmVtO1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogMTAyNHB4KSBhbmQgKG1heC13aWR0aDogMTM2NnB4KSB7XG4gIHNlY3Rpb24gI2ZpcnN0LXRhYiAuZmxleCwgc2VjdGlvbiAjc2Vjb25kLXRhYiAuZmxleCB7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6IDEzNjZweCkge1xuICAuaGVhZGVyX19jb250YWluZXIge1xuICAgIG1heC13aWR0aDogMTIwMHB4O1xuICB9XG4gIC5oZWFkZXJfX2NvbnRhaW5lciBkaXY6Zmlyc3QtY2hpbGQge1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgd2lkdGg6IDgwJTtcbiAgICBtYXJnaW4tdG9wOiA1MHB4O1xuICB9XG4gIC5oZWFkZXJfX2NvbnRhaW5lciBoMSB7XG4gICAgZm9udC1zaXplOiAzLjVyZW07XG4gIH1cblxuICBzZWN0aW9uIC5jb250YWluZXIgLmZsZXgge1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIH1cbiAgc2VjdGlvbiAuY29udGFpbmVyIC5mbGV4I3dlYmhvc3RpbmctaGVscCB7XG4gICAgZmxleC13cmFwOiB3cmFwO1xuICB9XG4gIHNlY3Rpb24gLmNvbnRhaW5lciAuZmxleCN3ZWJob3N0aW5nLWhlbHAgLmNvbHVtbiB7XG4gICAgZmxleC1iYXNpczogNTAlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gIH1cbiAgc2VjdGlvbiAuY29udGFpbmVyIC5mbGV4I3dlYmhvc3RpbmctaGVscCAuY29sdW1uIC5jYXJkIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogODEzcHgpIHtcbiAgLmhlYWRlcl9fY29udGFpbmVyIGRpdiB7XG4gICAgd2lkdGg6IDc1JTtcbiAgfVxuXG4gIC5oZWFkZXJfX2NvbnRhaW5lciBkaXY6Zmlyc3QtY2hpbGQge1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gIH1cbiAgLmhlYWRlcl9fY29udGFpbmVyIGRpdjpmaXJzdC1jaGlsZCBoMSB7XG4gICAgZm9udC1zaXplOiAzLjVyZW07XG4gIH1cbn0iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ProductsComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-products',
                templateUrl: './products.component.html',
                styleUrls: ['./products.component.scss']
            }]
    }], function () { return [{ type: _pricing_tab_service__WEBPACK_IMPORTED_MODULE_1__["PricingTabService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/shared/footer/footer.component.ts":
/*!***************************************************!*\
  !*** ./src/app/shared/footer/footer.component.ts ***!
  \***************************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");



class FooterComponent {
    constructor() { }
    ngOnInit() {
    }
}
FooterComponent.ɵfac = function FooterComponent_Factory(t) { return new (t || FooterComponent)(); };
FooterComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: FooterComponent, selectors: [["app-footer"]], decls: 27, vars: 0, consts: [[1, "footer"], [1, "container"], [1, "flex"], [1, "column"], ["routerLink", "/about", "routerLinkActive", "active", 1, "hover"], ["href", "#", "routerLink", "/wordpress-hosting", 1, "hover"], ["routerLink", "/wordpress-maintenance", 1, "hover"], [1, "footer__contact", "column"], ["routerLink", ""], ["src", "assets/images/logo.png", 1, "logo"]], template: function FooterComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "footer", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "a", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "About");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "Services");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "a", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Wordpress Hosting");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "a", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "Wordpress Maintenance");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "Contact Us");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "a", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](20, "img", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "HostFront");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, "\u00A9 2020 HostFront. all rights reserved.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, "sales@quickhostuk.com");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLinkWithHref"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLinkActive"]], styles: [".footer[_ngcontent-%COMP%] {\n  background: linear-gradient(to right, #2980b9, #2c3e50);\n  padding: 1.5rem 1.5rem 2.5rem 1.5rem;\n  color: #ffffff;\n}\n.footer[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  text-decoration: none;\n  color: #ffffff;\n  position: relative;\n  display: block;\n}\n.footer[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   a.hover[_ngcontent-%COMP%]:before {\n  content: \"\";\n  position: absolute;\n  width: 0;\n  height: 2.5px;\n  bottom: -8px;\n  left: 0;\n  background: #fff;\n  visibility: hidden;\n  \n  transition: all 0.3s ease-in-out 0s;\n}\n.footer[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   a.hover[_ngcontent-%COMP%]:hover:before {\n  visibility: visible;\n  width: 100%;\n}\n.footer__contact[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  color: #ffffff;\n  text-decoration: none;\n  display: inline-flex !important;\n  align-items: center;\n}\n.footer__contact[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]   .logo[_ngcontent-%COMP%] {\n  width: 75px;\n}\n.footer__contact[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  font-size: 1.5em;\n  font-weight: bold;\n  font-family: \"Open Sans\", sans-serif;\n}\n.footer__contact[_ngcontent-%COMP%]   h3[_ngcontent-%COMP%], .footer__contact[_ngcontent-%COMP%]   .copyright[_ngcontent-%COMP%] {\n  padding-left: 1em;\n}\n.footer[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%] {\n  padding: 30px 0px;\n}\n.footer[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%] {\n  display: flex;\n  flex-direction: column;\n  justify-content: space-around;\n  text-align: center;\n}\n.footer[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%] {\n  padding: 0;\n}\n.footer[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   li[_ngcontent-%COMP%] {\n  list-style: none;\n}\n@media (min-width: 768px) {\n  .footer[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%] {\n    flex-direction: row;\n    text-align: left;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2NoaWRvL3Byb2plY3RzL2hvc3Rmcm9udC1wcm9qZWN0L2hvc3Rmcm9udC1mcm9udGVuZC9zcmMvYXBwL3NoYXJlZC9mb290ZXIvZm9vdGVyLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9zaGFyZWQvZm9vdGVyL2Zvb3Rlci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHVEQUFBO0VBQ0Esb0NBQUE7RUFDQSxjQUFBO0FDQ0Y7QURBRTtFQUNFLHFCQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtBQ0VKO0FEQU07RUFDRSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxRQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7RUFDQSxPQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLDRDQUFBO0VBQ0EsbUNBQUE7QUNFUjtBREFNO0VBQ0UsbUJBQUE7RUFDQSxXQUFBO0FDRVI7QURHSTtFQUNFLGNBQUE7RUFDQSxxQkFBQTtFQUNBLCtCQUFBO0VBQ0EsbUJBQUE7QUNETjtBREVNO0VBQ0UsV0FBQTtBQ0FSO0FERU07RUFDRSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0Esb0NBQUE7QUNBUjtBREdJO0VBQ0UsaUJBQUE7QUNETjtBREtFO0VBQ0UsaUJBQUE7QUNISjtBREtJO0VBQ0UsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsNkJBQUE7RUFDQSxrQkFBQTtBQ0hOO0FESU07RUFDRSxVQUFBO0FDRlI7QURHUTtFQUNFLGdCQUFBO0FDRFY7QURRQTtFQUVJO0lBQ0UsbUJBQUE7SUFDQSxnQkFBQTtFQ05KO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9zaGFyZWQvZm9vdGVyL2Zvb3Rlci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5mb290ZXIge1xuICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICMyOTgwYjksICMyYzNlNTApO1xuICBwYWRkaW5nOiAxLjVyZW0gMS41cmVtIDIuNXJlbSAxLjVyZW07XG4gIGNvbG9yOiAjZmZmZmZmO1xuICAmIC5jb2x1bW4gYSB7XG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICAgIGNvbG9yOiAjZmZmZmZmO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICAmLmhvdmVyIHtcbiAgICAgICY6YmVmb3JlIHtcbiAgICAgICAgY29udGVudDogXCJcIjtcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICB3aWR0aDogMDtcbiAgICAgICAgaGVpZ2h0OiAyLjVweDtcbiAgICAgICAgYm90dG9tOiAtOHB4O1xuICAgICAgICBsZWZ0OiAwO1xuICAgICAgICBiYWNrZ3JvdW5kOiAjZmZmO1xuICAgICAgICB2aXNpYmlsaXR5OiBoaWRkZW47XG4gICAgICAgIC8qICBBbmltYXRlIHdpdGggYSBkdXJhdGlvbiBvZiAwLjMgc2Vjb25kcyAqL1xuICAgICAgICB0cmFuc2l0aW9uOiBhbGwgMC4zcyBlYXNlLWluLW91dCAwcztcbiAgICAgIH1cbiAgICAgICY6aG92ZXI6YmVmb3JlIHtcbiAgICAgICAgdmlzaWJpbGl0eTogdmlzaWJsZTtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICB9XG4gICAgfVxuICB9XG4gICZfX2NvbnRhY3Qge1xuICAgICYgYSB7XG4gICAgICBjb2xvcjogI2ZmZmZmZjtcbiAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgICAgIGRpc3BsYXk6IGlubGluZS1mbGV4ICFpbXBvcnRhbnQ7XG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgJiAubG9nbyB7XG4gICAgICAgIHdpZHRoOiA3NXB4O1xuICAgICAgfVxuICAgICAgJiBzcGFuIHtcbiAgICAgICAgZm9udC1zaXplOiAxLjVlbTtcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgICAgIGZvbnQtZmFtaWx5OiBcIk9wZW4gU2Fuc1wiLCBzYW5zLXNlcmlmO1xuICAgICAgfVxuICAgIH1cbiAgICAmIGgzLCAuY29weXJpZ2h0IHtcbiAgICAgIHBhZGRpbmctbGVmdDogMWVtO1xuICAgIH1cbiAgfVxuXG4gICYgLmNvbnRhaW5lciB7XG4gICAgcGFkZGluZzogMzBweCAwcHg7XG4gIFxuICAgICYgLmZsZXgge1xuICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICYgdWwge1xuICAgICAgICBwYWRkaW5nOiAwO1xuICAgICAgICAmIGxpIHtcbiAgICAgICAgICBsaXN0LXN0eWxlOiBub25lO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG59XG5cbkBtZWRpYSAobWluLXdpZHRoOiA3NjhweCkge1xuICAuZm9vdGVyIHtcbiAgICAmIC5jb250YWluZXIgLmZsZXgge1xuICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgfVxuICB9XG59XG4iLCIuZm9vdGVyIHtcbiAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjMjk4MGI5LCAjMmMzZTUwKTtcbiAgcGFkZGluZzogMS41cmVtIDEuNXJlbSAyLjVyZW0gMS41cmVtO1xuICBjb2xvcjogI2ZmZmZmZjtcbn1cbi5mb290ZXIgLmNvbHVtbiBhIHtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBkaXNwbGF5OiBibG9jaztcbn1cbi5mb290ZXIgLmNvbHVtbiBhLmhvdmVyOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgd2lkdGg6IDA7XG4gIGhlaWdodDogMi41cHg7XG4gIGJvdHRvbTogLThweDtcbiAgbGVmdDogMDtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbiAgdmlzaWJpbGl0eTogaGlkZGVuO1xuICAvKiAgQW5pbWF0ZSB3aXRoIGEgZHVyYXRpb24gb2YgMC4zIHNlY29uZHMgKi9cbiAgdHJhbnNpdGlvbjogYWxsIDAuM3MgZWFzZS1pbi1vdXQgMHM7XG59XG4uZm9vdGVyIC5jb2x1bW4gYS5ob3Zlcjpob3ZlcjpiZWZvcmUge1xuICB2aXNpYmlsaXR5OiB2aXNpYmxlO1xuICB3aWR0aDogMTAwJTtcbn1cbi5mb290ZXJfX2NvbnRhY3QgYSB7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gIGRpc3BsYXk6IGlubGluZS1mbGV4ICFpbXBvcnRhbnQ7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4uZm9vdGVyX19jb250YWN0IGEgLmxvZ28ge1xuICB3aWR0aDogNzVweDtcbn1cbi5mb290ZXJfX2NvbnRhY3QgYSBzcGFuIHtcbiAgZm9udC1zaXplOiAxLjVlbTtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGZvbnQtZmFtaWx5OiBcIk9wZW4gU2Fuc1wiLCBzYW5zLXNlcmlmO1xufVxuLmZvb3Rlcl9fY29udGFjdCBoMywgLmZvb3Rlcl9fY29udGFjdCAuY29weXJpZ2h0IHtcbiAgcGFkZGluZy1sZWZ0OiAxZW07XG59XG4uZm9vdGVyIC5jb250YWluZXIge1xuICBwYWRkaW5nOiAzMHB4IDBweDtcbn1cbi5mb290ZXIgLmNvbnRhaW5lciAuZmxleCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uZm9vdGVyIC5jb250YWluZXIgLmZsZXggdWwge1xuICBwYWRkaW5nOiAwO1xufVxuLmZvb3RlciAuY29udGFpbmVyIC5mbGV4IHVsIGxpIHtcbiAgbGlzdC1zdHlsZTogbm9uZTtcbn1cblxuQG1lZGlhIChtaW4td2lkdGg6IDc2OHB4KSB7XG4gIC5mb290ZXIgLmNvbnRhaW5lciAuZmxleCB7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICB9XG59Il19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](FooterComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-footer',
                templateUrl: './footer.component.html',
                styleUrls: ['./footer.component.scss']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/shared/navbar/navbar.component.ts":
/*!***************************************************!*\
  !*** ./src/app/shared/navbar/navbar.component.ts ***!
  \***************************************************/
/*! exports provided: NavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarComponent", function() { return NavbarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");




class NavbarComponent {
    constructor() { }
    ngOnInit() {
    }
    responsiveNav() {
        var x = document.getElementById("myTopnav");
        if (x.className === "navbar") {
            x.className += " responsive";
        }
        else {
            x.className = "navbar";
        }
    }
    onWindowScroll(e) {
        let element = document.querySelector('.navbar');
        if (window.pageYOffset > 300) {
            element.classList.add('navbar--scrolled');
        }
        else {
            element.classList.remove('navbar--scrolled');
        }
    }
}
NavbarComponent.ɵfac = function NavbarComponent_Factory(t) { return new (t || NavbarComponent)(); };
NavbarComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: NavbarComponent, selectors: [["app-navbar"]], hostBindings: function NavbarComponent_HostBindings(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("scroll", function NavbarComponent_scroll_HostBindingHandler($event) { return ctx.onWindowScroll($event); }, false, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵresolveWindow"]);
    } }, decls: 33, vars: 0, consts: [[1, "topbar"], [1, "topbar__container"], [1, "topbar__wrapper"], [1, "topbar__contacts"], ["id", "myTopnav", 1, "navbar"], [1, "navbar__container"], [1, "navbar__container__brand"], ["routerLink", ""], ["src", "assets/images/logo.png", 1, "logo"], ["href", "javascript:void(0);", 1, "icon", 3, "click"], [1, "fa", "fa-bars"], [1, "navbar__container__main-nav"], ["routerLink", "/about", "routerLinkActive", "active", 1, "hover"], [1, "dropdown"], ["routerLinkActive", "active", 1, "hover"], [1, "fa", "fa-caret-down"], [1, "dropdown__content"], ["routerLink", "/wordpress-hosting"], ["routerLink", "/wordpress-maintenance"], ["routerLink", "/contact", "routerLinkActive", "active", 1, "hover"]], template: function NavbarComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "020 8081 0255");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "sales@quickhostuk.com");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "nav", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "img", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "HostFront");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "a", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function NavbarComponent_Template_a_click_15_listener() { return ctx.responsiveNav(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "i", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "ul", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "a", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "About");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "li", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "a", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, "Services ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](24, "i", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "a", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, "Wordpress Hosting");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "a", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "Wordpress Maintenance");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "a", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32, "Contact");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLinkWithHref"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLinkActive"]], styles: [".topbar[_ngcontent-%COMP%] {\n  width: 100%;\n  padding: 0 1.5em;\n  background-color: #16324a;\n  color: #ffffff;\n}\n.topbar__container[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: center;\n}\n.topbar__container[_ngcontent-%COMP%]   div[_ngcontent-%COMP%]   div[_ngcontent-%COMP%] {\n  padding: 20px 0;\n  margin: auto;\n}\n.topbar__contacts[_ngcontent-%COMP%] {\n  display: flex;\n  align-items: center;\n}\n.topbar__contacts[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n  margin: 0 0.5em;\n}\n.navbar[_ngcontent-%COMP%] {\n  padding: 0.5em;\n  width: 100%;\n  z-index: 2;\n}\n.navbar--scrolled[_ngcontent-%COMP%] {\n  background-color: rgba(0, 0, 0, 0.8);\n  transition: background-color 2s;\n}\n.navbar__container__brand[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: space-between;\n  align-items: center;\n  flex-direction: row;\n}\n.navbar__container__brand[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  display: flex;\n  align-items: center;\n}\n.navbar__container__brand[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]   .logo[_ngcontent-%COMP%] {\n  width: 75px;\n}\n.navbar__container__brand[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  font-size: 1.5em;\n  font-weight: bold;\n  font-family: \"Open Sans\", sans-serif;\n}\n.navbar__container__main-nav[_ngcontent-%COMP%] {\n  list-style-type: none;\n  margin: 0;\n  padding: 0;\n  display: none;\n}\n.navbar__container__main-nav[_ngcontent-%COMP%]   .dropdown[_ngcontent-%COMP%] {\n  overflow: hidden;\n  margin: 0 !important;\n  display: block;\n}\n.navbar__container__main-nav[_ngcontent-%COMP%]   .dropdown__content[_ngcontent-%COMP%] {\n  flex-direction: column;\n  display: none;\n  position: relative;\n}\n.navbar__container__main-nav[_ngcontent-%COMP%]   .dropdown__content[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:hover {\n  color: #16324a;\n  transition: color 1s;\n}\n.navbar__container__main-nav[_ngcontent-%COMP%]   .dropdown[_ngcontent-%COMP%]:hover   .dropdown__content[_ngcontent-%COMP%] {\n  display: flex;\n}\n.navbar__container[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  text-decoration: none;\n  margin: 10px 15px;\n  padding: 5px;\n  font-weight: bold;\n  color: #ffffff;\n  position: relative;\n  cursor: pointer;\n}\n.navbar__container[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]   .fa[_ngcontent-%COMP%] {\n  padding: 0 3px;\n}\n.navbar__container[_ngcontent-%COMP%]   a.hover[_ngcontent-%COMP%]:before {\n  content: \"\";\n  position: absolute;\n  width: 0;\n  height: 2.5px;\n  bottom: -2px;\n  left: 0;\n  background: #fff;\n  visibility: hidden;\n  \n  transition: all 0.3s ease-in-out 0s;\n}\n.navbar__container[_ngcontent-%COMP%]   a.hover[_ngcontent-%COMP%]:hover:before {\n  visibility: visible;\n  width: 100%;\n}\n@media (min-width: 768px) {\n  .navbar__container[_ngcontent-%COMP%] {\n    display: flex;\n    justify-content: space-between;\n    align-items: center;\n  }\n\n  .navbar__container__brand[_ngcontent-%COMP%], .navbar__container__main-nav[_ngcontent-%COMP%], .navbar__container__main-nav[_ngcontent-%COMP%]   div[_ngcontent-%COMP%] {\n    display: flex;\n  }\n  .navbar__container__brand[_ngcontent-%COMP%]   a[_ngcontent-%COMP%], .navbar__container__main-nav[_ngcontent-%COMP%]   a[_ngcontent-%COMP%], .navbar__container__main-nav[_ngcontent-%COMP%]   div[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n    display: flex;\n    align-items: center;\n  }\n\n  .navbar__container__brand[_ngcontent-%COMP%] {\n    flex-direction: column;\n    align-items: center;\n  }\n\n  #myTopnav[_ngcontent-%COMP%]   .icon[_ngcontent-%COMP%] {\n    display: none;\n  }\n\n  .topbar__wrapper[_ngcontent-%COMP%] {\n    width: 100%;\n    display: flex;\n    justify-content: space-between;\n  }\n}\n@media (min-width: 1025px) {\n  .navbar__container__brand[_ngcontent-%COMP%] {\n    flex-direction: row;\n    justify-content: space-between;\n  }\n}\n@media (min-width: 768px) {\n  .navbar__container__main-nav[_ngcontent-%COMP%]   .dropdown__content[_ngcontent-%COMP%] {\n    position: absolute;\n  }\n}\n@media screen and (max-width: 820px) {\n  .navbar--scrolled--responsive[_ngcontent-%COMP%] {\n    background-color: rgba(0, 0, 0, 0.5);\n    transition: background-color 1s;\n  }\n\n  .navbar.responsive[_ngcontent-%COMP%] {\n    position: relative;\n  }\n\n  .navbar.responsive[_ngcontent-%COMP%]   .navbar__container[_ngcontent-%COMP%] {\n    flex-direction: column;\n  }\n\n  .navbar.responsive[_ngcontent-%COMP%]   div[_ngcontent-%COMP%] {\n    float: none;\n    text-align: left;\n  }\n\n  .navbar.responsive[_ngcontent-%COMP%]   .navbar__container__main-nav[_ngcontent-%COMP%] {\n    display: block;\n  }\n\n  .navbar.responsive[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   div[_ngcontent-%COMP%] {\n    margin-top: 50px;\n  }\n\n  .navbar.responsive[_ngcontent-%COMP%]   li[_ngcontent-%COMP%] {\n    margin: 15px 0;\n  }\n\n  .navbar.responsive[_ngcontent-%COMP%]   .navbar__container__main-nav[_ngcontent-%COMP%] {\n    width: 100%;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2NoaWRvL3Byb2plY3RzL2hvc3Rmcm9udC1wcm9qZWN0L2hvc3Rmcm9udC1mcm9udGVuZC9zcmMvYXBwL3NoYXJlZC9uYXZiYXIvbmF2YmFyLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9zaGFyZWQvbmF2YmFyL25hdmJhci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFdBQUE7RUFDQSxnQkFBQTtFQUNBLHlCQUFBO0VBQ0EsY0FBQTtBQ0NGO0FEQ0U7RUFDRSxhQUFBO0VBQ0EsdUJBQUE7QUNDSjtBREVNO0VBQ0UsZUFBQTtFQUNBLFlBQUE7QUNBUjtBREtFO0VBQ0UsYUFBQTtFQUNBLG1CQUFBO0FDSEo7QURLSTtFQUNFLGVBQUE7QUNITjtBRFFBO0VBQ0UsY0FBQTtFQUVBLFdBQUE7RUFDQSxVQUFBO0FDTkY7QURRRTtFQUNFLG9DQUFBO0VBQ0EsK0JBQUE7QUNOSjtBRFVJO0VBQ0UsYUFBQTtFQUNBLDhCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtBQ1JOO0FEV007RUFDRSxhQUFBO0VBQ0EsbUJBQUE7QUNUUjtBRFdRO0VBQ0UsV0FBQTtBQ1RWO0FEV1E7RUFDRSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0Esb0NBQUE7QUNUVjtBRGNJO0VBQ0UscUJBQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtFQUNBLGFBQUE7QUNaTjtBRGNNO0VBQ0UsZ0JBQUE7RUFDQSxvQkFBQTtFQUNBLGNBQUE7QUNaUjtBRGFRO0VBQ0Usc0JBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7QUNYVjtBRFlVO0VBQ0UsY0FBQTtFQUNBLG9CQUFBO0FDVlo7QURlTTtFQUNFLGFBQUE7QUNiUjtBRGlCSTtFQUNFLHFCQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0FDZk47QURnQk07RUFDRSxjQUFBO0FDZFI7QURpQlE7RUFDRSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxRQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7RUFDQSxPQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLDRDQUFBO0VBQ0EsbUNBQUE7QUNmVjtBRGlCUTtFQUNFLG1CQUFBO0VBQ0EsV0FBQTtBQ2ZWO0FEc0JBO0VBQ0U7SUFDRSxhQUFBO0lBQ0EsOEJBQUE7SUFDQSxtQkFBQTtFQ25CRjs7RURxQkE7OztJQUdFLGFBQUE7RUNsQkY7RURvQkU7OztJQUNFLGFBQUE7SUFDQSxtQkFBQTtFQ2hCSjs7RURtQkE7SUFDRSxzQkFBQTtJQUNBLG1CQUFBO0VDaEJGOztFRG1CRTtJQUNFLGFBQUE7RUNoQko7O0VEb0JBO0lBQ0UsV0FBQTtJQUNBLGFBQUE7SUFDQSw4QkFBQTtFQ2pCRjtBQUNGO0FEb0JBO0VBQ0U7SUFDRSxtQkFBQTtJQUNBLDhCQUFBO0VDbEJGO0FBQ0Y7QURxQkE7RUFDRTtJQUNFLGtCQUFBO0VDbkJGO0FBQ0Y7QURzQkE7RUFDRTtJQUNFLG9DQUFBO0lBQ0EsK0JBQUE7RUNwQkY7O0VEc0JBO0lBQ0Usa0JBQUE7RUNuQkY7O0VEcUJBO0lBQ0Usc0JBQUE7RUNsQkY7O0VEb0JBO0lBQ0UsV0FBQTtJQUNBLGdCQUFBO0VDakJGOztFRG1CQTtJQUNFLGNBQUE7RUNoQkY7O0VEa0JBO0lBQ0UsZ0JBQUE7RUNmRjs7RURpQkE7SUFDRSxjQUFBO0VDZEY7O0VEZ0JBO0lBQ0UsV0FBQTtFQ2JGO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9zaGFyZWQvbmF2YmFyL25hdmJhci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi50b3BiYXIge1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogMCAxLjVlbTtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzE2MzI0YTtcbiAgY29sb3I6ICNmZmZmZmY7XG5cbiAgJl9fY29udGFpbmVyIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuXG4gICAgJiBkaXYge1xuICAgICAgJiBkaXYge1xuICAgICAgICBwYWRkaW5nOiAyMHB4IDA7XG4gICAgICAgIG1hcmdpbjogYXV0bztcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICAmX19jb250YWN0cyB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuXG4gICAgJiBwIHtcbiAgICAgIG1hcmdpbjogMCAwLjVlbTtcbiAgICB9XG4gIH1cbn1cblxuLm5hdmJhciB7XG4gIHBhZGRpbmc6IDAuNWVtO1xuICAvLyBwb3NpdGlvbjogZml4ZWQ7XG4gIHdpZHRoOiAxMDAlO1xuICB6LWluZGV4OiAyO1xuXG4gICYtLXNjcm9sbGVkIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuOCk7XG4gICAgdHJhbnNpdGlvbjogYmFja2dyb3VuZC1jb2xvciAycztcbiAgfVxuXG4gICZfX2NvbnRhaW5lciB7XG4gICAgJl9fYnJhbmQge1xuICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgLy8gd2lkdGg6IDEwMCU7XG5cbiAgICAgICYgYSB7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG5cbiAgICAgICAgJiAubG9nbyB7XG4gICAgICAgICAgd2lkdGg6IDc1cHg7XG4gICAgICAgIH1cbiAgICAgICAgJiBzcGFuIHtcbiAgICAgICAgICBmb250LXNpemU6IDEuNWVtO1xuICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgICAgIGZvbnQtZmFtaWx5OiBcIk9wZW4gU2Fuc1wiLCBzYW5zLXNlcmlmO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgJl9fbWFpbi1uYXYge1xuICAgICAgbGlzdC1zdHlsZS10eXBlOiBub25lO1xuICAgICAgbWFyZ2luOiAwO1xuICAgICAgcGFkZGluZzogMDtcbiAgICAgIGRpc3BsYXk6IG5vbmU7XG5cbiAgICAgICYgLmRyb3Bkb3duIHtcbiAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICAgICAgbWFyZ2luOiAwICFpbXBvcnRhbnQ7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAmX19jb250ZW50IHtcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAgICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICAgICYgYTpob3ZlciB7XG4gICAgICAgICAgICBjb2xvcjogIzE2MzI0YTtcbiAgICAgICAgICAgIHRyYW5zaXRpb246IGNvbG9yIDFzO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICAmIC5kcm9wZG93bjpob3ZlciAuZHJvcGRvd25fX2NvbnRlbnQge1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgfVxuICAgIH1cblxuICAgICYgYSB7XG4gICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gICAgICBtYXJnaW46IDEwcHggMTVweDtcbiAgICAgIHBhZGRpbmc6IDVweDtcbiAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgY29sb3I6ICNmZmZmZmY7XG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgICAmIC5mYSB7XG4gICAgICAgIHBhZGRpbmc6IDAgM3B4O1xuICAgICAgfVxuICAgICAgJi5ob3ZlciB7XG4gICAgICAgICY6YmVmb3JlIHtcbiAgICAgICAgICBjb250ZW50OiBcIlwiO1xuICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICB3aWR0aDogMDtcbiAgICAgICAgICBoZWlnaHQ6IDIuNXB4O1xuICAgICAgICAgIGJvdHRvbTogLTJweDtcbiAgICAgICAgICBsZWZ0OiAwO1xuICAgICAgICAgIGJhY2tncm91bmQ6ICNmZmY7XG4gICAgICAgICAgdmlzaWJpbGl0eTogaGlkZGVuO1xuICAgICAgICAgIC8qICBBbmltYXRlIHdpdGggYSBkdXJhdGlvbiBvZiAwLjMgc2Vjb25kcyAqL1xuICAgICAgICAgIHRyYW5zaXRpb246IGFsbCAwLjNzIGVhc2UtaW4tb3V0IDBzO1xuICAgICAgICB9XG4gICAgICAgICY6aG92ZXI6YmVmb3JlIHtcbiAgICAgICAgICB2aXNpYmlsaXR5OiB2aXNpYmxlO1xuICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG59XG5cbkBtZWRpYSAobWluLXdpZHRoOiA3NjhweCkge1xuICAubmF2YmFyX19jb250YWluZXIge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIH1cbiAgLm5hdmJhcl9fY29udGFpbmVyX19icmFuZCxcbiAgLm5hdmJhcl9fY29udGFpbmVyX19tYWluLW5hdixcbiAgLm5hdmJhcl9fY29udGFpbmVyX19tYWluLW5hdiBkaXYge1xuICAgIGRpc3BsYXk6IGZsZXg7XG5cbiAgICAmIGEge1xuICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgfVxuICB9XG4gIC5uYXZiYXJfX2NvbnRhaW5lcl9fYnJhbmQge1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgfVxuICAjbXlUb3BuYXYge1xuICAgICYgLmljb24ge1xuICAgICAgZGlzcGxheTogbm9uZTtcbiAgICB9XG4gIH1cblxuICAudG9wYmFyX193cmFwcGVyIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgfVxufVxuXG5AbWVkaWEgKG1pbi13aWR0aDogMTAyNXB4KSB7XG4gIC5uYXZiYXJfX2NvbnRhaW5lcl9fYnJhbmQge1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICB9XG59XG5cbkBtZWRpYShtaW4td2lkdGg6IDc2OHB4KSB7XG4gIC5uYXZiYXJfX2NvbnRhaW5lcl9fbWFpbi1uYXYgLmRyb3Bkb3duX19jb250ZW50IHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIH1cbn1cblxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogODIwcHgpIHtcbiAgLm5hdmJhci0tc2Nyb2xsZWQtLXJlc3BvbnNpdmUge1xuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwgMCwgMCwgMC41KTtcbiAgICB0cmFuc2l0aW9uOiBiYWNrZ3JvdW5kLWNvbG9yIDFzO1xuICB9XG4gIC5uYXZiYXIucmVzcG9uc2l2ZSB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB9XG4gIC5uYXZiYXIucmVzcG9uc2l2ZSAubmF2YmFyX19jb250YWluZXIge1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIH1cbiAgLm5hdmJhci5yZXNwb25zaXZlIGRpdiB7XG4gICAgZmxvYXQ6IG5vbmU7XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgfVxuICAubmF2YmFyLnJlc3BvbnNpdmUgLm5hdmJhcl9fY29udGFpbmVyX19tYWluLW5hdiB7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gIH1cbiAgLm5hdmJhci5yZXNwb25zaXZlIHVsIGRpdiB7XG4gICAgbWFyZ2luLXRvcDogNTBweDtcbiAgfVxuICAubmF2YmFyLnJlc3BvbnNpdmUgbGkge1xuICAgIG1hcmdpbjogMTVweCAwO1xuICB9XG4gIC5uYXZiYXIucmVzcG9uc2l2ZSAubmF2YmFyX19jb250YWluZXJfX21haW4tbmF2IHtcbiAgICB3aWR0aDogMTAwJTtcbiAgfVxufSIsIi50b3BiYXIge1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogMCAxLjVlbTtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzE2MzI0YTtcbiAgY29sb3I6ICNmZmZmZmY7XG59XG4udG9wYmFyX19jb250YWluZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cbi50b3BiYXJfX2NvbnRhaW5lciBkaXYgZGl2IHtcbiAgcGFkZGluZzogMjBweCAwO1xuICBtYXJnaW46IGF1dG87XG59XG4udG9wYmFyX19jb250YWN0cyB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4udG9wYmFyX19jb250YWN0cyBwIHtcbiAgbWFyZ2luOiAwIDAuNWVtO1xufVxuXG4ubmF2YmFyIHtcbiAgcGFkZGluZzogMC41ZW07XG4gIHdpZHRoOiAxMDAlO1xuICB6LWluZGV4OiAyO1xufVxuLm5hdmJhci0tc2Nyb2xsZWQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuOCk7XG4gIHRyYW5zaXRpb246IGJhY2tncm91bmQtY29sb3IgMnM7XG59XG4ubmF2YmFyX19jb250YWluZXJfX2JyYW5kIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xufVxuLm5hdmJhcl9fY29udGFpbmVyX19icmFuZCBhIHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5uYXZiYXJfX2NvbnRhaW5lcl9fYnJhbmQgYSAubG9nbyB7XG4gIHdpZHRoOiA3NXB4O1xufVxuLm5hdmJhcl9fY29udGFpbmVyX19icmFuZCBhIHNwYW4ge1xuICBmb250LXNpemU6IDEuNWVtO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1mYW1pbHk6IFwiT3BlbiBTYW5zXCIsIHNhbnMtc2VyaWY7XG59XG4ubmF2YmFyX19jb250YWluZXJfX21haW4tbmF2IHtcbiAgbGlzdC1zdHlsZS10eXBlOiBub25lO1xuICBtYXJnaW46IDA7XG4gIHBhZGRpbmc6IDA7XG4gIGRpc3BsYXk6IG5vbmU7XG59XG4ubmF2YmFyX19jb250YWluZXJfX21haW4tbmF2IC5kcm9wZG93biB7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIG1hcmdpbjogMCAhaW1wb3J0YW50O1xuICBkaXNwbGF5OiBibG9jaztcbn1cbi5uYXZiYXJfX2NvbnRhaW5lcl9fbWFpbi1uYXYgLmRyb3Bkb3duX19jb250ZW50IHtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgZGlzcGxheTogbm9uZTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLm5hdmJhcl9fY29udGFpbmVyX19tYWluLW5hdiAuZHJvcGRvd25fX2NvbnRlbnQgYTpob3ZlciB7XG4gIGNvbG9yOiAjMTYzMjRhO1xuICB0cmFuc2l0aW9uOiBjb2xvciAxcztcbn1cbi5uYXZiYXJfX2NvbnRhaW5lcl9fbWFpbi1uYXYgLmRyb3Bkb3duOmhvdmVyIC5kcm9wZG93bl9fY29udGVudCB7XG4gIGRpc3BsYXk6IGZsZXg7XG59XG4ubmF2YmFyX19jb250YWluZXIgYSB7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgbWFyZ2luOiAxMHB4IDE1cHg7XG4gIHBhZGRpbmc6IDVweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cbi5uYXZiYXJfX2NvbnRhaW5lciBhIC5mYSB7XG4gIHBhZGRpbmc6IDAgM3B4O1xufVxuLm5hdmJhcl9fY29udGFpbmVyIGEuaG92ZXI6YmVmb3JlIHtcbiAgY29udGVudDogXCJcIjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB3aWR0aDogMDtcbiAgaGVpZ2h0OiAyLjVweDtcbiAgYm90dG9tOiAtMnB4O1xuICBsZWZ0OiAwO1xuICBiYWNrZ3JvdW5kOiAjZmZmO1xuICB2aXNpYmlsaXR5OiBoaWRkZW47XG4gIC8qICBBbmltYXRlIHdpdGggYSBkdXJhdGlvbiBvZiAwLjMgc2Vjb25kcyAqL1xuICB0cmFuc2l0aW9uOiBhbGwgMC4zcyBlYXNlLWluLW91dCAwcztcbn1cbi5uYXZiYXJfX2NvbnRhaW5lciBhLmhvdmVyOmhvdmVyOmJlZm9yZSB7XG4gIHZpc2liaWxpdHk6IHZpc2libGU7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG5AbWVkaWEgKG1pbi13aWR0aDogNzY4cHgpIHtcbiAgLm5hdmJhcl9fY29udGFpbmVyIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICB9XG5cbiAgLm5hdmJhcl9fY29udGFpbmVyX19icmFuZCxcbi5uYXZiYXJfX2NvbnRhaW5lcl9fbWFpbi1uYXYsXG4ubmF2YmFyX19jb250YWluZXJfX21haW4tbmF2IGRpdiB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgfVxuICAubmF2YmFyX19jb250YWluZXJfX2JyYW5kIGEsXG4ubmF2YmFyX19jb250YWluZXJfX21haW4tbmF2IGEsXG4ubmF2YmFyX19jb250YWluZXJfX21haW4tbmF2IGRpdiBhIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIH1cblxuICAubmF2YmFyX19jb250YWluZXJfX2JyYW5kIHtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIH1cblxuICAjbXlUb3BuYXYgLmljb24ge1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gIH1cblxuICAudG9wYmFyX193cmFwcGVyIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6IDEwMjVweCkge1xuICAubmF2YmFyX19jb250YWluZXJfX2JyYW5kIHtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6IDc2OHB4KSB7XG4gIC5uYXZiYXJfX2NvbnRhaW5lcl9fbWFpbi1uYXYgLmRyb3Bkb3duX19jb250ZW50IHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIH1cbn1cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDgyMHB4KSB7XG4gIC5uYXZiYXItLXNjcm9sbGVkLS1yZXNwb25zaXZlIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuNSk7XG4gICAgdHJhbnNpdGlvbjogYmFja2dyb3VuZC1jb2xvciAxcztcbiAgfVxuXG4gIC5uYXZiYXIucmVzcG9uc2l2ZSB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB9XG5cbiAgLm5hdmJhci5yZXNwb25zaXZlIC5uYXZiYXJfX2NvbnRhaW5lciB7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgfVxuXG4gIC5uYXZiYXIucmVzcG9uc2l2ZSBkaXYge1xuICAgIGZsb2F0OiBub25lO1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gIH1cblxuICAubmF2YmFyLnJlc3BvbnNpdmUgLm5hdmJhcl9fY29udGFpbmVyX19tYWluLW5hdiB7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gIH1cblxuICAubmF2YmFyLnJlc3BvbnNpdmUgdWwgZGl2IHtcbiAgICBtYXJnaW4tdG9wOiA1MHB4O1xuICB9XG5cbiAgLm5hdmJhci5yZXNwb25zaXZlIGxpIHtcbiAgICBtYXJnaW46IDE1cHggMDtcbiAgfVxuXG4gIC5uYXZiYXIucmVzcG9uc2l2ZSAubmF2YmFyX19jb250YWluZXJfX21haW4tbmF2IHtcbiAgICB3aWR0aDogMTAwJTtcbiAgfVxufSJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NavbarComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-navbar',
                templateUrl: './navbar.component.html',
                styleUrls: ['./navbar.component.scss']
            }]
    }], function () { return []; }, { onWindowScroll: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
            args: ['window:scroll', ['$event']]
        }] }); })();


/***/ }),

/***/ "./src/app/shared/scroll/scroll.component.ts":
/*!***************************************************!*\
  !*** ./src/app/shared/scroll/scroll.component.ts ***!
  \***************************************************/
/*! exports provided: ScrollComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ScrollComponent", function() { return ScrollComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");




const _c0 = function (a0) { return { "show-scrollTop": a0 }; };
class ScrollComponent {
    constructor(document) {
        this.document = document;
    }
    onWindowScroll() {
        if (window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop > 100) {
            this.windowScrolled = true;
        }
        else if (this.windowScrolled && window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop < 10) {
            this.windowScrolled = false;
        }
    }
    scrollToTop() {
        (function smoothscroll() {
            var currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
            if (currentScroll > 0) {
                window.requestAnimationFrame(smoothscroll);
                window.scrollTo(0, currentScroll - (currentScroll / 8));
            }
        })();
    }
    ngOnInit() { }
}
ScrollComponent.ɵfac = function ScrollComponent_Factory(t) { return new (t || ScrollComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_common__WEBPACK_IMPORTED_MODULE_1__["DOCUMENT"])); };
ScrollComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ScrollComponent, selectors: [["app-scroll"]], hostBindings: function ScrollComponent_HostBindings(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("scroll", function ScrollComponent_scroll_HostBindingHandler() { return ctx.onWindowScroll(); }, false, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵresolveWindow"]);
    } }, decls: 3, vars: 3, consts: [[1, "scroll-to-top", 3, "ngClass"], ["id", "myBtn", "title", "Go to top", 3, "click"], [1, "fa", "fa-chevron-up"]], template: function ScrollComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "button", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ScrollComponent_Template_button_click_1_listener() { return ctx.scrollToTop(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "i", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](1, _c0, ctx.windowScrolled));
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["NgClass"]], styles: ["#myBtn[_ngcontent-%COMP%] {\n  position: fixed;\n  \n  bottom: 20px;\n  \n  right: 30px;\n  \n  z-index: 99;\n  \n  border: none;\n  \n  outline: none;\n  \n  background-color: #4b79a1;\n  \n  color: white;\n  \n  cursor: pointer;\n  \n  padding: 15px;\n  \n  border-radius: 10px;\n  \n  font-size: 18px;\n  \n}\n\n#myBtn[_ngcontent-%COMP%]:hover {\n  background-color: #283e51;\n  \n  transition: all 1s ease-in-out;\n}\n\n.scroll-to-top[_ngcontent-%COMP%] {\n  opacity: 0;\n  transition: all 0.2s ease-in-out;\n}\n\n.show-scrollTop[_ngcontent-%COMP%] {\n  opacity: 1;\n  transition: all 0.2s ease-in-out;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2NoaWRvL3Byb2plY3RzL2hvc3Rmcm9udC1wcm9qZWN0L2hvc3Rmcm9udC1mcm9udGVuZC9zcmMvYXBwL3NoYXJlZC9zY3JvbGwvc2Nyb2xsLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9zaGFyZWQvc2Nyb2xsL3Njcm9sbC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGVBQUE7RUFBaUIsMEJBQUE7RUFDakIsWUFBQTtFQUFjLCtDQUFBO0VBQ2QsV0FBQTtFQUFhLHlDQUFBO0VBQ2IsV0FBQTtFQUFhLGtDQUFBO0VBQ2IsWUFBQTtFQUFjLG1CQUFBO0VBQ2QsYUFBQTtFQUFlLG1CQUFBO0VBQ2YseUJBQUE7RUFBMkIsMkJBQUE7RUFDM0IsWUFBQTtFQUFjLGVBQUE7RUFDZCxlQUFBO0VBQWlCLGlDQUFBO0VBQ2pCLGFBQUE7RUFBZSxpQkFBQTtFQUNmLG1CQUFBO0VBQXFCLG9CQUFBO0VBQ3JCLGVBQUE7RUFBaUIsdUJBQUE7QUNhbkI7O0FEVkE7RUFDRSx5QkFBQTtFQUEyQix3Q0FBQTtFQUMzQiw4QkFBQTtBQ2NGOztBRFhBO0VBQ0UsVUFBQTtFQUNBLGdDQUFBO0FDY0Y7O0FEWkM7RUFDQyxVQUFBO0VBQ0EsZ0NBQUE7QUNlRiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9zY3JvbGwvc2Nyb2xsLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI215QnRuIHtcbiAgcG9zaXRpb246IGZpeGVkOyAvKiBGaXhlZC9zdGlja3kgcG9zaXRpb24gKi9cbiAgYm90dG9tOiAyMHB4OyAvKiBQbGFjZSB0aGUgYnV0dG9uIGF0IHRoZSBib3R0b20gb2YgdGhlIHBhZ2UgKi9cbiAgcmlnaHQ6IDMwcHg7IC8qIFBsYWNlIHRoZSBidXR0b24gMzBweCBmcm9tIHRoZSByaWdodCAqL1xuICB6LWluZGV4OiA5OTsgLyogTWFrZSBzdXJlIGl0IGRvZXMgbm90IG92ZXJsYXAgKi9cbiAgYm9yZGVyOiBub25lOyAvKiBSZW1vdmUgYm9yZGVycyAqL1xuICBvdXRsaW5lOiBub25lOyAvKiBSZW1vdmUgb3V0bGluZSAqL1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNGI3OWExOyAvKiBTZXQgYSBiYWNrZ3JvdW5kIGNvbG9yICovXG4gIGNvbG9yOiB3aGl0ZTsgLyogVGV4dCBjb2xvciAqL1xuICBjdXJzb3I6IHBvaW50ZXI7IC8qIEFkZCBhIG1vdXNlIHBvaW50ZXIgb24gaG92ZXIgKi9cbiAgcGFkZGluZzogMTVweDsgLyogU29tZSBwYWRkaW5nICovXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7IC8qIFJvdW5kZWQgY29ybmVycyAqL1xuICBmb250LXNpemU6IDE4cHg7IC8qIEluY3JlYXNlIGZvbnQgc2l6ZSAqL1xufVxuXG4jbXlCdG46aG92ZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjgzZTUxOyAvKiBBZGQgYSBkYXJrLWdyZXkgYmFja2dyb3VuZCBvbiBob3ZlciAqL1xuICB0cmFuc2l0aW9uOiBhbGwgMXMgZWFzZS1pbi1vdXQ7XG59XG5cbi5zY3JvbGwtdG8tdG9wIHtcbiAgb3BhY2l0eTogMDtcbiAgdHJhbnNpdGlvbjogYWxsIC4ycyBlYXNlLWluLW91dDtcbiB9XG4gLnNob3ctc2Nyb2xsVG9wIHtcbiAgb3BhY2l0eTogMTtcbiAgdHJhbnNpdGlvbjogYWxsIC4ycyBlYXNlLWluLW91dDtcbiB9IiwiI215QnRuIHtcbiAgcG9zaXRpb246IGZpeGVkO1xuICAvKiBGaXhlZC9zdGlja3kgcG9zaXRpb24gKi9cbiAgYm90dG9tOiAyMHB4O1xuICAvKiBQbGFjZSB0aGUgYnV0dG9uIGF0IHRoZSBib3R0b20gb2YgdGhlIHBhZ2UgKi9cbiAgcmlnaHQ6IDMwcHg7XG4gIC8qIFBsYWNlIHRoZSBidXR0b24gMzBweCBmcm9tIHRoZSByaWdodCAqL1xuICB6LWluZGV4OiA5OTtcbiAgLyogTWFrZSBzdXJlIGl0IGRvZXMgbm90IG92ZXJsYXAgKi9cbiAgYm9yZGVyOiBub25lO1xuICAvKiBSZW1vdmUgYm9yZGVycyAqL1xuICBvdXRsaW5lOiBub25lO1xuICAvKiBSZW1vdmUgb3V0bGluZSAqL1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNGI3OWExO1xuICAvKiBTZXQgYSBiYWNrZ3JvdW5kIGNvbG9yICovXG4gIGNvbG9yOiB3aGl0ZTtcbiAgLyogVGV4dCBjb2xvciAqL1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIC8qIEFkZCBhIG1vdXNlIHBvaW50ZXIgb24gaG92ZXIgKi9cbiAgcGFkZGluZzogMTVweDtcbiAgLyogU29tZSBwYWRkaW5nICovXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIC8qIFJvdW5kZWQgY29ybmVycyAqL1xuICBmb250LXNpemU6IDE4cHg7XG4gIC8qIEluY3JlYXNlIGZvbnQgc2l6ZSAqL1xufVxuXG4jbXlCdG46aG92ZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjgzZTUxO1xuICAvKiBBZGQgYSBkYXJrLWdyZXkgYmFja2dyb3VuZCBvbiBob3ZlciAqL1xuICB0cmFuc2l0aW9uOiBhbGwgMXMgZWFzZS1pbi1vdXQ7XG59XG5cbi5zY3JvbGwtdG8tdG9wIHtcbiAgb3BhY2l0eTogMDtcbiAgdHJhbnNpdGlvbjogYWxsIDAuMnMgZWFzZS1pbi1vdXQ7XG59XG5cbi5zaG93LXNjcm9sbFRvcCB7XG4gIG9wYWNpdHk6IDE7XG4gIHRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xufSJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ScrollComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-scroll',
                templateUrl: './scroll.component.html',
                styleUrls: ['./scroll.component.scss']
            }]
    }], function () { return [{ type: Document, decorators: [{
                type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
                args: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["DOCUMENT"]]
            }] }]; }, { onWindowScroll: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
            args: ["window:scroll", []]
        }] }); })();


/***/ }),

/***/ "./src/app/wordpress-maintenance/wordpress-maintenance.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/wordpress-maintenance/wordpress-maintenance.component.ts ***!
  \**************************************************************************/
/*! exports provided: WordpressMaintenanceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WordpressMaintenanceComponent", function() { return WordpressMaintenanceComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _pricing_tab_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../pricing-tab.service */ "./src/app/pricing-tab.service.ts");
/* harmony import */ var _shared_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../shared/navbar/navbar.component */ "./src/app/shared/navbar/navbar.component.ts");
/* harmony import */ var _shared_scroll_scroll_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../shared/scroll/scroll.component */ "./src/app/shared/scroll/scroll.component.ts");
/* harmony import */ var _shared_footer_footer_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../shared/footer/footer.component */ "./src/app/shared/footer/footer.component.ts");






class WordpressMaintenanceComponent {
    constructor(_pricingTabService) {
        this._pricingTabService = _pricingTabService;
    }
    yearlyTab() {
        this._pricingTabService.yearlyClick();
    }
    monthlyTab() {
        this._pricingTabService.monthlyClick();
    }
    ngOnInit() {
    }
}
WordpressMaintenanceComponent.ɵfac = function WordpressMaintenanceComponent_Factory(t) { return new (t || WordpressMaintenanceComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_pricing_tab_service__WEBPACK_IMPORTED_MODULE_1__["PricingTabService"])); };
WordpressMaintenanceComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: WordpressMaintenanceComponent, selectors: [["app-wordpress-maintenance"]], decls: 181, vars: 0, consts: [[1, "header"], ["id", "curve", "data-name", "Layer 1", "xmlns", "http://www.w3.org/2000/svg", "viewBox", "0 0 1416.99 174.01"], ["d", "M0,280.8S283.66,59,608.94,163.56s437.93,150.57,808,10.34V309.54H0V280.8Z", "transform", "translate(0 -135.53)", 1, "cls-1"], [1, "header__container"], [1, "section", "wordpress-hosting"], [1, "container"], [1, "flex", "heading"], ["data-aos", "fade-up", 1, "flex"], ["id", "wplogo", "src", "assets/images/wplogo.png"], [1, "section", "section--color--dark"], [1, "flex"], ["id", "wordpress-maintenance", 1, "section"], ["data-aos", "fade-up", 1, "column"], [1, "column__icon"], ["src", "assets/images/cloud-computing.png"], [1, "column__body"], [1, "section", "section--color"], ["id", "first-tab", 1, "container"], [1, "column"], [1, "card"], [1, "card__icon"], ["src", "assets/images/cloud-computing.svg"], [1, "card__body"], [1, "card__title"], [1, "card__text"], [1, "card__price"], [1, "btn", "btn--std", "btn--std01"], [1, "card", "active"], [1, "btn", "btn--std", "btn--std02"], ["id", "second-tab", 1, "container"], [1, "btn", "btn--ghost", "btn--ghost--01"], [1, "btn__slide", "btn__slide--01"], ["href", "#"], [1, "btn", "btn--ghost", "btn--ghost--02"], [1, "btn__slide", "btn__slide--02"], [1, "section", "section--color--bg"], [1, "child"]], template: function WordpressMaintenanceComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "header", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "app-navbar");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceSVG"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "svg", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "path", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceHTML"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "A WORDPRESS MAINTENANCE SERVICE IS CRITICAL FOR YOUR ONLINE SUCCESS");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "section", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "WHAT IS WORDPRESS WEBSITE MAINTENANCE?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "img", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu..");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "section", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu..");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "section", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, "Services");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](32, "img", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35, "Free Website SSL");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](40, "img", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43, "Free Website SSL");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](48, "img", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](51, "Free Website SSL");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](53, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](56, "img", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](59, "Free Website SSL");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](61, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](64, "img", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](67, "Free Website SSL");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](69, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](70, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](71, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](72, "img", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](75, "Free Website SSL");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](76, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](77, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "section", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](79, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](80, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](82, "Our Plans");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](83, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](84, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](87, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](88, "img", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](89, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](90, "h3", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](91, "Cloud Computing");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](92, "p", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](93, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](94, "p", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](95, "\u00A31.99");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](96, "span", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](97, "Click Here");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](98, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](99, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](100, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](101, "img", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](102, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](103, "h3", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](104, "Cloud Computing");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](105, "p", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](106, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](107, "p", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](108, "\u00A359.99");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](109, "span", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](110, "Click Here");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](111, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](112, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](113, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](114, "img", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](115, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](116, "h3", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](117, "Cloud Computing");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](118, "p", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](119, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](120, "p", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](121, "\u00A32.99");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](122, "span", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](123, "Click Here");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](124, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](125, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](126, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](127, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](128, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](129, "img", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](130, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](131, "h3", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](132, "Cloud Computing");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](133, "p", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](134, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](135, "p", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](136, "\u00A30.99");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](137, "span", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](138, "span", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](139, "a", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](140, "Search");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](141, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](142, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](143, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](144, "img", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](145, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](146, "h3", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](147, "Cloud Computing");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](148, "p", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](149, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](150, "p", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](151, "\u00A336.99");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](152, "span", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](153, "span", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](154, "a", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](155, "Search");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](156, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](157, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](158, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](159, "img", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](160, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](161, "h3", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](162, "Cloud Computing");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](163, "p", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](164, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](165, "p", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](166, "\u00A36.59");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](167, "span", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](168, "span", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](169, "a", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](170, "Search");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](171, "section", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](172, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](173, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](174, "div", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](175, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](176, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](177, "span", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](178, "Click Here");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](179, "app-scroll");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](180, "app-footer");
    } }, directives: [_shared_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_2__["NavbarComponent"], _shared_scroll_scroll_component__WEBPACK_IMPORTED_MODULE_3__["ScrollComponent"], _shared_footer_footer_component__WEBPACK_IMPORTED_MODULE_4__["FooterComponent"]], styles: [".header[_ngcontent-%COMP%] {\n  position: relative;\n  height: 750px;\n  background-image: linear-gradient(to right, #2c3e50, rgba(41, 128, 185, 0.5)), url(\"/assets/images/services-bg.jpg\");\n  background-attachment: fixed;\n  background-position: 50% 50%;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n.header[_ngcontent-%COMP%]   svg[_ngcontent-%COMP%] {\n  position: absolute;\n  bottom: 0;\n  width: 100%;\n}\n.header[_ngcontent-%COMP%]   svg[_ngcontent-%COMP%]   path[_ngcontent-%COMP%] {\n  fill: #ffffff;\n}\n.header__container[_ngcontent-%COMP%] {\n  width: 100%;\n  height: 100%;\n  display: flex;\n  margin: auto;\n  color: white;\n}\n.header__container[_ngcontent-%COMP%]   div[_ngcontent-%COMP%] {\n  text-align: center;\n}\n.header__container[_ngcontent-%COMP%]   div[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n  margin: 20 0 20 0;\n}\n.section[_ngcontent-%COMP%] {\n  padding: 100px 0;\n}\n.section.wordpress-hosting[_ngcontent-%COMP%] {\n  line-height: 2;\n  text-align: center;\n}\n.section.wordpress-hosting[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%] {\n  align-items: center;\n}\n.section--color[_ngcontent-%COMP%] {\n  background-color: #f8fafe;\n}\n.section--color--dark[_ngcontent-%COMP%] {\n  background-image: linear-gradient(#2c3e50, #2980b9);\n}\n.section--color--dark[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%] {\n  text-align: center;\n  line-height: 2;\n  font-size: 1.25em;\n  color: white;\n}\n.section--color--bg[_ngcontent-%COMP%] {\n  background-image: linear-gradient(to right, #2980b9, rgba(44, 62, 80, 0.5)), url(\"/assets/images/bg_02.jpg\");\n  background-attachment: fixed;\n  background-position: center top;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n.section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%] {\n  padding: 30px 0px;\n  max-width: 1140px;\n  margin: auto;\n  overflow: hidden;\n}\n.section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: center;\n  flex-direction: column;\n}\n.section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%]   #wplogo[_ngcontent-%COMP%] {\n  max-width: 250px;\n  margin: 3em;\n}\n.section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .heading[_ngcontent-%COMP%] {\n  text-align: center;\n}\n.section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  text-transform: uppercase;\n  font-size: 2em;\n  color: #283e51;\n}\n.section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .tab[_ngcontent-%COMP%] {\n  display: flex;\n  flex-wrap: wrap;\n  position: relative;\n  box-shadow: 0px 20px 45px 0px rgba(0, 0, 0, 0.08);\n  border-radius: 1.61765rem;\n  max-width: 370px;\n  margin: 0 auto;\n  background-color: #ffffff;\n  justify-content: center;\n  padding: 5px 0;\n  z-index: 1;\n}\n.section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .tab[_ngcontent-%COMP%]   li[_ngcontent-%COMP%] {\n  display: inline-block;\n  margin: 0;\n  padding: 0;\n  list-style-type: none;\n  -webkit-tap-highlight-color: transparent;\n}\n.section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .tab[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  border-radius: 1.35294rem;\n  display: block;\n  color: #ffffff;\n  min-width: 125px;\n  padding: 8px 31px;\n  text-align: center;\n  text-decoration: none;\n  cursor: pointer;\n}\n.section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .tab[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   #yearly-tab[_ngcontent-%COMP%] {\n  color: #2c3e50;\n}\n.section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .tab[_ngcontent-%COMP%]   li#indicator[_ngcontent-%COMP%] {\n  width: 150px;\n  background-color: #1868dd;\n  position: absolute;\n  height: calc(100% - 10px);\n  transform: translateY(-50%);\n  top: 50%;\n  border-radius: 1.35294rem;\n  z-index: -1;\n  left: 5px;\n}\n.section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%] {\n  padding: 20px;\n  flex: 1;\n  line-height: 1.5;\n  color: #2980b9;\n}\n.section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .column__icon[_ngcontent-%COMP%] {\n  height: 244px;\n  width: 244px;\n}\n.section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .column__icon[_ngcontent-%COMP%], .section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .card__icon[_ngcontent-%COMP%] {\n  position: relative;\n  margin: auto;\n}\n.section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .column__icon[_ngcontent-%COMP%]   img[_ngcontent-%COMP%], .section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .card__icon[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  max-height: 100%;\n  max-width: 100%;\n  padding: 0;\n}\n.section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .card__icon[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  max-height: 50%;\n  max-width: 50%;\n}\n.section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column__body[_ngcontent-%COMP%] {\n  text-align: center;\n}\n.section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column__body[_ngcontent-%COMP%]   h3[_ngcontent-%COMP%] {\n  font-size: 1.5em;\n  color: #283e51;\n}\n.section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%] {\n  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);\n  border-radius: 10px;\n  padding: 16px;\n  background-color: #ffffff;\n  text-align: center;\n}\n.section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .card__body[_ngcontent-%COMP%] {\n  padding: 2em;\n}\n.section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .card__title[_ngcontent-%COMP%] {\n  font-size: 1.5em;\n  color: #283e51;\n}\n.section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .card__text[_ngcontent-%COMP%] {\n  line-height: 1.5;\n}\n.section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .card__text[_ngcontent-%COMP%]   li[_ngcontent-%COMP%] {\n  margin-bottom: 20px;\n}\n.section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .card__price[_ngcontent-%COMP%] {\n  font-family: \"Arial Black\";\n  font-size: 3em;\n  background: linear-gradient(#2c3e50, #2980b9);\n  -webkit-background-clip: text;\n  -webkit-text-fill-color: transparent;\n}\n.section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .active[_ngcontent-%COMP%] {\n  background-image: linear-gradient(#2c3e50, #2980b9);\n  padding: 50px 16px;\n}\n.section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .active[_ngcontent-%COMP%]   .card__title[_ngcontent-%COMP%], .section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .active[_ngcontent-%COMP%]   .card__body[_ngcontent-%COMP%], .section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .active[_ngcontent-%COMP%]   .card__price[_ngcontent-%COMP%] {\n  color: #ffffff;\n}\n.section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .active[_ngcontent-%COMP%]   .card__price[_ngcontent-%COMP%] {\n  -webkit-text-fill-color: initial;\n}\n.section[_ngcontent-%COMP%]   #second-tab[_ngcontent-%COMP%] {\n  display: none;\n  opacity: 0;\n}\n.section[_ngcontent-%COMP%]   .child[_ngcontent-%COMP%] {\n  text-align: center;\n}\n.section[_ngcontent-%COMP%]   .child[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n  text-transform: uppercase;\n  color: white;\n  font-size: 2em;\n}\n@media (min-width: 768px) and (max-width: 1366px) {\n  .header__container[_ngcontent-%COMP%] {\n    max-width: 960px;\n  }\n  .header__container[_ngcontent-%COMP%]   div[_ngcontent-%COMP%] {\n    width: 50%;\n  }\n\n  section[_ngcontent-%COMP%] {\n    padding: 100px 0;\n  }\n  section[_ngcontent-%COMP%]   #first-tab[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%], section[_ngcontent-%COMP%]   #second-tab[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%] {\n    flex-direction: row;\n  }\n  section[_ngcontent-%COMP%]   #first-tab[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%]   .card__body[_ngcontent-%COMP%], section[_ngcontent-%COMP%]   #second-tab[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%]   .card__body[_ngcontent-%COMP%] {\n    padding: 2em 0;\n  }\n\n  #webhosting-help[_ngcontent-%COMP%] {\n    flex-wrap: wrap;\n  }\n  #webhosting-help[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%] {\n    flex-basis: 50%;\n    display: flex;\n  }\n  #webhosting-help[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%] {\n    display: flex;\n  }\n}\n@media (min-width: 768px) and (max-width: 1366px) {\n  .header__container[_ngcontent-%COMP%] {\n    max-width: 960px;\n  }\n  .header__container[_ngcontent-%COMP%]   div[_ngcontent-%COMP%] {\n    width: 50%;\n  }\n}\n@media (min-width: 768px) {\n  .header__container[_ngcontent-%COMP%] {\n    max-width: 85%;\n  }\n  .header__container[_ngcontent-%COMP%]   div[_ngcontent-%COMP%]:first-child {\n    text-align: left;\n    width: 80%;\n    margin-top: 50px;\n  }\n  .header__container[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n    font-size: 2em;\n  }\n\n  .section#wordpress-maintenance[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%] {\n    flex-wrap: wrap;\n    align-items: center;\n  }\n  .section#wordpress-maintenance[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%] {\n    flex-basis: 20%;\n    display: flex;\n  }\n  .section#wordpress-maintenance[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    padding: 20px;\n  }\n  .section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%] {\n    flex-direction: row;\n    text-align: left;\n    justify-content: space-around;\n  }\n  .section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column__body[_ngcontent-%COMP%] {\n    text-align: left;\n  }\n  .section--color--dark[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%] {\n    text-align: center;\n  }\n}\n@media (min-width: 1366px) {\n  .header__container[_ngcontent-%COMP%] {\n    max-width: 1200px;\n  }\n  .header__container[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n    font-size: 3.5rem;\n  }\n}\n@media (max-width: 1366px) and (min-width: 1024px) {\n  .section[_ngcontent-%COMP%]   #first-tab[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%], section[_ngcontent-%COMP%]   #second-tab[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%] {\n    flex-direction: row;\n  }\n}\n@media (min-width: 813px) {\n  .header__container[_ngcontent-%COMP%]   div[_ngcontent-%COMP%] {\n    width: 50%;\n  }\n\n  .header__container[_ngcontent-%COMP%]   div[_ngcontent-%COMP%]:first-child {\n    text-align: left;\n  }\n  .header__container[_ngcontent-%COMP%]   div[_ngcontent-%COMP%]:first-child   h1[_ngcontent-%COMP%] {\n    font-size: 3.5rem;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2NoaWRvL3Byb2plY3RzL2hvc3Rmcm9udC1wcm9qZWN0L2hvc3Rmcm9udC1mcm9udGVuZC9zcmMvYXBwL3dvcmRwcmVzcy1tYWludGVuYW5jZS93b3JkcHJlc3MtbWFpbnRlbmFuY2UuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3dvcmRwcmVzcy1tYWludGVuYW5jZS93b3JkcHJlc3MtbWFpbnRlbmFuY2UuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxvSEFBQTtFQUVBLDRCQUFBO0VBQ0EsNEJBQUE7RUFDQSw0QkFBQTtFQUNBLHNCQUFBO0FDQUY7QURJRTtFQUNFLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFdBQUE7QUNGSjtBRElJO0VBQ0UsYUFBQTtBQ0ZOO0FETUU7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtBQ0pKO0FETUk7RUFDRSxrQkFBQTtBQ0pOO0FETU07RUFDRSxpQkFBQTtBQ0pSO0FEVUE7RUFDRSxnQkFBQTtBQ1BGO0FEUUU7RUFDRSxjQUFBO0VBQ0Esa0JBQUE7QUNOSjtBRE9JO0VBQ0UsbUJBQUE7QUNMTjtBRFFFO0VBQ0UseUJBQUE7QUNOSjtBRFFJO0VBQ0UsbURBQUE7QUNOTjtBRFFNO0VBQ0Usa0JBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0FDTlI7QURVSTtFQUNFLDRHQUFBO0VBRUEsNEJBQUE7RUFDQSwrQkFBQTtFQUNBLDRCQUFBO0VBQ0Esc0JBQUE7QUNUTjtBRGFFO0VBQ0UsaUJBQUE7RUFDQSxpQkFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtBQ1hKO0FEYUk7RUFDRSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxzQkFBQTtBQ1hOO0FEYU07RUFDRSxnQkFBQTtFQUNBLFdBQUE7QUNYUjtBRGVJO0VBQ0Usa0JBQUE7QUNiTjtBRGdCSTtFQUNFLHlCQUFBO0VBQ0EsY0FBQTtFQUNBLGNBQUE7QUNkTjtBRGlCSTtFQUNFLGFBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxpREFBQTtFQUNBLHlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EseUJBQUE7RUFDQSx1QkFBQTtFQUNBLGNBQUE7RUFDQSxVQUFBO0FDZk47QURpQk07RUFDRSxxQkFBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0VBQ0EscUJBQUE7RUFDQSx3Q0FBQTtBQ2ZSO0FEaUJRO0VBQ0UseUJBQUE7RUFDQSxjQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLHFCQUFBO0VBQ0EsZUFBQTtBQ2ZWO0FEa0JRO0VBQ0UsY0FBQTtBQ2hCVjtBRG1CUTtFQUNFLFlBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSwyQkFBQTtFQUNBLFFBQUE7RUFDQSx5QkFBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0FDakJWO0FEc0JJO0VBQ0UsYUFBQTtFQUNBLE9BQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7QUNwQk47QURzQk07RUFDRSxhQUFBO0VBQ0EsWUFBQTtBQ3BCUjtBRHVCTTs7RUFFRSxrQkFBQTtFQUNBLFlBQUE7QUNyQlI7QUR1QlE7O0VBQ0UsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsVUFBQTtBQ3BCVjtBRHlCUTtFQUNFLGVBQUE7RUFDQSxjQUFBO0FDdkJWO0FEMkJNO0VBQ0Usa0JBQUE7QUN6QlI7QUQyQlE7RUFDRSxnQkFBQTtFQUNBLGNBQUE7QUN6QlY7QUQ2Qk07RUFDRSwwQ0FBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7QUMzQlI7QUQ2QlE7RUFDRSxZQUFBO0FDM0JWO0FEOEJRO0VBQ0UsZ0JBQUE7RUFDQSxjQUFBO0FDNUJWO0FEK0JRO0VBQ0UsZ0JBQUE7QUM3QlY7QUQ4QlU7RUFDRSxtQkFBQTtBQzVCWjtBRGdDUTtFQUNFLDBCQUFBO0VBQ0EsY0FBQTtFQUNBLDZDQUFBO0VBQ0EsNkJBQUE7RUFDQSxvQ0FBQTtBQzlCVjtBRGtDTTtFQUNFLG1EQUFBO0VBQ0Esa0JBQUE7QUNoQ1I7QURpQ1E7OztFQUdFLGNBQUE7QUMvQlY7QURpQ1E7RUFDRSxnQ0FBQTtBQy9CVjtBRHFDRTtFQUNFLGFBQUE7RUFDQSxVQUFBO0FDbkNKO0FEc0NFO0VBQ0Usa0JBQUE7QUNwQ0o7QURxQ0k7RUFDRSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxjQUFBO0FDbkNOO0FEd0NBO0VBQ0U7SUFDRSxnQkFBQTtFQ3JDRjtFRHNDRTtJQUNFLFVBQUE7RUNwQ0o7O0VEdUNBO0lBQ0UsZ0JBQUE7RUNwQ0Y7RURzQ0k7SUFDRSxtQkFBQTtFQ3BDTjtFRHFDTTtJQUNFLGNBQUE7RUNuQ1I7O0VEd0NBO0lBQ0UsZUFBQTtFQ3JDRjtFRHNDRTtJQUNFLGVBQUE7SUFDQSxhQUFBO0VDcENKO0VEcUNJO0lBQ0UsYUFBQTtFQ25DTjtBQUNGO0FEd0NBO0VBQ0U7SUFDRSxnQkFBQTtFQ3RDRjtFRHVDRTtJQUNFLFVBQUE7RUNyQ0o7QUFDRjtBRHlDQTtFQUNFO0lBQ0UsY0FBQTtFQ3ZDRjtFRHdDRTtJQUNFLGdCQUFBO0lBQ0EsVUFBQTtJQUNBLGdCQUFBO0VDdENKO0VEd0NFO0lBQ0UsY0FBQTtFQ3RDSjs7RUQ0Q0k7SUFDRSxlQUFBO0lBQ0EsbUJBQUE7RUN6Q047RUQwQ007SUFDRSxlQUFBO0lBQ0EsYUFBQTtFQ3hDUjtFRHlDUTtJQUNFLGFBQUE7RUN2Q1Y7RUQ2Q0k7SUFDRSxtQkFBQTtJQUNBLGdCQUFBO0lBQ0EsNkJBQUE7RUMzQ047RUQrQ007SUFDRSxnQkFBQTtFQzdDUjtFRG1ESTtJQUNFLGtCQUFBO0VDakROO0FBQ0Y7QURzREE7RUFDRTtJQUNFLGlCQUFBO0VDcERGO0VEcURFO0lBQ0UsaUJBQUE7RUNuREo7QUFDRjtBRHVEQTtFQUNBO0lBR0ksbUJBQUE7RUNyREY7QUFDRjtBRHdEQTtFQUVJO0lBQ0UsVUFBQTtFQ3ZESjs7RUQ2REk7SUFDRSxnQkFBQTtFQzFETjtFRDRETTtJQUNFLGlCQUFBO0VDMURSO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC93b3JkcHJlc3MtbWFpbnRlbmFuY2Uvd29yZHByZXNzLW1haW50ZW5hbmNlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmhlYWRlciB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgaGVpZ2h0OiA3NTBweDtcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCByZ2JhKDQ0LCA2MiwgODAsIDEpLCByZ2JhKDQxLCAxMjgsIDE4NSwgMC41KSksXG4gICAgdXJsKFwiL2Fzc2V0cy9pbWFnZXMvc2VydmljZXMtYmcuanBnXCIpO1xuICBiYWNrZ3JvdW5kLWF0dGFjaG1lbnQ6IGZpeGVkO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiA1MCUgNTAlO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuXG4gIC8vICMyOTgwYjkgIzJjM2U1MFxuXG4gICYgc3ZnIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYm90dG9tOiAwO1xuICAgIHdpZHRoOiAxMDAlO1xuXG4gICAgJiBwYXRoIHtcbiAgICAgIGZpbGw6ICNmZmZmZmY7XG4gICAgfVxuICB9XG5cbiAgJl9fY29udGFpbmVyIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBtYXJnaW46IGF1dG87XG4gICAgY29sb3I6IHdoaXRlO1xuXG4gICAgJiBkaXYge1xuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuXG4gICAgICAmIHAge1xuICAgICAgICBtYXJnaW46IDIwIDAgMjAgMDtcbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cblxuLnNlY3Rpb24ge1xuICBwYWRkaW5nOiAxMDBweCAwO1xuICAmLndvcmRwcmVzcy1ob3N0aW5nIHtcbiAgICBsaW5lLWhlaWdodDogMjtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgJiAuY29udGFpbmVyIC5mbGV4IHtcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgfVxuICB9XG4gICYtLWNvbG9yIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjhmYWZlO1xuXG4gICAgJi0tZGFyayB7XG4gICAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoIzJjM2U1MCwgIzI5ODBiOSk7XG5cbiAgICAgICYgLmNvbnRhaW5lciAuZmxleCB7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgbGluZS1oZWlnaHQ6IDI7XG4gICAgICAgIGZvbnQtc2l6ZTogMS4yNWVtO1xuICAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgICB9XG4gICAgfVxuXG4gICAgJi0tYmcge1xuICAgICAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCByZ2JhKDQxLCAxMjgsIDE4NSwgMSksIHJnYmEoNDQsIDYyLCA4MCwgMC41KSksXG4gICAgICAgIHVybChcIi9hc3NldHMvaW1hZ2VzL2JnXzAyLmpwZ1wiKTtcbiAgICAgIGJhY2tncm91bmQtYXR0YWNobWVudDogZml4ZWQ7XG4gICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXIgdG9wO1xuICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgfVxuICB9XG4gXG4gICYgLmNvbnRhaW5lciB7XG4gICAgcGFkZGluZzogMzBweCAwcHg7XG4gICAgbWF4LXdpZHRoOiAxMTQwcHg7XG4gICAgbWFyZ2luOiBhdXRvO1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gIFxuICAgICYgLmZsZXgge1xuICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcblxuICAgICAgJiAjd3Bsb2dvIHtcbiAgICAgICAgbWF4LXdpZHRoOiAyNTBweDtcbiAgICAgICAgbWFyZ2luOiAzZW07XG4gICAgICB9XG4gICAgfVxuXG4gICAgJiAuaGVhZGluZyB7XG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgfVxuXG4gICAgJiBoMiB7XG4gICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgICAgZm9udC1zaXplOiAyZW07XG4gICAgICBjb2xvcjogIzI4M2U1MTtcbiAgICB9XG5cbiAgICAmIC50YWIge1xuICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgIGZsZXgtd3JhcDogd3JhcDtcbiAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgIGJveC1zaGFkb3c6IDBweCAyMHB4IDQ1cHggMHB4IHJnYmEoMCwgMCwgMCwgMC4wOCk7XG4gICAgICBib3JkZXItcmFkaXVzOiAxLjYxNzY1cmVtO1xuICAgICAgbWF4LXdpZHRoOiAzNzBweDtcbiAgICAgIG1hcmdpbjogMCBhdXRvO1xuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgcGFkZGluZzogNXB4IDA7XG4gICAgICB6LWluZGV4OiAxO1xuXG4gICAgICAmIGxpIHtcbiAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgICAgICBtYXJnaW46IDA7XG4gICAgICAgIHBhZGRpbmc6IDA7XG4gICAgICAgIGxpc3Qtc3R5bGUtdHlwZTogbm9uZTtcbiAgICAgICAgLXdlYmtpdC10YXAtaGlnaGxpZ2h0LWNvbG9yOiB0cmFuc3BhcmVudDtcblxuICAgICAgICAmIGEge1xuICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDEuMzUyOTRyZW07XG4gICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgY29sb3I6ICNmZmZmZmY7XG4gICAgICAgICAgbWluLXdpZHRoOiAxMjVweDtcbiAgICAgICAgICBwYWRkaW5nOiA4cHggMzFweDtcbiAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICAgICAgfVxuXG4gICAgICAgICYgI3llYXJseS10YWIge1xuICAgICAgICAgIGNvbG9yOiAjMmMzZTUwO1xuICAgICAgICB9XG5cbiAgICAgICAgJiNpbmRpY2F0b3Ige1xuICAgICAgICAgIHdpZHRoOiAxNTBweDtcbiAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMTg2OGRkO1xuICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICBoZWlnaHQ6IGNhbGMoMTAwJSAtIDEwcHgpO1xuICAgICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNTAlKTtcbiAgICAgICAgICB0b3A6IDUwJTtcbiAgICAgICAgICBib3JkZXItcmFkaXVzOiAxLjM1Mjk0cmVtO1xuICAgICAgICAgIHotaW5kZXg6IC0xO1xuICAgICAgICAgIGxlZnQ6IDVweDtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuICAgICYgLmNvbHVtbiB7XG4gICAgICBwYWRkaW5nOiAyMHB4O1xuICAgICAgZmxleDogMTtcbiAgICAgIGxpbmUtaGVpZ2h0OiAxLjU7XG4gICAgICBjb2xvcjogIzI5ODBiOTtcblxuICAgICAgLmNvbHVtbl9faWNvbiB7XG4gICAgICAgIGhlaWdodDogMjQ0cHg7XG4gICAgICAgIHdpZHRoOiAyNDRweDtcbiAgICAgIH1cblxuICAgICAgLmNvbHVtbl9faWNvbixcbiAgICAgIC5jYXJkX19pY29uIHtcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICBtYXJnaW46IGF1dG87XG5cbiAgICAgICAgJiBpbWcge1xuICAgICAgICAgIG1heC1oZWlnaHQ6IDEwMCU7XG4gICAgICAgICAgbWF4LXdpZHRoOiAxMDAlO1xuICAgICAgICAgIHBhZGRpbmc6IDA7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgLmNhcmRfX2ljb24ge1xuICAgICAgICAmIGltZyB7XG4gICAgICAgICAgbWF4LWhlaWdodDogNTAlO1xuICAgICAgICAgIG1heC13aWR0aDogNTAlO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgICZfX2JvZHkge1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG5cbiAgICAgICAgJiBoMyB7XG4gICAgICAgICAgZm9udC1zaXplOiAxLjVlbTtcbiAgICAgICAgICBjb2xvcjogIzI4M2U1MTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICAmIC5jYXJkIHtcbiAgICAgICAgYm94LXNoYWRvdzogMCA0cHggOHB4IDAgcmdiYSgwLCAwLCAwLCAwLjIpO1xuICAgICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgICAgICBwYWRkaW5nOiAxNnB4O1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG5cbiAgICAgICAgJl9fYm9keSB7XG4gICAgICAgICAgcGFkZGluZzogMmVtO1xuICAgICAgICB9XG5cbiAgICAgICAgJl9fdGl0bGUge1xuICAgICAgICAgIGZvbnQtc2l6ZTogMS41ZW07XG4gICAgICAgICAgY29sb3I6ICMyODNlNTE7XG4gICAgICAgIH1cblxuICAgICAgICAmX190ZXh0IHtcbiAgICAgICAgICBsaW5lLWhlaWdodDogMS41O1xuICAgICAgICAgICYgbGkge1xuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMjBweDtcbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICAmX19wcmljZSB7XG4gICAgICAgICAgZm9udC1mYW1pbHk6IFwiQXJpYWwgQmxhY2tcIjtcbiAgICAgICAgICBmb250LXNpemU6IDNlbTtcbiAgICAgICAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoIzJjM2U1MCwgIzI5ODBiOSk7XG4gICAgICAgICAgLXdlYmtpdC1iYWNrZ3JvdW5kLWNsaXA6IHRleHQ7XG4gICAgICAgICAgLXdlYmtpdC10ZXh0LWZpbGwtY29sb3I6IHRyYW5zcGFyZW50O1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgICYgLmFjdGl2ZSB7XG4gICAgICAgIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCgjMmMzZTUwLCAjMjk4MGI5KTtcbiAgICAgICAgcGFkZGluZzogNTBweCAxNnB4O1xuICAgICAgICAmIC5jYXJkX190aXRsZSxcbiAgICAgICAgLmNhcmRfX2JvZHksXG4gICAgICAgIC5jYXJkX19wcmljZSB7XG4gICAgICAgICAgY29sb3I6ICNmZmZmZmY7XG4gICAgICAgIH1cbiAgICAgICAgJiAuY2FyZF9fcHJpY2Uge1xuICAgICAgICAgIC13ZWJraXQtdGV4dC1maWxsLWNvbG9yOiBpbml0aWFsO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgJiAjc2Vjb25kLXRhYiB7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgICBvcGFjaXR5OiAwO1xuICB9XG5cbiAgLmNoaWxkIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgJiBoMSB7XG4gICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgICAgY29sb3I6IHdoaXRlO1xuICAgICAgZm9udC1zaXplOiAyZW07XG4gICAgfVxuICB9XG59XG5cbkBtZWRpYSAobWluLXdpZHRoOiA3NjhweCkgYW5kICAobWF4LXdpZHRoOiAxMzY2cHgpIHtcbiAgLmhlYWRlcl9fY29udGFpbmVyIHtcbiAgICBtYXgtd2lkdGg6IDk2MHB4O1xuICAgICYgZGl2IHtcbiAgICAgIHdpZHRoOiA1MCU7XG4gICAgfVxuICB9XG4gIHNlY3Rpb24ge1xuICAgIHBhZGRpbmc6IDEwMHB4IDA7XG4gICAgJiAgI2ZpcnN0LXRhYiwgI3NlY29uZC10YWJ7XG4gICAgICAuZmxleCB7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgICYgLmNvbHVtbiAuY2FyZCAuY2FyZF9fYm9keSB7XG4gICAgICAgICAgcGFkZGluZzogMmVtIDA7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cbiAgI3dlYmhvc3RpbmctaGVscCB7XG4gICAgZmxleC13cmFwOiB3cmFwO1xuICAgICYgLmNvbHVtbiB7XG4gICAgICBmbGV4LWJhc2lzOiA1MCU7XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgJiAuY2FyZCB7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICB9XG4gICAgfVxuICB9XG59XG5cbkBtZWRpYSAobWluLXdpZHRoOiA3NjhweCkgYW5kICAobWF4LXdpZHRoOiAxMzY2cHgpIHtcbiAgLmhlYWRlcl9fY29udGFpbmVyIHtcbiAgICBtYXgtd2lkdGg6IDk2MHB4O1xuICAgICYgZGl2IHtcbiAgICAgIHdpZHRoOiA1MCU7XG4gICAgfVxuICB9XG59XG5cbkBtZWRpYSAobWluLXdpZHRoOiA3NjhweCkge1xuICAuaGVhZGVyX19jb250YWluZXIge1xuICAgIG1heC13aWR0aDogODUlO1xuICAgICYgZGl2OmZpcnN0LWNoaWxkIHtcbiAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgICB3aWR0aDogODAlO1xuICAgICAgbWFyZ2luLXRvcDogNTBweDtcbiAgICB9XG4gICAgJiBoMSB7XG4gICAgICBmb250LXNpemU6IDJlbTtcbiAgICB9XG4gIH1cblxuICAuc2VjdGlvbiB7XG4gICAgJiN3b3JkcHJlc3MtbWFpbnRlbmFuY2Uge1xuICAgICAgJiAuZmxleCB7XG4gICAgICAgIGZsZXgtd3JhcDogd3JhcDtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgJiAuY29sdW1uIHtcbiAgICAgICAgICBmbGV4LWJhc2lzOiAyMCU7XG4gICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAmIGltZyB7XG4gICAgICAgICAgICBwYWRkaW5nOiAyMHB4O1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgICAuY29udGFpbmVyIHtcbiAgICAgIC5mbGV4IHtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG5cbiAgICAgIH1cbiAgICAgICYgLmNvbHVtbiB7XG4gICAgICAgICZfX2JvZHkge1xuICAgICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG5cbiAgICAmLS1jb2xvci0tZGFyayB7XG4gICAgICAmIC5jb250YWluZXIgLmZsZXgge1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICB9XG4gICAgfVxuICB9XG59XG5cbkBtZWRpYSAobWluLXdpZHRoOiAxMzY2cHgpIHtcbiAgLmhlYWRlcl9fY29udGFpbmVyIHtcbiAgICBtYXgtd2lkdGg6IDEyMDBweDtcbiAgICAmIGgxIHtcbiAgICAgIGZvbnQtc2l6ZTogMy41cmVtO1xuICAgIH1cbiAgfVxufVxuXG5AbWVkaWEgKG1heC13aWR0aDogMTM2NnB4KSBhbmQgKG1pbi13aWR0aDogMTAyNHB4KSB7XG4uc2VjdGlvbiAjZmlyc3QtdGFiIC5mbGV4LCBzZWN0aW9uICNzZWNvbmQtdGFiIC5mbGV4IHtcbiAgICAtd2Via2l0LWJveC1vcmllbnQ6IGhvcml6b250YWw7XG4gICAgLXdlYmtpdC1ib3gtZGlyZWN0aW9uOiBub3JtYWw7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbn1cbn1cblxuQG1lZGlhIChtaW4td2lkdGg6IDgxM3B4KSB7XG4gIC5oZWFkZXJfX2NvbnRhaW5lciB7XG4gICAgJiBkaXYge1xuICAgICAgd2lkdGg6IDUwJTtcbiAgICB9XG4gIH1cblxuICAuaGVhZGVyIHtcbiAgICAmX19jb250YWluZXIge1xuICAgICAgJiBkaXY6Zmlyc3QtY2hpbGQge1xuICAgICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuXG4gICAgICAgICYgaDEge1xuICAgICAgICAgIGZvbnQtc2l6ZTogMy41cmVtO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG59IiwiLmhlYWRlciB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgaGVpZ2h0OiA3NTBweDtcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjMmMzZTUwLCByZ2JhKDQxLCAxMjgsIDE4NSwgMC41KSksIHVybChcIi9hc3NldHMvaW1hZ2VzL3NlcnZpY2VzLWJnLmpwZ1wiKTtcbiAgYmFja2dyb3VuZC1hdHRhY2htZW50OiBmaXhlZDtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogNTAlIDUwJTtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3Zlcjtcbn1cbi5oZWFkZXIgc3ZnIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBib3R0b206IDA7XG4gIHdpZHRoOiAxMDAlO1xufVxuLmhlYWRlciBzdmcgcGF0aCB7XG4gIGZpbGw6ICNmZmZmZmY7XG59XG4uaGVhZGVyX19jb250YWluZXIge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xuICBkaXNwbGF5OiBmbGV4O1xuICBtYXJnaW46IGF1dG87XG4gIGNvbG9yOiB3aGl0ZTtcbn1cbi5oZWFkZXJfX2NvbnRhaW5lciBkaXYge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uaGVhZGVyX19jb250YWluZXIgZGl2IHAge1xuICBtYXJnaW46IDIwIDAgMjAgMDtcbn1cblxuLnNlY3Rpb24ge1xuICBwYWRkaW5nOiAxMDBweCAwO1xufVxuLnNlY3Rpb24ud29yZHByZXNzLWhvc3Rpbmcge1xuICBsaW5lLWhlaWdodDogMjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLnNlY3Rpb24ud29yZHByZXNzLWhvc3RpbmcgLmNvbnRhaW5lciAuZmxleCB7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4uc2VjdGlvbi0tY29sb3Ige1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjhmYWZlO1xufVxuLnNlY3Rpb24tLWNvbG9yLS1kYXJrIHtcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KCMyYzNlNTAsICMyOTgwYjkpO1xufVxuLnNlY3Rpb24tLWNvbG9yLS1kYXJrIC5jb250YWluZXIgLmZsZXgge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGxpbmUtaGVpZ2h0OiAyO1xuICBmb250LXNpemU6IDEuMjVlbTtcbiAgY29sb3I6IHdoaXRlO1xufVxuLnNlY3Rpb24tLWNvbG9yLS1iZyB7XG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgIzI5ODBiOSwgcmdiYSg0NCwgNjIsIDgwLCAwLjUpKSwgdXJsKFwiL2Fzc2V0cy9pbWFnZXMvYmdfMDIuanBnXCIpO1xuICBiYWNrZ3JvdW5kLWF0dGFjaG1lbnQ6IGZpeGVkO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXIgdG9wO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xufVxuLnNlY3Rpb24gLmNvbnRhaW5lciB7XG4gIHBhZGRpbmc6IDMwcHggMHB4O1xuICBtYXgtd2lkdGg6IDExNDBweDtcbiAgbWFyZ2luOiBhdXRvO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuLnNlY3Rpb24gLmNvbnRhaW5lciAuZmxleCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xufVxuLnNlY3Rpb24gLmNvbnRhaW5lciAuZmxleCAjd3Bsb2dvIHtcbiAgbWF4LXdpZHRoOiAyNTBweDtcbiAgbWFyZ2luOiAzZW07XG59XG4uc2VjdGlvbiAuY29udGFpbmVyIC5oZWFkaW5nIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLnNlY3Rpb24gLmNvbnRhaW5lciBoMiB7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGZvbnQtc2l6ZTogMmVtO1xuICBjb2xvcjogIzI4M2U1MTtcbn1cbi5zZWN0aW9uIC5jb250YWluZXIgLnRhYiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtd3JhcDogd3JhcDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBib3gtc2hhZG93OiAwcHggMjBweCA0NXB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMDgpO1xuICBib3JkZXItcmFkaXVzOiAxLjYxNzY1cmVtO1xuICBtYXgtd2lkdGg6IDM3MHB4O1xuICBtYXJnaW46IDAgYXV0bztcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIHBhZGRpbmc6IDVweCAwO1xuICB6LWluZGV4OiAxO1xufVxuLnNlY3Rpb24gLmNvbnRhaW5lciAudGFiIGxpIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBtYXJnaW46IDA7XG4gIHBhZGRpbmc6IDA7XG4gIGxpc3Qtc3R5bGUtdHlwZTogbm9uZTtcbiAgLXdlYmtpdC10YXAtaGlnaGxpZ2h0LWNvbG9yOiB0cmFuc3BhcmVudDtcbn1cbi5zZWN0aW9uIC5jb250YWluZXIgLnRhYiBsaSBhIHtcbiAgYm9yZGVyLXJhZGl1czogMS4zNTI5NHJlbTtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICBtaW4td2lkdGg6IDEyNXB4O1xuICBwYWRkaW5nOiA4cHggMzFweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cbi5zZWN0aW9uIC5jb250YWluZXIgLnRhYiBsaSAjeWVhcmx5LXRhYiB7XG4gIGNvbG9yOiAjMmMzZTUwO1xufVxuLnNlY3Rpb24gLmNvbnRhaW5lciAudGFiIGxpI2luZGljYXRvciB7XG4gIHdpZHRoOiAxNTBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzE4NjhkZDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBoZWlnaHQ6IGNhbGMoMTAwJSAtIDEwcHgpO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XG4gIHRvcDogNTAlO1xuICBib3JkZXItcmFkaXVzOiAxLjM1Mjk0cmVtO1xuICB6LWluZGV4OiAtMTtcbiAgbGVmdDogNXB4O1xufVxuLnNlY3Rpb24gLmNvbnRhaW5lciAuY29sdW1uIHtcbiAgcGFkZGluZzogMjBweDtcbiAgZmxleDogMTtcbiAgbGluZS1oZWlnaHQ6IDEuNTtcbiAgY29sb3I6ICMyOTgwYjk7XG59XG4uc2VjdGlvbiAuY29udGFpbmVyIC5jb2x1bW4gLmNvbHVtbl9faWNvbiB7XG4gIGhlaWdodDogMjQ0cHg7XG4gIHdpZHRoOiAyNDRweDtcbn1cbi5zZWN0aW9uIC5jb250YWluZXIgLmNvbHVtbiAuY29sdW1uX19pY29uLFxuLnNlY3Rpb24gLmNvbnRhaW5lciAuY29sdW1uIC5jYXJkX19pY29uIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtYXJnaW46IGF1dG87XG59XG4uc2VjdGlvbiAuY29udGFpbmVyIC5jb2x1bW4gLmNvbHVtbl9faWNvbiBpbWcsXG4uc2VjdGlvbiAuY29udGFpbmVyIC5jb2x1bW4gLmNhcmRfX2ljb24gaW1nIHtcbiAgbWF4LWhlaWdodDogMTAwJTtcbiAgbWF4LXdpZHRoOiAxMDAlO1xuICBwYWRkaW5nOiAwO1xufVxuLnNlY3Rpb24gLmNvbnRhaW5lciAuY29sdW1uIC5jYXJkX19pY29uIGltZyB7XG4gIG1heC1oZWlnaHQ6IDUwJTtcbiAgbWF4LXdpZHRoOiA1MCU7XG59XG4uc2VjdGlvbiAuY29udGFpbmVyIC5jb2x1bW5fX2JvZHkge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uc2VjdGlvbiAuY29udGFpbmVyIC5jb2x1bW5fX2JvZHkgaDMge1xuICBmb250LXNpemU6IDEuNWVtO1xuICBjb2xvcjogIzI4M2U1MTtcbn1cbi5zZWN0aW9uIC5jb250YWluZXIgLmNvbHVtbiAuY2FyZCB7XG4gIGJveC1zaGFkb3c6IDAgNHB4IDhweCAwIHJnYmEoMCwgMCwgMCwgMC4yKTtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgcGFkZGluZzogMTZweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLnNlY3Rpb24gLmNvbnRhaW5lciAuY29sdW1uIC5jYXJkX19ib2R5IHtcbiAgcGFkZGluZzogMmVtO1xufVxuLnNlY3Rpb24gLmNvbnRhaW5lciAuY29sdW1uIC5jYXJkX190aXRsZSB7XG4gIGZvbnQtc2l6ZTogMS41ZW07XG4gIGNvbG9yOiAjMjgzZTUxO1xufVxuLnNlY3Rpb24gLmNvbnRhaW5lciAuY29sdW1uIC5jYXJkX190ZXh0IHtcbiAgbGluZS1oZWlnaHQ6IDEuNTtcbn1cbi5zZWN0aW9uIC5jb250YWluZXIgLmNvbHVtbiAuY2FyZF9fdGV4dCBsaSB7XG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XG59XG4uc2VjdGlvbiAuY29udGFpbmVyIC5jb2x1bW4gLmNhcmRfX3ByaWNlIHtcbiAgZm9udC1mYW1pbHk6IFwiQXJpYWwgQmxhY2tcIjtcbiAgZm9udC1zaXplOiAzZW07XG4gIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgjMmMzZTUwLCAjMjk4MGI5KTtcbiAgLXdlYmtpdC1iYWNrZ3JvdW5kLWNsaXA6IHRleHQ7XG4gIC13ZWJraXQtdGV4dC1maWxsLWNvbG9yOiB0cmFuc3BhcmVudDtcbn1cbi5zZWN0aW9uIC5jb250YWluZXIgLmNvbHVtbiAuYWN0aXZlIHtcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KCMyYzNlNTAsICMyOTgwYjkpO1xuICBwYWRkaW5nOiA1MHB4IDE2cHg7XG59XG4uc2VjdGlvbiAuY29udGFpbmVyIC5jb2x1bW4gLmFjdGl2ZSAuY2FyZF9fdGl0bGUsXG4uc2VjdGlvbiAuY29udGFpbmVyIC5jb2x1bW4gLmFjdGl2ZSAuY2FyZF9fYm9keSxcbi5zZWN0aW9uIC5jb250YWluZXIgLmNvbHVtbiAuYWN0aXZlIC5jYXJkX19wcmljZSB7XG4gIGNvbG9yOiAjZmZmZmZmO1xufVxuLnNlY3Rpb24gLmNvbnRhaW5lciAuY29sdW1uIC5hY3RpdmUgLmNhcmRfX3ByaWNlIHtcbiAgLXdlYmtpdC10ZXh0LWZpbGwtY29sb3I6IGluaXRpYWw7XG59XG4uc2VjdGlvbiAjc2Vjb25kLXRhYiB7XG4gIGRpc3BsYXk6IG5vbmU7XG4gIG9wYWNpdHk6IDA7XG59XG4uc2VjdGlvbiAuY2hpbGQge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uc2VjdGlvbiAuY2hpbGQgaDEge1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMmVtO1xufVxuXG5AbWVkaWEgKG1pbi13aWR0aDogNzY4cHgpIGFuZCAobWF4LXdpZHRoOiAxMzY2cHgpIHtcbiAgLmhlYWRlcl9fY29udGFpbmVyIHtcbiAgICBtYXgtd2lkdGg6IDk2MHB4O1xuICB9XG4gIC5oZWFkZXJfX2NvbnRhaW5lciBkaXYge1xuICAgIHdpZHRoOiA1MCU7XG4gIH1cblxuICBzZWN0aW9uIHtcbiAgICBwYWRkaW5nOiAxMDBweCAwO1xuICB9XG4gIHNlY3Rpb24gI2ZpcnN0LXRhYiAuZmxleCwgc2VjdGlvbiAjc2Vjb25kLXRhYiAuZmxleCB7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgfVxuICBzZWN0aW9uICNmaXJzdC10YWIgLmZsZXggLmNvbHVtbiAuY2FyZCAuY2FyZF9fYm9keSwgc2VjdGlvbiAjc2Vjb25kLXRhYiAuZmxleCAuY29sdW1uIC5jYXJkIC5jYXJkX19ib2R5IHtcbiAgICBwYWRkaW5nOiAyZW0gMDtcbiAgfVxuXG4gICN3ZWJob3N0aW5nLWhlbHAge1xuICAgIGZsZXgtd3JhcDogd3JhcDtcbiAgfVxuICAjd2ViaG9zdGluZy1oZWxwIC5jb2x1bW4ge1xuICAgIGZsZXgtYmFzaXM6IDUwJTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICB9XG4gICN3ZWJob3N0aW5nLWhlbHAgLmNvbHVtbiAuY2FyZCB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6IDc2OHB4KSBhbmQgKG1heC13aWR0aDogMTM2NnB4KSB7XG4gIC5oZWFkZXJfX2NvbnRhaW5lciB7XG4gICAgbWF4LXdpZHRoOiA5NjBweDtcbiAgfVxuICAuaGVhZGVyX19jb250YWluZXIgZGl2IHtcbiAgICB3aWR0aDogNTAlO1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogNzY4cHgpIHtcbiAgLmhlYWRlcl9fY29udGFpbmVyIHtcbiAgICBtYXgtd2lkdGg6IDg1JTtcbiAgfVxuICAuaGVhZGVyX19jb250YWluZXIgZGl2OmZpcnN0LWNoaWxkIHtcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgIHdpZHRoOiA4MCU7XG4gICAgbWFyZ2luLXRvcDogNTBweDtcbiAgfVxuICAuaGVhZGVyX19jb250YWluZXIgaDEge1xuICAgIGZvbnQtc2l6ZTogMmVtO1xuICB9XG5cbiAgLnNlY3Rpb24jd29yZHByZXNzLW1haW50ZW5hbmNlIC5mbGV4IHtcbiAgICBmbGV4LXdyYXA6IHdyYXA7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgfVxuICAuc2VjdGlvbiN3b3JkcHJlc3MtbWFpbnRlbmFuY2UgLmZsZXggLmNvbHVtbiB7XG4gICAgZmxleC1iYXNpczogMjAlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gIH1cbiAgLnNlY3Rpb24jd29yZHByZXNzLW1haW50ZW5hbmNlIC5mbGV4IC5jb2x1bW4gaW1nIHtcbiAgICBwYWRkaW5nOiAyMHB4O1xuICB9XG4gIC5zZWN0aW9uIC5jb250YWluZXIgLmZsZXgge1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcbiAgfVxuICAuc2VjdGlvbiAuY29udGFpbmVyIC5jb2x1bW5fX2JvZHkge1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gIH1cbiAgLnNlY3Rpb24tLWNvbG9yLS1kYXJrIC5jb250YWluZXIgLmZsZXgge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6IDEzNjZweCkge1xuICAuaGVhZGVyX19jb250YWluZXIge1xuICAgIG1heC13aWR0aDogMTIwMHB4O1xuICB9XG4gIC5oZWFkZXJfX2NvbnRhaW5lciBoMSB7XG4gICAgZm9udC1zaXplOiAzLjVyZW07XG4gIH1cbn1cbkBtZWRpYSAobWF4LXdpZHRoOiAxMzY2cHgpIGFuZCAobWluLXdpZHRoOiAxMDI0cHgpIHtcbiAgLnNlY3Rpb24gI2ZpcnN0LXRhYiAuZmxleCwgc2VjdGlvbiAjc2Vjb25kLXRhYiAuZmxleCB7XG4gICAgLXdlYmtpdC1ib3gtb3JpZW50OiBob3Jpem9udGFsO1xuICAgIC13ZWJraXQtYm94LWRpcmVjdGlvbjogbm9ybWFsO1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIH1cbn1cbkBtZWRpYSAobWluLXdpZHRoOiA4MTNweCkge1xuICAuaGVhZGVyX19jb250YWluZXIgZGl2IHtcbiAgICB3aWR0aDogNTAlO1xuICB9XG5cbiAgLmhlYWRlcl9fY29udGFpbmVyIGRpdjpmaXJzdC1jaGlsZCB7XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgfVxuICAuaGVhZGVyX19jb250YWluZXIgZGl2OmZpcnN0LWNoaWxkIGgxIHtcbiAgICBmb250LXNpemU6IDMuNXJlbTtcbiAgfVxufSJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](WordpressMaintenanceComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-wordpress-maintenance',
                templateUrl: './wordpress-maintenance.component.html',
                styleUrls: ['./wordpress-maintenance.component.scss']
            }]
    }], function () { return [{ type: _pricing_tab_service__WEBPACK_IMPORTED_MODULE_1__["PricingTabService"] }]; }, null); })();


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false,
    baseurl: 'http://hostfront-api.local',
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["platformBrowser"]().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/chido/projects/hostfront-project/hostfront-frontend/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map