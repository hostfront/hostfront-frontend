(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/about/about.component.ts":
/*!******************************************!*\
  !*** ./src/app/about/about.component.ts ***!
  \******************************************/
/*! exports provided: AboutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutComponent", function() { return AboutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _shared_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../shared/navbar/navbar.component */ "./src/app/shared/navbar/navbar.component.ts");
/* harmony import */ var _shared_scroll_scroll_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../shared/scroll/scroll.component */ "./src/app/shared/scroll/scroll.component.ts");
/* harmony import */ var _shared_footer_footer_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../shared/footer/footer.component */ "./src/app/shared/footer/footer.component.ts");





class AboutComponent {
    constructor() { }
    ngOnInit() {
    }
}
AboutComponent.ɵfac = function AboutComponent_Factory(t) { return new (t || AboutComponent)(); };
AboutComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AboutComponent, selectors: [["app-about"]], decls: 100, vars: 0, consts: [[1, "header"], ["id", "curve", "data-name", "Layer 1", "xmlns", "http://www.w3.org/2000/svg", "viewBox", "0 0 1416.99 174.01"], ["d", "M0,280.8S283.66,59,608.94,163.56s437.93,150.57,808,10.34V309.54H0V280.8Z", "transform", "translate(0 -135.53)", 1, "cls-1"], [1, "header__container"], [1, "header__container__content"], [1, "section"], [1, "container"], ["data-aos", "fade-left", 1, "flex"], [1, "cta", "cta--left"], ["src", "assets/images/laptop.svg", 1, "cta__img"], ["data-aos", "fade-right", 1, "flex"], ["src", "assets/images/co2.svg", 1, "cta__img"], [1, "cta", "cta--right"], [1, "section", "color--bg", "color--wavy"], [1, "flex"], ["data-aos", "fade-left", 1, "cta", "cta--color", "cta--left"], ["data-aos", "fade-right", 1, "cta", "cta--color", "cta--right"], ["id", "carbon-offset", 1, "section"], [1, "flex", "heading"], ["data-aos", "fade-up", 1, "flex"], ["src", "assets/images/cloud-computing.png"], [1, "column"], [1, "column__icon"], [1, "column__body"], [1, "btn", "btn--std", "btn--std01"], [1, "section", "color--bg"], [1, "text-container"]], template: function AboutComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "header", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "app-navbar");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceSVG"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "svg", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "path", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceHTML"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "Lorem ipsum dolor sit amet.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "section", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "About Us");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](18, "img", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "img", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, "Carbon Offsetting");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "section", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32, "Lorem ipsum dolor sit amet.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, "Lorem ipsum dolor sit amet.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](41, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "section", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](46, "Our trusted climate partners");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](49, "img", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](52, "Our trusted climate partners");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](54, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "section", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](59, "Why Choose Us?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](64, "img", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "div", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](67, "Free Website SSL");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](69, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](70, "span", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](71, "Click Here");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](74, "img", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](75, "div", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](76, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](77, "Free Website SSL");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](79, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](80, "span", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](81, "Click Here");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](82, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](83, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](84, "img", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "div", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](87, "Free Website SSL");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](88, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](89, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](90, "span", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](91, "Click Here");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](92, "section", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](93, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](94, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](95, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](96, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](97, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](98, "app-scroll");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](99, "app-footer");
    } }, directives: [_shared_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_1__["NavbarComponent"], _shared_scroll_scroll_component__WEBPACK_IMPORTED_MODULE_2__["ScrollComponent"], _shared_footer_footer_component__WEBPACK_IMPORTED_MODULE_3__["FooterComponent"]], styles: [".header[_ngcontent-%COMP%] {\n  background-image: linear-gradient(to right, #2c3e50, rgba(41, 128, 185, 0.5)), url(\"/assets/images/about-bg.jpg\");\n}\n\nsection.color--bg[_ngcontent-%COMP%] {\n  background-image: linear-gradient(to right, #2c3e50, rgba(41, 128, 185, 0.5)), url(\"/assets/images/about-bg.jpg\");\n}\n\nsection.color--wavy[_ngcontent-%COMP%] {\n  background-image: linear-gradient(to right, #2980b9, rgba(44, 62, 80, 0.5)), url(\"/assets/images/forest.jpg\");\n  background-position: 50% 50%;\n}\n\nsection#carbon-offset[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%] {\n  align-items: center;\n  text-align: center;\n}\n\nsection#carbon-offset[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  max-width: 250px;\n  margin: 3em;\n}\n\nform[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  text-align: center;\n}\n\nform[_ngcontent-%COMP%]   .form-container[_ngcontent-%COMP%], form[_ngcontent-%COMP%]   .form-wrapper[_ngcontent-%COMP%] {\n  width: 100%;\n}\n\nform[_ngcontent-%COMP%]   textarea[_ngcontent-%COMP%] {\n  min-height: 150px;\n}\n\nform[_ngcontent-%COMP%]   .btn-wrapper[_ngcontent-%COMP%] {\n  justify-content: center;\n}\n\n@media (min-width: 736px) {\n  .header__container__content[_ngcontent-%COMP%] {\n    width: 75%;\n    margin-top: 50px;\n  }\n\n  form[_ngcontent-%COMP%] {\n    width: 75%;\n  }\n}\n\n@media (min-width: 1024px) {\n  section.section.color--bg[_ngcontent-%COMP%]::before {\n    height: 12%;\n  }\n  section.section.color--bg[_ngcontent-%COMP%]::after {\n    height: 12%;\n  }\n  section.section#carbon-offset[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%]   div[_ngcontent-%COMP%] {\n    text-align: left;\n  }\n}\n\n@media (min-width: 1366px) {\n  section.section.color--bg[_ngcontent-%COMP%]::before {\n    height: 18%;\n  }\n  section.section.color--bg[_ngcontent-%COMP%]::after {\n    height: 18%;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2NoaWRvL3Byb2plY3RzL2hvc3Rmcm9udC1wcm9qZWN0L2hvc3Rmcm9udC1mcm9udGVuZC9zcmMvYXBwL2Fib3V0L2Fib3V0LmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9hYm91dC9hYm91dC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGlIQUFBO0FDQ0Y7O0FES0k7RUFDRSxpSEFBQTtBQ0ZOOztBREtJO0VBQ0UsNkdBQUE7RUFFQSw0QkFBQTtBQ0pOOztBRFFJO0VBQ0UsbUJBQUE7RUFDQSxrQkFBQTtBQ05OOztBRFFNO0VBQ0ksZ0JBQUE7RUFDQSxXQUFBO0FDTlY7O0FEYUU7RUFDRSxrQkFBQTtBQ1ZKOztBRFlFO0VBQ0UsV0FBQTtBQ1ZKOztBRFlFO0VBQ0UsaUJBQUE7QUNWSjs7QURZRTtFQUNFLHVCQUFBO0FDVko7O0FEZUE7RUFFSTtJQUNFLFVBQUE7SUFDQSxnQkFBQTtFQ2JKOztFRGdCQTtJQUNFLFVBQUE7RUNiRjtBQUNGOztBRGtCQTtFQUlRO0lBQ0UsV0FBQTtFQ25CUjtFRHFCTTtJQUNFLFdBQUE7RUNuQlI7RUR1QkU7SUFDRSxnQkFBQTtFQ3JCSjtBQUNGOztBRDBCQTtFQUlRO0lBQ0UsV0FBQTtFQzNCUjtFRDZCTTtJQUNFLFdBQUE7RUMzQlI7QUFDRiIsImZpbGUiOiJzcmMvYXBwL2Fib3V0L2Fib3V0LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmhlYWRlciB7XG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgcmdiYSg0NCwgNjIsIDgwLCAxKSwgcmdiYSg0MSwgMTI4LCAxODUsIDAuNSkpLFxuICAgIHVybChcIi9hc3NldHMvaW1hZ2VzL2Fib3V0LWJnLmpwZ1wiKTtcbn1cblxuc2VjdGlvbiB7XG4gICYuY29sb3Ige1xuICAgICYtLWJnIHtcbiAgICAgIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgcmdiYSg0NCwgNjIsIDgwLCAxKSwgcmdiYSg0MSwgMTI4LCAxODUsIDAuNSkpLFxuICAgIHVybChcIi9hc3NldHMvaW1hZ2VzL2Fib3V0LWJnLmpwZ1wiKTtcbiAgICB9XG4gICAgJi0td2F2eSB7XG4gICAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsIHJnYmEoNDEsIDEyOCwgMTg1LCAxKSwgcmdiYSg0NCwgNjIsIDgwLCAwLjUpKSxcbiAgICAgICAgdXJsKFwiL2Fzc2V0cy9pbWFnZXMvZm9yZXN0LmpwZ1wiKTtcbiAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IDUwJSA1MCU7XG4gICAgfVxuICB9XG4gICYjY2FyYm9uLW9mZnNldCB7XG4gICAgJiAuZmxleCB7XG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuXG4gICAgICAmIGltZyB7XG4gICAgICAgICAgbWF4LXdpZHRoOiAyNTBweDtcbiAgICAgICAgICBtYXJnaW46IDNlbTtcbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cblxuZm9ybSB7XG4gICYgaDIge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgfVxuICAmIC5mb3JtLWNvbnRhaW5lciwgLmZvcm0td3JhcHBlciB7XG4gICAgd2lkdGg6IDEwMCU7XG4gIH1cbiAgdGV4dGFyZWF7XG4gICAgbWluLWhlaWdodDogMTUwcHg7XG4gIH1cbiAgJiAuYnRuLXdyYXBwZXIge1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICB9XG59XG5cbi8vIHRhYmxldCB2aWV3XG5AbWVkaWEgKG1pbi13aWR0aDogNzM2cHgpIHtcbiAgLmhlYWRlcl9fY29udGFpbmVyIHtcbiAgICAmX19jb250ZW50IHtcbiAgICAgIHdpZHRoOiA3NSU7XG4gICAgICBtYXJnaW4tdG9wOiA1MHB4O1xuICAgIH1cbiAgfVxuICBmb3JtIHtcbiAgICB3aWR0aDogNzUlO1xuICB9XG59XG5cblxuLy8gTGFyZ2UgdGFibGV0IHZpZXdcbkBtZWRpYSAobWluLXdpZHRoOiAxMDI0cHgpIHtcbiAgc2VjdGlvbi5zZWN0aW9uIHtcbiAgICAmLmNvbG9yIHtcbiAgICAgICYtLWJnIHtcbiAgICAgICAgJjo6YmVmb3Jle1xuICAgICAgICAgIGhlaWdodDogMTIlO1xuICAgICAgICB9XG4gICAgICAgICY6OmFmdGVyIHtcbiAgICAgICAgICBoZWlnaHQ6IDEyJTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgICAmI2NhcmJvbi1vZmZzZXQgLmNvbnRhaW5lciAuZmxleCBkaXYge1xuICAgICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICB9XG4gIH1cbn1cblxuLy8gRGVza3RvcCBWaWV3XG5AbWVkaWEgKG1pbi13aWR0aDogMTM2NnB4KSB7XG4gIHNlY3Rpb24uc2VjdGlvbiB7XG4gICAgJi5jb2xvciB7XG4gICAgICAmLS1iZyB7XG4gICAgICAgICY6OmJlZm9yZXtcbiAgICAgICAgICBoZWlnaHQ6IDE4JTtcbiAgICAgICAgfVxuICAgICAgICAmOjphZnRlciB7XG4gICAgICAgICAgaGVpZ2h0OiAxOCU7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cbiIsIi5oZWFkZXIge1xuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICMyYzNlNTAsIHJnYmEoNDEsIDEyOCwgMTg1LCAwLjUpKSwgdXJsKFwiL2Fzc2V0cy9pbWFnZXMvYWJvdXQtYmcuanBnXCIpO1xufVxuXG5zZWN0aW9uLmNvbG9yLS1iZyB7XG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgIzJjM2U1MCwgcmdiYSg0MSwgMTI4LCAxODUsIDAuNSkpLCB1cmwoXCIvYXNzZXRzL2ltYWdlcy9hYm91dC1iZy5qcGdcIik7XG59XG5zZWN0aW9uLmNvbG9yLS13YXZ5IHtcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjMjk4MGI5LCByZ2JhKDQ0LCA2MiwgODAsIDAuNSkpLCB1cmwoXCIvYXNzZXRzL2ltYWdlcy9mb3Jlc3QuanBnXCIpO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiA1MCUgNTAlO1xufVxuc2VjdGlvbiNjYXJib24tb2Zmc2V0IC5mbGV4IHtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuc2VjdGlvbiNjYXJib24tb2Zmc2V0IC5mbGV4IGltZyB7XG4gIG1heC13aWR0aDogMjUwcHg7XG4gIG1hcmdpbjogM2VtO1xufVxuXG5mb3JtIGgyIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuZm9ybSAuZm9ybS1jb250YWluZXIsIGZvcm0gLmZvcm0td3JhcHBlciB7XG4gIHdpZHRoOiAxMDAlO1xufVxuZm9ybSB0ZXh0YXJlYSB7XG4gIG1pbi1oZWlnaHQ6IDE1MHB4O1xufVxuZm9ybSAuYnRuLXdyYXBwZXIge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuQG1lZGlhIChtaW4td2lkdGg6IDczNnB4KSB7XG4gIC5oZWFkZXJfX2NvbnRhaW5lcl9fY29udGVudCB7XG4gICAgd2lkdGg6IDc1JTtcbiAgICBtYXJnaW4tdG9wOiA1MHB4O1xuICB9XG5cbiAgZm9ybSB7XG4gICAgd2lkdGg6IDc1JTtcbiAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6IDEwMjRweCkge1xuICBzZWN0aW9uLnNlY3Rpb24uY29sb3ItLWJnOjpiZWZvcmUge1xuICAgIGhlaWdodDogMTIlO1xuICB9XG4gIHNlY3Rpb24uc2VjdGlvbi5jb2xvci0tYmc6OmFmdGVyIHtcbiAgICBoZWlnaHQ6IDEyJTtcbiAgfVxuICBzZWN0aW9uLnNlY3Rpb24jY2FyYm9uLW9mZnNldCAuY29udGFpbmVyIC5mbGV4IGRpdiB7XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6IDEzNjZweCkge1xuICBzZWN0aW9uLnNlY3Rpb24uY29sb3ItLWJnOjpiZWZvcmUge1xuICAgIGhlaWdodDogMTglO1xuICB9XG4gIHNlY3Rpb24uc2VjdGlvbi5jb2xvci0tYmc6OmFmdGVyIHtcbiAgICBoZWlnaHQ6IDE4JTtcbiAgfVxufSJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AboutComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-about',
                templateUrl: './about.component.html',
                styleUrls: ['./about.component.scss']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule, RoutingComponents */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoutingComponents", function() { return RoutingComponents; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _shared_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./shared/navbar/navbar.component */ "./src/app/shared/navbar/navbar.component.ts");
/* harmony import */ var _shared_footer_footer_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./shared/footer/footer.component */ "./src/app/shared/footer/footer.component.ts");
/* harmony import */ var _about_about_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./about/about.component */ "./src/app/about/about.component.ts");
/* harmony import */ var _products_products_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./products/products.component */ "./src/app/products/products.component.ts");
/* harmony import */ var _wordpress_maintenance_wordpress_maintenance_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./wordpress-maintenance/wordpress-maintenance.component */ "./src/app/wordpress-maintenance/wordpress-maintenance.component.ts");
/* harmony import */ var _contact_contact_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./contact/contact.component */ "./src/app/contact/contact.component.ts");











const routes = [
    { path: '', component: _home_home_component__WEBPACK_IMPORTED_MODULE_2__["HomeComponent"] },
    { path: 'about', component: _about_about_component__WEBPACK_IMPORTED_MODULE_5__["AboutComponent"] },
    { path: 'wordpress-hosting', component: _products_products_component__WEBPACK_IMPORTED_MODULE_6__["ProductsComponent"] },
    { path: 'wordpress-maintenance', component: _wordpress_maintenance_wordpress_maintenance_component__WEBPACK_IMPORTED_MODULE_7__["WordpressMaintenanceComponent"] },
    { path: 'contact', component: _contact_contact_component__WEBPACK_IMPORTED_MODULE_8__["ContactComponent"] }
];
class AppRoutingModule {
}
AppRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: AppRoutingModule });
AppRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function AppRoutingModule_Factory(t) { return new (t || AppRoutingModule)(); }, imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
        _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AppRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppRoutingModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
                exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
            }]
    }], null, null); })();
const RoutingComponents = [_home_home_component__WEBPACK_IMPORTED_MODULE_2__["HomeComponent"], _shared_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_3__["NavbarComponent"], _about_about_component__WEBPACK_IMPORTED_MODULE_5__["AboutComponent"], _wordpress_maintenance_wordpress_maintenance_component__WEBPACK_IMPORTED_MODULE_7__["WordpressMaintenanceComponent"], _shared_footer_footer_component__WEBPACK_IMPORTED_MODULE_4__["FooterComponent"], _contact_contact_component__WEBPACK_IMPORTED_MODULE_8__["ContactComponent"]];


/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");



class AppComponent {
    constructor() {
        this.title = 'project';
    }
}
AppComponent.ɵfac = function AppComponent_Factory(t) { return new (t || AppComponent)(); };
AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AppComponent, selectors: [["app-root"]], decls: 1, vars: 0, template: function AppComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "router-outlet");
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterOutlet"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-root',
                templateUrl: './app.component.html',
                styleUrls: ['./app.component.scss']
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _products_products_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./products/products.component */ "./src/app/products/products.component.ts");
/* harmony import */ var _wordpress_maintenance_wordpress_maintenance_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./wordpress-maintenance/wordpress-maintenance.component */ "./src/app/wordpress-maintenance/wordpress-maintenance.component.ts");
/* harmony import */ var _pricing_tab_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./pricing-tab.service */ "./src/app/pricing-tab.service.ts");
/* harmony import */ var _contact_contact_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./contact/contact.component */ "./src/app/contact/contact.component.ts");
/* harmony import */ var _shared_scroll_scroll_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./shared/scroll/scroll.component */ "./src/app/shared/scroll/scroll.component.ts");
/* harmony import */ var _shared_button_button_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./shared/button/button.component */ "./src/app/shared/button/button.component.ts");
/* harmony import */ var _shared_header_header_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./shared/header/header.component */ "./src/app/shared/header/header.component.ts");
/* harmony import */ var _shared_price_card_price_card_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./shared/price-card/price-card.component */ "./src/app/shared/price-card/price-card.component.ts");
/* harmony import */ var _shared_cta_cta_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./shared/cta/cta.component */ "./src/app/shared/cta/cta.component.ts");
/* harmony import */ var _shared_hero_text_hero_text_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./shared/hero-text/hero-text.component */ "./src/app/shared/hero-text/hero-text.component.ts");
/* harmony import */ var _shared_form_form_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./shared/form/form.component */ "./src/app/shared/form/form.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _shared_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./shared/navbar/navbar.component */ "./src/app/shared/navbar/navbar.component.ts");
/* harmony import */ var _about_about_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./about/about.component */ "./src/app/about/about.component.ts");
/* harmony import */ var _shared_footer_footer_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./shared/footer/footer.component */ "./src/app/shared/footer/footer.component.ts");
























class AppModule {
}
AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineNgModule"]({ type: AppModule, bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]] });
AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjector"]({ factory: function AppModule_Factory(t) { return new (t || AppModule)(); }, providers: [_pricing_tab_service__WEBPACK_IMPORTED_MODULE_8__["PricingTabService"]], imports: [[
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsetNgModuleScope"](AppModule, { declarations: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"], _home_home_component__WEBPACK_IMPORTED_MODULE_17__["HomeComponent"], _shared_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_18__["NavbarComponent"], _about_about_component__WEBPACK_IMPORTED_MODULE_19__["AboutComponent"], _wordpress_maintenance_wordpress_maintenance_component__WEBPACK_IMPORTED_MODULE_7__["WordpressMaintenanceComponent"], _shared_footer_footer_component__WEBPACK_IMPORTED_MODULE_20__["FooterComponent"], _contact_contact_component__WEBPACK_IMPORTED_MODULE_9__["ContactComponent"], _products_products_component__WEBPACK_IMPORTED_MODULE_6__["ProductsComponent"],
        _wordpress_maintenance_wordpress_maintenance_component__WEBPACK_IMPORTED_MODULE_7__["WordpressMaintenanceComponent"],
        _contact_contact_component__WEBPACK_IMPORTED_MODULE_9__["ContactComponent"],
        _shared_scroll_scroll_component__WEBPACK_IMPORTED_MODULE_10__["ScrollComponent"],
        _shared_button_button_component__WEBPACK_IMPORTED_MODULE_11__["ButtonComponent"],
        _shared_header_header_component__WEBPACK_IMPORTED_MODULE_12__["HeaderComponent"],
        _shared_price_card_price_card_component__WEBPACK_IMPORTED_MODULE_13__["PriceCardComponent"],
        _shared_cta_cta_component__WEBPACK_IMPORTED_MODULE_14__["CtaComponent"],
        _shared_hero_text_hero_text_component__WEBPACK_IMPORTED_MODULE_15__["HeroTextComponent"],
        _shared_form_form_component__WEBPACK_IMPORTED_MODULE_16__["FormComponent"]], imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
        _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](AppModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"],
        args: [{
                declarations: [
                    _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
                    _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["RoutingComponents"],
                    _products_products_component__WEBPACK_IMPORTED_MODULE_6__["ProductsComponent"],
                    _wordpress_maintenance_wordpress_maintenance_component__WEBPACK_IMPORTED_MODULE_7__["WordpressMaintenanceComponent"],
                    _contact_contact_component__WEBPACK_IMPORTED_MODULE_9__["ContactComponent"],
                    _shared_scroll_scroll_component__WEBPACK_IMPORTED_MODULE_10__["ScrollComponent"],
                    _shared_button_button_component__WEBPACK_IMPORTED_MODULE_11__["ButtonComponent"],
                    _shared_header_header_component__WEBPACK_IMPORTED_MODULE_12__["HeaderComponent"],
                    _shared_price_card_price_card_component__WEBPACK_IMPORTED_MODULE_13__["PriceCardComponent"],
                    _shared_cta_cta_component__WEBPACK_IMPORTED_MODULE_14__["CtaComponent"],
                    _shared_hero_text_hero_text_component__WEBPACK_IMPORTED_MODULE_15__["HeroTextComponent"],
                    _shared_form_form_component__WEBPACK_IMPORTED_MODULE_16__["FormComponent"],
                ],
                imports: [
                    _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                    _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
                    _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"]
                ],
                providers: [_pricing_tab_service__WEBPACK_IMPORTED_MODULE_8__["PricingTabService"]],
                bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/contact/contact.component.ts":
/*!**********************************************!*\
  !*** ./src/app/contact/contact.component.ts ***!
  \**********************************************/
/*! exports provided: ContactComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactComponent", function() { return ContactComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _models_User__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../models/User */ "./src/app/models/User.ts");
/* harmony import */ var _models_Page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../models/Page */ "./src/app/models/Page.ts");
/* harmony import */ var _services_contact_form_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/contact-form.service */ "./src/app/services/contact-form.service.ts");
/* harmony import */ var _services_hostfront_api_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/hostfront-api.service */ "./src/app/services/hostfront-api.service.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var _shared_header_header_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../shared/header/header.component */ "./src/app/shared/header/header.component.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _shared_scroll_scroll_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../shared/scroll/scroll.component */ "./src/app/shared/scroll/scroll.component.ts");
/* harmony import */ var _shared_footer_footer_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../shared/footer/footer.component */ "./src/app/shared/footer/footer.component.ts");













function ContactComponent_div_2_h2_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "h2");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Thank You Your Message Has Been Recieved");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function ContactComponent_div_2_h2_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "h2");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Complete the form below and we will respond on the next business day. ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, " or call ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "a", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, " 075 3442 0485 ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, " for Hostfront support.");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function ContactComponent_div_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, ContactComponent_div_2_h2_2_Template, 2, 0, "h2", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, ContactComponent_div_2_h2_3_Template, 7, 0, "h2", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r8.submitted);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx_r8.submitted);
} }
function ContactComponent_div_6_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "May show blog posts in this section");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function ContactComponent_form_8_div_7_small_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "small", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Your name is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function ContactComponent_form_8_div_7_small_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "small", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Your name must be at least 3 characters");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function ContactComponent_form_8_div_7_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ContactComponent_form_8_div_7_small_1_Template, 2, 0, "small", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, ContactComponent_form_8_div_7_small_2_Template, 2, 0, "small", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r13 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r13.getInputField("name").errors == null ? null : ctx_r13.getInputField("name").errors.required);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r13.getInputField("name").errors == null ? null : ctx_r13.getInputField("name").errors.minlength);
} }
function ContactComponent_form_8_div_12_small_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "small", 32);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Your email is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function ContactComponent_form_8_div_12_small_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "small", 32);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Please provide a valid email address");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function ContactComponent_form_8_div_12_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ContactComponent_form_8_div_12_small_1_Template, 2, 0, "small", 31);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, ContactComponent_form_8_div_12_small_2_Template, 2, 0, "small", 31);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r14.getInputField("email").errors == null ? null : ctx_r14.getInputField("email").errors.required);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r14.getInputField("email").errors == null ? null : ctx_r14.getInputField("email").errors.pattern);
} }
function ContactComponent_form_8_div_17_small_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "small");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Your phone number is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function ContactComponent_form_8_div_17_small_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "small");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Your phone number must be at least 10 digits");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function ContactComponent_form_8_div_17_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ContactComponent_form_8_div_17_small_1_Template, 2, 0, "small", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, ContactComponent_form_8_div_17_small_2_Template, 2, 0, "small", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r15.getInputField("phone").errors == null ? null : ctx_r15.getInputField("phone").errors.required);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r15.getInputField("phone").errors == null ? null : ctx_r15.getInputField("phone").errors.pattern);
} }
function ContactComponent_form_8_div_22_small_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "small", 32);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Please provide a valid website [http://example.com]");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function ContactComponent_form_8_div_22_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ContactComponent_form_8_div_22_small_1_Template, 2, 0, "small", 31);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r16.getInputField("website").errors == null ? null : ctx_r16.getInputField("website").errors.pattern);
} }
function ContactComponent_form_8_option_33_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "option");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const topic_r26 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", topic_r26, "");
} }
function ContactComponent_form_8_div_40_small_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "small");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Your message is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function ContactComponent_form_8_div_40_small_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "small");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Your message must be at least 10 characters long");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function ContactComponent_form_8_div_40_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ContactComponent_form_8_div_40_small_1_Template, 2, 0, "small", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, ContactComponent_form_8_div_40_small_2_Template, 2, 0, "small", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r18 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r18.getInputField("message").errors == null ? null : ctx_r18.getInputField("message").errors.required);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r18.getInputField("message").errors == null ? null : ctx_r18.getInputField("message").errors.minlength);
} }
function ContactComponent_form_8_Template(rf, ctx) { if (rf & 1) {
    const _r30 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "form", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function ContactComponent_form_8_Template_form_ngSubmit_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r30); const ctx_r29 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r29.onSubmit(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "label");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Name *");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "input", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, ContactComponent_form_8_div_7_Template, 3, 2, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "label");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, " Email *");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "input", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](12, ContactComponent_form_8_div_12_Template, 3, 2, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "label");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, " Phone *");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "input", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](17, ContactComponent_form_8_div_17_Template, 3, 2, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "label");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, " Website");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "input", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](22, ContactComponent_form_8_div_22_Template, 2, 1, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "div", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "label");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, " Business Name");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "input", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "label", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "Topic *");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "select", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("blur", function ContactComponent_form_8_Template_select_blur_30_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r30); const ctx_r31 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r31.validateTopic(ctx_r31.getInputField("topic").value); })("change", function ContactComponent_form_8_Template_select_change_30_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r30); const ctx_r32 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r32.validateTopic(ctx_r32.getInputField("topic").value); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "option", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32, "I am interested in");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](33, ContactComponent_form_8_option_33_Template, 2, 1, "option", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "small");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35, "Please choose a topic");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "label", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](38, "Message *");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](39, "textarea", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](40, ContactComponent_form_8_div_40_Template, 3, 2, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](41, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "div", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](43, "input", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "label");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45, "I consent to the collection, storage and use of the information I have entered here. I understand that Hostfront may use this information to contact me, customize my site experience and optimize the Hostfront website in accordance with its Privacy Policy.");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "button", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](48, "Send");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx_r10.contactForm);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("invalid", ctx_r10.getInputField("name").invalid && ctx_r10.getInputField("name").touched);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r10.getInputField("name").invalid && ctx_r10.getInputField("name").touched);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("invalid", ctx_r10.getInputField("email").invalid && ctx_r10.getInputField("email").touched);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r10.getInputField("email").invalid && ctx_r10.getInputField("email").touched || ctx_r10.getInputField("email").dirty);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("invalid", ctx_r10.getInputField("phone").invalid && ctx_r10.getInputField("phone").touched);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r10.getInputField("phone").invalid && ctx_r10.getInputField("phone").touched || ctx_r10.getInputField("phone").dirty);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("invalid", ctx_r10.getInputField("website").invalid && ctx_r10.getInputField("website").touched);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r10.getInputField("website").invalid && ctx_r10.getInputField("website").touched || ctx_r10.getInputField("website").dirty);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("invalid", ctx_r10.getInputField("businessName").invalid && ctx_r10.getInputField("businessName").touched);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("invalid", ctx_r10.getInputField("topic").invalid && ctx_r10.getInputField("topic").touched);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r10.topics);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("d-none", !ctx_r10.getInputField("topic").invalid && ctx_r10.getInputField("topic").touched);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("invalid", ctx_r10.getInputField("message").invalid && ctx_r10.getInputField("message").touched);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r10.getInputField("message").invalid && ctx_r10.getInputField("message").touched || ctx_r10.getInputField("message").dirty);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", !ctx_r10.contactForm.valid);
} }
class ContactComponent {
    constructor(fb, _contactFormService, _http, _titleService, _metaService) {
        this.fb = fb;
        this._contactFormService = _contactFormService;
        this._http = _http;
        this._titleService = _titleService;
        this._metaService = _metaService;
        this.urlRegex = '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?';
        this.errorMsg = '';
        this.submitted = false;
        this.primaryBtn = {
            primary: true,
        };
        this.secondaryBtn = {
            secondary: true
        };
        this.contact = "contact";
        //We want to bind these to a select option in the HTML
        this.topics = [
            'Wordpress Hosting',
            'Wordpress Maintenance',
            'Wordpress Website Audit',
            'Wordpress Website Migration',
            'Wordpress Website Optimization',
            'Other'
        ];
    }
    getInputField(input) {
        return this.contactForm.get(input);
    }
    ngOnInit() {
        //  Output Contact Form On Contact Page
        this.contactForm = this.fb.group({
            name: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3)]],
            email: ['', [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")
                ]],
            website: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern(this.urlRegex)]],
            phone: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(10), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern(new RegExp("[0-9 ]{10}"))]],
            businessName: ['',],
            topic: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]],
            message: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(10)]],
            checked: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]],
        });
        //  Call Contact Page From API
        this._http.getContactPage().subscribe(data => {
            this.pageModel = new _models_Page__WEBPACK_IMPORTED_MODULE_3__["wordpressCustomPagePostType"](data);
            this.pageModelEssentialData = this.pageModel.pageEssentials;
            this.pageModelDynamicGlobalModules = this.pageModel.pageModules;
            console.log(this.pageModelDynamicGlobalModules.api_res_page_content[0].page_text);
            console.log(this.pageModelDynamicGlobalModules);
            this._titleService.setTitle(this.pageModel.pageEssentials['post_title']);
            this._metaService.addTags([
                //  Static Meta Tags
                { name: 'robots', content: 'index, follow' },
                { name: 'og:logcale', content: 'en_GB' },
                { name: 'og:type', content: 'article' },
                { name: 'og:site_name', content: 'Hostfront' },
                // //  Dynamic Meta Tags
                { name: 'description', content: this.pageModel.pageEssentials['post_seo'].meta_description },
                { name: 'og:title', content: this.pageModel.pageEssentials['post_seo'].meta_title },
                { name: 'og:description', content: this.pageModel.pageEssentials['post_seo'].meta_facebook_desc },
                { name: 'twitter:card', content: this.pageModel.pageEssentials['post_seo'].meta_twitter_image },
                { name: 'twitter:description', content: this.pageModel.pageEssentials['post_seo'].meta_twitter_desc },
                { name: 'twitter:title', content: this.pageModel.pageEssentials['post_seo'].meta_twitter_title }
            ]);
        });
    }
    //  Contact Form Component Functions
    validateTopic(value) {
        this._contactFormService.validateTopic(value);
    }
    //  When client clicks 'SUBMIT' on form
    onSubmit() {
        this.userModel = new _models_User__WEBPACK_IMPORTED_MODULE_2__["User"](this.contactForm.value.name, this.contactForm.value.email, this.contactForm.value.phone, this.contactForm.value.website, this.contactForm.value.businessName, this.contactForm.value.topic, this.contactForm.value.message, this.contactForm.value.checked);
        this._contactFormService.postContactForm(this.userModel)
            .subscribe(data => console.log('Success', data), error => this.errorMsg = error.statusText);
        this.submitted = true;
    }
}
ContactComponent.ɵfac = function ContactComponent_Factory(t) { return new (t || ContactComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_contact_form_service__WEBPACK_IMPORTED_MODULE_4__["ContactFormService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_hostfront_api_service__WEBPACK_IMPORTED_MODULE_5__["HostfrontApiService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_6__["Title"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_6__["Meta"])); };
ContactComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ContactComponent, selectors: [["app-contact"]], decls: 11, vars: 5, consts: [[3, "data", "headerClass"], [1, "section", "wordpress-hosting"], ["class", "container", 4, "ngIf"], [1, "container"], [1, "flex"], [1, "form-container"], [4, "ngIf"], ["data-aos", "fade-up"], [3, "formGroup", "ngSubmit", 4, "ngIf"], [1, "flex", "heading"], ["href", "tel:+7534420485"], [3, "formGroup", "ngSubmit"], [1, "form-wrapper"], [1, "input-wrapper"], [1, "form-group"], ["type", "text", "formControlName", "name"], ["type", "email", "formControlName", "email"], ["type", "text", "formControlName", "phone"], ["type", "text", "formControlName", "website"], ["type", "text", "formControlName", "businessName"], ["for", "input-topic"], ["formControlName", "topic", 3, "blur", "change"], ["value", "", "disabled", ""], [4, "ngFor", "ngForOf"], ["for", "input-message"], ["formControlName", "message"], ["type", "checkbox", "formControlName", "checked"], [1, "btn-wrapper"], [1, "btn", "btn--std", "btn--std01", 3, "disabled"], ["class", "", 4, "ngIf"], [1, ""], ["class", "text-danger", 4, "ngIf"], [1, "text-danger"]], template: function ContactComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "app-header", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "section", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, ContactComponent_div_2_Template, 4, 2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](6, ContactComponent_div_6_Template, 2, 0, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](8, ContactComponent_form_8_Template, 49, 24, "form", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "app-scroll");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "app-footer");
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", ctx.pageModelDynamicGlobalModules)("headerClass", ctx.contact);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.pageModelEssentialData);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.submitted);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.submitted);
    } }, directives: [_shared_header_header_component__WEBPACK_IMPORTED_MODULE_7__["HeaderComponent"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["NgIf"], _shared_scroll_scroll_component__WEBPACK_IMPORTED_MODULE_9__["ScrollComponent"], _shared_footer_footer_component__WEBPACK_IMPORTED_MODULE_10__["FooterComponent"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["SelectControlValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgSelectOption"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_x"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["NgForOf"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["CheckboxControlValueAccessor"]], styles: [".header[_ngcontent-%COMP%] {\n  background-image: linear-gradient(to right, #2c3e50, rgba(41, 128, 185, 0.5)), url(\"https://images.pexels.com/photos/3183196/pexels-photo-3183196.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260\");\n  background-position: 50% 50%;\n}\n\nh2[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  text-decoration: none;\n  color: #4b79a1;\n}\n\nform[_ngcontent-%COMP%] {\n  width: 80%;\n  margin: auto;\n  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);\n  border-radius: 10px;\n  padding: 16px;\n}\n\n.form-wrapper[_ngcontent-%COMP%] {\n  width: 100%;\n}\n\n@media (min-width: 1024px) {\n  .card[_ngcontent-%COMP%] {\n    display: flex;\n  }\n\n  .form-container[_ngcontent-%COMP%] {\n    width: 100%;\n  }\n}\n\n@media (min-width: 1300px) {\n  .header__container__content[_ngcontent-%COMP%] {\n    width: 80%;\n    margin-top: 50px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2NoaWRvL3Byb2plY3RzL2hvc3Rmcm9udC1wcm9qZWN0L2hvc3Rmcm9udC1mcm9udGVuZC9zcmMvYXBwL2NvbnRhY3QvY29udGFjdC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvY29udGFjdC9jb250YWN0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0cscU1BQUE7RUFFRCw0QkFBQTtBQ0FGOztBRElFO0VBQ0UscUJBQUE7RUFDQSxjQUFBO0FDREo7O0FESUE7RUFDRSxVQUFBO0VBQ0EsWUFBQTtFQUNBLDBDQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0FDREY7O0FESUE7RUFDRSxXQUFBO0FDREY7O0FESUE7RUFDRTtJQUNFLGFBQUE7RUNERjs7RURJQTtJQUNFLFdBQUE7RUNERjtBQUNGOztBRElBO0VBRUk7SUFDRSxVQUFBO0lBQ0EsZ0JBQUE7RUNISjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvY29udGFjdC9jb250YWN0LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmhlYWRlciB7XG4gICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsIHJnYmEoNDQsIDYyLCA4MCwgMSksIHJnYmEoNDEsIDEyOCwgMTg1LCAwLjUpKSxcbiAgICB1cmwoXCJodHRwczovL2ltYWdlcy5wZXhlbHMuY29tL3Bob3Rvcy8zMTgzMTk2L3BleGVscy1waG90by0zMTgzMTk2LmpwZWc/YXV0bz1jb21wcmVzcyZjcz10aW55c3JnYiZkcHI9MiZoPTc1MCZ3PTEyNjBcIik7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IDUwJSA1MCU7XG59XG5cbmgyIHtcbiAgYSB7XG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICAgIGNvbG9yOiAjNGI3OWExO1xuICB9XG59XG5mb3JtIHtcbiAgd2lkdGg6IDgwJTtcbiAgbWFyZ2luOiBhdXRvO1xuICBib3gtc2hhZG93OiAwIDRweCA4cHggMCByZ2JhKDAsIDAsIDAsIDAuMik7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIHBhZGRpbmc6IDE2cHg7XG59XG5cbi5mb3JtLXdyYXBwZXIge1xuICB3aWR0aDogMTAwJTtcbn1cblxuQG1lZGlhIChtaW4td2lkdGg6IDEwMjRweCkge1xuICAuY2FyZCB7XG4gICAgZGlzcGxheTogZmxleCB7XG4gICAgfVxuICB9XG4gIC5mb3JtLWNvbnRhaW5lciB7XG4gICAgd2lkdGg6IDEwMCVcbiAgfVxufVxuXG5AbWVkaWEgKG1pbi13aWR0aDogMTMwMHB4KSB7XG4gIC5oZWFkZXJfX2NvbnRhaW5lciB7XG4gICAgJl9fY29udGVudCB7XG4gICAgICB3aWR0aDogODAlO1xuICAgICAgbWFyZ2luLXRvcDogNTBweDtcbiAgICB9XG4gIH1cbn0iLCIuaGVhZGVyIHtcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjMmMzZTUwLCByZ2JhKDQxLCAxMjgsIDE4NSwgMC41KSksIHVybChcImh0dHBzOi8vaW1hZ2VzLnBleGVscy5jb20vcGhvdG9zLzMxODMxOTYvcGV4ZWxzLXBob3RvLTMxODMxOTYuanBlZz9hdXRvPWNvbXByZXNzJmNzPXRpbnlzcmdiJmRwcj0yJmg9NzUwJnc9MTI2MFwiKTtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogNTAlIDUwJTtcbn1cblxuaDIgYSB7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgY29sb3I6ICM0Yjc5YTE7XG59XG5cbmZvcm0ge1xuICB3aWR0aDogODAlO1xuICBtYXJnaW46IGF1dG87XG4gIGJveC1zaGFkb3c6IDAgNHB4IDhweCAwIHJnYmEoMCwgMCwgMCwgMC4yKTtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgcGFkZGluZzogMTZweDtcbn1cblxuLmZvcm0td3JhcHBlciB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG5AbWVkaWEgKG1pbi13aWR0aDogMTAyNHB4KSB7XG4gIC5jYXJkIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICB9XG5cbiAgLmZvcm0tY29udGFpbmVyIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6IDEzMDBweCkge1xuICAuaGVhZGVyX19jb250YWluZXJfX2NvbnRlbnQge1xuICAgIHdpZHRoOiA4MCU7XG4gICAgbWFyZ2luLXRvcDogNTBweDtcbiAgfVxufSJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ContactComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-contact',
                templateUrl: './contact.component.html',
                styleUrls: ['./contact.component.scss']
            }]
    }], function () { return [{ type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"] }, { type: _services_contact_form_service__WEBPACK_IMPORTED_MODULE_4__["ContactFormService"] }, { type: _services_hostfront_api_service__WEBPACK_IMPORTED_MODULE_5__["HostfrontApiService"] }, { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_6__["Title"] }, { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_6__["Meta"] }]; }, null); })();


/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _models_Page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../models/Page */ "./src/app/models/Page.ts");
/* harmony import */ var _services_hostfront_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/hostfront-api.service */ "./src/app/services/hostfront-api.service.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var _shared_header_header_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../shared/header/header.component */ "./src/app/shared/header/header.component.ts");
/* harmony import */ var _shared_button_button_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../shared/button/button.component */ "./src/app/shared/button/button.component.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _shared_price_card_price_card_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../shared/price-card/price-card.component */ "./src/app/shared/price-card/price-card.component.ts");
/* harmony import */ var _shared_hero_text_hero_text_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../shared/hero-text/hero-text.component */ "./src/app/shared/hero-text/hero-text.component.ts");
/* harmony import */ var _shared_scroll_scroll_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../shared/scroll/scroll.component */ "./src/app/shared/scroll/scroll.component.ts");
/* harmony import */ var _shared_footer_footer_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../shared/footer/footer.component */ "./src/app/shared/footer/footer.component.ts");
/* harmony import */ var _shared_cta_cta_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../shared/cta/cta.component */ "./src/app/shared/cta/cta.component.ts");













function HomeComponent_div_31_div_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h3");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const icon_card_r118 = ctx.$implicit;
    const ctx_r117 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate2"]("src", "", ctx_r117.pageModel.baseUrl, "", icon_card_r118.icon.url, "", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](icon_card_r118.card_header);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](icon_card_r118.card_text);
} }
function HomeComponent_div_31_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, HomeComponent_div_31_div_1_Template, 8, 4, "div", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r114 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r114.pageModelDynamicGlobalModules.api_res_icon_card);
} }
function HomeComponent_section_55_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "section", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "h2");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "Hostfront Wordpress Services");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "A hosting service that offers 'agency like' deeper wordpresss solutions, that create rich ecosystems for website success.");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "app-cta", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r115 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", ctx_r115.pageModelDynamicGlobalModules)("baseUrl", ctx_r115.baseUrl);
} }
function HomeComponent_div_58_div_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 31);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 32);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h3");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "app-button", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const card_r120 = ctx.$implicit;
    const first_r121 = ctx.first;
    const ctx_r119 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵattribute"]("data-aos", first_r121 ? "fade-right" : "fade-left");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate2"]("src", "", ctx_r119.baseUrl, "", card_r120.icon.url, "", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](card_r120.card_header);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](card_r120.card_text);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("buttonConfig", ctx_r119.primaryBtn)("buttonText", card_r120.cta);
} }
function HomeComponent_div_58_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, HomeComponent_div_58_div_2_Template, 9, 7, "div", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r116 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r116.pageModelDynamicGlobalModules.api_res_icon_card_with_cta);
} }
class HomeComponent {
    constructor(_http, _titleService, _metaService) {
        this._http = _http;
        this._titleService = _titleService;
        this._metaService = _metaService;
        this.home = "home";
        this.title = 'Angular 9 Universal Example';
        this.pagePostData = {
            meta_title: '',
            meta_description: '',
            meta_facebook_title: '',
            meta_facebook_desc: '',
            meta_facebook_image: '',
            meta_twitter_title: '',
            meta_twitter_desc: '',
            meta_twitter_image: '',
            meta_keywords: '',
            homeData: function (data) {
                //  Page SEO Data
                this.meta_title = data[0].post_seo.meta_seo_title;
                this.meta_description = data[0].post_seo.meta_seo_desc;
                this.meta_facebook_title = data[0].post_seo.meta_metaopengraph_title;
                this.meta_facebook_desc = data[0].post_seo.meta_metaopengraph_desc;
                this.meta_facebook_image = data[0].post_seo.meta_metaopengraph_image;
                this.meta_twitter_title = data[0].post_seo._metaseo_metatwitter_title;
                this.meta_twitter_desc = data[0].post_seo._metaseo_metatwitter_desc;
                this.meta_twitter_image = data[0].post_seo._metaseo_metatwitter_image;
            }
        };
        this.primaryBtn = {
            primary: true,
        };
        this.secondaryBtn = {
            secondary: true
        };
        this.displayName = false;
    }
    ngOnInit() {
        this._http.getHome().subscribe(data => {
            this.pageModel = new _models_Page__WEBPACK_IMPORTED_MODULE_1__["wordpressCustomPagePostType"](data);
            this.baseUrl = this.pageModel.baseUrl;
            this.pageModelEssentialData = this.pageModel.pageEssentials;
            this.pageModelDynamicGlobalModules = this.pageModel.pageModules;
            // Object Method & Store Data
            this.pagePostData.homeData(data);
            //  Set Page Title 
            //  @TODO Change this post title to the Yoast Post title
            this._titleService.setTitle(this.pageModelEssentialData.post_title);
            //  Set Page Meta Tags
            //  @TODO Change META tags to Yoast Values
            this._metaService.addTags([
                //  Static Meta Tags
                { name: 'robots', content: 'index, follow' },
                { name: 'og:logcale', content: 'en_GB' },
                { name: 'og:type', content: 'article' },
                { name: 'og:site_name', content: 'Hostfront' },
                //  Dynamic Meta Tags
                { name: 'description', content: this.pagePostData.meta_description },
                { name: 'og:title', content: this.pagePostData.meta_title },
                { name: 'og:description', content: this.pagePostData.meta_facebook_desc },
                { name: 'twitter:card', content: this.pagePostData.meta_twitter_image },
                { name: 'twitter:description', content: this.pagePostData.meta_twitter_desc },
                { name: 'twitter:title', content: this.pagePostData.meta_twitter_title }
            ]);
            console.log('Page Essential Data', this.pageModelEssentialData);
            console.log('Page Dynamic Global Modules', this.pageModelDynamicGlobalModules);
            console.log('Full Page Model', this.pageModel);
        });
    }
}
HomeComponent.ɵfac = function HomeComponent_Factory(t) { return new (t || HomeComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_hostfront_api_service__WEBPACK_IMPORTED_MODULE_2__["HostfrontApiService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["Title"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["Meta"])); };
HomeComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: HomeComponent, selectors: [["app-home"]], decls: 61, vars: 11, consts: [[3, "data", "headerClass"], ["id", "intro", 1, "section"], [1, "container"], [1, "flex", "heading"], [1, "flex"], [1, "btn-wrapper"], [1, "section", "color--bg", "color--wavy"], ["data-aos", "fade-left", 1, "cta", "cta--color", "cta--left"], [3, "buttonConfig", "buttonText"], [1, "section"], ["class", "flex", "data-aos", "fade-up", 4, "ngIf"], [1, "section", "color--light", "service-feature"], [1, "flex", "service-feature"], [1, "service-feature__img"], ["src", "../../assets/images/macbook.png", "alt", "website-example"], [1, "service-feature__text"], [1, "section", "color"], [3, "prices", "baseUrl"], ["class", "section", 4, "ngIf"], [3, "data", "heroClass"], [1, "section", "section--feature"], ["class", "container", 4, "ngIf"], ["data-aos", "fade-up", 1, "flex"], ["class", "column", 4, "ngFor", "ngForOf"], [1, "column"], [1, "column__icon"], [3, "src"], [1, "column__body"], [3, "data", "baseUrl"], ["class", "feature-group", 4, "ngFor", "ngForOf"], [1, "feature-group"], [1, "feature-group__img"], [1, "feature-group__body"]], template: function HomeComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "app-header", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "section", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Launch A Wordpress Website");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, " Hostfront launches fast, lightweight wordpress websites on blazing fast web servers, we say, `speed wins`.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "We deliver a fully optimized wordpress suite within 24hrs, with multiple website staging environments. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "section", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "Our Hosting Service");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "When joining our hosting service we perform a full website audit. We optimize your website based on our audit findings, and then we host your wordpress website on Hostfront. All within 24hrs!");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "app-button", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "section", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, " Hosting Service Features");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "Hosting your wordpress website with Hostfront creates a wide range of possibilities for your website speed, scalability and performmance. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](31, HomeComponent_div_31_Template, 2, 1, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "section", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](36, "img", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, "WORDPRESS IS POWERING 35% OF THE WEB.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](41, "More than 1.1 million new registered wordpress domains every 6 months.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](42, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "section", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](47, "Wordpress Hosting Packages");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](50, " We designed this service to provide 'peace of mind' web hosting solutions that add value to your business. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](51, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "strong");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](53, "No hidden fees, what you see is what you get.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](54, "app-price-card", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](55, HomeComponent_section_55_Template, 9, 2, "section", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](56, "app-hero-text", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "section", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](58, HomeComponent_div_58_Template, 3, 1, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](59, "app-scroll");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](60, "app-footer");
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", ctx.pageModelDynamicGlobalModules)("headerClass", ctx.home);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("buttonConfig", ctx.secondaryBtn)("buttonText", "Try Hostfront: 30 Day Free Trial");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.pageModelDynamicGlobalModules);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("prices", ctx.pageModelDynamicGlobalModules)("baseUrl", ctx.baseUrl);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.pageModelDynamicGlobalModules);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", ctx.pageModelDynamicGlobalModules)("heroClass", ctx.home);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.pageModelDynamicGlobalModules);
    } }, directives: [_shared_header_header_component__WEBPACK_IMPORTED_MODULE_4__["HeaderComponent"], _shared_button_button_component__WEBPACK_IMPORTED_MODULE_5__["ButtonComponent"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgIf"], _shared_price_card_price_card_component__WEBPACK_IMPORTED_MODULE_7__["PriceCardComponent"], _shared_hero_text_hero_text_component__WEBPACK_IMPORTED_MODULE_8__["HeroTextComponent"], _shared_scroll_scroll_component__WEBPACK_IMPORTED_MODULE_9__["ScrollComponent"], _shared_footer_footer_component__WEBPACK_IMPORTED_MODULE_10__["FooterComponent"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgForOf"], _shared_cta_cta_component__WEBPACK_IMPORTED_MODULE_11__["CtaComponent"]], styles: ["section.color--wavy[_ngcontent-%COMP%] {\n  min-height: 650px;\n  background-image: linear-gradient(to right, #2980b9, rgba(44, 62, 80, 0.5)), url(\"https://images.pexels.com/photos/935985/pexels-photo-935985.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260\");\n  background-position: left center;\n}\nsection.section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%] {\n  flex-wrap: wrap;\n}\n.service-feature__img[_ngcontent-%COMP%] {\n  width: 100%;\n}\n.service-feature__img[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  width: 100%;\n}\n.service-feature__text[_ngcontent-%COMP%] {\n  width: 100%;\n}\n.service-feature__text[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  font-size: 3rem;\n}\n@media (min-width: 768px) {\n  .service-feature__text[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n    font-size: 2.5rem;\n  }\n}\n@media (min-width: 1024px) {\n  .service-feature__text[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n    font-size: 3rem;\n  }\n}\n@media (min-width: 1366px) {\n  .service-feature__text[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n    font-size: 3.5rem;\n  }\n}\n@media (min-width: 1024px) {\n  section.section.color--wavy[_ngcontent-%COMP%] {\n    height: 800px;\n  }\n  section.section.color--bg[_ngcontent-%COMP%]::before {\n    height: 25%;\n  }\n  section.section.color--bg[_ngcontent-%COMP%]::after {\n    height: 25%;\n  }\n}\n@media (min-width: 1680px) {\n  section.section.color--bg[_ngcontent-%COMP%]::before {\n    height: 30%;\n  }\n  section.section.color--bg[_ngcontent-%COMP%]::after {\n    height: 30%;\n  }\n}\n@media (min-width: 1366px) {\n  #intro[_ngcontent-%COMP%]   .heading[_ngcontent-%COMP%] {\n    width: 70%;\n    margin: auto;\n  }\n  #intro[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n    line-height: 2.75;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2NoaWRvL3Byb2plY3RzL2hvc3Rmcm9udC1wcm9qZWN0L2hvc3Rmcm9udC1mcm9udGVuZC9zcmMvYXBwL2hvbWUvaG9tZS5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvaG9tZS9ob21lLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVJO0VBQ0UsaUJBQUE7RUFDQSxpTUFBQTtFQUVBLGdDQUFBO0FDRk47QURPTTtFQUNFLGVBQUE7QUNMUjtBRFlFO0VBQ0UsV0FBQTtBQ1RKO0FEVUk7RUFDRSxXQUFBO0FDUk47QURXRTtFQUNFLFdBQUE7QUNUSjtBRFVJO0VBQ0UsZUFBQTtBQ1JOO0FEYUE7RUFHTTtJQUNFLGlCQUFBO0VDWk47QUFDRjtBRGlCQTtFQUdNO0lBQ0UsZUFBQTtFQ2pCTjtBQUNGO0FEc0JBO0VBR007SUFDRSxpQkFBQTtFQ3RCTjtBQUNGO0FEMkJBO0VBR007SUFDRSxhQUFBO0VDM0JOO0VEOEJNO0lBQ0UsV0FBQTtFQzVCUjtFRDhCTTtJQUNFLFdBQUE7RUM1QlI7QUFDRjtBRGtDQTtFQUlRO0lBQ0UsV0FBQTtFQ25DUjtFRHFDTTtJQUNFLFdBQUE7RUNuQ1I7QUFDRjtBRDBDQTtFQUVJO0lBQ0UsVUFBQTtJQUNBLFlBQUE7RUN6Q0o7RUQyQ0U7SUFDRSxpQkFBQTtFQ3pDSjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvaG9tZS9ob21lLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsic2VjdGlvbiB7XG4gICYuY29sb3Ige1xuICAgICYtLXdhdnkge1xuICAgICAgbWluLWhlaWdodDogNjUwcHg7XG4gICAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsIHJnYmEoNDEsIDEyOCwgMTg1LCAxKSwgcmdiYSg0NCwgNjIsIDgwLCAwLjUpKSxcbiAgICAgICAgdXJsKFwiaHR0cHM6Ly9pbWFnZXMucGV4ZWxzLmNvbS9waG90b3MvOTM1OTg1L3BleGVscy1waG90by05MzU5ODUuanBlZz9hdXRvPWNvbXByZXNzJmNzPXRpbnlzcmdiJmRwcj0yJmg9NzUwJnc9MTI2MFwiKTtcbiAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IGxlZnQgY2VudGVyO1xuICAgIH1cbiAgfVxuICAmLnNlY3Rpb257XG4gICAgJiAuY29udGFpbmVyIHtcbiAgICAgICYgLmZsZXgge1xuICAgICAgICBmbGV4LXdyYXA6IHdyYXA7XG4gICAgICB9XG4gICAgfVxuICB9XG59XG5cbi5zZXJ2aWNlLWZlYXR1cmUge1xuICAmX19pbWcge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgICYgaW1nIHtcbiAgICAgIHdpZHRoOiAxMDAlO1xuICAgIH1cbiAgfVxuICAmX190ZXh0IHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICAmIHNwYW4ge1xuICAgICAgZm9udC1zaXplOiAzcmVtO1xuICAgIH1cbiAgfVxufVxuXG5AbWVkaWEgKG1pbi13aWR0aDogNzY4cHgpIHtcbiAgLnNlcnZpY2UtZmVhdHVyZSB7XG4gICAgJl9fdGV4dCB7XG4gICAgICAmIHNwYW4ge1xuICAgICAgICBmb250LXNpemU6IDIuNXJlbTtcbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cblxuQG1lZGlhIChtaW4td2lkdGg6IDEwMjRweCkge1xuICAuc2VydmljZS1mZWF0dXJlIHtcbiAgICAmX190ZXh0IHtcbiAgICAgICYgc3BhbiB7XG4gICAgICAgIGZvbnQtc2l6ZTogM3JlbTtcbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cblxuQG1lZGlhIChtaW4td2lkdGg6IDEzNjZweCkge1xuICAuc2VydmljZS1mZWF0dXJlIHtcbiAgICAmX190ZXh0IHtcbiAgICAgICYgc3BhbiB7XG4gICAgICAgIGZvbnQtc2l6ZTogMy41cmVtO1xuICAgICAgfVxuICAgIH1cbiAgfVxufVxuXG5AbWVkaWEgKG1pbi13aWR0aDogMTAyNHB4KSB7XG4gIHNlY3Rpb24uc2VjdGlvbiB7XG4gICAgJi5jb2xvciB7XG4gICAgICAmLS13YXZ5IHtcbiAgICAgICAgaGVpZ2h0OiA4MDBweDtcbiAgICAgIH1cbiAgICAgICYtLWJnIHtcbiAgICAgICAgJjo6YmVmb3Jle1xuICAgICAgICAgIGhlaWdodDogMjUlO1xuICAgICAgICB9XG4gICAgICAgICY6OmFmdGVyIHtcbiAgICAgICAgICBoZWlnaHQ6IDI1JTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxufVxuXG5AbWVkaWEgKG1pbi13aWR0aDogMTY4MHB4KSB7XG4gIHNlY3Rpb24uc2VjdGlvbiB7XG4gICAgJi5jb2xvciB7XG4gICAgICAmLS1iZyB7XG4gICAgICAgICY6OmJlZm9yZXtcbiAgICAgICAgICBoZWlnaHQ6IDMwJTtcbiAgICAgICAgfVxuICAgICAgICAmOjphZnRlciB7XG4gICAgICAgICAgaGVpZ2h0OiAzMCU7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cblxuXG5AbWVkaWEgKG1pbi13aWR0aDogMTM2NnB4KSB7XG4gICNpbnRybyB7XG4gICAgJiAuaGVhZGluZyB7XG4gICAgICB3aWR0aDogNzAlO1xuICAgICAgbWFyZ2luOiBhdXRvO1xuICAgIH1cbiAgICAmIHAge1xuICAgICAgbGluZS1oZWlnaHQ6IDIuNzU7XG4gICAgfVxuICB9XG59Iiwic2VjdGlvbi5jb2xvci0td2F2eSB7XG4gIG1pbi1oZWlnaHQ6IDY1MHB4O1xuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICMyOTgwYjksIHJnYmEoNDQsIDYyLCA4MCwgMC41KSksIHVybChcImh0dHBzOi8vaW1hZ2VzLnBleGVscy5jb20vcGhvdG9zLzkzNTk4NS9wZXhlbHMtcGhvdG8tOTM1OTg1LmpwZWc/YXV0bz1jb21wcmVzcyZjcz10aW55c3JnYiZkcHI9MiZoPTc1MCZ3PTEyNjBcIik7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGxlZnQgY2VudGVyO1xufVxuc2VjdGlvbi5zZWN0aW9uIC5jb250YWluZXIgLmZsZXgge1xuICBmbGV4LXdyYXA6IHdyYXA7XG59XG5cbi5zZXJ2aWNlLWZlYXR1cmVfX2ltZyB7XG4gIHdpZHRoOiAxMDAlO1xufVxuLnNlcnZpY2UtZmVhdHVyZV9faW1nIGltZyB7XG4gIHdpZHRoOiAxMDAlO1xufVxuLnNlcnZpY2UtZmVhdHVyZV9fdGV4dCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuLnNlcnZpY2UtZmVhdHVyZV9fdGV4dCBzcGFuIHtcbiAgZm9udC1zaXplOiAzcmVtO1xufVxuXG5AbWVkaWEgKG1pbi13aWR0aDogNzY4cHgpIHtcbiAgLnNlcnZpY2UtZmVhdHVyZV9fdGV4dCBzcGFuIHtcbiAgICBmb250LXNpemU6IDIuNXJlbTtcbiAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6IDEwMjRweCkge1xuICAuc2VydmljZS1mZWF0dXJlX190ZXh0IHNwYW4ge1xuICAgIGZvbnQtc2l6ZTogM3JlbTtcbiAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6IDEzNjZweCkge1xuICAuc2VydmljZS1mZWF0dXJlX190ZXh0IHNwYW4ge1xuICAgIGZvbnQtc2l6ZTogMy41cmVtO1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogMTAyNHB4KSB7XG4gIHNlY3Rpb24uc2VjdGlvbi5jb2xvci0td2F2eSB7XG4gICAgaGVpZ2h0OiA4MDBweDtcbiAgfVxuICBzZWN0aW9uLnNlY3Rpb24uY29sb3ItLWJnOjpiZWZvcmUge1xuICAgIGhlaWdodDogMjUlO1xuICB9XG4gIHNlY3Rpb24uc2VjdGlvbi5jb2xvci0tYmc6OmFmdGVyIHtcbiAgICBoZWlnaHQ6IDI1JTtcbiAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6IDE2ODBweCkge1xuICBzZWN0aW9uLnNlY3Rpb24uY29sb3ItLWJnOjpiZWZvcmUge1xuICAgIGhlaWdodDogMzAlO1xuICB9XG4gIHNlY3Rpb24uc2VjdGlvbi5jb2xvci0tYmc6OmFmdGVyIHtcbiAgICBoZWlnaHQ6IDMwJTtcbiAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6IDEzNjZweCkge1xuICAjaW50cm8gLmhlYWRpbmcge1xuICAgIHdpZHRoOiA3MCU7XG4gICAgbWFyZ2luOiBhdXRvO1xuICB9XG4gICNpbnRybyBwIHtcbiAgICBsaW5lLWhlaWdodDogMi43NTtcbiAgfVxufSJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](HomeComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-home',
                templateUrl: './home.component.html',
                styleUrls: ['./home.component.scss']
            }]
    }], function () { return [{ type: _services_hostfront_api_service__WEBPACK_IMPORTED_MODULE_2__["HostfrontApiService"] }, { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["Title"] }, { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["Meta"] }]; }, null); })();


/***/ }),

/***/ "./src/app/models/Page.ts":
/*!********************************!*\
  !*** ./src/app/models/Page.ts ***!
  \********************************/
/*! exports provided: wordpressCustomPagePostType */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "wordpressCustomPagePostType", function() { return wordpressCustomPagePostType; });
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");

class wordpressCustomPagePostType {
    //////  UNCOMMENT IF NEED TO USE CLASS PROP FOR REFERENCING //////
    // public  ID?: number;
    // public  post_title?: string;
    // public  slug?: string;
    // public  post_featured_image_url?: string;
    // public  post_featured_image_alt_tag?: string;
    // public  meta_title?: string;
    // public  meta_description?: string;
    // public  meta_facebook_title?: string;
    // public  meta_facebook_desc?: string;
    // public  meta_facebook_image?:string;
    // public  meta_twitter_title?: string;
    // public  meta_twitter_desc?:string;
    // public  meta_twitter_image?:string;
    // public  meta_keywords?:string;
    // public  hero_module_header?: string;
    // public  hero_module_excerpt?: string;
    // public  hero_module_left_cta?: string;
    // public  hero_module_right_cta?: string;
    // public  icon_card?: string;
    // public  price_card?: string;
    // public  full_width_card?: Array<object>;
    // public  footer_header_text?: string;
    // public  footer_image?: string;
    // public  icon_card_with_cta?: Array<object>;
    //  When class is instantiated - pass in the DATA from API into constructor and then the WorspressCustomPagePostType has been created
    constructor(data) {
        this.baseUrl = `${_environments_environment__WEBPACK_IMPORTED_MODULE_0__["environment"].baseurl}`;
        //  Assign Class Props To Incoming Data
        this.pageEssentials = data[0];
        this.pageModules = data[1];
    }
}


/***/ }),

/***/ "./src/app/models/User.ts":
/*!********************************!*\
  !*** ./src/app/models/User.ts ***!
  \********************************/
/*! exports provided: User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function() { return User; });
class User {
    constructor(name, email, phone, website, businessName, topic, message, checked) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.website = website;
        this.businessName = businessName;
        this.topic = topic;
        this.message = message;
        this.checked = checked;
    }
}


/***/ }),

/***/ "./src/app/pricing-tab.service.ts":
/*!****************************************!*\
  !*** ./src/app/pricing-tab.service.ts ***!
  \****************************************/
/*! exports provided: PricingTabService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PricingTabService", function() { return PricingTabService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


class PricingTabService {
    constructor() { }
    yearlyClick() {
        const indicator = document.getElementById('indicator');
        const secondTab = document.getElementById('second-tab');
        const firstTab = document.getElementById('first-tab');
        const yearlyTabColor = document.getElementById('yearly-tab');
        const monthlyTabColor = document.getElementById('monthly-tab');
        secondTab.style.display = 'block';
        secondTab.style.opacity = '1';
        secondTab.style.transition = 'opacity 1s ease-in';
        firstTab.style.display = 'none';
        firstTab.style.opacity = '0';
        yearlyTabColor.style.color = '#ffffff';
        monthlyTabColor.style.color = '#2c3e50';
        indicator.style.left = '120px';
        indicator.style.transition = 'all 0.35s ease-Out';
    }
    monthlyClick() {
        const indicator = document.getElementById('indicator');
        const secondTab = document.getElementById('second-tab');
        const firstTab = document.getElementById('first-tab');
        const yearlyTabColor = document.getElementById('yearly-tab');
        const monthlyTabColor = document.getElementById('monthly-tab');
        firstTab.style.display = 'block';
        firstTab.style.opacity = '1';
        firstTab.style.transition = 'opacity 1s ease-in';
        indicator.style.left = '5px';
        indicator.style.transition = 'all 0.35s ease-Out';
        secondTab.style.display = 'none';
        secondTab.style.opacity = '0';
        yearlyTabColor.style.color = '#2c3e50';
        monthlyTabColor.style.color = '#ffffff';
    }
}
PricingTabService.ɵfac = function PricingTabService_Factory(t) { return new (t || PricingTabService)(); };
PricingTabService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: PricingTabService, factory: PricingTabService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](PricingTabService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/products/products.component.ts":
/*!************************************************!*\
  !*** ./src/app/products/products.component.ts ***!
  \************************************************/
/*! exports provided: ProductsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductsComponent", function() { return ProductsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _pricing_tab_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../pricing-tab.service */ "./src/app/pricing-tab.service.ts");
/* harmony import */ var _shared_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../shared/navbar/navbar.component */ "./src/app/shared/navbar/navbar.component.ts");
/* harmony import */ var _shared_scroll_scroll_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../shared/scroll/scroll.component */ "./src/app/shared/scroll/scroll.component.ts");
/* harmony import */ var _shared_footer_footer_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../shared/footer/footer.component */ "./src/app/shared/footer/footer.component.ts");






class ProductsComponent {
    constructor(_pricingTabService) {
        this._pricingTabService = _pricingTabService;
    }
    yearlyTab() {
        this._pricingTabService.yearlyClick();
    }
    monthlyTab() {
        this._pricingTabService.monthlyClick();
    }
    ngOnInit() {
    }
}
ProductsComponent.ɵfac = function ProductsComponent_Factory(t) { return new (t || ProductsComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_pricing_tab_service__WEBPACK_IMPORTED_MODULE_1__["PricingTabService"])); };
ProductsComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ProductsComponent, selectors: [["app-products"]], decls: 147, vars: 0, consts: [[1, "header"], ["id", "curve", "data-name", "Layer 1", "xmlns", "http://www.w3.org/2000/svg", "viewBox", "0 0 1416.99 174.01"], ["d", "M0,280.8S283.66,59,608.94,163.56s437.93,150.57,808,10.34V309.54H0V280.8Z", "transform", "translate(0 -135.53)", 1, "cls-1"], [1, "header__container"], [1, "header__container__content"], ["id", "wordpress-hosting", 1, "section"], [1, "container"], [1, "flex", "heading"], [1, "flex"], [1, "section", "color"], ["id", "webhosting-help", 1, "flex"], [1, "column"], [1, "card"], [1, "card__body"], [1, "card__title"], [1, "card__text"], ["data-aos", "fade-left", 1, "card"], ["data-aos", "fade-right", 1, "card"], ["id", "first-tab", 1, "container"], [1, "card__icon"], ["src", "assets/images/cloud-computing.svg"], [1, "card__price"], [1, "btn", "btn--std", "btn--std01"], [1, "card", "active"], [1, "btn", "btn--std", "btn--std02"], [1, "section", "color--bg"], [1, "text-container"]], template: function ProductsComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "header", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "app-navbar");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceSVG"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "svg", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "path", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceHTML"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, " WORDPRESS WEBSITE HOSTING");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "section", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "What is wordpress hosting?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu..");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "section", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, "How can it help my business?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "h3", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, "Website Speed");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "p", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "h3", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37, "Managed WordPress Hosting");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "p", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "h3", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](44, "Hack Protection Guarantee");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "p", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](46, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "h3", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](51, "Eco-Friendly Website Hosting");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "p", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](53, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "h3", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](58, "Cloud Computing");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "ul", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](61, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](63, "Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](65, "In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a,");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](69, "h3", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](70, "OPTIMISED WEBSITE HOSTING FEATURES ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](71, "ul", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](73, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](75, "Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](76, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](77, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](79, "h3", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](80, "Advanced Site Speed");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "p", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](82, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](83, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](84, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "h3", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](87, "CDN \u2013 Content Delivery Network");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](88, "p", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](89, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](90, "section", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](91, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](92, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](93, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](94, "Our Plans");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](95, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](96, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](97, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](98, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](99, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](100, "img", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](101, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](102, "h3", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](103, "Cloud Computing");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](104, "p", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](105, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](106, "p", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](107, "\u00A31.99");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](108, "span", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](109, "Click Here");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](110, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](111, "div", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](112, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](113, "img", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](114, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](115, "h3", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](116, "Cloud Computing");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](117, "p", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](118, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](119, "p", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](120, "\u00A359.99");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](121, "span", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](122, "Click Here");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](123, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](124, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](125, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](126, "img", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](127, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](128, "h3", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](129, "Cloud Computing");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](130, "p", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](131, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](132, "p", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](133, "\u00A32.99");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](134, "span", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](135, "Click Here");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](136, "section", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](137, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](138, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](139, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](140, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](141, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](142, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](143, "span", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](144, "Click Here");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](145, "app-scroll");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](146, "app-footer");
    } }, directives: [_shared_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_2__["NavbarComponent"], _shared_scroll_scroll_component__WEBPACK_IMPORTED_MODULE_3__["ScrollComponent"], _shared_footer_footer_component__WEBPACK_IMPORTED_MODULE_4__["FooterComponent"]], styles: [".header[_ngcontent-%COMP%] {\n  background-image: linear-gradient(to right, #2c3e50, rgba(41, 128, 185, 0.5)), url(\"/assets/images/services-bg.jpg\");\n}\n\nsection.color--bg[_ngcontent-%COMP%] {\n  background-image: linear-gradient(to right, #2980b9, rgba(44, 62, 80, 0.5)), url(\"/assets/images/bg_02.jpg\");\n}\n\nsection.color--bg[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%] {\n  flex-direction: row;\n}\n\n@media (min-width: 736px) {\n  .header__container__content[_ngcontent-%COMP%] {\n    width: 100%;\n    margin-top: 50px;\n  }\n\n  #webhosting-help[_ngcontent-%COMP%] {\n    flex-wrap: wrap;\n  }\n  #webhosting-help[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%] {\n    flex-basis: 50%;\n    display: flex;\n  }\n}\n\n@media (min-width: 1024px) {\n  .header__container__content[_ngcontent-%COMP%] {\n    width: 75%;\n    margin-top: 50px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2NoaWRvL3Byb2plY3RzL2hvc3Rmcm9udC1wcm9qZWN0L2hvc3Rmcm9udC1mcm9udGVuZC9zcmMvYXBwL3Byb2R1Y3RzL3Byb2R1Y3RzLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9wcm9kdWN0cy9wcm9kdWN0cy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLG9IQUFBO0FDQ0Y7O0FES0k7RUFDRSw0R0FBQTtBQ0ZOOztBRElNO0VBQ0UsbUJBQUE7QUNGUjs7QURTQTtFQUVJO0lBQ0UsV0FBQTtJQUNBLGdCQUFBO0VDUEo7O0VEVUE7SUFDRSxlQUFBO0VDUEY7RURRRTtJQUNFLGVBQUE7SUFDQSxhQUFBO0VDTko7QUFDRjs7QURXQTtFQUVJO0lBQ0UsVUFBQTtJQUNBLGdCQUFBO0VDVko7QUFDRiIsImZpbGUiOiJzcmMvYXBwL3Byb2R1Y3RzL3Byb2R1Y3RzLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmhlYWRlciB7XG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgcmdiYSg0NCwgNjIsIDgwLCAxKSwgcmdiYSg0MSwgMTI4LCAxODUsIDAuNSkpLFxuICAgIHVybChcIi9hc3NldHMvaW1hZ2VzL3NlcnZpY2VzLWJnLmpwZ1wiKTtcbn1cblxuc2VjdGlvbiB7XG4gICYuY29sb3Ige1xuICAgICYtLWJnIHtcbiAgICAgIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgcmdiYSg0MSwgMTI4LCAxODUsIDEpLCByZ2JhKDQ0LCA2MiwgODAsIDAuNSkpLFxuICAgICAgICB1cmwoXCIvYXNzZXRzL2ltYWdlcy9iZ18wMi5qcGdcIik7XG4gICAgICAmIC5mbGV4IHtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cblxuLy8gVGFibGV0IHZpZXdcbkBtZWRpYSAobWluLXdpZHRoOiA3MzZweCkge1xuICAuaGVhZGVyX19jb250YWluZXIge1xuICAgICZfX2NvbnRlbnQge1xuICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICBtYXJnaW4tdG9wOiA1MHB4O1xuICAgIH1cbiAgfVxuICAjd2ViaG9zdGluZy1oZWxwIHtcbiAgICBmbGV4LXdyYXA6IHdyYXA7XG4gICAgJiAuY29sdW1uIHtcbiAgICAgIGZsZXgtYmFzaXM6IDUwJTsgXG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgIH1cbiAgfSAgXG59XG5cbi8vIExhcmdlIHRhYmxldCBhbmQgZGVza3RvcCB2aWV3IFxuQG1lZGlhIChtaW4td2lkdGg6IDEwMjRweCkge1xuICAuaGVhZGVyX19jb250YWluZXIge1xuICAgICZfX2NvbnRlbnQge1xuICAgICAgd2lkdGg6IDc1JTtcbiAgICAgIG1hcmdpbi10b3A6IDUwcHg7XG4gICAgfVxuICB9XG59IiwiLmhlYWRlciB7XG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgIzJjM2U1MCwgcmdiYSg0MSwgMTI4LCAxODUsIDAuNSkpLCB1cmwoXCIvYXNzZXRzL2ltYWdlcy9zZXJ2aWNlcy1iZy5qcGdcIik7XG59XG5cbnNlY3Rpb24uY29sb3ItLWJnIHtcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjMjk4MGI5LCByZ2JhKDQ0LCA2MiwgODAsIDAuNSkpLCB1cmwoXCIvYXNzZXRzL2ltYWdlcy9iZ18wMi5qcGdcIik7XG59XG5zZWN0aW9uLmNvbG9yLS1iZyAuZmxleCB7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG59XG5cbkBtZWRpYSAobWluLXdpZHRoOiA3MzZweCkge1xuICAuaGVhZGVyX19jb250YWluZXJfX2NvbnRlbnQge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIG1hcmdpbi10b3A6IDUwcHg7XG4gIH1cblxuICAjd2ViaG9zdGluZy1oZWxwIHtcbiAgICBmbGV4LXdyYXA6IHdyYXA7XG4gIH1cbiAgI3dlYmhvc3RpbmctaGVscCAuY29sdW1uIHtcbiAgICBmbGV4LWJhc2lzOiA1MCU7XG4gICAgZGlzcGxheTogZmxleDtcbiAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6IDEwMjRweCkge1xuICAuaGVhZGVyX19jb250YWluZXJfX2NvbnRlbnQge1xuICAgIHdpZHRoOiA3NSU7XG4gICAgbWFyZ2luLXRvcDogNTBweDtcbiAgfVxufSJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ProductsComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-products',
                templateUrl: './products.component.html',
                styleUrls: ['./products.component.scss']
            }]
    }], function () { return [{ type: _pricing_tab_service__WEBPACK_IMPORTED_MODULE_1__["PricingTabService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/services/contact-form.service.ts":
/*!**************************************************!*\
  !*** ./src/app/services/contact-form.service.ts ***!
  \**************************************************/
/*! exports provided: ContactFormService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactFormService", function() { return ContactFormService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");





class ContactFormService {
    constructor(_http) {
        this._http = _http;
        //  Contact Form Properties
        this.topicHasError = true;
        this._url = 'http://localhost:8080/contact';
    }
    postContactForm(userContactForm) {
        return this._http.post(this._url, userContactForm).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["catchError"])(this.errorHandler));
    }
    errorHandler(error) {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["throwError"])(error);
    }
    validateTopic(value) {
        if (value === 'default') {
            this.topicHasError = true;
        }
        else {
            this.topicHasError = false;
        }
        return this.topicHasError;
    }
}
ContactFormService.ɵfac = function ContactFormService_Factory(t) { return new (t || ContactFormService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"])); };
ContactFormService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: ContactFormService, factory: ContactFormService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ContactFormService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "./src/app/services/hostfront-api.service.ts":
/*!***************************************************!*\
  !*** ./src/app/services/hostfront-api.service.ts ***!
  \***************************************************/
/*! exports provided: HostfrontApiService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HostfrontApiService", function() { return HostfrontApiService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
//




class HostfrontApiService {
    constructor(http) {
        this.http = http;
    }
    getHome() {
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].baseurl}/wp-json/wp/v2/custom_page/183`);
    }
    getContactPage() {
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].baseurl}/wp-json/wp/v2/custom_page/190`);
    }
}
HostfrontApiService.ɵfac = function HostfrontApiService_Factory(t) { return new (t || HostfrontApiService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"])); };
HostfrontApiService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: HostfrontApiService, factory: HostfrontApiService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](HostfrontApiService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "./src/app/shared/button/button.component.ts":
/*!***************************************************!*\
  !*** ./src/app/shared/button/button.component.ts ***!
  \***************************************************/
/*! exports provided: ButtonComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ButtonComponent", function() { return ButtonComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");



function ButtonComponent_ng_container_0_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainer"](0);
} }
function ButtonComponent_ng_template_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](2, "uppercase");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r35 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](2, 1, ctx_r35.buttonText));
} }
function ButtonComponent_ng_template_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](2, "uppercase");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r37 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](2, 1, ctx_r37.buttonText));
} }
class ButtonComponent {
    constructor() { }
    ngOnInit() { }
}
ButtonComponent.ɵfac = function ButtonComponent_Factory(t) { return new (t || ButtonComponent)(); };
ButtonComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ButtonComponent, selectors: [["app-button"]], inputs: { buttonConfig: "buttonConfig", buttonText: "buttonText" }, decls: 5, vars: 1, consts: [[4, "ngTemplateOutletButtonText", "ngTemplateOutlet"], ["primary", ""], ["secondary", ""], [1, "btn", "btn--std", "btn--std01"], [1, "btn", "btn--std", "btn--std02"]], template: function ButtonComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, ButtonComponent_ng_container_0_Template, 1, 0, "ng-container", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ButtonComponent_ng_template_1_Template, 3, 3, "ng-template", null, 1, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplateRefExtractor"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, ButtonComponent_ng_template_3_Template, 3, 3, "ng-template", null, 2, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplateRefExtractor"]);
    } if (rf & 2) {
        const _r34 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](2);
        const _r36 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngTemplateOutlet", ctx.buttonConfig["primary"] ? _r34 : _r36);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["NgTemplateOutlet"]], pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["UpperCasePipe"]], styles: [".btn[_ngcontent-%COMP%] {\n  padding: 15px 35px;\n  cursor: pointer;\n  font-size: 0.8rem;\n  text-transform: uppercase;\n  font-weight: bold;\n  min-width: 200px;\n  text-align: center;\n  display: inline-block;\n  margin: 10px 0;\n}\n.btn--std[_ngcontent-%COMP%] {\n  border-radius: 30px;\n  border: none;\n  \n  transition-duration: 0.4s;\n}\n.btn--std01[_ngcontent-%COMP%] {\n  background: #2980b9;\n  color: #ffffff;\n}\n.btn--std01[_ngcontent-%COMP%]:hover {\n  background-color: #283e51;\n  box-shadow: 0 12px 16px 0 rgba(0, 0, 0, 0.24), 0 17px 50px 0 rgba(0, 0, 0, 0.19);\n}\n.btn--std02[_ngcontent-%COMP%] {\n  background-color: #ffffff;\n  color: #283e51;\n}\n.btn--std02[_ngcontent-%COMP%]:hover {\n  background-color: #283e51;\n  box-shadow: 0 12px 16px 0 rgba(0, 0, 0, 0.24), 0 17px 50px 0 rgba(0, 0, 0, 0.19);\n  color: #ffffff;\n}\n.btn__slide[_ngcontent-%COMP%] {\n  width: 100%;\n  height: 100%;\n  left: -200px;\n  position: absolute;\n  transition: all 0.35s ease-Out;\n  bottom: 0;\n}\n.btn[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  text-decoration: none;\n  letter-spacing: 1px;\n  transition: all 0.35s ease-Out;\n  position: relative;\n}\n.btn__slide--01[_ngcontent-%COMP%] {\n  background: #2980b9;\n}\n.btn__slide--02[_ngcontent-%COMP%] {\n  background: #ffffff;\n}\n.btn[_ngcontent-%COMP%]:hover   .btn__slide[_ngcontent-%COMP%] {\n  left: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2NoaWRvL3Byb2plY3RzL2hvc3Rmcm9udC1wcm9qZWN0L2hvc3Rmcm9udC1mcm9udGVuZC9zcmMvYXBwL3NoYXJlZC9idXR0b24vYnV0dG9uLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9zaGFyZWQvYnV0dG9uL2J1dHRvbi5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFNQTtFQUNFLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EseUJBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxxQkFBQTtFQUNBLGNBQUE7QUNMRjtBRE9FO0VBQ0UsbUJBQUE7RUFDQSxZQUFBO0VBQ21DLFdBQUE7RUFDbkMseUJBQUE7QUNKSjtBRE1JO0VBQ0UsbUJBckJXO0VBc0JYLGNBeEJFO0FDb0JSO0FETU07RUFDRSx5QkF4QmM7RUF5QmQsZ0ZBQUE7QUNKUjtBRFFJO0VBQ0UseUJBakNFO0VBa0NGLGNBL0JnQjtBQ3lCdEI7QURRTTtFQUNFLHlCQWxDYztFQW1DZCxnRkFBQTtFQUNBLGNBdkNBO0FDaUNSO0FEV0U7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLDhCQUFBO0VBQ0EsU0FBQTtBQ1RKO0FEWUU7RUFDRSxxQkFBQTtFQUNBLG1CQUFBO0VBQ0EsOEJBQUE7RUFDQSxrQkFBQTtBQ1ZKO0FEYUU7RUFDRSxtQkEzRGE7QUNnRGpCO0FEY0U7RUFDRSxtQkFqRUk7QUNxRFI7QURlRTtFQUNFLE9BQUE7QUNiSiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9idXR0b24vYnV0dG9uLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gQ29sb3IgVmFyaWFibGVzXG4kd2hpdGU6ICNmZmZmZmY7XG4kaG9zdGZyb250LWJsdWUtbGlnaHQ6ICNmOGZhZmU7XG4kaG9zdGZyb250LWJsdWU6ICMyOTgwYjk7IFxuJGhvc3Rmcm9udC1ibHVlLWRhcms6ICMyODNlNTE7XG5cbi5idG4ge1xuICBwYWRkaW5nOiAxNXB4IDM1cHg7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgZm9udC1zaXplOiAwLjhyZW07XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBtaW4td2lkdGg6IDIwMHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgbWFyZ2luOiAxMHB4IDA7XG5cbiAgJi0tc3RkIHtcbiAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xuICAgIGJvcmRlcjogbm9uZTtcbiAgICAtd2Via2l0LXRyYW5zaXRpb24tZHVyYXRpb246IDAuNHM7IC8qIFNhZmFyaSAqL1xuICAgIHRyYW5zaXRpb24tZHVyYXRpb246IDAuNHM7XG5cbiAgICAmMDEge1xuICAgICAgYmFja2dyb3VuZDogJGhvc3Rmcm9udC1ibHVlO1xuICAgICAgY29sb3I6ICR3aGl0ZTtcblxuICAgICAgJjpob3ZlciB7XG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICRob3N0ZnJvbnQtYmx1ZS1kYXJrO1xuICAgICAgICBib3gtc2hhZG93OiAwIDEycHggMTZweCAwIHJnYmEoMCwgMCwgMCwgMC4yNCksIDAgMTdweCA1MHB4IDAgcmdiYSgwLCAwLCAwLCAwLjE5KTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICAmMDIge1xuICAgICAgYmFja2dyb3VuZC1jb2xvcjogJHdoaXRlO1xuICAgICAgY29sb3I6ICRob3N0ZnJvbnQtYmx1ZS1kYXJrO1xuXG4gICAgICAmOmhvdmVyIHtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogJGhvc3Rmcm9udC1ibHVlLWRhcms7XG4gICAgICAgIGJveC1zaGFkb3c6IDAgMTJweCAxNnB4IDAgcmdiYSgwLCAwLCAwLCAwLjI0KSwgMCAxN3B4IDUwcHggMCByZ2JhKDAsIDAsIDAsIDAuMTkpO1xuICAgICAgICBjb2xvcjogJHdoaXRlO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gICZfX3NsaWRlIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgbGVmdDogLTIwMHB4O1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0cmFuc2l0aW9uOiBhbGwgMC4zNXMgZWFzZS1PdXQ7XG4gICAgYm90dG9tOiAwO1xuICB9XG5cbiAgJiBhIHtcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDFweDtcbiAgICB0cmFuc2l0aW9uOiBhbGwgMC4zNXMgZWFzZS1PdXQ7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB9XG5cbiAgJl9fc2xpZGUtLTAxIHtcbiAgICBiYWNrZ3JvdW5kOiAkaG9zdGZyb250LWJsdWU7XG4gIH1cblxuICAmX19zbGlkZS0tMDIge1xuICAgIGJhY2tncm91bmQ6ICR3aGl0ZTtcbiAgfVxuXG4gICY6aG92ZXIgJl9fc2xpZGUge1xuICAgIGxlZnQ6IDA7XG4gIH1cbn0iLCIuYnRuIHtcbiAgcGFkZGluZzogMTVweCAzNXB4O1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIGZvbnQtc2l6ZTogMC44cmVtO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgbWluLXdpZHRoOiAyMDBweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIG1hcmdpbjogMTBweCAwO1xufVxuLmJ0bi0tc3RkIHtcbiAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgYm9yZGVyOiBub25lO1xuICAtd2Via2l0LXRyYW5zaXRpb24tZHVyYXRpb246IDAuNHM7XG4gIC8qIFNhZmFyaSAqL1xuICB0cmFuc2l0aW9uLWR1cmF0aW9uOiAwLjRzO1xufVxuLmJ0bi0tc3RkMDEge1xuICBiYWNrZ3JvdW5kOiAjMjk4MGI5O1xuICBjb2xvcjogI2ZmZmZmZjtcbn1cbi5idG4tLXN0ZDAxOmhvdmVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzI4M2U1MTtcbiAgYm94LXNoYWRvdzogMCAxMnB4IDE2cHggMCByZ2JhKDAsIDAsIDAsIDAuMjQpLCAwIDE3cHggNTBweCAwIHJnYmEoMCwgMCwgMCwgMC4xOSk7XG59XG4uYnRuLS1zdGQwMiB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XG4gIGNvbG9yOiAjMjgzZTUxO1xufVxuLmJ0bi0tc3RkMDI6aG92ZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjgzZTUxO1xuICBib3gtc2hhZG93OiAwIDEycHggMTZweCAwIHJnYmEoMCwgMCwgMCwgMC4yNCksIDAgMTdweCA1MHB4IDAgcmdiYSgwLCAwLCAwLCAwLjE5KTtcbiAgY29sb3I6ICNmZmZmZmY7XG59XG4uYnRuX19zbGlkZSB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIGxlZnQ6IC0yMDBweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0cmFuc2l0aW9uOiBhbGwgMC4zNXMgZWFzZS1PdXQ7XG4gIGJvdHRvbTogMDtcbn1cbi5idG4gYSB7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgbGV0dGVyLXNwYWNpbmc6IDFweDtcbiAgdHJhbnNpdGlvbjogYWxsIDAuMzVzIGVhc2UtT3V0O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4uYnRuX19zbGlkZS0tMDEge1xuICBiYWNrZ3JvdW5kOiAjMjk4MGI5O1xufVxuLmJ0bl9fc2xpZGUtLTAyIHtcbiAgYmFja2dyb3VuZDogI2ZmZmZmZjtcbn1cbi5idG46aG92ZXIgLmJ0bl9fc2xpZGUge1xuICBsZWZ0OiAwO1xufSJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ButtonComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-button',
                templateUrl: './button.component.html',
                styleUrls: ['./button.component.scss']
            }]
    }], function () { return []; }, { buttonConfig: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }], buttonText: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }] }); })();


/***/ }),

/***/ "./src/app/shared/cta/cta.component.ts":
/*!*********************************************!*\
  !*** ./src/app/shared/cta/cta.component.ts ***!
  \*********************************************/
/*! exports provided: CtaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CtaComponent", function() { return CtaComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _button_button_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../button/button.component */ "./src/app/shared/button/button.component.ts");




function CtaComponent_div_0_div_1_div_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "h2");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "app-button", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "img", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r54 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    const first_r50 = ctx_r54.first;
    const card_r49 = ctx_r54.$implicit;
    const ctx_r51 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵattribute"]("data-aos", first_r50 ? "fade-left" : "fade-right");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", first_r50 ? "cta--left" : "cta--right");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](card_r49.card_header);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](card_r49.card_text);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("buttonConfig", ctx_r51.primaryBtn)("buttonText", card_r49.cta);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate2"]("src", "", ctx_r51.baseUrl, "", card_r49.icon.url, "", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
} }
function CtaComponent_div_0_div_1_ng_template_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h2");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "app-button", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r55 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    const first_r50 = ctx_r55.first;
    const card_r49 = ctx_r55.$implicit;
    const ctx_r53 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵattribute"]("data-aos", first_r50 ? "fade-left" : "fade-right");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate2"]("src", "", ctx_r53.baseUrl, "", card_r49.icon.url, "", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", first_r50 ? "cta--left" : "cta--right");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](card_r49.card_header);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](card_r49.card_text);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("buttonConfig", ctx_r53.primaryBtn)("buttonText", card_r49.cta);
} }
function CtaComponent_div_0_div_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, CtaComponent_div_0_div_1_div_1_Template, 10, 8, "div", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, CtaComponent_div_0_div_1_ng_template_2_Template, 10, 8, "ng-template", null, 4, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplateRefExtractor"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const first_r50 = ctx.first;
    const _r52 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", first_r50)("ngIfElse", _r52);
} }
function CtaComponent_div_0_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, CtaComponent_div_0_div_1_Template, 4, 2, "div", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r47 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r47.data.api_res_full_width_card);
} }
class CtaComponent {
    constructor() {
        this.primaryBtn = {
            primary: true,
        };
        this.secondaryBtn = {
            secondary: true
        };
    }
    ngOnInit() {
    }
}
CtaComponent.ɵfac = function CtaComponent_Factory(t) { return new (t || CtaComponent)(); };
CtaComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: CtaComponent, selectors: [["app-cta"]], inputs: { data: "data", baseUrl: "baseUrl", buttonConfig: "buttonConfig", buttonText: "buttonText" }, decls: 1, vars: 1, consts: [[4, "ngIf"], ["class", "container", 4, "ngFor", "ngForOf"], [1, "container"], [4, "ngIf", "ngIfElse"], ["elseBlock", ""], [1, "flex"], [1, "cta", 3, "ngClass"], [1, "btn-wrapper"], [3, "buttonConfig", "buttonText"], [1, "cta__img", 3, "src"]], template: function CtaComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, CtaComponent_div_0_Template, 2, 1, "div", 0);
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.data);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["NgIf"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["NgForOf"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["NgClass"], _button_button_component__WEBPACK_IMPORTED_MODULE_2__["ButtonComponent"]], styles: [".cta[_ngcontent-%COMP%] {\n  width: 100%;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  padding: 2em;\n}\n.cta[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  text-transform: uppercase;\n  font-size: 2em;\n  color: #283e51;\n}\n.cta[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n  line-height: 1.8;\n}\n.cta--right[_ngcontent-%COMP%], .cta--left[_ngcontent-%COMP%] {\n  text-align: center;\n  padding: 2em 0em;\n}\n.cta--right[_ngcontent-%COMP%]   .btn-wrapper[_ngcontent-%COMP%], .cta--left[_ngcontent-%COMP%]   .btn-wrapper[_ngcontent-%COMP%] {\n  margin: 2em 0;\n}\n.cta__img[_ngcontent-%COMP%] {\n  width: 50%;\n  margin: 0 auto;\n}\n.cta--color[_ngcontent-%COMP%] {\n  color: white;\n}\n.cta--color[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  color: white;\n}\n@media (min-width: 768px) {\n  .cta[_ngcontent-%COMP%] {\n    width: 50%;\n  }\n  .cta__img[_ngcontent-%COMP%] {\n    width: 40%;\n  }\n  .cta--right[_ngcontent-%COMP%] {\n    text-align: right;\n  }\n  .cta--left[_ngcontent-%COMP%] {\n    text-align: left;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2NoaWRvL3Byb2plY3RzL2hvc3Rmcm9udC1wcm9qZWN0L2hvc3Rmcm9udC1mcm9udGVuZC9zcmMvYXBwL3NoYXJlZC9jdGEvY3RhLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9zaGFyZWQvY3RhL2N0YS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFNQTtFQUNFLFdBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUNBLFlBQUE7QUNMRjtBRE9FO0VBQ0UseUJBQUE7RUFDQSxjQUFBO0VBQ0EsY0Faa0I7QUNPdEI7QURRRTtFQUNFLGdCQUFBO0FDTko7QURTRTtFQUNFLGtCQUFBO0VBQ0EsZ0JBQUE7QUNQSjtBRFFJO0VBQ0UsYUFBQTtBQ05OO0FEVUU7RUFDRSxVQUFBO0VBQ0EsY0FBQTtBQ1JKO0FEV0U7RUFDRSxZQUFBO0FDVEo7QURVSTtFQUNFLFlBQUE7QUNSTjtBRGNBO0VBQ0U7SUFDRSxVQUFBO0VDWEY7RURZRTtJQUNFLFVBQUE7RUNWSjtFRFlFO0lBQ0UsaUJBQUE7RUNWSjtFRFlFO0lBQ0UsZ0JBQUE7RUNWSjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvc2hhcmVkL2N0YS9jdGEuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBDb2xvciBWYXJpYWJsZXNcbiR3aGl0ZTogI2ZmZmZmZjtcbiRob3N0ZnJvbnQtYmx1ZS1saWdodDogI2Y4ZmFmZTtcbiRob3N0ZnJvbnQtYmx1ZTogIzI5ODBiOTsgXG4kaG9zdGZyb250LWJsdWUtZGFyazogIzI4M2U1MTtcblxuLmN0YSB7XG4gIHdpZHRoOiAxMDAlO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgcGFkZGluZzogMmVtO1xuXG4gICYgaDIge1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgZm9udC1zaXplOiAyZW07XG4gICAgY29sb3I6ICRob3N0ZnJvbnQtYmx1ZS1kYXJrO1xuICB9XG5cbiAgJiBwIHtcbiAgICBsaW5lLWhlaWdodDogMS44O1xuICB9XG5cbiAgJi0tcmlnaHQsJi0tbGVmdCB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIHBhZGRpbmc6IDJlbSAwZW07XG4gICAgJiAuYnRuLXdyYXBwZXIge1xuICAgICAgbWFyZ2luOiAyZW0gMDtcbiAgICB9XG4gIH1cblxuICAmX19pbWcge1xuICAgIHdpZHRoOiA1MCU7XG4gICAgbWFyZ2luOiAwIGF1dG87XG4gIH1cblxuICAmLS1jb2xvcntcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgJiBoMiB7XG4gICAgICBjb2xvcjogd2hpdGU7XG4gICAgfVxuICB9XG59XG5cbi8vIENUQSBjb21wb25lbnQgdGFibGV0IGFuZCBkZXNrdG9wIHZpZXdcbkBtZWRpYSAobWluLXdpZHRoOiA3NjhweCkge1xuICAuY3RhIHtcbiAgICB3aWR0aDogNTAlO1xuICAgICZfX2ltZyB7XG4gICAgICB3aWR0aDogNDAlO1xuICAgIH1cbiAgICAmLS1yaWdodCB7XG4gICAgICB0ZXh0LWFsaWduOiByaWdodDtcbiAgICB9XG4gICAgJi0tbGVmdCB7XG4gICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgIH1cbiAgfVxufSIsIi5jdGEge1xuICB3aWR0aDogMTAwJTtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIHBhZGRpbmc6IDJlbTtcbn1cbi5jdGEgaDIge1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBmb250LXNpemU6IDJlbTtcbiAgY29sb3I6ICMyODNlNTE7XG59XG4uY3RhIHAge1xuICBsaW5lLWhlaWdodDogMS44O1xufVxuLmN0YS0tcmlnaHQsIC5jdGEtLWxlZnQge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHBhZGRpbmc6IDJlbSAwZW07XG59XG4uY3RhLS1yaWdodCAuYnRuLXdyYXBwZXIsIC5jdGEtLWxlZnQgLmJ0bi13cmFwcGVyIHtcbiAgbWFyZ2luOiAyZW0gMDtcbn1cbi5jdGFfX2ltZyB7XG4gIHdpZHRoOiA1MCU7XG4gIG1hcmdpbjogMCBhdXRvO1xufVxuLmN0YS0tY29sb3Ige1xuICBjb2xvcjogd2hpdGU7XG59XG4uY3RhLS1jb2xvciBoMiB7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuQG1lZGlhIChtaW4td2lkdGg6IDc2OHB4KSB7XG4gIC5jdGEge1xuICAgIHdpZHRoOiA1MCU7XG4gIH1cbiAgLmN0YV9faW1nIHtcbiAgICB3aWR0aDogNDAlO1xuICB9XG4gIC5jdGEtLXJpZ2h0IHtcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcbiAgfVxuICAuY3RhLS1sZWZ0IHtcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICB9XG59Il19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](CtaComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-cta',
                templateUrl: './cta.component.html',
                styleUrls: ['./cta.component.scss']
            }]
    }], function () { return []; }, { data: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }], baseUrl: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }], buttonConfig: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }], buttonText: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }] }); })();


/***/ }),

/***/ "./src/app/shared/footer/footer.component.ts":
/*!***************************************************!*\
  !*** ./src/app/shared/footer/footer.component.ts ***!
  \***************************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");



class FooterComponent {
    constructor() { }
    ngOnInit() {
    }
}
FooterComponent.ɵfac = function FooterComponent_Factory(t) { return new (t || FooterComponent)(); };
FooterComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: FooterComponent, selectors: [["app-footer"]], decls: 21, vars: 0, consts: [[1, "footer"], [1, "container"], [1, "flex"], [1, "footer__contact", "column"], ["routerLink", ""], ["src", "assets/images/logo.png", "alt", "hostfront-logo", 1, "logo"], [1, "flex", "footer__bottom"], [1, "footer__bottom__01"], [1, "flex", "footer__social-media"]], template: function FooterComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "footer", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Contact Us");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "a", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "img", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "HostFront");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "\u00A9 2020 HostFront. all rights reserved.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "sales@hostfront.co.uk");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "hr");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "Hostfront pushes the concept of wordpress web hosting from 'one-click installs' towards a high quality, web hosting experience; that provides all our clients with a platform to launch their digital ideas.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "\u00A9 2020 HostFront. all rights reserved.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLinkWithHref"]], styles: [".footer[_ngcontent-%COMP%] {\n  background: linear-gradient(to right, #2980b9, #2c3e50);\n  padding: 1.5rem 1.5rem 2.5rem 1.5rem;\n  color: #ffffff;\n}\n.footer[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%] {\n  width: 75%;\n  margin: auto;\n}\n.footer[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   hr[_ngcontent-%COMP%] {\n  color: #ffffff;\n}\n.footer[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%] {\n  color: #ffffff;\n  flex: none;\n}\n.footer[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  text-decoration: none;\n  color: #ffffff;\n  position: relative;\n  display: block;\n}\n.footer[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   a.hover[_ngcontent-%COMP%]:before {\n  content: \"\";\n  position: absolute;\n  width: 0;\n  height: 2.5px;\n  bottom: -8px;\n  left: 0;\n  background: #fff;\n  visibility: hidden;\n  transition: all 0.3s ease-in-out 0s;\n}\n.footer[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   a.hover[_ngcontent-%COMP%]:hover:before {\n  visibility: visible;\n  width: 100%;\n}\n.footer__bottom[_ngcontent-%COMP%] {\n  padding: 2em 0;\n}\n.footer__bottom__01[_ngcontent-%COMP%] {\n  text-align: center;\n  width: 100%;\n  line-height: 2;\n}\n.footer__bottom__02[_ngcontent-%COMP%] {\n  display: flex;\n}\n.footer__bottom__02[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: center;\n  width: 100%;\n}\n.footer__bottom__02[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  width: 30%;\n}\n.footer__contact[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  color: #ffffff;\n  text-decoration: none;\n  display: inline-flex !important;\n  align-items: center;\n}\n.footer__contact[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]   .logo[_ngcontent-%COMP%] {\n  width: 75px;\n}\n.footer__contact[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  font-size: 1.5em;\n  font-weight: bold;\n  font-family: \"Open Sans\", sans-serif;\n}\n.footer__contact[_ngcontent-%COMP%]   h3[_ngcontent-%COMP%], .footer__contact[_ngcontent-%COMP%]   .copyright[_ngcontent-%COMP%] {\n  padding-left: 1em;\n}\n.footer[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%] {\n  padding: 30px 0px;\n}\n.footer[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%] {\n  display: flex;\n  flex-direction: column;\n  text-align: center;\n  justify-content: space-between;\n}\n.footer[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex.footer__social-media[_ngcontent-%COMP%] {\n  justify-content: space-between;\n}\n.footer[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex.footer__social-media[_ngcontent-%COMP%]   div[_ngcontent-%COMP%] {\n  width: 100%;\n  display: flex;\n  justify-content: space-between;\n}\n.footer[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%] {\n  padding: 0;\n}\n.footer[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   li[_ngcontent-%COMP%] {\n  list-style: none;\n}\n@media (min-width: 768px) {\n  .footer__bottom[_ngcontent-%COMP%] {\n    padding: 2em 0;\n  }\n  .footer__bottom__01[_ngcontent-%COMP%] {\n    width: 50%;\n    line-height: 2;\n    text-align: left;\n  }\n  .footer__bottom__02[_ngcontent-%COMP%] {\n    display: flex;\n  }\n  .footer__bottom__02[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%] {\n    display: flex;\n    justify-content: flex-end;\n  }\n  .footer__bottom__02[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    width: 30%;\n  }\n  .footer__contact[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n    color: #ffffff;\n    text-decoration: none;\n    display: inline-flex !important;\n    align-items: center;\n  }\n  .footer__contact[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]   .logo[_ngcontent-%COMP%] {\n    width: 75px;\n  }\n  .footer__contact[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n    font-size: 1.5em;\n    font-weight: bold;\n    font-family: \"Open Sans\", sans-serif;\n  }\n  .footer__contact[_ngcontent-%COMP%]   h3[_ngcontent-%COMP%], .footer__contact[_ngcontent-%COMP%]   .copyright[_ngcontent-%COMP%] {\n    padding-left: 1em;\n  }\n  .footer[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%] {\n    padding: 30px 0px;\n  }\n  .footer[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%] {\n    display: flex;\n    flex-direction: row;\n    text-align: left;\n    justify-content: space-between;\n  }\n  .footer[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex.footer__social-media[_ngcontent-%COMP%] {\n    justify-content: space-between;\n  }\n  .footer[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex.footer__social-media[_ngcontent-%COMP%]   div[_ngcontent-%COMP%] {\n    width: 30%;\n    display: flex;\n    justify-content: space-between;\n  }\n  .footer[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%] {\n    padding: 0;\n  }\n  .footer[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   li[_ngcontent-%COMP%] {\n    list-style: none;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2NoaWRvL3Byb2plY3RzL2hvc3Rmcm9udC1wcm9qZWN0L2hvc3Rmcm9udC1mcm9udGVuZC9zcmMvYXBwL3NoYXJlZC9mb290ZXIvZm9vdGVyLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9zaGFyZWQvZm9vdGVyL2Zvb3Rlci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHVEQUFBO0VBQ0Esb0NBQUE7RUFDQSxjQUFBO0FDQ0Y7QURBRTtFQUNFLFVBQUE7RUFDQSxZQUFBO0FDRUo7QURBSTtFQUNFLGNBQUE7QUNFTjtBRENFO0VBQ0UsY0FBQTtFQUNBLFVBQUE7QUNDSjtBRENFO0VBQ0UscUJBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0FDQ0o7QURDTTtFQUNFLFdBQUE7RUFDQSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxhQUFBO0VBQ0EsWUFBQTtFQUNBLE9BQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUNBQUE7QUNDUjtBRENNO0VBQ0UsbUJBQUE7RUFDQSxXQUFBO0FDQ1I7QURHSTtFQUNFLGNBQUE7QUNETjtBREVNO0VBQ0Usa0JBQUE7RUFDQSxXQUFBO0VBQ0EsY0FBQTtBQ0FSO0FERU07RUFDRSxhQUFBO0FDQVI7QURFUTtFQUNFLGFBQUE7RUFDQSx1QkFBQTtFQUNBLFdBQUE7QUNBVjtBRENVO0VBQ0UsVUFBQTtBQ0NaO0FES007RUFDRSxjQUFBO0VBQ0EscUJBQUE7RUFDQSwrQkFBQTtFQUNBLG1CQUFBO0FDSFI7QURJUTtFQUNFLFdBQUE7QUNGVjtBRElRO0VBQ0UsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLG9DQUFBO0FDRlY7QURLTTtFQUNFLGlCQUFBO0FDSFI7QURPSTtFQUNFLGlCQUFBO0FDTE47QURPTTtFQUNFLGFBQUE7RUFDQSxzQkFBQTtFQUNBLGtCQUFBO0VBQ0EsOEJBQUE7QUNMUjtBRE1RO0VBQ0UsOEJBQUE7QUNKVjtBREtVO0VBQ0UsV0FBQTtFQUNBLGFBQUE7RUFDQSw4QkFBQTtBQ0haO0FETVE7RUFDRSxVQUFBO0FDSlY7QURLVTtFQUNFLGdCQUFBO0FDSFo7QURhQTtFQUVJO0lBQ0UsY0FBQTtFQ1hKO0VEWUk7SUFDRSxVQUFBO0lBQ0EsY0FBQTtJQUNBLGdCQUFBO0VDVk47RURZSTtJQUNFLGFBQUE7RUNWTjtFRFlNO0lBQ0UsYUFBQTtJQUNBLHlCQUFBO0VDVlI7RURXUTtJQUNFLFVBQUE7RUNUVjtFRGVJO0lBQ0UsY0FBQTtJQUNBLHFCQUFBO0lBQ0EsK0JBQUE7SUFDQSxtQkFBQTtFQ2JOO0VEY007SUFDRSxXQUFBO0VDWlI7RURjTTtJQUNFLGdCQUFBO0lBQ0EsaUJBQUE7SUFDQSxvQ0FBQTtFQ1pSO0VEZUk7SUFDRSxpQkFBQTtFQ2JOO0VEaUJFO0lBQ0UsaUJBQUE7RUNmSjtFRGlCSTtJQUNFLGFBQUE7SUFDQSxtQkFBQTtJQUNBLGdCQUFBO0lBQ0EsOEJBQUE7RUNmTjtFRGdCTTtJQUNFLDhCQUFBO0VDZFI7RURlUTtJQUNFLFVBQUE7SUFDQSxhQUFBO0lBQ0EsOEJBQUE7RUNiVjtFRGdCTTtJQUNFLFVBQUE7RUNkUjtFRGVRO0lBQ0UsZ0JBQUE7RUNiVjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvc2hhcmVkL2Zvb3Rlci9mb290ZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZm9vdGVyIHtcbiAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjMjk4MGI5LCAjMmMzZTUwKTtcbiAgcGFkZGluZzogMS41cmVtIDEuNXJlbSAyLjVyZW0gMS41cmVtO1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgJiAuY29udGFpbmVyIHtcbiAgICB3aWR0aDogNzUlO1xuICAgIG1hcmdpbjogYXV0bztcblxuICAgICYgaHIge1xuICAgICAgY29sb3I6ICNmZmZmZmY7XG4gICAgfVxuICB9XG4gIC5jb2x1bW4ge1xuICAgIGNvbG9yOiAjZmZmZmZmO1xuICAgIGZsZXg6IG5vbmU7XG4gIH1cbiAgJiAuY29sdW1uIGEge1xuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgICBjb2xvcjogI2ZmZmZmZjtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgJi5ob3ZlciB7XG4gICAgICAmOmJlZm9yZSB7XG4gICAgICAgIGNvbnRlbnQ6IFwiXCI7XG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgd2lkdGg6IDA7XG4gICAgICAgIGhlaWdodDogMi41cHg7XG4gICAgICAgIGJvdHRvbTogLThweDtcbiAgICAgICAgbGVmdDogMDtcbiAgICAgICAgYmFja2dyb3VuZDogI2ZmZjtcbiAgICAgICAgdmlzaWJpbGl0eTogaGlkZGVuO1xuICAgICAgICB0cmFuc2l0aW9uOiBhbGwgMC4zcyBlYXNlLWluLW91dCAwcztcbiAgICAgIH1cbiAgICAgICY6aG92ZXI6YmVmb3JlIHtcbiAgICAgICAgdmlzaWJpbGl0eTogdmlzaWJsZTtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICB9XG4gICAgfVxuICB9XG4gICAgJl9fYm90dG9tIHtcbiAgICAgIHBhZGRpbmc6IDJlbSAwO1xuICAgICAgJl9fMDEge1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBsaW5lLWhlaWdodDogMjtcbiAgICAgIH1cbiAgICAgICZfXzAyIHtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgXG4gICAgICAgICYgLmNvbHVtbiB7XG4gICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgICAmIGltZyB7XG4gICAgICAgICAgICB3aWR0aDogMzAlO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgICAmX19jb250YWN0IHtcbiAgICAgICYgYSB7XG4gICAgICAgIGNvbG9yOiAjZmZmZmZmO1xuICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gICAgICAgIGRpc3BsYXk6IGlubGluZS1mbGV4ICFpbXBvcnRhbnQ7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgICYgLmxvZ28ge1xuICAgICAgICAgIHdpZHRoOiA3NXB4O1xuICAgICAgICB9XG4gICAgICAgICYgc3BhbiB7XG4gICAgICAgICAgZm9udC1zaXplOiAxLjVlbTtcbiAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgICBmb250LWZhbWlseTogXCJPcGVuIFNhbnNcIiwgc2Fucy1zZXJpZjtcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgJiBoMywgLmNvcHlyaWdodCB7XG4gICAgICAgIHBhZGRpbmctbGVmdDogMWVtO1xuICAgICAgfVxuICAgIH1cbiAgXG4gICAgJiAuY29udGFpbmVyIHtcbiAgICAgIHBhZGRpbmc6IDMwcHggMHB4O1xuICAgIFxuICAgICAgJiAuZmxleCB7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgICAgICAmLmZvb3Rlcl9fc29jaWFsLW1lZGlhIHtcbiAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgICAgICAgJiBkaXYge1xuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICAmIHVsIHtcbiAgICAgICAgICBwYWRkaW5nOiAwO1xuICAgICAgICAgICYgbGkge1xuICAgICAgICAgICAgbGlzdC1zdHlsZTogbm9uZTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cblxuXG5cblxuQG1lZGlhIChtaW4td2lkdGg6IDc2OHB4KSB7XG4gIC5mb290ZXIge1xuICAgICZfX2JvdHRvbSB7XG4gICAgICBwYWRkaW5nOiAyZW0gMDtcbiAgICAgICZfXzAxIHtcbiAgICAgICAgd2lkdGg6IDUwJTtcbiAgICAgICAgbGluZS1oZWlnaHQ6IDI7XG4gICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgICB9XG4gICAgICAmX18wMiB7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIFxuICAgICAgICAmIC5jb2x1bW4ge1xuICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbiAgICAgICAgICAmIGltZyB7XG4gICAgICAgICAgICB3aWR0aDogMzAlO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgICAmX19jb250YWN0IHtcbiAgICAgICYgYSB7XG4gICAgICAgIGNvbG9yOiAjZmZmZmZmO1xuICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gICAgICAgIGRpc3BsYXk6IGlubGluZS1mbGV4ICFpbXBvcnRhbnQ7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgICYgLmxvZ28ge1xuICAgICAgICAgIHdpZHRoOiA3NXB4O1xuICAgICAgICB9XG4gICAgICAgICYgc3BhbiB7XG4gICAgICAgICAgZm9udC1zaXplOiAxLjVlbTtcbiAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgICBmb250LWZhbWlseTogXCJPcGVuIFNhbnNcIiwgc2Fucy1zZXJpZjtcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgJiBoMywgLmNvcHlyaWdodCB7XG4gICAgICAgIHBhZGRpbmctbGVmdDogMWVtO1xuICAgICAgfVxuICAgIH1cbiAgXG4gICAgJiAuY29udGFpbmVyIHtcbiAgICAgIHBhZGRpbmc6IDMwcHggMHB4O1xuICAgIFxuICAgICAgJiAuZmxleCB7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICAgICAgJi5mb290ZXJfX3NvY2lhbC1tZWRpYSB7XG4gICAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgICAgICAgICYgZGl2IHtcbiAgICAgICAgICAgIHdpZHRoOiAzMCU7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICAmIHVsIHtcbiAgICAgICAgICBwYWRkaW5nOiAwO1xuICAgICAgICAgICYgbGkge1xuICAgICAgICAgICAgbGlzdC1zdHlsZTogbm9uZTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cbiIsIi5mb290ZXIge1xuICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICMyOTgwYjksICMyYzNlNTApO1xuICBwYWRkaW5nOiAxLjVyZW0gMS41cmVtIDIuNXJlbSAxLjVyZW07XG4gIGNvbG9yOiAjZmZmZmZmO1xufVxuLmZvb3RlciAuY29udGFpbmVyIHtcbiAgd2lkdGg6IDc1JTtcbiAgbWFyZ2luOiBhdXRvO1xufVxuLmZvb3RlciAuY29udGFpbmVyIGhyIHtcbiAgY29sb3I6ICNmZmZmZmY7XG59XG4uZm9vdGVyIC5jb2x1bW4ge1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgZmxleDogbm9uZTtcbn1cbi5mb290ZXIgLmNvbHVtbiBhIHtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBkaXNwbGF5OiBibG9jaztcbn1cbi5mb290ZXIgLmNvbHVtbiBhLmhvdmVyOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgd2lkdGg6IDA7XG4gIGhlaWdodDogMi41cHg7XG4gIGJvdHRvbTogLThweDtcbiAgbGVmdDogMDtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbiAgdmlzaWJpbGl0eTogaGlkZGVuO1xuICB0cmFuc2l0aW9uOiBhbGwgMC4zcyBlYXNlLWluLW91dCAwcztcbn1cbi5mb290ZXIgLmNvbHVtbiBhLmhvdmVyOmhvdmVyOmJlZm9yZSB7XG4gIHZpc2liaWxpdHk6IHZpc2libGU7XG4gIHdpZHRoOiAxMDAlO1xufVxuLmZvb3Rlcl9fYm90dG9tIHtcbiAgcGFkZGluZzogMmVtIDA7XG59XG4uZm9vdGVyX19ib3R0b21fXzAxIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB3aWR0aDogMTAwJTtcbiAgbGluZS1oZWlnaHQ6IDI7XG59XG4uZm9vdGVyX19ib3R0b21fXzAyIHtcbiAgZGlzcGxheTogZmxleDtcbn1cbi5mb290ZXJfX2JvdHRvbV9fMDIgLmNvbHVtbiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICB3aWR0aDogMTAwJTtcbn1cbi5mb290ZXJfX2JvdHRvbV9fMDIgLmNvbHVtbiBpbWcge1xuICB3aWR0aDogMzAlO1xufVxuLmZvb3Rlcl9fY29udGFjdCBhIHtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgZGlzcGxheTogaW5saW5lLWZsZXggIWltcG9ydGFudDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5mb290ZXJfX2NvbnRhY3QgYSAubG9nbyB7XG4gIHdpZHRoOiA3NXB4O1xufVxuLmZvb3Rlcl9fY29udGFjdCBhIHNwYW4ge1xuICBmb250LXNpemU6IDEuNWVtO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1mYW1pbHk6IFwiT3BlbiBTYW5zXCIsIHNhbnMtc2VyaWY7XG59XG4uZm9vdGVyX19jb250YWN0IGgzLCAuZm9vdGVyX19jb250YWN0IC5jb3B5cmlnaHQge1xuICBwYWRkaW5nLWxlZnQ6IDFlbTtcbn1cbi5mb290ZXIgLmNvbnRhaW5lciB7XG4gIHBhZGRpbmc6IDMwcHggMHB4O1xufVxuLmZvb3RlciAuY29udGFpbmVyIC5mbGV4IHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG4uZm9vdGVyIC5jb250YWluZXIgLmZsZXguZm9vdGVyX19zb2NpYWwtbWVkaWEge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG4uZm9vdGVyIC5jb250YWluZXIgLmZsZXguZm9vdGVyX19zb2NpYWwtbWVkaWEgZGl2IHtcbiAgd2lkdGg6IDEwMCU7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2Vlbjtcbn1cbi5mb290ZXIgLmNvbnRhaW5lciAuZmxleCB1bCB7XG4gIHBhZGRpbmc6IDA7XG59XG4uZm9vdGVyIC5jb250YWluZXIgLmZsZXggdWwgbGkge1xuICBsaXN0LXN0eWxlOiBub25lO1xufVxuXG5AbWVkaWEgKG1pbi13aWR0aDogNzY4cHgpIHtcbiAgLmZvb3Rlcl9fYm90dG9tIHtcbiAgICBwYWRkaW5nOiAyZW0gMDtcbiAgfVxuICAuZm9vdGVyX19ib3R0b21fXzAxIHtcbiAgICB3aWR0aDogNTAlO1xuICAgIGxpbmUtaGVpZ2h0OiAyO1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gIH1cbiAgLmZvb3Rlcl9fYm90dG9tX18wMiB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgfVxuICAuZm9vdGVyX19ib3R0b21fXzAyIC5jb2x1bW4ge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbiAgfVxuICAuZm9vdGVyX19ib3R0b21fXzAyIC5jb2x1bW4gaW1nIHtcbiAgICB3aWR0aDogMzAlO1xuICB9XG4gIC5mb290ZXJfX2NvbnRhY3QgYSB7XG4gICAgY29sb3I6ICNmZmZmZmY7XG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICAgIGRpc3BsYXk6IGlubGluZS1mbGV4ICFpbXBvcnRhbnQ7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgfVxuICAuZm9vdGVyX19jb250YWN0IGEgLmxvZ28ge1xuICAgIHdpZHRoOiA3NXB4O1xuICB9XG4gIC5mb290ZXJfX2NvbnRhY3QgYSBzcGFuIHtcbiAgICBmb250LXNpemU6IDEuNWVtO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIGZvbnQtZmFtaWx5OiBcIk9wZW4gU2Fuc1wiLCBzYW5zLXNlcmlmO1xuICB9XG4gIC5mb290ZXJfX2NvbnRhY3QgaDMsIC5mb290ZXJfX2NvbnRhY3QgLmNvcHlyaWdodCB7XG4gICAgcGFkZGluZy1sZWZ0OiAxZW07XG4gIH1cbiAgLmZvb3RlciAuY29udGFpbmVyIHtcbiAgICBwYWRkaW5nOiAzMHB4IDBweDtcbiAgfVxuICAuZm9vdGVyIC5jb250YWluZXIgLmZsZXgge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgfVxuICAuZm9vdGVyIC5jb250YWluZXIgLmZsZXguZm9vdGVyX19zb2NpYWwtbWVkaWEge1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgfVxuICAuZm9vdGVyIC5jb250YWluZXIgLmZsZXguZm9vdGVyX19zb2NpYWwtbWVkaWEgZGl2IHtcbiAgICB3aWR0aDogMzAlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICB9XG4gIC5mb290ZXIgLmNvbnRhaW5lciAuZmxleCB1bCB7XG4gICAgcGFkZGluZzogMDtcbiAgfVxuICAuZm9vdGVyIC5jb250YWluZXIgLmZsZXggdWwgbGkge1xuICAgIGxpc3Qtc3R5bGU6IG5vbmU7XG4gIH1cbn0iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](FooterComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-footer',
                templateUrl: './footer.component.html',
                styleUrls: ['./footer.component.scss']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/shared/form/form.component.ts":
/*!***********************************************!*\
  !*** ./src/app/shared/form/form.component.ts ***!
  \***********************************************/
/*! exports provided: FormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormComponent", function() { return FormComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


class FormComponent {
    constructor() { }
    ngOnInit() {
    }
}
FormComponent.ɵfac = function FormComponent_Factory(t) { return new (t || FormComponent)(); };
FormComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: FormComponent, selectors: [["app-form"]], decls: 2, vars: 0, template: function FormComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "form works!");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9mb3JtL2Zvcm0uY29tcG9uZW50LnNjc3MifQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](FormComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-form',
                templateUrl: './form.component.html',
                styleUrls: ['./form.component.scss']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/shared/header/header.component.ts":
/*!***************************************************!*\
  !*** ./src/app/shared/header/header.component.ts ***!
  \***************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../navbar/navbar.component */ "./src/app/shared/navbar/navbar.component.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _button_button_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../button/button.component */ "./src/app/shared/button/button.component.ts");





function HeaderComponent_div_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceSVG"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceHTML"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "app-button", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "app-button", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r38 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r38.data.api_res_hero_module.hero_header);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r38.data.api_res_hero_module.hero_excerpt);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("buttonConfig", ctx_r38.primaryBtn)("buttonText", ctx_r38.data.api_res_hero_module.hero_double_button_cta[0].hero_cta_left_side_button);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("buttonConfig", ctx_r38.secondaryBtn)("buttonText", ctx_r38.data.api_res_hero_module.hero_double_button_cta[0].hero_cta_right_side_button);
} }
class HeaderComponent {
    constructor() {
        this.primaryBtn = {
            primary: true,
        };
        this.secondaryBtn = {
            secondary: true
        };
    }
    ngOnInit() {
    }
}
HeaderComponent.ɵfac = function HeaderComponent_Factory(t) { return new (t || HeaderComponent)(); };
HeaderComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: HeaderComponent, selectors: [["app-header"]], inputs: { data: "data", headerClass: "headerClass" }, decls: 5, vars: 3, consts: [[1, "header"], ["id", "curve", "data-name", "Layer 1", "xmlns", "http://www.w3.org/2000/svg", "viewBox", "0 0 1416.99 174.01"], ["d", "M0,280.8S283.66,59,608.94,163.56s437.93,150.57,808,10.34V309.54H0V280.8Z", "transform", "translate(0 -135.53)", 1, "cls-1"], ["class", "header__container", 4, "ngIf"], [1, "header__container"], [1, "header__container__content"], [1, "btn-wrapper"], [3, "buttonConfig", "buttonText"]], template: function HeaderComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "header", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "app-navbar");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceSVG"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "svg", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "path", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, HeaderComponent_div_4_Template, 9, 6, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassMap"](ctx.headerClass);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.data);
    } }, directives: [_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_1__["NavbarComponent"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], _button_button_component__WEBPACK_IMPORTED_MODULE_3__["ButtonComponent"]], styles: [".header[_ngcontent-%COMP%] {\n  position: relative;\n  height: 750px;\n  background-attachment: fixed;\n  background-position: center top;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n.header.home[_ngcontent-%COMP%] {\n  background-image: linear-gradient(to right, #2c3e50, rgba(41, 128, 185, 0.5)), url(\"/assets/images/bg.jpg\");\n}\n.header.contact[_ngcontent-%COMP%] {\n  background-image: linear-gradient(to right, #2c3e50, rgba(41, 128, 185, 0.5)), url(\"https://images.pexels.com/photos/3183196/pexels-photo-3183196.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260\");\n  background-position: 50% 50%;\n}\n.header[_ngcontent-%COMP%]   svg[_ngcontent-%COMP%] {\n  position: absolute;\n  bottom: 0;\n  width: 100%;\n}\n.header[_ngcontent-%COMP%]   svg[_ngcontent-%COMP%]   path[_ngcontent-%COMP%] {\n  fill: #fff;\n}\n.header__container[_ngcontent-%COMP%] {\n  max-width: 85%;\n  height: 100%;\n  display: flex;\n  margin: auto;\n  color: white;\n}\n.header__container__content[_ngcontent-%COMP%] {\n  text-align: center;\n}\n@media (min-width: 768px) {\n  .header__container__content[_ngcontent-%COMP%] {\n    width: 75%;\n    text-align: left;\n  }\n  .header__container__content[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n    font-size: 3.5rem;\n  }\n  .header__container__content[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n    margin-bottom: 3em;\n  }\n  .header__container__content[_ngcontent-%COMP%]   .btn-wrapper[_ngcontent-%COMP%] {\n    display: inline;\n  }\n  .header__container__content[_ngcontent-%COMP%]   .btn-wrapper[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%] {\n    padding: 20px 30px;\n    margin-right: 20px;\n  }\n}\n@media (min-width: 1367px) {\n  .header__container__content[_ngcontent-%COMP%] {\n    width: 50%;\n  }\n}\n@media only screen and (min-width: 1680px) {\n  .header[_ngcontent-%COMP%] {\n    height: 900px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2NoaWRvL3Byb2plY3RzL2hvc3Rmcm9udC1wcm9qZWN0L2hvc3Rmcm9udC1mcm9udGVuZC9zcmMvYXBwL3NoYXJlZC9oZWFkZXIvaGVhZGVyLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9zaGFyZWQvaGVhZGVyL2hlYWRlci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLDRCQUFBO0VBQ0EsK0JBQUE7RUFDQSw0QkFBQTtFQUNBLHNCQUFBO0FDQ0Y7QURDRTtFQUNFLDJHQUFBO0FDQ0o7QURHRTtFQUNFLHFNQUFBO0VBRUYsNEJBQUE7QUNGRjtBREtFO0VBQ0Usa0JBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtBQ0hKO0FES0k7RUFDRSxVQUFBO0FDSE47QURPRTtFQUNFLGNBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0FDTEo7QURPSTtFQUNFLGtCQUFBO0FDTE47QURXQTtFQUVJO0lBQ0UsVUFBQTtJQUNBLGdCQUFBO0VDVEo7RURVSTtJQUNFLGlCQUFBO0VDUk47RURVSTtJQUNFLGtCQUFBO0VDUk47RURVSTtJQUNFLGVBQUE7RUNSTjtFRFNNO0lBQ0Usa0JBQUE7SUFDQSxrQkFBQTtFQ1BSO0FBQ0Y7QURjQTtFQUVJO0lBQ0UsVUFBQTtFQ2JKO0FBQ0Y7QURrQkE7RUFDRTtJQUNFLGFBQUE7RUNoQkY7QUFDRiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9oZWFkZXIvaGVhZGVyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmhlYWRlciB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgaGVpZ2h0OiA3NTBweDtcbiAgYmFja2dyb3VuZC1hdHRhY2htZW50OiBmaXhlZDtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyIHRvcDtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcblxuICAmLmhvbWUge1xuICAgIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgcmdiYSg0NCwgNjIsIDgwLCAxKSwgcmdiYSg0MSwgMTI4LCAxODUsIDAuNSkpLFxuICAgIHVybChcIi9hc3NldHMvaW1hZ2VzL2JnLmpwZ1wiKTtcbiAgfVxuXG4gICYuY29udGFjdCB7XG4gICAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCByZ2JhKDQ0LCA2MiwgODAsIDEpLCByZ2JhKDQxLCAxMjgsIDE4NSwgMC41KSksXG4gICAgdXJsKFwiaHR0cHM6Ly9pbWFnZXMucGV4ZWxzLmNvbS9waG90b3MvMzE4MzE5Ni9wZXhlbHMtcGhvdG8tMzE4MzE5Ni5qcGVnP2F1dG89Y29tcHJlc3MmY3M9dGlueXNyZ2ImZHByPTImaD03NTAmdz0xMjYwXCIpO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiA1MCUgNTAlO1xuICB9XG5cbiAgJiBzdmcge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBib3R0b206IDA7XG4gICAgd2lkdGg6IDEwMCU7XG5cbiAgICAmIHBhdGgge1xuICAgICAgZmlsbDogI2ZmZjtcbiAgICB9XG4gIH1cblxuICAmX19jb250YWluZXIge1xuICAgIG1heC13aWR0aDogODUlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIG1hcmdpbjogYXV0bztcbiAgICBjb2xvcjogd2hpdGU7XG5cbiAgICAmX19jb250ZW50IHtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB9XG4gIH1cbn1cblxuLy8gSGVhZGVyIENvbXBvbmVudCBUYWJsZXQgVmlld1xuQG1lZGlhIChtaW4td2lkdGg6IDc2OHB4KSB7XG4gIC5oZWFkZXJfX2NvbnRhaW5lciB7XG4gICAgJl9fY29udGVudCB7XG4gICAgICB3aWR0aDogNzUlO1xuICAgICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICAgICYgaDEge1xuICAgICAgICBmb250LXNpemU6IDMuNXJlbTtcbiAgICAgIH1cbiAgICAgICYgcCB7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDNlbTtcbiAgICAgIH1cbiAgICAgICYgLmJ0bi13cmFwcGVyIHtcbiAgICAgICAgZGlzcGxheTogaW5saW5lO1xuICAgICAgICAmIC5idG4ge1xuICAgICAgICAgIHBhZGRpbmc6IDIwcHggMzBweDtcbiAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDIwcHg7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cblxuLy8gSGVhZGVyIENvbXBvbmVudCBEZXNrdG9wIFZpZXdcbkBtZWRpYSAobWluLXdpZHRoOiAxMzY3cHgpIHtcbiAgLmhlYWRlcl9fY29udGFpbmVyIHtcbiAgICAmX19jb250ZW50IHtcbiAgICAgIHdpZHRoOiA1MCU7XG4gICAgfVxuICB9XG59XG5cbi8vIEhlYWRlciBDb21wb25lbnQgTGFyZ2UgRGVza3RvcFxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiAxNjgwcHgpIHtcbiAgLmhlYWRlciB7XG4gICAgaGVpZ2h0OiA5MDBweDtcbiAgfVxufVxuIiwiLmhlYWRlciB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgaGVpZ2h0OiA3NTBweDtcbiAgYmFja2dyb3VuZC1hdHRhY2htZW50OiBmaXhlZDtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyIHRvcDtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3Zlcjtcbn1cbi5oZWFkZXIuaG9tZSB7XG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgIzJjM2U1MCwgcmdiYSg0MSwgMTI4LCAxODUsIDAuNSkpLCB1cmwoXCIvYXNzZXRzL2ltYWdlcy9iZy5qcGdcIik7XG59XG4uaGVhZGVyLmNvbnRhY3Qge1xuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICMyYzNlNTAsIHJnYmEoNDEsIDEyOCwgMTg1LCAwLjUpKSwgdXJsKFwiaHR0cHM6Ly9pbWFnZXMucGV4ZWxzLmNvbS9waG90b3MvMzE4MzE5Ni9wZXhlbHMtcGhvdG8tMzE4MzE5Ni5qcGVnP2F1dG89Y29tcHJlc3MmY3M9dGlueXNyZ2ImZHByPTImaD03NTAmdz0xMjYwXCIpO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiA1MCUgNTAlO1xufVxuLmhlYWRlciBzdmcge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJvdHRvbTogMDtcbiAgd2lkdGg6IDEwMCU7XG59XG4uaGVhZGVyIHN2ZyBwYXRoIHtcbiAgZmlsbDogI2ZmZjtcbn1cbi5oZWFkZXJfX2NvbnRhaW5lciB7XG4gIG1heC13aWR0aDogODUlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIG1hcmdpbjogYXV0bztcbiAgY29sb3I6IHdoaXRlO1xufVxuLmhlYWRlcl9fY29udGFpbmVyX19jb250ZW50IHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG5AbWVkaWEgKG1pbi13aWR0aDogNzY4cHgpIHtcbiAgLmhlYWRlcl9fY29udGFpbmVyX19jb250ZW50IHtcbiAgICB3aWR0aDogNzUlO1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gIH1cbiAgLmhlYWRlcl9fY29udGFpbmVyX19jb250ZW50IGgxIHtcbiAgICBmb250LXNpemU6IDMuNXJlbTtcbiAgfVxuICAuaGVhZGVyX19jb250YWluZXJfX2NvbnRlbnQgcCB7XG4gICAgbWFyZ2luLWJvdHRvbTogM2VtO1xuICB9XG4gIC5oZWFkZXJfX2NvbnRhaW5lcl9fY29udGVudCAuYnRuLXdyYXBwZXIge1xuICAgIGRpc3BsYXk6IGlubGluZTtcbiAgfVxuICAuaGVhZGVyX19jb250YWluZXJfX2NvbnRlbnQgLmJ0bi13cmFwcGVyIC5idG4ge1xuICAgIHBhZGRpbmc6IDIwcHggMzBweDtcbiAgICBtYXJnaW4tcmlnaHQ6IDIwcHg7XG4gIH1cbn1cbkBtZWRpYSAobWluLXdpZHRoOiAxMzY3cHgpIHtcbiAgLmhlYWRlcl9fY29udGFpbmVyX19jb250ZW50IHtcbiAgICB3aWR0aDogNTAlO1xuICB9XG59XG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDE2ODBweCkge1xuICAuaGVhZGVyIHtcbiAgICBoZWlnaHQ6IDkwMHB4O1xuICB9XG59Il19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](HeaderComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-header',
                templateUrl: './header.component.html',
                styleUrls: ['./header.component.scss']
            }]
    }], function () { return []; }, { data: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }], headerClass: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }] }); })();


/***/ }),

/***/ "./src/app/shared/hero-text/hero-text.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/shared/hero-text/hero-text.component.ts ***!
  \*********************************************************/
/*! exports provided: HeroTextComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeroTextComponent", function() { return HeroTextComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");



function HeroTextComponent_div_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r56 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r56.data.api_res_footer_hero_module.footer_hero_text);
} }
class HeroTextComponent {
    constructor() { }
    ngOnInit() {
    }
}
HeroTextComponent.ɵfac = function HeroTextComponent_Factory(t) { return new (t || HeroTextComponent)(); };
HeroTextComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: HeroTextComponent, selectors: [["app-hero-text"]], inputs: { data: "data", heroClass: "heroClass" }, decls: 4, vars: 3, consts: [[1, "section", "color--bg"], [1, "container"], [1, "flex"], ["class", "text-container", 4, "ngIf"], [1, "text-container"]], template: function HeroTextComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "section", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, HeroTextComponent_div_3_Template, 3, 1, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassMap"](ctx.heroClass);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.data);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["NgIf"]], styles: ["section.home[_ngcontent-%COMP%] {\n  background-image: linear-gradient(to right, #2980b9, rgba(44, 62, 80, 0.5)), url(\"https://images.pexels.com/photos/1742370/pexels-photo-1742370.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260\");\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2NoaWRvL3Byb2plY3RzL2hvc3Rmcm9udC1wcm9qZWN0L2hvc3Rmcm9udC1mcm9udGVuZC9zcmMvYXBwL3NoYXJlZC9oZXJvLXRleHQvaGVyby10ZXh0LmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9zaGFyZWQvaGVyby10ZXh0L2hlcm8tdGV4dC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDRTtFQUNFLG1NQUFBO0FDQUoiLCJmaWxlIjoic3JjL2FwcC9zaGFyZWQvaGVyby10ZXh0L2hlcm8tdGV4dC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbInNlY3Rpb24ge1xuICAmLmhvbWUge1xuICAgIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgcmdiYSg0MSwgMTI4LCAxODUsIDEpLCByZ2JhKDQ0LCA2MiwgODAsIDAuNSkpLFxuICAgICAgICB1cmwoXCJodHRwczovL2ltYWdlcy5wZXhlbHMuY29tL3Bob3Rvcy8xNzQyMzcwL3BleGVscy1waG90by0xNzQyMzcwLmpwZWc/YXV0bz1jb21wcmVzcyZjcz10aW55c3JnYiZkcHI9MiZoPTc1MCZ3PTEyNjBcIik7XG4gIH1cbn1cbiIsInNlY3Rpb24uaG9tZSB7XG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgIzI5ODBiOSwgcmdiYSg0NCwgNjIsIDgwLCAwLjUpKSwgdXJsKFwiaHR0cHM6Ly9pbWFnZXMucGV4ZWxzLmNvbS9waG90b3MvMTc0MjM3MC9wZXhlbHMtcGhvdG8tMTc0MjM3MC5qcGVnP2F1dG89Y29tcHJlc3MmY3M9dGlueXNyZ2ImZHByPTImaD03NTAmdz0xMjYwXCIpO1xufSJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](HeroTextComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-hero-text',
                templateUrl: './hero-text.component.html',
                styleUrls: ['./hero-text.component.scss']
            }]
    }], function () { return []; }, { data: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }], heroClass: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }] }); })();


/***/ }),

/***/ "./src/app/shared/navbar/navbar.component.ts":
/*!***************************************************!*\
  !*** ./src/app/shared/navbar/navbar.component.ts ***!
  \***************************************************/
/*! exports provided: NavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarComponent", function() { return NavbarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");




class NavbarComponent {
    constructor() { }
    ngOnInit() {
    }
    responsiveNav() {
        var x = document.getElementById("myTopnav");
        if (x.className === "navbar") {
            x.className += " responsive";
        }
        else {
            x.className = "navbar";
        }
    }
    onWindowScroll(e) {
        let element = document.querySelector('.navbar');
        if (window.pageYOffset > 300) {
            element.classList.add('navbar--scrolled');
        }
        else {
            element.classList.remove('navbar--scrolled');
        }
    }
}
NavbarComponent.ɵfac = function NavbarComponent_Factory(t) { return new (t || NavbarComponent)(); };
NavbarComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: NavbarComponent, selectors: [["app-navbar"]], hostBindings: function NavbarComponent_HostBindings(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("scroll", function NavbarComponent_scroll_HostBindingHandler($event) { return ctx.onWindowScroll($event); }, false, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵresolveWindow"]);
    } }, decls: 21, vars: 0, consts: [[1, "topbar"], [1, "topbar__container"], [1, "topbar__wrapper"], [1, "topbar__contacts"], ["href", "tel:+7534420485", 1, "hover"], ["href", "mailto:sales@hostfront.co.uk", 1, "hover"], ["id", "myTopnav", 1, "navbar"], [1, "navbar__container"], [1, "navbar__container__brand"], ["routerLink", ""], ["src", "assets/images/logo.png", "alt", "Hosfront Logo", 1, "logo"], ["href", "javascript:void(0);", 1, "icon", 3, "click"], [1, "fa", "fa-bars"], [1, "navbar__container__main-nav"], ["routerLink", "/contact", "routerLinkActive", "active", 1, "hover"]], template: function NavbarComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "a", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, " Call: 07534420485");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "a", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "Email: sales@hostfront.co.uk");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "nav", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "a", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "img", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "HostFront");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "a", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function NavbarComponent_Template_a_click_15_listener() { return ctx.responsiveNav(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "i", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "ul", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "a", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "Contact");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLinkWithHref"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLinkActive"]], styles: [".topbar[_ngcontent-%COMP%] {\n  width: 100%;\n  padding: 0 1.5em;\n  background-color: #16324a;\n  color: #ffffff;\n}\n.topbar__container[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: center;\n}\n.topbar__container[_ngcontent-%COMP%]   div[_ngcontent-%COMP%]   div[_ngcontent-%COMP%] {\n  padding: 20px 0;\n  margin: auto;\n}\n.topbar__contacts[_ngcontent-%COMP%] {\n  display: flex;\n  align-items: center;\n}\n.topbar__contacts[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  color: #ffffff;\n  text-decoration: none;\n  margin: 0 0.5em;\n}\n.navbar[_ngcontent-%COMP%] {\n  padding: 0.5em;\n  width: 100%;\n  z-index: 2;\n}\n.navbar--scrolled[_ngcontent-%COMP%] {\n  background-color: rgba(0, 0, 0, 0.8);\n  transition: background-color 2s;\n}\n.navbar__container__brand[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: space-between;\n  align-items: center;\n  flex-direction: row;\n}\n.navbar__container__brand[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  display: flex;\n  align-items: center;\n}\n.navbar__container__brand[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]   .logo[_ngcontent-%COMP%] {\n  width: 75px;\n}\n.navbar__container__brand[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  font-size: 1.5em;\n  font-weight: bold;\n}\n.navbar__container__main-nav[_ngcontent-%COMP%] {\n  list-style-type: none;\n  margin: 0;\n  padding: 0;\n  display: none;\n}\n.navbar__container__main-nav[_ngcontent-%COMP%]   .dropdown[_ngcontent-%COMP%] {\n  overflow: hidden;\n  margin: 0 !important;\n  display: block;\n}\n.navbar__container__main-nav[_ngcontent-%COMP%]   .dropdown__content[_ngcontent-%COMP%] {\n  flex-direction: column;\n  display: none;\n  position: relative;\n}\n.navbar__container__main-nav[_ngcontent-%COMP%]   .dropdown__content[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:hover {\n  color: #16324a;\n  transition: color 1s;\n}\n.navbar__container__main-nav[_ngcontent-%COMP%]   .dropdown[_ngcontent-%COMP%]:hover   .dropdown__content[_ngcontent-%COMP%] {\n  display: flex;\n}\n.navbar__container[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  text-decoration: none;\n  margin: 10px 15px;\n  padding: 5px;\n  font-weight: bold;\n  color: #ffffff;\n  position: relative;\n  cursor: pointer;\n}\n.navbar__container[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]   .fa[_ngcontent-%COMP%] {\n  padding: 0 3px;\n}\n.hover[_ngcontent-%COMP%]:before {\n  content: \"\";\n  position: absolute;\n  width: 0;\n  height: 2.5px;\n  bottom: -2px;\n  left: 0;\n  background: #fff;\n  visibility: hidden;\n  \n  transition: all 0.3s ease-in-out 0s;\n}\n.hover[_ngcontent-%COMP%]:hover:before {\n  visibility: visible;\n  width: 100%;\n}\n@media (min-width: 768px) {\n  .navbar__container[_ngcontent-%COMP%] {\n    display: flex;\n    justify-content: space-between;\n    align-items: center;\n  }\n\n  .navbar__container__brand[_ngcontent-%COMP%], .navbar__container__main-nav[_ngcontent-%COMP%], .navbar__container__main-nav[_ngcontent-%COMP%]   div[_ngcontent-%COMP%] {\n    display: flex;\n  }\n  .navbar__container__brand[_ngcontent-%COMP%]   a[_ngcontent-%COMP%], .navbar__container__main-nav[_ngcontent-%COMP%]   a[_ngcontent-%COMP%], .navbar__container__main-nav[_ngcontent-%COMP%]   div[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n    display: flex;\n    align-items: center;\n  }\n\n  .navbar__container__brand[_ngcontent-%COMP%] {\n    flex-direction: column;\n    align-items: center;\n  }\n\n  #myTopnav[_ngcontent-%COMP%]   .icon[_ngcontent-%COMP%] {\n    display: none;\n  }\n\n  .topbar__wrapper[_ngcontent-%COMP%] {\n    width: 100%;\n    display: flex;\n    justify-content: space-between;\n  }\n}\n@media (min-width: 1025px) {\n  .navbar__container__brand[_ngcontent-%COMP%] {\n    flex-direction: row;\n    justify-content: space-between;\n  }\n}\n@media (min-width: 768px) {\n  .navbar__container__main-nav[_ngcontent-%COMP%]   .dropdown__content[_ngcontent-%COMP%] {\n    position: absolute;\n  }\n}\n@media screen and (max-width: 820px) {\n  .navbar--scrolled--responsive[_ngcontent-%COMP%] {\n    background-color: rgba(0, 0, 0, 0.5);\n    transition: background-color 1s;\n  }\n\n  .navbar.responsive[_ngcontent-%COMP%] {\n    position: relative;\n  }\n\n  .navbar.responsive[_ngcontent-%COMP%]   .navbar__container[_ngcontent-%COMP%] {\n    flex-direction: column;\n  }\n\n  .navbar.responsive[_ngcontent-%COMP%]   div[_ngcontent-%COMP%] {\n    float: none;\n    text-align: left;\n  }\n\n  .navbar.responsive[_ngcontent-%COMP%]   .navbar__container__main-nav[_ngcontent-%COMP%] {\n    display: block;\n  }\n\n  .navbar.responsive[_ngcontent-%COMP%]   li[_ngcontent-%COMP%] {\n    margin: 15px 0;\n  }\n\n  .navbar.responsive[_ngcontent-%COMP%]   .navbar__container__main-nav[_ngcontent-%COMP%] {\n    width: 100%;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2NoaWRvL3Byb2plY3RzL2hvc3Rmcm9udC1wcm9qZWN0L2hvc3Rmcm9udC1mcm9udGVuZC9zcmMvYXBwL3NoYXJlZC9uYXZiYXIvbmF2YmFyLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9zaGFyZWQvbmF2YmFyL25hdmJhci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFdBQUE7RUFDQSxnQkFBQTtFQUNBLHlCQUFBO0VBQ0EsY0FBQTtBQ0NGO0FEQ0U7RUFDRSxhQUFBO0VBQ0EsdUJBQUE7QUNDSjtBREVNO0VBQ0UsZUFBQTtFQUNBLFlBQUE7QUNBUjtBREtFO0VBQ0UsYUFBQTtFQUNBLG1CQUFBO0FDSEo7QURLSTtFQUNFLGNBQUE7RUFDQSxxQkFBQTtFQUNBLGVBQUE7QUNITjtBRFFBO0VBQ0UsY0FBQTtFQUVBLFdBQUE7RUFDQSxVQUFBO0FDTkY7QURRRTtFQUNFLG9DQUFBO0VBQ0EsK0JBQUE7QUNOSjtBRFVJO0VBQ0UsYUFBQTtFQUNBLDhCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtBQ1JOO0FEV007RUFDRSxhQUFBO0VBQ0EsbUJBQUE7QUNUUjtBRFdRO0VBQ0UsV0FBQTtBQ1RWO0FEV1E7RUFDRSxnQkFBQTtFQUNBLGlCQUFBO0FDVFY7QURjSTtFQUNFLHFCQUFBO0VBQ0EsU0FBQTtFQUNBLFVBQUE7RUFDQSxhQUFBO0FDWk47QURjTTtFQUNFLGdCQUFBO0VBQ0Esb0JBQUE7RUFDQSxjQUFBO0FDWlI7QURhUTtFQUNFLHNCQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0FDWFY7QURZVTtFQUNFLGNBQUE7RUFDQSxvQkFBQTtBQ1ZaO0FEZU07RUFDRSxhQUFBO0FDYlI7QURpQkk7RUFDRSxxQkFBQTtFQUNBLGlCQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtBQ2ZOO0FEZ0JNO0VBQ0UsY0FBQTtBQ2RSO0FEcUJFO0VBQ0UsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0VBQ0EsT0FBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSw0Q0FBQTtFQUNBLG1DQUFBO0FDbEJKO0FEb0JFO0VBQ0UsbUJBQUE7RUFDQSxXQUFBO0FDbEJKO0FEc0JBO0VBQ0U7SUFDRSxhQUFBO0lBQ0EsOEJBQUE7SUFDQSxtQkFBQTtFQ25CRjs7RURxQkE7OztJQUdFLGFBQUE7RUNsQkY7RURvQkU7OztJQUNFLGFBQUE7SUFDQSxtQkFBQTtFQ2hCSjs7RURtQkE7SUFDRSxzQkFBQTtJQUNBLG1CQUFBO0VDaEJGOztFRG1CRTtJQUNFLGFBQUE7RUNoQko7O0VEb0JBO0lBQ0UsV0FBQTtJQUNBLGFBQUE7SUFDQSw4QkFBQTtFQ2pCRjtBQUNGO0FEb0JBO0VBQ0U7SUFDRSxtQkFBQTtJQUNBLDhCQUFBO0VDbEJGO0FBQ0Y7QURxQkE7RUFDRTtJQUNFLGtCQUFBO0VDbkJGO0FBQ0Y7QURzQkE7RUFDRTtJQUNFLG9DQUFBO0lBQ0EsK0JBQUE7RUNwQkY7O0VEc0JBO0lBQ0Usa0JBQUE7RUNuQkY7O0VEcUJBO0lBQ0Usc0JBQUE7RUNsQkY7O0VEb0JBO0lBQ0UsV0FBQTtJQUNBLGdCQUFBO0VDakJGOztFRG1CQTtJQUNFLGNBQUE7RUNoQkY7O0VEa0JBO0lBQ0UsY0FBQTtFQ2ZGOztFRGlCQTtJQUNFLFdBQUE7RUNkRjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvc2hhcmVkL25hdmJhci9uYXZiYXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIudG9wYmFyIHtcbiAgd2lkdGg6IDEwMCU7XG4gIHBhZGRpbmc6IDAgMS41ZW07XG4gIGJhY2tncm91bmQtY29sb3I6ICMxNjMyNGE7XG4gIGNvbG9yOiAjZmZmZmZmO1xuXG4gICZfX2NvbnRhaW5lciB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcblxuICAgICYgZGl2IHtcbiAgICAgICYgZGl2IHtcbiAgICAgICAgcGFkZGluZzogMjBweCAwO1xuICAgICAgICBtYXJnaW46IGF1dG87XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgJl9fY29udGFjdHMge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcblxuICAgICYgYSB7XG4gICAgICBjb2xvcjogI2ZmZmZmZjtcbiAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgICAgIG1hcmdpbjogMCAwLjVlbTtcbiAgICB9XG4gIH1cbn1cblxuLm5hdmJhciB7XG4gIHBhZGRpbmc6IDAuNWVtO1xuICAvLyBwb3NpdGlvbjogZml4ZWQ7XG4gIHdpZHRoOiAxMDAlO1xuICB6LWluZGV4OiAyO1xuXG4gICYtLXNjcm9sbGVkIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuOCk7XG4gICAgdHJhbnNpdGlvbjogYmFja2dyb3VuZC1jb2xvciAycztcbiAgfVxuXG4gICZfX2NvbnRhaW5lciB7XG4gICAgJl9fYnJhbmQge1xuICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgLy8gd2lkdGg6IDEwMCU7XG5cbiAgICAgICYgYSB7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG5cbiAgICAgICAgJiAubG9nbyB7XG4gICAgICAgICAgd2lkdGg6IDc1cHg7XG4gICAgICAgIH1cbiAgICAgICAgJiBzcGFuIHtcbiAgICAgICAgICBmb250LXNpemU6IDEuNWVtO1xuICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgJl9fbWFpbi1uYXYge1xuICAgICAgbGlzdC1zdHlsZS10eXBlOiBub25lO1xuICAgICAgbWFyZ2luOiAwO1xuICAgICAgcGFkZGluZzogMDtcbiAgICAgIGRpc3BsYXk6IG5vbmU7XG5cbiAgICAgICYgLmRyb3Bkb3duIHtcbiAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICAgICAgbWFyZ2luOiAwICFpbXBvcnRhbnQ7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAmX19jb250ZW50IHtcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAgICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICAgICYgYTpob3ZlciB7XG4gICAgICAgICAgICBjb2xvcjogIzE2MzI0YTtcbiAgICAgICAgICAgIHRyYW5zaXRpb246IGNvbG9yIDFzO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICAmIC5kcm9wZG93bjpob3ZlciAuZHJvcGRvd25fX2NvbnRlbnQge1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgfVxuICAgIH1cblxuICAgICYgYSB7XG4gICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gICAgICBtYXJnaW46IDEwcHggMTVweDtcbiAgICAgIHBhZGRpbmc6IDVweDtcbiAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgY29sb3I6ICNmZmZmZmY7XG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgICAmIC5mYSB7XG4gICAgICAgIHBhZGRpbmc6IDAgM3B4O1xuICAgICAgfVxuICAgIH1cbiAgfVxufVxuXG4uaG92ZXIge1xuICAmOmJlZm9yZSB7XG4gICAgY29udGVudDogXCJcIjtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgd2lkdGg6IDA7XG4gICAgaGVpZ2h0OiAyLjVweDtcbiAgICBib3R0b206IC0ycHg7XG4gICAgbGVmdDogMDtcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xuICAgIHZpc2liaWxpdHk6IGhpZGRlbjtcbiAgICAvKiAgQW5pbWF0ZSB3aXRoIGEgZHVyYXRpb24gb2YgMC4zIHNlY29uZHMgKi9cbiAgICB0cmFuc2l0aW9uOiBhbGwgMC4zcyBlYXNlLWluLW91dCAwcztcbiAgfVxuICAmOmhvdmVyOmJlZm9yZSB7XG4gICAgdmlzaWJpbGl0eTogdmlzaWJsZTtcbiAgICB3aWR0aDogMTAwJTtcbiAgfVxufVxuXG5AbWVkaWEgKG1pbi13aWR0aDogNzY4cHgpIHtcbiAgLm5hdmJhcl9fY29udGFpbmVyIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICB9XG4gIC5uYXZiYXJfX2NvbnRhaW5lcl9fYnJhbmQsXG4gIC5uYXZiYXJfX2NvbnRhaW5lcl9fbWFpbi1uYXYsXG4gIC5uYXZiYXJfX2NvbnRhaW5lcl9fbWFpbi1uYXYgZGl2IHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuXG4gICAgJiBhIHtcbiAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIH1cbiAgfVxuICAubmF2YmFyX19jb250YWluZXJfX2JyYW5kIHtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIH1cbiAgI215VG9wbmF2IHtcbiAgICAmIC5pY29uIHtcbiAgICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgfVxuICB9XG5cbiAgLnRvcGJhcl9fd3JhcHBlciB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIH1cbn1cblxuQG1lZGlhIChtaW4td2lkdGg6IDEwMjVweCkge1xuICAubmF2YmFyX19jb250YWluZXJfX2JyYW5kIHtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgfVxufVxuXG5AbWVkaWEobWluLXdpZHRoOiA3NjhweCkge1xuICAubmF2YmFyX19jb250YWluZXJfX21haW4tbmF2IC5kcm9wZG93bl9fY29udGVudCB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICB9XG59XG5cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDgyMHB4KSB7XG4gIC5uYXZiYXItLXNjcm9sbGVkLS1yZXNwb25zaXZlIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuNSk7XG4gICAgdHJhbnNpdGlvbjogYmFja2dyb3VuZC1jb2xvciAxcztcbiAgfVxuICAubmF2YmFyLnJlc3BvbnNpdmUge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgfVxuICAubmF2YmFyLnJlc3BvbnNpdmUgLm5hdmJhcl9fY29udGFpbmVyIHtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICB9XG4gIC5uYXZiYXIucmVzcG9uc2l2ZSBkaXYge1xuICAgIGZsb2F0OiBub25lO1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gIH1cbiAgLm5hdmJhci5yZXNwb25zaXZlIC5uYXZiYXJfX2NvbnRhaW5lcl9fbWFpbi1uYXYge1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICB9XG4gIC5uYXZiYXIucmVzcG9uc2l2ZSBsaSB7XG4gICAgbWFyZ2luOiAxNXB4IDA7XG4gIH1cbiAgLm5hdmJhci5yZXNwb25zaXZlIC5uYXZiYXJfX2NvbnRhaW5lcl9fbWFpbi1uYXYge1xuICAgIHdpZHRoOiAxMDAlO1xuICB9XG59IiwiLnRvcGJhciB7XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nOiAwIDEuNWVtO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMTYzMjRhO1xuICBjb2xvcjogI2ZmZmZmZjtcbn1cbi50b3BiYXJfX2NvbnRhaW5lciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuLnRvcGJhcl9fY29udGFpbmVyIGRpdiBkaXYge1xuICBwYWRkaW5nOiAyMHB4IDA7XG4gIG1hcmdpbjogYXV0bztcbn1cbi50b3BiYXJfX2NvbnRhY3RzIHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi50b3BiYXJfX2NvbnRhY3RzIGEge1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICBtYXJnaW46IDAgMC41ZW07XG59XG5cbi5uYXZiYXIge1xuICBwYWRkaW5nOiAwLjVlbTtcbiAgd2lkdGg6IDEwMCU7XG4gIHotaW5kZXg6IDI7XG59XG4ubmF2YmFyLS1zY3JvbGxlZCB7XG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwgMCwgMCwgMC44KTtcbiAgdHJhbnNpdGlvbjogYmFja2dyb3VuZC1jb2xvciAycztcbn1cbi5uYXZiYXJfX2NvbnRhaW5lcl9fYnJhbmQge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG59XG4ubmF2YmFyX19jb250YWluZXJfX2JyYW5kIGEge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLm5hdmJhcl9fY29udGFpbmVyX19icmFuZCBhIC5sb2dvIHtcbiAgd2lkdGg6IDc1cHg7XG59XG4ubmF2YmFyX19jb250YWluZXJfX2JyYW5kIGEgc3BhbiB7XG4gIGZvbnQtc2l6ZTogMS41ZW07XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLm5hdmJhcl9fY29udGFpbmVyX19tYWluLW5hdiB7XG4gIGxpc3Qtc3R5bGUtdHlwZTogbm9uZTtcbiAgbWFyZ2luOiAwO1xuICBwYWRkaW5nOiAwO1xuICBkaXNwbGF5OiBub25lO1xufVxuLm5hdmJhcl9fY29udGFpbmVyX19tYWluLW5hdiAuZHJvcGRvd24ge1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBtYXJnaW46IDAgIWltcG9ydGFudDtcbiAgZGlzcGxheTogYmxvY2s7XG59XG4ubmF2YmFyX19jb250YWluZXJfX21haW4tbmF2IC5kcm9wZG93bl9fY29udGVudCB7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGRpc3BsYXk6IG5vbmU7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5uYXZiYXJfX2NvbnRhaW5lcl9fbWFpbi1uYXYgLmRyb3Bkb3duX19jb250ZW50IGE6aG92ZXIge1xuICBjb2xvcjogIzE2MzI0YTtcbiAgdHJhbnNpdGlvbjogY29sb3IgMXM7XG59XG4ubmF2YmFyX19jb250YWluZXJfX21haW4tbmF2IC5kcm9wZG93bjpob3ZlciAuZHJvcGRvd25fX2NvbnRlbnQge1xuICBkaXNwbGF5OiBmbGV4O1xufVxuLm5hdmJhcl9fY29udGFpbmVyIGEge1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gIG1hcmdpbjogMTBweCAxNXB4O1xuICBwYWRkaW5nOiA1cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG4ubmF2YmFyX19jb250YWluZXIgYSAuZmEge1xuICBwYWRkaW5nOiAwIDNweDtcbn1cblxuLmhvdmVyOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgd2lkdGg6IDA7XG4gIGhlaWdodDogMi41cHg7XG4gIGJvdHRvbTogLTJweDtcbiAgbGVmdDogMDtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbiAgdmlzaWJpbGl0eTogaGlkZGVuO1xuICAvKiAgQW5pbWF0ZSB3aXRoIGEgZHVyYXRpb24gb2YgMC4zIHNlY29uZHMgKi9cbiAgdHJhbnNpdGlvbjogYWxsIDAuM3MgZWFzZS1pbi1vdXQgMHM7XG59XG4uaG92ZXI6aG92ZXI6YmVmb3JlIHtcbiAgdmlzaWJpbGl0eTogdmlzaWJsZTtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbkBtZWRpYSAobWluLXdpZHRoOiA3NjhweCkge1xuICAubmF2YmFyX19jb250YWluZXIge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIH1cblxuICAubmF2YmFyX19jb250YWluZXJfX2JyYW5kLFxuLm5hdmJhcl9fY29udGFpbmVyX19tYWluLW5hdixcbi5uYXZiYXJfX2NvbnRhaW5lcl9fbWFpbi1uYXYgZGl2IHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICB9XG4gIC5uYXZiYXJfX2NvbnRhaW5lcl9fYnJhbmQgYSxcbi5uYXZiYXJfX2NvbnRhaW5lcl9fbWFpbi1uYXYgYSxcbi5uYXZiYXJfX2NvbnRhaW5lcl9fbWFpbi1uYXYgZGl2IGEge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgfVxuXG4gIC5uYXZiYXJfX2NvbnRhaW5lcl9fYnJhbmQge1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgfVxuXG4gICNteVRvcG5hdiAuaWNvbiB7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgfVxuXG4gIC50b3BiYXJfX3dyYXBwZXIge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogMTAyNXB4KSB7XG4gIC5uYXZiYXJfX2NvbnRhaW5lcl9fYnJhbmQge1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogNzY4cHgpIHtcbiAgLm5hdmJhcl9fY29udGFpbmVyX19tYWluLW5hdiAuZHJvcGRvd25fX2NvbnRlbnQge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgfVxufVxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogODIwcHgpIHtcbiAgLm5hdmJhci0tc2Nyb2xsZWQtLXJlc3BvbnNpdmUge1xuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwgMCwgMCwgMC41KTtcbiAgICB0cmFuc2l0aW9uOiBiYWNrZ3JvdW5kLWNvbG9yIDFzO1xuICB9XG5cbiAgLm5hdmJhci5yZXNwb25zaXZlIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIH1cblxuICAubmF2YmFyLnJlc3BvbnNpdmUgLm5hdmJhcl9fY29udGFpbmVyIHtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICB9XG5cbiAgLm5hdmJhci5yZXNwb25zaXZlIGRpdiB7XG4gICAgZmxvYXQ6IG5vbmU7XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgfVxuXG4gIC5uYXZiYXIucmVzcG9uc2l2ZSAubmF2YmFyX19jb250YWluZXJfX21haW4tbmF2IHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgfVxuXG4gIC5uYXZiYXIucmVzcG9uc2l2ZSBsaSB7XG4gICAgbWFyZ2luOiAxNXB4IDA7XG4gIH1cblxuICAubmF2YmFyLnJlc3BvbnNpdmUgLm5hdmJhcl9fY29udGFpbmVyX19tYWluLW5hdiB7XG4gICAgd2lkdGg6IDEwMCU7XG4gIH1cbn0iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NavbarComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-navbar',
                templateUrl: './navbar.component.html',
                styleUrls: ['./navbar.component.scss']
            }]
    }], function () { return []; }, { onWindowScroll: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
            args: ['window:scroll', ['$event']]
        }] }); })();


/***/ }),

/***/ "./src/app/shared/price-card/price-card.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/shared/price-card/price-card.component.ts ***!
  \***********************************************************/
/*! exports provided: PriceCardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PriceCardComponent", function() { return PriceCardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _button_button_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../button/button.component */ "./src/app/shared/button/button.component.ts");




function PriceCardComponent_div_1_div_1_ul_11_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ul", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "li");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "span", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "i", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "li");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "span", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "i", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "li");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "span", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "i", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "li");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "span", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "i", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const details_r44 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](details_r44.site_visits);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](details_r44.storage);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](details_r44.bandwidth);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](details_r44.sites_included);
} }
const _c0 = function (a0) { return { active: a0 }; };
function PriceCardComponent_div_1_div_1_Template(rf, ctx) { if (rf & 1) {
    const _r46 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "img", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h3", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "p", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "p", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](11, PriceCardComponent_div_1_div_1_ul_11_Template, 17, 4, "ul", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "h3", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "Included for free");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "ul");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "li");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "span", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "i", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "Automated SSL Certificates");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "li");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "span", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "i", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, "Dev/Stage/Prod Environments");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "li");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "span", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](27, "i", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, "Global CDN");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "li");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "span", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](31, "i", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32, "Malware scanning");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "li");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "span", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "i", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](36, "PHP 7.4 ready");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "li");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "span", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](39, "i", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](40, "Website Backups ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "li");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "span", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](43, "i", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](44, "Website Migration");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "li");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "span", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](47, "i", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](48, "Website Performance Check");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "li");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "span", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](51, "i", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](52, "Wordpress Plugins Starter Kit");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](53, "app-button", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "a", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PriceCardComponent_div_1_div_1_Template_a_click_54_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r46); const ctx_r45 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2); return ctx_r45.showMoreContent(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](55, "Click here to compare all features");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const price_card_r41 = ctx.$implicit;
    const odd_r42 = ctx.odd;
    const ctx_r40 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](9, _c0, odd_r42));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate2"]("src", "", ctx_r40.baseUrl, "", price_card_r41.icon.url, "", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](price_card_r41.card_header);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](price_card_r41.card_text);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", price_card_r41.price, " p/m");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", price_card_r41.package_details);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](42);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("buttonText", price_card_r41.cta);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("buttonConfig", odd_r42 ? ctx_r40.secondaryBtn : ctx_r40.primaryBtn);
} }
function PriceCardComponent_div_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, PriceCardComponent_div_1_div_1_Template, 56, 11, "div", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r39 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r39.prices.api_res_price_card);
} }
class PriceCardComponent {
    constructor() {
        this.primaryBtn = {
            primary: true,
        };
        this.secondaryBtn = {
            secondary: true
        };
    }
    showMoreContent() {
        let cardExtra = document.querySelectorAll(".card__body__extra");
        var i;
        for (i = 0; i < cardExtra.length; i++) {
            if (cardExtra[i].style.display === "none") {
                cardExtra[i].style.display = "block";
            }
            else {
                cardExtra[i].style.display = "none";
            }
        }
    }
    ngOnInit() {
    }
}
PriceCardComponent.ɵfac = function PriceCardComponent_Factory(t) { return new (t || PriceCardComponent)(); };
PriceCardComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: PriceCardComponent, selectors: [["app-price-card"]], inputs: { prices: "prices", baseUrl: "baseUrl" }, decls: 2, vars: 1, consts: [["id", "first-tab", 1, "container"], ["class", "flex", 4, "ngIf"], [1, "flex"], ["class", "column", 4, "ngFor", "ngForOf"], [1, "column"], [1, "card", 3, "ngClass"], [1, "card__icon"], [3, "src"], [1, "card__body"], [1, "card__title"], [1, "card__text"], [1, "card__price"], ["style", "margin-bottom: 18%;", 4, "ngFor", "ngForOf"], [1, "card__body__extra", 2, "display", "none"], [1, "card__body__extra__01"], [1, "fa-li"], [1, "fa", "fa-check-square"], [3, "buttonConfig", "buttonText"], [3, "click"], [2, "margin-bottom", "18%"]], template: function PriceCardComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, PriceCardComponent_div_1_Template, 2, 1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.prices);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["NgIf"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["NgForOf"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["NgClass"], _button_button_component__WEBPACK_IMPORTED_MODULE_2__["ButtonComponent"]], styles: [".card[_ngcontent-%COMP%] {\n  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);\n  border-radius: 10px;\n  padding: 16px;\n  text-align: center;\n  background-color: #ffffff;\n}\n.card[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%] {\n  list-style: none;\n}\n.card[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   li[_ngcontent-%COMP%] {\n  position: relative;\n}\n.card__icon[_ngcontent-%COMP%] {\n  position: relative;\n  margin: auto;\n}\n.card__icon[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  max-height: 30%;\n  max-width: 30%;\n}\n.card__body[_ngcontent-%COMP%] {\n  text-align: center;\n}\n.card__body[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  display: block;\n  margin-top: 2em;\n  color: #283e51;\n  text-decoration: underline;\n  cursor: pointer;\n}\n.card__body__extra[_ngcontent-%COMP%] {\n  padding: 10px;\n  margin-bottom: 5em;\n  text-align: left;\n}\n.card__body__extra[_ngcontent-%COMP%]   h3[_ngcontent-%COMP%] {\n  margin: 0;\n}\n.card__body__extra[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%] {\n  padding-left: 16px;\n}\n.card__body__extra[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   li[_ngcontent-%COMP%] {\n  margin-bottom: 5px;\n}\n.card__body__extra__02[_ngcontent-%COMP%] {\n  margin-top: 5em;\n}\n.card__title[_ngcontent-%COMP%] {\n  font-size: 1.5em;\n  color: #283e51;\n}\n.card__text[_ngcontent-%COMP%] {\n  line-height: 2;\n}\n.card__price[_ngcontent-%COMP%] {\n  font-family: \"Arial Black\";\n  font-size: 3em;\n  background: linear-gradient(#283e51, #2980b9);\n  -webkit-background-clip: text;\n  -webkit-text-fill-color: transparent;\n}\n.card.active[_ngcontent-%COMP%] {\n  background-image: linear-gradient(#283e51, #2980b9);\n  padding: 50px 16px;\n}\n.card.active[_ngcontent-%COMP%]   .card__title[_ngcontent-%COMP%], .card.active[_ngcontent-%COMP%]   .card__body[_ngcontent-%COMP%], .card.active[_ngcontent-%COMP%]   .card__price[_ngcontent-%COMP%] {\n  color: #ffffff;\n}\n.card.active[_ngcontent-%COMP%]   .card__price[_ngcontent-%COMP%] {\n  -webkit-text-fill-color: initial;\n}\n.card.active[_ngcontent-%COMP%]   .card__body[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  color: #fff;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2NoaWRvL3Byb2plY3RzL2hvc3Rmcm9udC1wcm9qZWN0L2hvc3Rmcm9udC1mcm9udGVuZC9zcmMvYXBwL3NoYXJlZC9wcmljZS1jYXJkL3ByaWNlLWNhcmQuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3NoYXJlZC9wcmljZS1jYXJkL3ByaWNlLWNhcmQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBTUE7RUFDRSwwQ0FBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0VBQ0EseUJBVk07QUNLUjtBRE9FO0VBQ0UsZ0JBQUE7QUNMSjtBRE1JO0VBQ0Usa0JBQUE7QUNKTjtBRFFFO0VBQ0Usa0JBQUE7RUFDQSxZQUFBO0FDTko7QURPSTtFQUNFLGVBQUE7RUFDQSxjQUFBO0FDTE47QURTRTtFQUNFLGtCQUFBO0FDUEo7QURRSTtFQUNFLGNBQUE7RUFDQSxlQUFBO0VBQ0EsY0E5QmdCO0VBK0JoQiwwQkFBQTtFQUNBLGVBQUE7QUNOTjtBRFFJO0VBRUUsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUNQTjtBRFFNO0VBQ0UsU0FBQTtBQ05SO0FEUU07RUFDRSxrQkFBQTtBQ05SO0FET1E7RUFDRSxrQkFBQTtBQ0xWO0FEUU07RUFDRSxlQUFBO0FDTlI7QURXRTtFQUNFLGdCQUFBO0VBQ0EsY0F4RGtCO0FDK0N0QjtBRFlFO0VBQ0UsY0FBQTtBQ1ZKO0FEYUU7RUFDRSwwQkFBQTtFQUNBLGNBQUE7RUFDQSw2Q0FBQTtFQUNBLDZCQUFBO0VBQ0Esb0NBQUE7QUNYSjtBRGNFO0VBQ0UsbURBQUE7RUFDQSxrQkFBQTtBQ1pKO0FEYUk7OztFQUdFLGNBaEZFO0FDcUVSO0FEYUk7RUFDRSxnQ0FBQTtBQ1hOO0FEY007RUFDRSxXQUFBO0FDWlIiLCJmaWxlIjoic3JjL2FwcC9zaGFyZWQvcHJpY2UtY2FyZC9wcmljZS1jYXJkLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gQ29sb3IgVmFyaWFibGVzXG4kd2hpdGU6ICNmZmZmZmY7XG4kaG9zdGZyb250LWJsdWUtbGlnaHQ6ICNmOGZhZmU7XG4kaG9zdGZyb250LWJsdWU6ICMyOTgwYjk7IFxuJGhvc3Rmcm9udC1ibHVlLWRhcms6ICMyODNlNTE7XG5cbi5jYXJkIHtcbiAgYm94LXNoYWRvdzogMCA0cHggOHB4IDAgcmdiYSgwLCAwLCAwLCAwLjIpO1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBwYWRkaW5nOiAxNnB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtY29sb3I6ICR3aGl0ZTtcblxuICAmIHVsIHtcbiAgICBsaXN0LXN0eWxlOiBub25lO1xuICAgICYgbGkge1xuICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIH1cbiAgfVxuXG4gICZfX2ljb24ge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBtYXJnaW46IGF1dG87XG4gICAgJiBpbWcge1xuICAgICAgbWF4LWhlaWdodDogMzAlO1xuICAgICAgbWF4LXdpZHRoOiAzMCU7XG4gICAgfVxuICB9XG5cbiAgJl9fYm9keSB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICYgYSB7XG4gICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgIG1hcmdpbi10b3A6IDJlbTtcbiAgICAgIGNvbG9yOiAkaG9zdGZyb250LWJsdWUtZGFyaztcbiAgICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xuICAgICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIH1cbiAgICAmX19leHRyYSB7XG4gICAgICAvLyBkaXNwbGF5OiBub25lO1xuICAgICAgcGFkZGluZzogMTBweDtcbiAgICAgIG1hcmdpbi1ib3R0b206IDVlbTtcbiAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgICAmIGgzIHtcbiAgICAgICAgbWFyZ2luOiAwO1xuICAgICAgfVxuICAgICAgJiB1bCB7XG4gICAgICAgIHBhZGRpbmctbGVmdDogMTZweDtcbiAgICAgICAgJiBsaSB7XG4gICAgICAgICAgbWFyZ2luLWJvdHRvbTogNXB4O1xuICAgICAgICB9XG4gICAgICB9XG4gICAgICAmX18wMiB7XG4gICAgICAgIG1hcmdpbi10b3A6IDVlbTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICAmX190aXRsZSB7XG4gICAgZm9udC1zaXplOiAxLjVlbTtcbiAgICBjb2xvcjogJGhvc3Rmcm9udC1ibHVlLWRhcms7XG4gIH1cblxuICAmX190ZXh0IHtcbiAgICBsaW5lLWhlaWdodDogMjtcbiAgfVxuXG4gICZfX3ByaWNlIHtcbiAgICBmb250LWZhbWlseTogXCJBcmlhbCBCbGFja1wiO1xuICAgIGZvbnQtc2l6ZTogM2VtO1xuICAgIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgkaG9zdGZyb250LWJsdWUtZGFyaywgJGhvc3Rmcm9udC1ibHVlKTtcbiAgICAtd2Via2l0LWJhY2tncm91bmQtY2xpcDogdGV4dDtcbiAgICAtd2Via2l0LXRleHQtZmlsbC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIH1cblxuICAmLmFjdGl2ZSB7XG4gICAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KCRob3N0ZnJvbnQtYmx1ZS1kYXJrLCAkaG9zdGZyb250LWJsdWUpO1xuICAgIHBhZGRpbmc6IDUwcHggMTZweDtcbiAgICAmIC5jYXJkX190aXRsZSxcbiAgICAuY2FyZF9fYm9keSxcbiAgICAuY2FyZF9fcHJpY2Uge1xuICAgICAgY29sb3I6ICR3aGl0ZTtcbiAgICB9XG4gICAgJiAuY2FyZF9fcHJpY2Uge1xuICAgICAgLXdlYmtpdC10ZXh0LWZpbGwtY29sb3I6IGluaXRpYWw7XG4gICAgfVxuICAgICYgLmNhcmRfX2JvZHkge1xuICAgICAgJiBhIHtcbiAgICAgICAgY29sb3I6ICNmZmY7XG4gICAgICB9XG4gICAgfVxuICB9XG59XG4iLCIuY2FyZCB7XG4gIGJveC1zaGFkb3c6IDAgNHB4IDhweCAwIHJnYmEoMCwgMCwgMCwgMC4yKTtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgcGFkZGluZzogMTZweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xufVxuLmNhcmQgdWwge1xuICBsaXN0LXN0eWxlOiBub25lO1xufVxuLmNhcmQgdWwgbGkge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4uY2FyZF9faWNvbiB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbWFyZ2luOiBhdXRvO1xufVxuLmNhcmRfX2ljb24gaW1nIHtcbiAgbWF4LWhlaWdodDogMzAlO1xuICBtYXgtd2lkdGg6IDMwJTtcbn1cbi5jYXJkX19ib2R5IHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLmNhcmRfX2JvZHkgYSB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBtYXJnaW4tdG9wOiAyZW07XG4gIGNvbG9yOiAjMjgzZTUxO1xuICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuLmNhcmRfX2JvZHlfX2V4dHJhIHtcbiAgcGFkZGluZzogMTBweDtcbiAgbWFyZ2luLWJvdHRvbTogNWVtO1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xufVxuLmNhcmRfX2JvZHlfX2V4dHJhIGgzIHtcbiAgbWFyZ2luOiAwO1xufVxuLmNhcmRfX2JvZHlfX2V4dHJhIHVsIHtcbiAgcGFkZGluZy1sZWZ0OiAxNnB4O1xufVxuLmNhcmRfX2JvZHlfX2V4dHJhIHVsIGxpIHtcbiAgbWFyZ2luLWJvdHRvbTogNXB4O1xufVxuLmNhcmRfX2JvZHlfX2V4dHJhX18wMiB7XG4gIG1hcmdpbi10b3A6IDVlbTtcbn1cbi5jYXJkX190aXRsZSB7XG4gIGZvbnQtc2l6ZTogMS41ZW07XG4gIGNvbG9yOiAjMjgzZTUxO1xufVxuLmNhcmRfX3RleHQge1xuICBsaW5lLWhlaWdodDogMjtcbn1cbi5jYXJkX19wcmljZSB7XG4gIGZvbnQtZmFtaWx5OiBcIkFyaWFsIEJsYWNrXCI7XG4gIGZvbnQtc2l6ZTogM2VtO1xuICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoIzI4M2U1MSwgIzI5ODBiOSk7XG4gIC13ZWJraXQtYmFja2dyb3VuZC1jbGlwOiB0ZXh0O1xuICAtd2Via2l0LXRleHQtZmlsbC1jb2xvcjogdHJhbnNwYXJlbnQ7XG59XG4uY2FyZC5hY3RpdmUge1xuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoIzI4M2U1MSwgIzI5ODBiOSk7XG4gIHBhZGRpbmc6IDUwcHggMTZweDtcbn1cbi5jYXJkLmFjdGl2ZSAuY2FyZF9fdGl0bGUsXG4uY2FyZC5hY3RpdmUgLmNhcmRfX2JvZHksXG4uY2FyZC5hY3RpdmUgLmNhcmRfX3ByaWNlIHtcbiAgY29sb3I6ICNmZmZmZmY7XG59XG4uY2FyZC5hY3RpdmUgLmNhcmRfX3ByaWNlIHtcbiAgLXdlYmtpdC10ZXh0LWZpbGwtY29sb3I6IGluaXRpYWw7XG59XG4uY2FyZC5hY3RpdmUgLmNhcmRfX2JvZHkgYSB7XG4gIGNvbG9yOiAjZmZmO1xufSJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](PriceCardComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-price-card',
                templateUrl: './price-card.component.html',
                styleUrls: ['./price-card.component.scss']
            }]
    }], function () { return []; }, { prices: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }], baseUrl: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }] }); })();


/***/ }),

/***/ "./src/app/shared/scroll/scroll.component.ts":
/*!***************************************************!*\
  !*** ./src/app/shared/scroll/scroll.component.ts ***!
  \***************************************************/
/*! exports provided: ScrollComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ScrollComponent", function() { return ScrollComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");




const _c0 = function (a0) { return { "show-scrollTop": a0 }; };
class ScrollComponent {
    constructor(document) {
        this.document = document;
    }
    onWindowScroll() {
        if (window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop > 100) {
            this.windowScrolled = true;
        }
        else if (this.windowScrolled && window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop < 10) {
            this.windowScrolled = false;
        }
    }
    scrollToTop() {
        (function smoothscroll() {
            var currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
            if (currentScroll > 0) {
                window.requestAnimationFrame(smoothscroll);
                window.scrollTo(0, currentScroll - (currentScroll / 8));
            }
        })();
    }
    ngOnInit() { }
}
ScrollComponent.ɵfac = function ScrollComponent_Factory(t) { return new (t || ScrollComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_common__WEBPACK_IMPORTED_MODULE_1__["DOCUMENT"])); };
ScrollComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ScrollComponent, selectors: [["app-scroll"]], hostBindings: function ScrollComponent_HostBindings(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("scroll", function ScrollComponent_scroll_HostBindingHandler() { return ctx.onWindowScroll(); }, false, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵresolveWindow"]);
    } }, decls: 3, vars: 3, consts: [[1, "scroll-to-top", 3, "ngClass"], ["id", "myBtn", "title", "Go to top", 3, "click"], [1, "fa", "fa-chevron-up"]], template: function ScrollComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "button", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ScrollComponent_Template_button_click_1_listener() { return ctx.scrollToTop(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "i", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](1, _c0, ctx.windowScrolled));
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["NgClass"]], styles: ["#myBtn[_ngcontent-%COMP%] {\n  position: fixed;\n  \n  bottom: 20px;\n  \n  right: 30px;\n  \n  z-index: 99;\n  \n  border: none;\n  \n  outline: none;\n  \n  background-color: #4b79a1;\n  \n  color: white;\n  \n  cursor: pointer;\n  \n  padding: 15px;\n  \n  border-radius: 10px;\n  \n  font-size: 18px;\n  \n}\n\n#myBtn[_ngcontent-%COMP%]:hover {\n  background-color: #283e51;\n  \n  transition: all 1s ease-in-out;\n}\n\n.scroll-to-top[_ngcontent-%COMP%] {\n  opacity: 0;\n  transition: all 0.2s ease-in-out;\n}\n\n.show-scrollTop[_ngcontent-%COMP%] {\n  opacity: 1;\n  transition: all 0.2s ease-in-out;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2NoaWRvL3Byb2plY3RzL2hvc3Rmcm9udC1wcm9qZWN0L2hvc3Rmcm9udC1mcm9udGVuZC9zcmMvYXBwL3NoYXJlZC9zY3JvbGwvc2Nyb2xsLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9zaGFyZWQvc2Nyb2xsL3Njcm9sbC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGVBQUE7RUFBaUIsMEJBQUE7RUFDakIsWUFBQTtFQUFjLCtDQUFBO0VBQ2QsV0FBQTtFQUFhLHlDQUFBO0VBQ2IsV0FBQTtFQUFhLGtDQUFBO0VBQ2IsWUFBQTtFQUFjLG1CQUFBO0VBQ2QsYUFBQTtFQUFlLG1CQUFBO0VBQ2YseUJBQUE7RUFBMkIsMkJBQUE7RUFDM0IsWUFBQTtFQUFjLGVBQUE7RUFDZCxlQUFBO0VBQWlCLGlDQUFBO0VBQ2pCLGFBQUE7RUFBZSxpQkFBQTtFQUNmLG1CQUFBO0VBQXFCLG9CQUFBO0VBQ3JCLGVBQUE7RUFBaUIsdUJBQUE7QUNhbkI7O0FEVkE7RUFDRSx5QkFBQTtFQUEyQix3Q0FBQTtFQUMzQiw4QkFBQTtBQ2NGOztBRFhBO0VBQ0UsVUFBQTtFQUNBLGdDQUFBO0FDY0Y7O0FEWkM7RUFDQyxVQUFBO0VBQ0EsZ0NBQUE7QUNlRiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9zY3JvbGwvc2Nyb2xsLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI215QnRuIHtcbiAgcG9zaXRpb246IGZpeGVkOyAvKiBGaXhlZC9zdGlja3kgcG9zaXRpb24gKi9cbiAgYm90dG9tOiAyMHB4OyAvKiBQbGFjZSB0aGUgYnV0dG9uIGF0IHRoZSBib3R0b20gb2YgdGhlIHBhZ2UgKi9cbiAgcmlnaHQ6IDMwcHg7IC8qIFBsYWNlIHRoZSBidXR0b24gMzBweCBmcm9tIHRoZSByaWdodCAqL1xuICB6LWluZGV4OiA5OTsgLyogTWFrZSBzdXJlIGl0IGRvZXMgbm90IG92ZXJsYXAgKi9cbiAgYm9yZGVyOiBub25lOyAvKiBSZW1vdmUgYm9yZGVycyAqL1xuICBvdXRsaW5lOiBub25lOyAvKiBSZW1vdmUgb3V0bGluZSAqL1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNGI3OWExOyAvKiBTZXQgYSBiYWNrZ3JvdW5kIGNvbG9yICovXG4gIGNvbG9yOiB3aGl0ZTsgLyogVGV4dCBjb2xvciAqL1xuICBjdXJzb3I6IHBvaW50ZXI7IC8qIEFkZCBhIG1vdXNlIHBvaW50ZXIgb24gaG92ZXIgKi9cbiAgcGFkZGluZzogMTVweDsgLyogU29tZSBwYWRkaW5nICovXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7IC8qIFJvdW5kZWQgY29ybmVycyAqL1xuICBmb250LXNpemU6IDE4cHg7IC8qIEluY3JlYXNlIGZvbnQgc2l6ZSAqL1xufVxuXG4jbXlCdG46aG92ZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjgzZTUxOyAvKiBBZGQgYSBkYXJrLWdyZXkgYmFja2dyb3VuZCBvbiBob3ZlciAqL1xuICB0cmFuc2l0aW9uOiBhbGwgMXMgZWFzZS1pbi1vdXQ7XG59XG5cbi5zY3JvbGwtdG8tdG9wIHtcbiAgb3BhY2l0eTogMDtcbiAgdHJhbnNpdGlvbjogYWxsIC4ycyBlYXNlLWluLW91dDtcbiB9XG4gLnNob3ctc2Nyb2xsVG9wIHtcbiAgb3BhY2l0eTogMTtcbiAgdHJhbnNpdGlvbjogYWxsIC4ycyBlYXNlLWluLW91dDtcbiB9IiwiI215QnRuIHtcbiAgcG9zaXRpb246IGZpeGVkO1xuICAvKiBGaXhlZC9zdGlja3kgcG9zaXRpb24gKi9cbiAgYm90dG9tOiAyMHB4O1xuICAvKiBQbGFjZSB0aGUgYnV0dG9uIGF0IHRoZSBib3R0b20gb2YgdGhlIHBhZ2UgKi9cbiAgcmlnaHQ6IDMwcHg7XG4gIC8qIFBsYWNlIHRoZSBidXR0b24gMzBweCBmcm9tIHRoZSByaWdodCAqL1xuICB6LWluZGV4OiA5OTtcbiAgLyogTWFrZSBzdXJlIGl0IGRvZXMgbm90IG92ZXJsYXAgKi9cbiAgYm9yZGVyOiBub25lO1xuICAvKiBSZW1vdmUgYm9yZGVycyAqL1xuICBvdXRsaW5lOiBub25lO1xuICAvKiBSZW1vdmUgb3V0bGluZSAqL1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNGI3OWExO1xuICAvKiBTZXQgYSBiYWNrZ3JvdW5kIGNvbG9yICovXG4gIGNvbG9yOiB3aGl0ZTtcbiAgLyogVGV4dCBjb2xvciAqL1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIC8qIEFkZCBhIG1vdXNlIHBvaW50ZXIgb24gaG92ZXIgKi9cbiAgcGFkZGluZzogMTVweDtcbiAgLyogU29tZSBwYWRkaW5nICovXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIC8qIFJvdW5kZWQgY29ybmVycyAqL1xuICBmb250LXNpemU6IDE4cHg7XG4gIC8qIEluY3JlYXNlIGZvbnQgc2l6ZSAqL1xufVxuXG4jbXlCdG46aG92ZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjgzZTUxO1xuICAvKiBBZGQgYSBkYXJrLWdyZXkgYmFja2dyb3VuZCBvbiBob3ZlciAqL1xuICB0cmFuc2l0aW9uOiBhbGwgMXMgZWFzZS1pbi1vdXQ7XG59XG5cbi5zY3JvbGwtdG8tdG9wIHtcbiAgb3BhY2l0eTogMDtcbiAgdHJhbnNpdGlvbjogYWxsIDAuMnMgZWFzZS1pbi1vdXQ7XG59XG5cbi5zaG93LXNjcm9sbFRvcCB7XG4gIG9wYWNpdHk6IDE7XG4gIHRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xufSJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ScrollComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-scroll',
                templateUrl: './scroll.component.html',
                styleUrls: ['./scroll.component.scss']
            }]
    }], function () { return [{ type: Document, decorators: [{
                type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
                args: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["DOCUMENT"]]
            }] }]; }, { onWindowScroll: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
            args: ["window:scroll", []]
        }] }); })();


/***/ }),

/***/ "./src/app/wordpress-maintenance/wordpress-maintenance.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/wordpress-maintenance/wordpress-maintenance.component.ts ***!
  \**************************************************************************/
/*! exports provided: WordpressMaintenanceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WordpressMaintenanceComponent", function() { return WordpressMaintenanceComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _pricing_tab_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../pricing-tab.service */ "./src/app/pricing-tab.service.ts");
/* harmony import */ var _shared_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../shared/navbar/navbar.component */ "./src/app/shared/navbar/navbar.component.ts");
/* harmony import */ var _shared_scroll_scroll_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../shared/scroll/scroll.component */ "./src/app/shared/scroll/scroll.component.ts");
/* harmony import */ var _shared_footer_footer_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../shared/footer/footer.component */ "./src/app/shared/footer/footer.component.ts");






class WordpressMaintenanceComponent {
    constructor(_pricingTabService) {
        this._pricingTabService = _pricingTabService;
    }
    yearlyTab() {
        this._pricingTabService.yearlyClick();
    }
    monthlyTab() {
        this._pricingTabService.monthlyClick();
    }
    ngOnInit() {
    }
}
WordpressMaintenanceComponent.ɵfac = function WordpressMaintenanceComponent_Factory(t) { return new (t || WordpressMaintenanceComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_pricing_tab_service__WEBPACK_IMPORTED_MODULE_1__["PricingTabService"])); };
WordpressMaintenanceComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: WordpressMaintenanceComponent, selectors: [["app-wordpress-maintenance"]], decls: 135, vars: 0, consts: [[1, "header"], ["id", "curve", "data-name", "Layer 1", "xmlns", "http://www.w3.org/2000/svg", "viewBox", "0 0 1416.99 174.01"], ["d", "M0,280.8S283.66,59,608.94,163.56s437.93,150.57,808,10.34V309.54H0V280.8Z", "transform", "translate(0 -135.53)", 1, "cls-1"], [1, "header__container"], [1, "header__container__content"], ["id", "wordpress-hosting", 1, "section"], [1, "container"], [1, "flex", "heading"], ["data-aos", "fade-up", 1, "flex"], ["id", "wplogo", "src", "assets/images/wplogo.png"], [1, "section", "color--dark"], [1, "flex"], ["id", "wordpress-maintenance", 1, "section"], ["data-aos", "fade-up", 1, "column"], [1, "column__icon"], ["src", "assets/images/cloud-computing.png"], [1, "column__body"], [1, "section", "color"], ["id", "first-tab", 1, "container"], [1, "column"], [1, "card"], [1, "card__icon"], ["src", "assets/images/cloud-computing.svg"], [1, "card__body"], [1, "card__title"], [1, "card__text"], [1, "card__price"], [1, "btn", "btn--std", "btn--std01"], [1, "card", "active"], [1, "btn", "btn--std", "btn--std02"], [1, "section", "color--bg"], [1, "text-container"]], template: function WordpressMaintenanceComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "header", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "app-navbar");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceSVG"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "svg", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "path", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceHTML"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "A WORDPRESS MAINTENANCE SERVICE IS CRITICAL FOR YOUR ONLINE SUCCESS");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "section", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "WHAT IS WORDPRESS WEBSITE MAINTENANCE?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "img", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu..");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "section", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu..");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "section", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, "Services");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](32, "img", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35, "Free Website SSL");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](40, "img", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43, "Free Website SSL");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](48, "img", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](51, "Free Website SSL");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](53, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](56, "img", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](59, "Free Website SSL");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](61, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](64, "img", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](67, "Free Website SSL");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](69, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](70, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](71, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](72, "img", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](75, "Free Website SSL");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](76, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](77, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "section", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](79, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](80, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](82, "Our Plans");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](83, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](84, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](87, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](88, "img", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](89, "div", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](90, "h3", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](91, "Cloud Computing");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](92, "p", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](93, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](94, "p", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](95, "\u00A31.99");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](96, "span", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](97, "Click Here");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](98, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](99, "div", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](100, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](101, "img", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](102, "div", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](103, "h3", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](104, "Cloud Computing");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](105, "p", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](106, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](107, "p", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](108, "\u00A359.99");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](109, "span", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](110, "Click Here");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](111, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](112, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](113, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](114, "img", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](115, "div", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](116, "h3", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](117, "Cloud Computing");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](118, "p", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](119, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](120, "p", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](121, "\u00A32.99");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](122, "span", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](123, "Click Here");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](124, "section", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](125, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](126, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](127, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](128, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](129, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](130, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](131, "span", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](132, "Click Here");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](133, "app-scroll");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](134, "app-footer");
    } }, directives: [_shared_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_2__["NavbarComponent"], _shared_scroll_scroll_component__WEBPACK_IMPORTED_MODULE_3__["ScrollComponent"], _shared_footer_footer_component__WEBPACK_IMPORTED_MODULE_4__["FooterComponent"]], styles: [".header[_ngcontent-%COMP%] {\n  background-image: linear-gradient(to right, #2c3e50, rgba(41, 128, 185, 0.5)), url(\"/assets/images/services-bg.jpg\");\n}\n\n.section.color--bg[_ngcontent-%COMP%] {\n  background-image: linear-gradient(to right, #2980b9, rgba(44, 62, 80, 0.5)), url(\"/assets/images/bg_02.jpg\");\n}\n\n.section.wordpress-hosting[_ngcontent-%COMP%] {\n  line-height: 2;\n  text-align: center;\n}\n\n.section.wordpress-hosting[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%] {\n  align-items: center;\n}\n\n.section[_ngcontent-%COMP%]   #wplogo[_ngcontent-%COMP%] {\n  max-width: 250px;\n  margin: 3em;\n}\n\n@media (min-width: 768px) {\n  .header__container__content[_ngcontent-%COMP%] {\n    text-align: left;\n    width: 100%;\n    margin-top: 50px;\n  }\n\n  .section#wordpress-maintenance[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%] {\n    flex-wrap: wrap;\n    align-items: center;\n  }\n  .section#wordpress-maintenance[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%] {\n    flex-basis: 50%;\n    display: flex;\n  }\n  .section#wordpress-maintenance[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%]   .column__icon[_ngcontent-%COMP%] {\n    margin: 0;\n  }\n  .section#wordpress-maintenance[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    padding: 20px;\n  }\n  .section#wordpress-hosting[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%] {\n    align-items: center;\n  }\n  .section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%] {\n    flex-direction: row;\n    justify-content: space-around;\n  }\n  .section[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .column__body[_ngcontent-%COMP%] {\n    text-align: left;\n  }\n  .section--color--dark[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%] {\n    text-align: center;\n  }\n}\n\n@media (min-width: 1300px) {\n  .header__container__content[_ngcontent-%COMP%] {\n    text-align: left;\n    width: 75%;\n    margin-top: 50px;\n  }\n\n  .section#wordpress-maintenance[_ngcontent-%COMP%]   .flex[_ngcontent-%COMP%]   .column[_ngcontent-%COMP%] {\n    flex-basis: 20%;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2NoaWRvL3Byb2plY3RzL2hvc3Rmcm9udC1wcm9qZWN0L2hvc3Rmcm9udC1mcm9udGVuZC9zcmMvYXBwL3dvcmRwcmVzcy1tYWludGVuYW5jZS93b3JkcHJlc3MtbWFpbnRlbmFuY2UuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3dvcmRwcmVzcy1tYWludGVuYW5jZS93b3JkcHJlc3MtbWFpbnRlbmFuY2UuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxvSEFBQTtBQ0NGOztBREtJO0VBQ0UsNEdBQUE7QUNGTjs7QURPRTtFQUNFLGNBQUE7RUFDQSxrQkFBQTtBQ0xKOztBRE1JO0VBQ0UsbUJBQUE7QUNKTjs7QURRRTtFQUNFLGdCQUFBO0VBQ0EsV0FBQTtBQ05KOztBRFdBO0VBRUk7SUFDRSxnQkFBQTtJQUNBLFdBQUE7SUFDQSxnQkFBQTtFQ1RKOztFRGNJO0lBQ0UsZUFBQTtJQUNBLG1CQUFBO0VDWE47RURZTTtJQUNFLGVBQUE7SUFDQSxhQUFBO0VDVlI7RURXUTtJQUNFLFNBQUE7RUNUVjtFRFdRO0lBQ0UsYUFBQTtFQ1RWO0VEZUk7SUFDRSxtQkFBQTtFQ2JOO0VEaUJJO0lBQ0UsbUJBQUE7SUFDQSw2QkFBQTtFQ2ZOO0VEa0JNO0lBQ0UsZ0JBQUE7RUNoQlI7RURzQkk7SUFDRSxrQkFBQTtFQ3BCTjtBQUNGOztBRDBCQTtFQUVJO0lBQ0UsZ0JBQUE7SUFDQSxVQUFBO0lBQ0EsZ0JBQUE7RUN6Qko7O0VEK0JNO0lBQ0UsZUFBQTtFQzVCUjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvd29yZHByZXNzLW1haW50ZW5hbmNlL3dvcmRwcmVzcy1tYWludGVuYW5jZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5oZWFkZXIge1xuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsIHJnYmEoNDQsIDYyLCA4MCwgMSksIHJnYmEoNDEsIDEyOCwgMTg1LCAwLjUpKSxcbiAgICB1cmwoXCIvYXNzZXRzL2ltYWdlcy9zZXJ2aWNlcy1iZy5qcGdcIik7XG59XG5cbi5zZWN0aW9uIHtcbiAgJi5jb2xvciB7XG4gICAgJi0tYmcge1xuICAgICAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCByZ2JhKDQxLCAxMjgsIDE4NSwgMSksIHJnYmEoNDQsIDYyLCA4MCwgMC41KSksXG4gICAgICAgIHVybChcIi9hc3NldHMvaW1hZ2VzL2JnXzAyLmpwZ1wiKTtcbiAgICB9XG4gIH1cblxuICAmLndvcmRwcmVzcy1ob3N0aW5nIHtcbiAgICBsaW5lLWhlaWdodDogMjtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgJiAuY29udGFpbmVyIC5mbGV4IHtcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgfVxuICB9XG4gIFxuICAmICN3cGxvZ28ge1xuICAgIG1heC13aWR0aDogMjUwcHg7XG4gICAgbWFyZ2luOiAzZW07XG4gIH1cbn1cblxuLy8gVGFibGV0IGFuZCBkZXNrdG9wIHZpZXdcbkBtZWRpYSAobWluLXdpZHRoOiA3NjhweCkge1xuICAuaGVhZGVyX19jb250YWluZXIge1xuICAgICZfX2NvbnRlbnQge1xuICAgICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgbWFyZ2luLXRvcDogNTBweDtcbiAgICB9XG4gIH1cbiAgLnNlY3Rpb24ge1xuICAgICYjd29yZHByZXNzLW1haW50ZW5hbmNlIHtcbiAgICAgICYgLmZsZXgge1xuICAgICAgICBmbGV4LXdyYXA6IHdyYXA7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgICYgLmNvbHVtbiB7XG4gICAgICAgICAgZmxleC1iYXNpczogNTAlO1xuICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgJl9faWNvbiB7XG4gICAgICAgICAgICBtYXJnaW46IDA7XG4gICAgICAgICAgfVxuICAgICAgICAgICYgaW1nIHtcbiAgICAgICAgICAgIHBhZGRpbmc6IDIwcHg7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICAgICYjd29yZHByZXNzLWhvc3Rpbmcge1xuICAgICAgJiAuZmxleCB7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICB9XG4gICAgfVxuICAgICYgLmNvbnRhaW5lciB7XG4gICAgICAmIC5mbGV4IHtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4gICAgICB9XG4gICAgICAmIC5jb2x1bW4ge1xuICAgICAgICAmX19ib2R5IHtcbiAgICAgICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgJi0tY29sb3ItLWRhcmsge1xuICAgICAgJiAuY29udGFpbmVyIC5mbGV4IHtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgfVxuICAgIH1cbiAgfVxufVxuXG4vLyBEZXNrdG9wIHZpZXcgXG5AbWVkaWEgKG1pbi13aWR0aDogMTMwMHB4KSB7XG4gIC5oZWFkZXJfX2NvbnRhaW5lciB7XG4gICAgJl9fY29udGVudCB7XG4gICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgICAgd2lkdGg6IDc1JTtcbiAgICAgIG1hcmdpbi10b3A6IDUwcHg7XG4gICAgfVxuICB9XG4gIC5zZWN0aW9uIHtcbiAgICAmI3dvcmRwcmVzcy1tYWludGVuYW5jZSB7XG4gICAgICAmIC5mbGV4IHtcbiAgICAgICAgJiAuY29sdW1uIHtcbiAgICAgICAgICBmbGV4LWJhc2lzOiAyMCU7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cbiIsIi5oZWFkZXIge1xuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICMyYzNlNTAsIHJnYmEoNDEsIDEyOCwgMTg1LCAwLjUpKSwgdXJsKFwiL2Fzc2V0cy9pbWFnZXMvc2VydmljZXMtYmcuanBnXCIpO1xufVxuXG4uc2VjdGlvbi5jb2xvci0tYmcge1xuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICMyOTgwYjksIHJnYmEoNDQsIDYyLCA4MCwgMC41KSksIHVybChcIi9hc3NldHMvaW1hZ2VzL2JnXzAyLmpwZ1wiKTtcbn1cbi5zZWN0aW9uLndvcmRwcmVzcy1ob3N0aW5nIHtcbiAgbGluZS1oZWlnaHQ6IDI7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5zZWN0aW9uLndvcmRwcmVzcy1ob3N0aW5nIC5jb250YWluZXIgLmZsZXgge1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLnNlY3Rpb24gI3dwbG9nbyB7XG4gIG1heC13aWR0aDogMjUwcHg7XG4gIG1hcmdpbjogM2VtO1xufVxuXG5AbWVkaWEgKG1pbi13aWR0aDogNzY4cHgpIHtcbiAgLmhlYWRlcl9fY29udGFpbmVyX19jb250ZW50IHtcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIG1hcmdpbi10b3A6IDUwcHg7XG4gIH1cblxuICAuc2VjdGlvbiN3b3JkcHJlc3MtbWFpbnRlbmFuY2UgLmZsZXgge1xuICAgIGZsZXgtd3JhcDogd3JhcDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICB9XG4gIC5zZWN0aW9uI3dvcmRwcmVzcy1tYWludGVuYW5jZSAuZmxleCAuY29sdW1uIHtcbiAgICBmbGV4LWJhc2lzOiA1MCU7XG4gICAgZGlzcGxheTogZmxleDtcbiAgfVxuICAuc2VjdGlvbiN3b3JkcHJlc3MtbWFpbnRlbmFuY2UgLmZsZXggLmNvbHVtbl9faWNvbiB7XG4gICAgbWFyZ2luOiAwO1xuICB9XG4gIC5zZWN0aW9uI3dvcmRwcmVzcy1tYWludGVuYW5jZSAuZmxleCAuY29sdW1uIGltZyB7XG4gICAgcGFkZGluZzogMjBweDtcbiAgfVxuICAuc2VjdGlvbiN3b3JkcHJlc3MtaG9zdGluZyAuZmxleCB7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgfVxuICAuc2VjdGlvbiAuY29udGFpbmVyIC5mbGV4IHtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuICB9XG4gIC5zZWN0aW9uIC5jb250YWluZXIgLmNvbHVtbl9fYm9keSB7XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgfVxuICAuc2VjdGlvbi0tY29sb3ItLWRhcmsgLmNvbnRhaW5lciAuZmxleCB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogMTMwMHB4KSB7XG4gIC5oZWFkZXJfX2NvbnRhaW5lcl9fY29udGVudCB7XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICB3aWR0aDogNzUlO1xuICAgIG1hcmdpbi10b3A6IDUwcHg7XG4gIH1cblxuICAuc2VjdGlvbiN3b3JkcHJlc3MtbWFpbnRlbmFuY2UgLmZsZXggLmNvbHVtbiB7XG4gICAgZmxleC1iYXNpczogMjAlO1xuICB9XG59Il19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](WordpressMaintenanceComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-wordpress-maintenance',
                templateUrl: './wordpress-maintenance.component.html',
                styleUrls: ['./wordpress-maintenance.component.scss']
            }]
    }], function () { return [{ type: _pricing_tab_service__WEBPACK_IMPORTED_MODULE_1__["PricingTabService"] }]; }, null); })();


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false,
    baseurl: 'https://staging-api.hostfront.co.uk',
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["platformBrowser"]().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/chido/projects/hostfront-project/hostfront-frontend/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map