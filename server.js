
//////  Main Libraries //////

//  Install Express server
const express = require('express');
//  Grab Form Input Fields & Parse
const bodyParser = require('body-parser');
//  Cross Site Script
const cors = require('cors');
//  Enforce HTTPS on Heroku 
const enforce = require('express-sslify');

//////  Custom Functions //////

// Console Log Functions
const log = console.log;

//  Initialise Express App Server 
const app = express();

//  Node Server Local Port No#
const PORT = 8080;

//  Enforce HTTPS 
app.use(enforce.HTTPS({ trustProtoHeader: true }));

// Serve only the static files form the dist directory
app.use(express.static(__dirname + '/dist/project'));

//  Parse JSON
app.use(express.urlencoded({ extended: false }))
app.use(express.json())
app.use(bodyParser.json())
//  Cors
app.use(cors());

//  Load Router 
const router = require('./nodejsapp/router')
app.use('/', router)

// Start the app by listening on the default Heroku port
app.listen(process.env.PORT || PORT, () => {
    log(`Server running on localhost ${PORT}`);
});